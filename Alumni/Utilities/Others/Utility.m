//
//  Utility.m
//  Alumni
//
//  Created by Rahul Chandera on 26/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "Utility.h"
#import <CoreLocation/CoreLocation.h>

@implementation Utility

//Get diffrence between dates
+ (NSString*)daysDiffrentFromDateString:(NSString*)dateFrom
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateFormat = @"dd-MM-yyyy HH:mm:ss";
    NSDate *toDate = [dateFormatter dateFromString:dateFrom];
    NSTimeInterval secondsBetween = [[NSDate date] timeIntervalSinceDate:toDate];
    
    
    int diffYears   = (int)secondsBetween / (12*30*24 * 60 * 60) %12;
    if((diffYears) >0)
        return [NSString stringWithFormat:@"%dY AGO",diffYears];
    
    int diffMonths  = (int)secondsBetween / (30*24 * 60 * 60) %12;
    if((diffMonths) >0)
        return [NSString stringWithFormat:@"%dM AGO",diffMonths];
    
    int diffDays    = (int)secondsBetween / (24 * 60 * 60) % 30;
    if((diffDays) >0)
        return [NSString stringWithFormat:@"%dD AGO",diffDays];
    
    int diffHours   = (int)secondsBetween / (60 * 60) % 24;
    if (diffHours > 0)
        return [NSString stringWithFormat:@"%dH AGO",diffHours];
    
    int diffMinutes = (int)secondsBetween / 60;
    if((diffMinutes)>0)
        return [NSString stringWithFormat:@"%dM AGO",diffMinutes];
    
    int diffSeconds = (int)secondsBetween % 60;
    if((diffSeconds)>0)
        return [NSString stringWithFormat:@"%dS AGO",diffSeconds];
    
    return @"JUST NOW";
}

+ (NSString*)daysDiffrentFromDate:(NSDate*)dateFrom
{
    NSTimeInterval secondsBetween = [[NSDate date] timeIntervalSinceDate:dateFrom];
    
    int diffYears   = (int)secondsBetween / (12*30*24 * 60 * 60) %12;
    if((diffYears) >0)
        return [NSString stringWithFormat:@"%dY AGO",diffYears];
    
    int diffMonths  = (int)secondsBetween / (30*24 * 60 * 60) %12;
    if((diffMonths) >0)
        return [NSString stringWithFormat:@"%dM AGO",diffMonths];
    
    int diffDays    = (int)secondsBetween / (24 * 60 * 60) % 30;
    if((diffDays) >0)
        return [NSString stringWithFormat:@"%dD AGO",diffDays];
    
    int diffHours   = (int)secondsBetween / (60 * 60) % 24;
    if (diffHours > 0)
        return [NSString stringWithFormat:@"%dH AGO",diffHours];
    
    int diffMinutes = (int)secondsBetween / 60;
    if((diffMinutes)>0)
        return [NSString stringWithFormat:@"%dM AGO",diffMinutes];
    
    int diffSeconds = (int)secondsBetween % 60;
    if((diffSeconds)>0)
        return [NSString stringWithFormat:@"%dS AGO",diffSeconds];
    
    return @"JUST NOW";
}

+ (NSString*)distanceFromLat:(NSString*)fromLat FromLong:(NSString*)fromLong ToLat:(NSString*)toLat ToLong:(NSString*)toLong
{
    CLLocation *location1 = [[CLLocation alloc] initWithLatitude:[fromLat doubleValue] longitude:[fromLong doubleValue]];
    CLLocation *location2 = [[CLLocation alloc] initWithLatitude:[toLat doubleValue] longitude:[toLong doubleValue]];
    float distance = [location1 distanceFromLocation:location2];
    if (distance >= 1000) {
        distance = distance / 1000;
        return [NSString stringWithFormat:@"%dKM", (int)distance];
    }
    else {
        return [NSString stringWithFormat:@"%dM", (int)distance];
    }
}

@end

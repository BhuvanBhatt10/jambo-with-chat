//
//  Utility.h
//  Alumni
//
//  Created by Rahul Chandera on 26/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utility : NSObject
+ (NSString*)daysDiffrentFromDateString:(NSString*)dateFrom;
+ (NSString*)daysDiffrentFromDate:(NSDate*)dateFrom;
+ (NSString*)distanceFromLat:(NSString*)fromLat FromLong:(NSString*)fromLong ToLat:(NSString*)toLat ToLong:(NSString*)toLong;

@end

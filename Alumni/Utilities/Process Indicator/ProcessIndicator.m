//
//  ProcessIndicator.m
//  eMenu
//
//  Created by Rahul Chandera on 6/10/14.
//  Copyright (c) 2014 Enerjik. All rights reserved.
//

#import "ProcessIndicator.h"
#import "AppConstants.h"

@implementation ProcessIndicator
@synthesize hud;

static ProcessIndicator *processIndicator = nil;

//============= singleton object ===============//
+ (ProcessIndicator*)sharedInstance {
	
    if (!processIndicator){
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            processIndicator = [[self alloc] init];
            [processIndicator initialize];
        });
    }
    return processIndicator;
}
- (void)initialize {
    
}

//========= Display process indicator ============//
- (void)showProcessIndicatorInView:(UIView*)view Message:(NSString*)message
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible=YES;
    self.hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    self.hud.color = [UIColor grayColor];
    self.hud.dimBackground = YES;
    self.hud.labelText = message;
    
    [self.hud hide:YES afterDelay:60.0];
}

//========== Hide process indicator ============//
- (void)hideProcessIndicator
{
    [self.hud hide:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
	[MBProgressHUD hideHUDForView:[UIApplication sharedApplication].keyWindow animated:YES];
}

//========== Indicator with check-mark ============//
- (void)completeTaskWithMessage:(NSString*)msg displayTime:(int)delay
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Process_Checkmark.png"]];
    self.hud.customView = imageView;
    self.hud.mode = MBProgressHUDModeCustomView;
    self.hud.labelText = msg;
    self.hud.color = GREEN_TRASPARENT_COLOR;
    [self.hud hide:YES afterDelay:delay];
}

//========== Indicator with check-mark ============//
- (void)completeTaskWithErrorMessage:(NSString*)msg displayTime:(int)delay
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible=NO;
    self.hud.mode = MBProgressHUDModeText;
    self.hud.labelText = msg;
    self.hud.color = LIGHT_RED_COLOR;
    [self.hud hide:YES afterDelay:delay];
}

@end

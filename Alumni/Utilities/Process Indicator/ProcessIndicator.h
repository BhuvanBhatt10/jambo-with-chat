//
//  ProcessIndicator.h
//  eMenu
//
//  Created by Rahul Chandera on 6/10/14.
//  Copyright (c) 2014 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"

@interface ProcessIndicator : NSObject

@property(nonatomic,retain)MBProgressHUD *hud;
+ (ProcessIndicator*)sharedInstance;
- (void)showProcessIndicatorInView:(UIView*)view Message:(NSString*)message;
- (void)hideProcessIndicator;
- (void)completeTaskWithMessage:(NSString*)msg displayTime:(int)delay;
- (void)completeTaskWithErrorMessage:(NSString*)msg displayTime:(int)delay;

@end

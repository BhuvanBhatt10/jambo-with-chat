//
//  TagEntity.swift
//  Jambo
//
//  Created by Rahul Chandera on 11/05/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

import UIKit

class TagEntity: NSObject {
   
    var isSelected: Bool
    var isLocked: Bool
    var textContent: String
    
    init(isSelected: Bool, isLocked: Bool, textContent: String) {
        
        self.isSelected = isSelected
        self.isLocked = isLocked
        self.textContent = textContent
        super.init()
    }
    
    convenience override init() {
        self.init(isSelected: false, isLocked: false, textContent: "")
    }
}

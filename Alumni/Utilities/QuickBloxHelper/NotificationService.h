//
//  NotificationService.h
//  Jambo
//
//  Created by Bhuvan on 10/01/16.
//  Copyright © 2016 Enerjik. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <Quickblox/Quickblox.h>

@protocol NotificationServiceDelegate <NSObject>
@required

/**
 *  Is called when dialog fetching is complete and ready to return requested dialog
 *
 *  @param chatDialog QBChatDialog instance. Successfully fetched dialog
 */
- (void)notificationServiceDidSucceedFetchingDialog:(QBChatDialog *)chatDialog;

@optional

/**
 *  Is called when dialog was not found nor in memory storage nor in cache
 *  and NotificationService started requesting dialog from server
 */
- (void)notificationServiceDidStartLoadingDialogFromServer;

/**
 *  Is called when dialog request from server was completed
 */
- (void)notificationServiceDidFinishLoadingDialogFromServer;

/**
 *  Is called when dialog was not found in both memory storage and cache
 *  and server request return nil
 */
- (void)notificationServiceDidFailFetchingDialog;
@end

@interface NotificationService : NSObject

/**
 *  NotificationServiceDelegate protocol delegate
 */
@property (nonatomic, weak) id <NotificationServiceDelegate> delegate;

/**
 *  Dialog id that was recieved from push
 */
@property (nonatomic, strong) NSString *pushDialogID;

/*
 *  Handle push notification method
 */
- (void)handlePushNotificationWithDelegate:(id<NotificationServiceDelegate>)delegate;

@end

//
//  UserdefaultManager.m
//  Jambo
//
//  Created by Bhuvan on 14/12/15.
//  Copyright © 2015 Enerjik. All rights reserved.
//

#import "UserdefaultManager.h"

@implementation UserdefaultManager

+ (void)setQBUserId:(NSInteger)ID {
    if (ID) {
        [[NSUserDefaults standardUserDefaults] setInteger:ID forKey:@"qbUserID"];
    } else {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"qbUserID"];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSInteger)getQBUserID {
    return [[NSUserDefaults standardUserDefaults] integerForKey:@"qbUserID"];
}

+ (void)setQBPassword:(NSString *)password {
    if (password.length > 0) {
        [[NSUserDefaults standardUserDefaults] setObject:password forKey:@"qbUserPassword"];
    } else {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"qbUserPassword"];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (NSString *)getQBPassword {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"qbUserPassword"];
}

+ (void)setUserEmail:(NSString *)email {
    if (email.length > 0) {
        [[NSUserDefaults standardUserDefaults] setObject:email forKey:@"email"];
    } else {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"email"];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)getUserEmail {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"email"];
}

@end

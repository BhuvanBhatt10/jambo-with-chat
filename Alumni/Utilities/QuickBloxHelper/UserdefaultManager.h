//
//  UserdefaultManager.h
//  Jambo
//
//  Created by Bhuvan on 14/12/15.
//  Copyright © 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserdefaultManager : NSObject

+ (NSInteger)getQBUserID;
+ (void)setQBUserId:(NSInteger)ID;

+ (NSString *)getQBPassword;
+ (void)setQBPassword:(NSString *)password;

+ (NSString *)getUserEmail;
+ (void)setUserEmail:(NSString *)email;

@end

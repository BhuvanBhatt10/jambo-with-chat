//
//  QuickBloxHelper.h
//  Jambo
//
//  Created by Bhuvan on 13/12/15.
//  Copyright © 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServicesManager.h"

@class QBUUser;
@class QBChatDialog;
@interface QuickBloxHelper : NSObject

+ (QuickBloxHelper *)sharedInstance;
@property(nonatomic, strong )QBUUser *currentUser;

// Find User with UserID
- (void)getUserIdWithLoginEmail:(NSString *)email
                        success:(void(^)(QBUUser *user))success
                        failure:(void(^)(id error))failure;


// Login quickblox
- (void)loginWithUsername:(NSString *)login
              andPassword:(NSString *)password
                  success:(void(^)(id responseObject))success
                  failure:(void(^)(id error))failure;

// Update Full Name
//- (void)updateFullName;
// Connect quickblox
- (void)connectQBOnCompletion:(void(^)(QBUUser *currentUser))completion;

// Check Connection
- (BOOL)isConnected;

// User Exist in UserMemoryStorage
- (void)isUserExistInStorage:(NSString *)userEmail
                Oncompletion:(void(^)(QBUUser *opponentUser))completion;


// Create Chat dialogue and check if already present
- (void)createChatDialogueWithOpponent:(QBUUser *)opponentUser
                            Oncompletion:(void(^)(QBChatDialog *dialogue))completion;

- (void)loadAllDialogOnCompletion:(void(^)(NSArray *allDialog))completion;

- (void)setSessionWithEmail:(NSString *)email;

@end

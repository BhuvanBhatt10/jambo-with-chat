//
//  QuickBloxHelper.m
//  Jambo
//
//  Created by Bhuvan on 13/12/15.
//  Copyright © 2015 Enerjik. All rights reserved.
//

#import "QuickBloxHelper.h"
#import <Quickblox/Quickblox.h>
#import <QMServicesManager.h>
#import "UserdefaultManager.h"
#import "ServicesManager.h"


@implementation QuickBloxHelper

#pragma mark - Shared Instance
+ (QuickBloxHelper *)sharedInstance {
    static QuickBloxHelper *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[QuickBloxHelper alloc] init];
    });
    return sharedInstance;
}

#pragma mark - User from UserID
- (void)getUserIdWithLoginEmail:(NSString *)email success:(void (^)(QBUUser *))success failure:(void (^)(id))failure {
    [QBRequest usersWithLogins:@[email] successBlock:^(QBResponse * _Nonnull response, QBGeneralResponsePage * _Nullable page, NSArray<QBUUser *> * _Nullable users) {
        if (users.count > 0) {
            success(users[0]);
        } else {
            success(nil);
        }
    } errorBlock:^(QBResponse * _Nonnull response) {
        failure(response);
    }];

}

#pragma mark - Login Quickblox
- (void)loginWithUsername:(NSString *)login andPassword:(NSString *)password success:(void (^)(id))success failure:(void (^)(id))failure {
    QBUUser *authenticateUser  = [QBUUser user];
    authenticateUser.email = login;
    authenticateUser.password = login;

    [[ServicesManager instance] logInWithUser:authenticateUser completion:^(BOOL success, NSString *errorMessage) {
        if (success) {
            NSLog(@"Logged In");
        } else {
            NSLog(@"Errror found");
        }
    }];
    
    [QBRequest logInWithUserLogin:login password:login successBlock:^(QBResponse * _Nonnull response, QBUUser * _Nullable user) {
        QBUUser *authenticateUser  = [QBUUser user];
        authenticateUser.ID = user.ID;
        authenticateUser.password = login;
        
        //Set Session
        [UserdefaultManager setQBPassword:login];
        [UserdefaultManager setQBUserId:(NSInteger)user.ID];
        [UserdefaultManager setUserEmail:login];
        
        
        
        if ([[QBChat instance] isConnected]) {
            success(user);
        } else {
            [[QBChat instance] connectWithUser:authenticateUser completion:^(NSError * _Nullable error) {
                if (error == nil) {
                    self.currentUser = user;
                    success(user);
                }
            }];
        }
        
    } errorBlock:^(QBResponse * _Nonnull response) {
        failure(response);
    }];
}

#pragma mark - Connect Quickblox
- (void)connectQBOnCompletion:(void (^)(QBUUser *))completion {

    QBUUser *authenticateUser  = [QBUUser user];
    authenticateUser.ID = [UserdefaultManager getQBUserID];
    authenticateUser.password = [UserdefaultManager getQBPassword];
    
    if([[QBChat instance] isConnected]) {
        completion(authenticateUser);
    } else {
        [[QBChat instance] connectWithUser:authenticateUser completion:^(NSError * _Nullable error) {
            if (error == nil) {
                self.currentUser = authenticateUser;
                completion(authenticateUser);
            } else {
                completion(nil);
            }
        }];
    }
}

#pragma mark - Check Quickblox Connection Status
- (BOOL)isConnected {
    return [[QBChat instance] isConnected];
}

#pragma mark - Search User in Storage
- (void)isUserExistInStorage:(NSString *)userEmail Oncompletion:(void (^)(QBUUser *))completion {

    //All Users from Storage
    NSArray *usersArrayInMemory = [[QMServicesManager instance].usersService.usersMemoryStorage usersWithEmails:@[userEmail]];
    QBUUser *userFromStorage = [usersArrayInMemory firstObject];
    
    if ([userFromStorage.login isEqualToString:userEmail]) {
        completion(userFromStorage);
    } else {
        [QBRequest usersWithLogins:@[userEmail] successBlock:^(QBResponse * _Nonnull response, QBGeneralResponsePage * _Nullable page, NSArray<QBUUser *> * _Nullable users) {
            if (users.count > 0) {
                [[QMServicesManager instance].usersService.usersMemoryStorage addUser:users[0]];
                completion(users[0]);
            } else {
                completion(nil);
            }
        } errorBlock:^(QBResponse * _Nonnull response) {
            completion(nil);
        }];
    }
}

#pragma mark - Create Chat Dialogue with User
- (void)createChatDialogueWithOpponent:(QBUUser *)opponentUser Oncompletion:(void (^)(QBChatDialog *))completion {

    QBChatDialog *newDialogue = [[QMServicesManager instance].chatService.dialogsMemoryStorage privateChatDialogWithOpponentID:opponentUser.ID];

    // Return if dialogue already present
    if (newDialogue != nil) {
        completion(newDialogue);
    } else {
        
        // Create new dialogue
        [[QMServicesManager instance].chatService createPrivateChatDialogWithOpponentID:opponentUser.ID completion:^(QBResponse *response, QBChatDialog *createdDialog) {
            createdDialog.name = opponentUser.fullName;
            completion(createdDialog);
            // Add dialogue in storage + Join + Return dialogue
            /*[[QMServicesManager instance].chatService.dialogsMemoryStorage addChatDialog:createdDialog andJoin:YES completion:^(QBChatDialog *addedDialog, NSError *error) {
                
                if(error == nil) {
                    completion(addedDialog);
                } else {
                    completion(nil);
                }
            }];*/
        }];
    }
}

#pragma mark - Load All Dialog
- (void)loadAllDialogOnCompletion:(void (^)(NSArray *))completion {
    
    /*NSArray *allDialog = [NSArray new];
    allDialog = [[QMServicesManager instance].chatService.dialogsMemoryStorage dialogsSortByLastMessageDateWithAscending:NO];
    if (allDialog.count > 0) {
        [QMServicesManager instance].chatService 
    }*/
    
    
    completion([[QMServicesManager instance].chatService.dialogsMemoryStorage dialogsSortByLastMessageDateWithAscending:NO]);
    
}

- (void)setSessionWithEmail:(NSString *)email {

    QBUUser *authenticateUser  = [[QBSession currentSession] currentUser];
    [UserdefaultManager setQBPassword:email];
    [UserdefaultManager setQBUserId:(NSInteger)authenticateUser.ID];
    [UserdefaultManager setUserEmail:email];

}

@end

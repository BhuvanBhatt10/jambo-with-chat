//
//  ServicesManager.h
//  Jambo
//
//  Created by Bhuvan on 10/01/16.
//  Copyright © 2016 Enerjik. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "NotificationService.h"
#import <QMServices.h>

static NSString *const kChatCacheNameKey = @"sample-cache";
static NSString *const kContactListCacheNameKey = @"sample-cache-contacts";
static NSString *const kLastActivityDateKey = @"last_activity_date";

/**
 *  Implements logic connected with user's memory/disc storage, error handling, top bar notifications.
 */
@interface ServicesManager : QMServicesManager


/**
 *  Notification service.
 */
@property (nonatomic, readonly) NotificationService* notificationService;

/**
 *  Current opened dialog ID.
 */
@property (nonatomic, strong) NSString* currentDialogID;

/**
 *  Last activity date. Needed for updating chat dialogs when go back from tray.
 */
@property (strong, nonatomic) NSDate *lastActivityDate;

/**
 *  Downlaod latest users.
 */
//- (void)downloadLatestUsersWithSuccessBlock:(void(^)(NSArray *latestUsers))successBlock errorBlock:(void(^)(NSError *error))errorBlock;

/**
 *  Filtering array of users by current environment.
 *
 *  @return Filtered array of users from memory storage by current unvironment.
 */
//- (NSArray *)filteredUsersByCurrentEnvironment;
+ (instancetype)sharedManager;

@end

//
//  NetworkAvailability.m
//  eMenu
//
//  Created by Rahul Chandera on 6/10/14.
//  Copyright (c) 2014 Azilen. All rights reserved.
//

#import "NetworkAvailability.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import <netinet/in.h>

@implementation NetworkAvailability


//======== Check internet connection availability ==========//
+ (BOOL)CheckInternetConnection
{
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
    SCNetworkReachabilityFlags flags;
    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    CFRelease(defaultRouteReachability);
    if (!didRetrieveFlags)
    {
        NSLog(@"Error. Could not recover network reachability");
        return 0;
    }
    BOOL isReachable = flags & kSCNetworkFlagsReachable;
    BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
    BOOL network = (isReachable && !needsConnection) ? YES : NO;
    
    return network;
}
@end

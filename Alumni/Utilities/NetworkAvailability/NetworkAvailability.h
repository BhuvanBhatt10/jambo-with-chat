//
//  NetworkAvailability.h
//  eMenu
//
//  Created by Rahul Chandera on 6/10/14.
//  Copyright (c) 2014 Azilen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkAvailability : NSObject
+ (BOOL)CheckInternetConnection;
@end

//
//  CreateGroupScreen.h
//  Jambo
//
//  Created by Rahul Chandera on 26/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HPGrowingTextView.h"

@interface CreateGroupScreen : UIViewController <HPGrowingTextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtGroupName;
@property (strong, nonatomic) IBOutlet HPGrowingTextView *txtGroupDescription;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *dividerTopSpace;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *descriptionHeight;

@end

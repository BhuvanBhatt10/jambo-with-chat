//
//  NearbyScreen.h
//  Alumni
//
//  Created by Rahul Chandera on 16/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserManager.h"
#import "ContactInfoPopup.h"

@interface NearbyScreen : UIViewController <UserManagerDelegate,ContactInfoPopupDelegate>
{
    IBOutlet UITableView *TableView;
    IBOutlet UIButton *btnByDistance;
    IBOutlet UIButton *btnByLastUpdate;
}

@property(nonatomic,strong) ContactInfoPopup *contactInfoPopup;
- (IBAction)shortingHandler:(UIButton*)sender;

@end

//
//  AddAnswerScreen.m
//  Jambo
//
//  Created by Rahul Chandera on 27/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "AddAnswerScreen.h"
#import "AppConstants.h"
#import "NavigationManager.h"
#import "DiscussionManager.h"
#import "ProcessIndicator.h"
//#import "GiFHUD.h"

@interface AddAnswerScreen () <DiscussionManagerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@end

NavigationManager *navigationManager;
DiscussionManager *discussionManager;
ProcessIndicator *processIndicator;
UIImage *answerImage;

@implementation AddAnswerScreen
@synthesize questionId;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeUI];
    [self initializeObjects];
}

#pragma mark - Initialize UI
- (void)initializeUI {
    
    self.navigationController.navigationBarHidden = NO;
    
    //Menu Button
    UIButton *btnPost = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 70, 44)];
    [btnPost addTarget:self action:@selector(postAnswer) forControlEvents:UIControlEventTouchUpInside];
    [btnPost setBackgroundColor:LIGHT_RED_COLOR];
    [btnPost setTitle:@"POST" forState:UIControlStateNormal];
    btnPost.titleLabel.textColor = [UIColor whiteColor];
    btnPost.titleLabel.font = [UIFont fontWithName:@"Roboto-Bold" size:14.0];
    [btnPost setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    UIBarButtonItem *bbiPost = [[UIBarButtonItem alloc]initWithCustomView:btnPost];
    bbiPost.style = UIBarButtonItemStylePlain;
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    spacer.width = -20;
    self.navigationItem.rightBarButtonItems = @[spacer, bbiPost];
    
    //Set up grouwing textview
    tvAnswer.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    tvAnswer.minNumberOfLines = 1;
    tvAnswer.maxNumberOfLines = 5;
    tvAnswer.returnKeyType = UIReturnKeyDone;
    tvAnswer.font = [UIFont fontWithName:@"Roboto-Regular" size:18.0f];
    tvAnswer.delegate = self;
    tvAnswer.placeholder = @"Write your answer here...";
    tvAnswer.placeholderColor = [UIColor colorWithWhite:0.600 alpha:0.500];
}

#pragma mark - Growing textview delegates
-(void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    [self.view layoutIfNeeded];
    
    tvHeight.constant = height;
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (BOOL)growingTextViewShouldReturn:(HPGrowingTextView *)growingTextView;
{
    [growingTextView resignFirstResponder];
    return YES;
}

#pragma mark - Initialize Objects
- (void)initializeObjects
{
    navigationManager = [NavigationManager sharedInstance];
    discussionManager = [DiscussionManager sharedInstance];
    processIndicator = [ProcessIndicator sharedInstance];
    answerImage = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [tvAnswer becomeFirstResponder];
}

#pragma mark - Add Photo
- (IBAction)addPhotoHandler:(UIButton*)sender
{
    [[[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take a Photo",@"Photo from Library", nil] showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != 2)
    {
        [tvAnswer resignFirstResponder];
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        if (buttonIndex == 0)
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        else
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [picker setModalPresentationStyle:UIModalPresentationOverFullScreen];
        
        [[UIApplication sharedApplication]setStatusBarHidden:YES];
        [self presentViewController:picker animated:YES completion:NULL];
        [[UIApplication sharedApplication]setStatusBarHidden:NO];
    }
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    answerImage = info[UIImagePickerControllerEditedImage];
    imgAnswer.image = answerImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)postAnswer
{
    if (tvAnswer.text.length>0)
    {
        [tvAnswer resignFirstResponder];
        [processIndicator showProcessIndicatorInView:self.view Message:@"Processing..."];
        discussionManager.delegate = self;
        [discussionManager postAnswer:self.questionId Answer:tvAnswer.text withImage:answerImage];
    }
}

#pragma mark - Request Complete
-(void)requestCompleted:(id)response{
    
    [processIndicator hideProcessIndicator];
    
    if (![[response objectForKey:RESULT_KEY] boolValue])
    {
        NSError* err = nil;
        AnswerEntity *answerEntity = [[AnswerEntity alloc]initWithDictionary:response error:&err];
        NSDictionary *cellData = [[NSDictionary alloc]initWithObjectsAndKeys:answerEntity,@"Answer", nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"addNewAnswer" object:cellData];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        if ([[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_INTERNET_NOT_AVAILABLE_CODE)
        {
            [[[UIAlertView alloc] initWithTitle:@"Alumni" message:@"No internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
        else if([[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_SERVER_CONNECTION_ERROR || [[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_INTERNAL_SERVICE_ERROR){
            [[[UIAlertView alloc] initWithTitle:@"Alumni" message:@"Something went wrong" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
        else
            [[[UIAlertView alloc] initWithTitle:@"Alumni" message:[response objectForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

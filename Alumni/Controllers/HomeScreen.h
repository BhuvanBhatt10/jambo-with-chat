//
//  HomeScreen.h
//  Alumni
//
//  Created by Rahul Chandera on 13/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DiscussionManager.h"
#import "ContactInfoPopup.h"

@interface HomeScreen : UIViewController <DiscussionManagerDelegate>
{
    IBOutlet UITableView *TableView;
    IBOutlet UICollectionView *nearbyCollection;
    
    IBOutlet UIView *viewBtnContainer;
    IBOutlet UIButton *btnCreateGroup;
    IBOutlet UIButton *btnCreateEvent;
    IBOutlet UIButton *btnPlusButton;
    IBOutlet UIButton *btnNearBy;
}
@property(nonatomic,strong) ContactInfoPopup *contactInfoPopup;

- (IBAction)createGroup:(id)sender;
- (IBAction)createEvent:(id)sender;
- (IBAction)plusButtonHandler:(UIButton*)sender;

@end

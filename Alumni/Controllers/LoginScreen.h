//
//  LoginSignupScreen.h
//  Alumni
//
//  Created by Rahul Chandera on 22/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserManager.h"

@interface LoginScreen : UIViewController {
    IBOutlet UITextField *txtEmail;
    IBOutlet UITextField *txtPassword;
    IBOutlet UIView *loginView;
    IBOutlet UIActivityIndicatorView *indicator;
}
- (IBAction)loginHandler:(id)sender;

@end

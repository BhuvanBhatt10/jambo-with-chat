//
//  QuestionsCollectionView.h
//  Jambo
//
//  Created by Rahul Chandera on 27/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupEntity.h"

@protocol QuestionsCollectionViewDelegate
-(void)selectedQuestion:(QuestionEntity*)questionEntity;
-(void)addQuestionToGroup:(int)index;
-(void)inviteAlumniToGroup:(int)index;
-(void)viewAllQuestionOfGroup:(int)index;
@end

@interface QuestionsCollectionView : UIViewController
{
    IBOutlet UILabel *lblGroupName;
    IBOutlet UIButton *btnViewAll;
}

@property (strong,nonatomic) id <QuestionsCollectionViewDelegate> delegate;
@property (strong, nonatomic) IBOutlet UICollectionView *CollectionView;
@property (strong, nonatomic) NSArray *questionsArray;
- (void)setQuestionsData:(GroupEntity*)groupEntity;
- (IBAction)viewAllTapHandler:(id)sender;

@end

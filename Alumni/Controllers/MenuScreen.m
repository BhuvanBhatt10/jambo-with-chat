//
//  MenuScreen.m
//  Alumni
//
//  Created by Rahul Chandera on 14/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "MenuScreen.h"
#import "AppConstants.h"
#import "UIImageView+WebCache.h"
#import "NavigationManager.h"
#import "MCPanelViewController.h"
#import "UserManager.h"
#import "UserProfileScreen.h"
#import "AppDelegate.h"

#import "HomeScreen.h"
#import "GroupsListScreen.h"
#import "EventsScreen.h"
#import "NearbyScreen.h"
#import "InboxScreen.h"
#import "AppInvitation.h"
#import "SearchScreen.h"
#import "SettingsScreen.h"
#import "UserdefaultManager.h"
#import "ServicesManager.h"

@interface MenuScreen ()

@end

enum {
    Home,
    Groups,
    Events,
    Nearby,
    Inbox,
    Invite,
    Search,
    Settings,
    Logout
} cellType;


NavigationManager *navigationManager;
UserManager *userManager;
NSArray *optionsArray;
int selectedMenuIndex;

@implementation MenuScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeUI];
    [self initializeObjects];
}

#pragma mark - Initialize UI
- (void)initializeUI {

    UIInterpolatingMotionEffect *interpolationHorizontal = [[UIInterpolatingMotionEffect alloc]initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    interpolationHorizontal.minimumRelativeValue = @-50.0;
    interpolationHorizontal.maximumRelativeValue = @50.0;
    
    UIInterpolatingMotionEffect *interpolationVertical = [[UIInterpolatingMotionEffect alloc]initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    interpolationVertical.minimumRelativeValue = @-50.0;
    interpolationVertical.maximumRelativeValue = @50.0;
    
    [imgHeaderBackground addMotionEffect:interpolationHorizontal];
    [imgHeaderBackground addMotionEffect:interpolationVertical];
    
    //imgHeaderBackground.contentMode = UIViewContentModeScaleAspectFit;
    
    imgUserPhoto.layer.cornerRadius = 26.0;
    imgUserPhoto.layer.masksToBounds = YES;
}


#pragma mark - Initialize Objects
- (void)initializeObjects
{
    navigationManager = [NavigationManager sharedInstance];
    userManager = [UserManager sharedInstance];
    optionsArray = @[@"Home",@"Groups",@"Events",@"Nearby",@"Inbox",@"Invite Friends",@"Search",@"Settings",@"Logout"];
    
    [imgUserPhoto setImageWithURL:[NSURL URLWithString:userManager.userEntity.profilePic] placeholderImage:[UIImage imageNamed:DEFAULT_USER_IMAGE]];
    lblUserName.text = [NSString stringWithFormat:@"%@ %@",userManager.userEntity.firstName, userManager.userEntity.lastName];
    lblUserCode.text = userManager.userEntity.occupation;
}


#pragma mark - TableView Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [optionsArray count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"OptionsCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    UILabel *lblIndicator = (UILabel *)[cell.contentView viewWithTag:100];
    if(indexPath.row == selectedMenuIndex)
        lblIndicator.backgroundColor = NAVIGATIONBAR_COLOR;
    else
        lblIndicator.backgroundColor = [UIColor clearColor];
    
    UILabel *lblTitle = (UILabel *)[cell.contentView viewWithTag:101];
    lblTitle.text = [optionsArray objectAtIndex:indexPath.row];
    
    UILabel *lblCount = (UILabel *)[cell.contentView viewWithTag:102];
    if(indexPath.row == Invite) {
    
        lblCount.hidden = NO;
        lblCount.text = [NSString stringWithFormat:@"%d",userManager.totalInvites - (int)userManager.myReferredUsers.count];
    }
    else
        lblCount.hidden = YES;
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedMenuIndex = (int)indexPath.row;
    
    NSArray * vc = self.parentViewController.navigationController.viewControllers;
    UIViewController * lastController = [vc objectAtIndex:vc.count - 2];
    switch (indexPath.row) {
        
        case Home:
            if (![lastController isKindOfClass:[HomeScreen class]])
                [self.parentViewController.navigationController pushViewController:[navigationManager getScreen:nsHomeScreen] animated:YES];
            break;
            
        case Groups:
            if (![lastController isKindOfClass:[GroupsListScreen class]])
                [self.parentViewController.navigationController pushViewController:[navigationManager getScreen:nsGroupsListScreen] animated:YES];
            break;
        case Events:
            if (![lastController isKindOfClass:[EventsScreen class]])
                [self.parentViewController.navigationController pushViewController:[navigationManager getScreen:nsEventsScreen] animated:YES];
            break;
        case Nearby:
            if (![lastController isKindOfClass:[NearbyScreen class]])
                [self.parentViewController.navigationController pushViewController:[navigationManager getScreen:nsNearbyScreen] animated:YES];
            break;
        case Inbox:
            if (![lastController isKindOfClass:[InboxScreen class]])
                [self.parentViewController.navigationController pushViewController:[navigationManager getScreen:nsInboxScreen] animated:YES];
            break;
        case Invite:
            if (![lastController isKindOfClass:[AppInvitation class]])
                [self.parentViewController.navigationController pushViewController:[navigationManager getScreen:nsAppInvitation] animated:YES];
            break;
        case Search:
            if (![lastController isKindOfClass:[SearchScreen class]])
                [self.parentViewController.navigationController pushViewController:[navigationManager getScreen:nsSearchScreen] animated:YES];
            break;
        case Settings:
            if (![lastController isKindOfClass:[SettingsScreen class]])
                [self.parentViewController.navigationController pushViewController:[navigationManager getScreen:nsSettingsScreen] animated:YES];
            break;
        case Logout:
        {
            [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"IsUserLogin"];
            [UserdefaultManager setQBPassword:nil];
            [UserdefaultManager setQBUserId:0];
            [UserdefaultManager setUserEmail:nil];
            [ServicesManager instance].lastActivityDate = nil;
            
            AppDelegate * appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [appDelegate deleteAndRecreateStore];
            [self.parentViewController.navigationController popToRootViewControllerAnimated:YES];
        }
            break;
        
        default:
            break;
    }
    
    [self.panelViewController dismiss];
}



- (IBAction)openUserProfile:(id)sender
{
    UserProfileScreen *userProfileScreen = (UserProfileScreen*)[navigationManager getScreen:nsUserProfileScreen];
    userProfileScreen.userEntity = userManager.userEntity;
    [self.panelViewController dismiss];
    [self.parentViewController.navigationController pushViewController:userProfileScreen animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  InviteCell.m
//  Jambo
//
//  Created by Rakesh Pethani on 27/10/15.
//  Copyright © 2015 Enerjik. All rights reserved.
//

#import "InviteCell.h"

@implementation InviteCell

- (void)awakeFromNib {
    // Initialization code
    
    self.btnInvite.layer.cornerRadius = 2.0f;
    self.btnInvite.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setInvitedWithEmailId:(NSString *)email {
    
    self.txtEmailId.delegate = self;
    
    if (email) {
        self.txtEmailId.text = email;
        self.txtEmailId.enabled = NO;
        
        self.btnInvite.backgroundColor = [UIColor clearColor];
        [self.btnInvite setTitleColor:[UIColor colorWithWhite:0.425 alpha:1.000] forState:UIControlStateNormal];
        [self.btnInvite setTitle:@"INVITED" forState:UIControlStateNormal];
        self.btnInvite.enabled = NO;
    }
    
    if (self.txtEmailId.text.length > 0)
        self.btnInvite.hidden = NO;
    else
        self.btnInvite.hidden = YES;
}

- (IBAction)btnInviteClicked:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(btnInviteClickedForCell:)]) {
        [self.delegate btnInviteClickedForCell:self];
    }
}

-(BOOL)IsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

#pragma mark - Textfield delegates
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.txtEmailId resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];

    if (newString.length > 0 && [self IsValidEmail:newString])
        self.btnInvite.hidden = NO;
    else
        self.btnInvite.hidden = YES;
    
    return YES;
}

@end

//
//  InboxScreen.m
//  Alumni
//
//  Created by Rahul Chandera on 22/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#define kDialogsPageLimit 100

#import "InboxScreen.h"
#import "NavigationManager.h"
#import "UserManager.h"
#import "AppConstants.h"
#import "MessageGroupEntity.h"
#import "UIImageView+WebCache.h"
#import "Utility.h"
#import "NotificationEntity.h"
#import "Jambo-Swift.h"
#import "InboxDetailScreen.h"
#import <Quickblox/Quickblox.h>
#import <QMServices.h>
#import "ChatManager.h"
#import "ChatViewController.h"
#import "ProcessIndicator.h"
#import "QuickBloxHelper.h"
#import "ServicesManager.h"

@interface InboxScreen ()<
QMChatServiceDelegate,
QMAuthServiceDelegate,
QMChatConnectionDelegate
>
@property (nonatomic, readonly) NSArray* dialogs;

@end

NavigationManager *navigationManager;
MessageManager *messageManager;
UserManager *userManager;
ChatManager *chatManager;
ProcessIndicator *processIndicator;
NSDateFormatter *DateFormatter;
int senderId;

@implementation InboxScreen

- (void)viewDidLoad {
    [super viewDidLoad];

    [ServicesManager.instance.chatService addDelegate:self];
    if ([QBChat instance].isConnected) {
        [self loadDialog];
    } /*else {
        [[QuickBloxHelper sharedInstance] connectQBOnCompletion:^(QBUUser *currentUser) {
            [self loadDialog];
        }];
    }*/
    self.title = @"Inbox";
    
    [self initializeObjects];
}

- (void)viewWillAppear:(BOOL)animated {
    [TableView reloadData];
    [super viewWillAppear:YES];
}

#pragma mark - Initialize Objects
- (void)initializeObjects
{
    navigationManager = [NavigationManager sharedInstance];
    messageManager    = [MessageManager sharedInstance];
    userManager       = [UserManager sharedInstance];
    chatManager       = [ChatManager sharedInstance];
    DateFormatter     = [[NSDateFormatter alloc]init];
    processIndicator  = [ProcessIndicator sharedInstance];
    DateFormatter.dateFormat = @"dd/MM/yyyy HH:mm:ss";
    
}

#pragma mark - Load Dialogs

- (void)loadDialog {
    [QBRequest dialogsWithSuccessBlock:^(QBResponse * _Nonnull response, NSArray<QBChatDialog *> * _Nullable dialogObjects, NSSet<NSNumber *> * _Nullable dialogsUsersIDs) {
//        allDialogs = dialogObjects;
        [ServicesManager.instance.chatService.dialogsMemoryStorage addChatDialogs:dialogObjects andJoin:YES];
        [TableView reloadData];
    } errorBlock:^(QBResponse * _Nonnull response) {
        
    }];
    
    /*if ([ServicesManager sharedManager].lastActivityDate != nil) {
        [[ServicesManager instance].chatService fetchDialogsUpdatedFromDate:[ServicesManager sharedManager].lastActivityDate andPageLimit:kDialogsPageLimit iterationBlock:^(QBResponse *response, NSArray *dialogObjects, NSSet *dialogsUsersIDs, BOOL *stop) {
            //
            [TableView reloadData];
        } completionBlock:^(QBResponse *response) {
            //
            if ([ServicesManager instance].isAuthorized && response.success) {
                [ServicesManager sharedManager].lastActivityDate = [NSDate date];
            }
        }];
    }
    else {
        [processIndicator showProcessIndicatorInView:self.view Message:@"Loading..."];
        [[ServicesManager instance].chatService allDialogsWithPageLimit:kDialogsPageLimit extendedRequest:nil iterationBlock:^(QBResponse *response, NSArray *dialogObjects, NSSet *dialogsUsersIDs, BOOL *stop) {
            [TableView reloadData];
        } completion:^(QBResponse *response) {
            if ([ServicesManager instance].isAuthorized) {
                if (response.success) {
                    [processIndicator hideProcessIndicator];
                    [ServicesManager sharedManager].lastActivityDate = [NSDate date];
                }
                else {
                    [processIndicator hideProcessIndicator];
                }
            }
        }];
    }*/

}

- (NSArray *)dialogs
{
    
    NSArray *dialogArray = [ServicesManager.instance.chatService.dialogsMemoryStorage dialogsSortByUpdatedAtWithAscending:NO];
//    NSArray *dialogArray = allDialogs;
    
   
    //Remove empty chat dialog
    NSMutableArray *newFilteredArray = [[NSMutableArray alloc] init];
    for (QBChatDialog *dialog in dialogArray) {
        if (dialog.lastMessageText.length > 0) {
            [newFilteredArray addObject:dialog];
        }
    }
   
    // Retrieving dialogs sorted by updatedAt date from memory storage.
    return newFilteredArray;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
   // [refreshTimer invalidate];
}
#pragma mark - Request Complete
-(void)requestCompleted:(id)response{
    
    if ([messageManager.messageArray count]>0)
    {
        [TableView reloadData];
    }
    else
    {
        if ([[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_INTERNET_NOT_AVAILABLE_CODE)
        {
            [[[UIAlertView alloc] initWithTitle:@"Alumni" message:@"No internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
        else if([[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_SERVER_CONNECTION_ERROR || [[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_INTERNAL_SERVICE_ERROR){
            [[[UIAlertView alloc] initWithTitle:@"Alumni" message:@"Something went wrong" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
        else
            [[[UIAlertView alloc] initWithTitle:@"Alumni" message:[response objectForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}



#pragma mark - TableView Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dialogs.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 65;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"InboxCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    UIImageView *imgPhoto = (UIImageView *)[cell.contentView viewWithTag:101];
    UILabel *lblName = (UILabel *)[cell.contentView viewWithTag:102];
    UILabel *lblTime = (UILabel *)[cell.contentView viewWithTag:103];
    UILabel *lblMessage = (UILabel *)[cell.contentView viewWithTag:104];
    UILabel *lblMsgCount = (UILabel *)[cell.contentView viewWithTag:105];
    
    QBChatDialog *chatDialog = self.dialogs[indexPath.row];
    
    //Last Message
    lblMessage.text = @"";
    if(chatDialog.lastMessageText.length > 0) {
        lblMessage.text = chatDialog.lastMessageText;
    }
    
    //Dialog Name
    QBUUser *current = [[QBSession currentSession] currentUser];
    NSInteger recipentID = 0;
    for (NSNumber *number in chatDialog.occupantIDs) {
        NSInteger integerId = [number integerValue];
        NSInteger currentIdInteger = current.ID;
        if (integerId != currentIdInteger) {
            recipentID = integerId;
        }
    }
    if (recipentID) {
        QBUUser *tempUser = [[ServicesManager instance].usersService.usersMemoryStorage userWithID:recipentID];
        if (tempUser.fullName) {
            lblName.text = tempUser.fullName;
        } else {
            [QBRequest userWithID:recipentID successBlock:^(QBResponse * _Nonnull response, QBUUser * _Nullable user) {
                lblName.text = user.fullName;
                [[QMServicesManager instance].usersService.usersMemoryStorage addUser:user];
            } errorBlock:^(QBResponse * _Nonnull response) {
            }];
        }
    }
    
    //Dialog Image
    [imgPhoto sd_setImageWithURL:[NSURL URLWithString:chatDialog.photo] placeholderImage:[UIImage imageNamed:DEFAULT_USER_IMAGE]];
    
    //Time Stamp
    lblTime.text = [Utility daysDiffrentFromDate:chatDialog.lastMessageDate];
    
    //Unread Count
    BOOL hasUnreadMessages = chatDialog.unreadMessagesCount > 0;
    lblMsgCount.hidden = !hasUnreadMessages;
    if (hasUnreadMessages) {
        NSString* unreadText = nil;
        if (chatDialog.unreadMessagesCount > 99) {
            unreadText = @"99+";
        } else {
            unreadText = [NSString stringWithFormat:@"%lu", (unsigned long)chatDialog.unreadMessagesCount];
        }
        lblMsgCount.layer.cornerRadius = 5.0;
        lblMsgCount.layer.masksToBounds = YES;
        lblMsgCount.text = unreadText;
    } else {
        lblMsgCount.text = nil;
        
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{    
    ChatViewController *chatVC = (ChatViewController *)[navigationManager getScreen:nsChatScreen];
    QBUUser * currentUser = [QBUUser user];
    currentUser.login = userManager.userEntity.email;
    currentUser.password = userManager.userEntity.email;
    currentUser.fullName = [NSString stringWithFormat:@"%@ %@",userManager.userEntity.firstName,userManager.userEntity.lastName];
    
    //Check login
    if ([[QBChat instance] isConnected]) {
        chatVC.dialog = self.dialogs[indexPath.row];
        [self.navigationController pushViewController:chatVC animated:YES];
    } else {
        [processIndicator showProcessIndicatorInView:self.view Message:@"Loading..."];

        [QBRequest logInWithUserEmail:currentUser.login password:currentUser.login successBlock:^(QBResponse * _Nonnull response, QBUUser * _Nullable user) {
            [processIndicator hideProcessIndicator];
            chatVC.dialog = self.dialogs[indexPath.row];
            [self.navigationController pushViewController:chatVC animated:YES];
        } errorBlock:^(QBResponse * _Nonnull response) {
            [processIndicator hideProcessIndicator];
        }];

        /*[[ServicesManager instance] logInWithUser:currentUser completion:^(BOOL success, NSString *errorMessage) {
            if (success) {
                [processIndicator hideProcessIndicator];
                chatVC.dialog = self.dialogs[indexPath.row];
                [self.navigationController pushViewController:chatVC animated:YES];
            } else {
                [processIndicator hideProcessIndicator];
            }
        }];*/
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Chat Service Delegate

- (void)chatService:(QMChatService *)chatService didAddChatDialogsToMemoryStorage:(NSArray *)chatDialogs {
    
    [TableView reloadData];
}

- (void)chatService:(QMChatService *)chatService didAddChatDialogToMemoryStorage:(QBChatDialog *)chatDialog {
//    if ([[QMServicesManager instance].chatService.dialogsMemoryStorage chatDialogWithID:chatDialog.ID] == nil) {
//        [[QMServicesManager instance].chatService.dialogsMemoryStorage addChatDialog:chatDialog andJoin:YES completion:^(QBChatDialog *addedDialog, NSError *error) {
//            [self loadDialog];
//        }];
//    }
    [TableView reloadData];

}

- (void)chatService:(QMChatService *)chatService didUpdateChatDialogInMemoryStorage:(QBChatDialog *)chatDialog {
//    NSArray *dialogArray = [[QMServicesManager instance].chatService.dialogsMemoryStorage dialogsSortByLastMessageDateWithAscending:NO];
//    
//    for (QBChatDialog *newDialog in dialogArray) {
//        if (chatDialog.ID == newDialog.ID) {
//            [[QMServicesManager instance].chatService.dialogsMemoryStorage deleteChatDialogWithID:chatDialog.ID];
//            [[QMServicesManager instance].chatService.dialogsMemoryStorage addChatDialog:chatDialog andJoin:YES completion:^(QBChatDialog *addedDialog, NSError *error) {
//                
//            }];
//        }
//    }
    
    [TableView reloadData];
}

- (void)chatService:(QMChatService *)chatService didReceiveNotificationMessage:(QBChatMessage *)message createDialog:(QBChatDialog *)dialog {
//    [[QMServicesManager instance].chatService.messagesMemoryStorage addMessage:message forDialogID:dialog.ID];
    [TableView reloadData];
}

- (void)chatService:(QMChatService *)chatService didAddMessageToMemoryStorage:(QBChatMessage *)message forDialogID:(NSString *)dialogID {
//    [[QMServicesManager instance].chatService.messagesMemoryStorage addMessage:message forDialogID:dialogID];
    [TableView reloadData];
}

- (void)chatService:(QMChatService *)chatService didAddMessagesToMemoryStorage:(NSArray *)messages forDialogID:(NSString *)dialogID {
//    [[QMServicesManager instance].chatService.messagesMemoryStorage addMessages:messages forDialogID:dialogID];
    [TableView reloadData];
}

- (void)chatService:(QMChatService *)chatService didDeleteChatDialogWithIDFromMemoryStorage:(NSString *)chatDialogID {
    [TableView reloadData];
}

#pragma mark - QMChatConnectionDelegate

- (void)chatServiceChatDidConnect:(QMChatService *)chatService
{
    //[SVProgressHUD showSuccessWithStatus:@"Chat connected!" maskType:SVProgressHUDMaskTypeClear];
    [self loadDialog];
}

- (void)chatServiceChatDidReconnect:(QMChatService *)chatService
{
    /*[SVProgressHUD showSuccessWithStatus:@"Chat reconnected!" maskType:SVProgressHUDMaskTypeClear];
    [self loadDialogs];*/
    [self loadDialog];

}

- (void)chatServiceChatDidAccidentallyDisconnect:(QMChatService *)chatService
{
//    [SVProgressHUD showErrorWithStatus:@"Chat disconnected!"];
}

- (void)chatServiceChatDidNotLoginWithError:(NSError *)error
{
    /*[SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"Did not login with error: %@", [error description]]];*/
}

- (void)chatServiceChatDidFailWithStreamError:(NSError *)error
{
//    [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"Chat failed with error: %@", [error description]]];
}



@end

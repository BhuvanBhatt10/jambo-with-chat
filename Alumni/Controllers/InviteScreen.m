//
//  EventInviteScreen.m
//  Alumni
//
//  Created by Rahul Chandera on 05/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "InviteScreen.h"
#import "NavigationManager.h"
#import "DiscussionManager.h"
#import "ProcessIndicator.h"
#import "AppConstants.h"
#import "EventEntity.h"
#import "UIImageView+WebCache.h"
#import "UserEntity.h"
#import "HomeScreen.h"
#import "EventsScreen.h"

@interface InviteScreen () <DiscussionManagerDelegate>

@end

NavigationManager *navigationManager;
DiscussionManager *discussionManager;
ProcessIndicator *processIndicator;
EventManager *eventManager;
UserManager *userManager;

@implementation InviteScreen
{
    NSArray * searchArray;
    BOOL isSearching;
    NSTimer * searchTimer;
}

@synthesize isInEditMode;
@synthesize inviteType;
@synthesize groupId,groupName,groupDescription;


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initializeUI];
    [self initializeObjects];
}

#pragma mark - Initialize UI
- (void)initializeUI
{
    if(inviteType == InviteTypeEvent){
        
        self.title = @"Invite People & Group";
        tabsView.hidden = NO;
        self.searchTopSpace.constant = 50;
        [self.view setNeedsLayout];
        [self tabGruopPelple:btnGroupTab];
        [btnCreate removeTarget:self action:@selector(createGroupHandler:) forControlEvents:UIControlEventTouchUpInside];
        [btnCreate addTarget:self action:@selector(createEventHandler:) forControlEvents:UIControlEventTouchUpInside];
        [btnCreate setTitle:@"Send Invitation" forState:UIControlStateNormal];
    }
    else if(inviteType == InviteTypeGroupMember){
        
        self.title = @"Invite People";
        tabsView.hidden = YES;
        self.searchTopSpace.constant = 0;
        [self.view setNeedsLayout];
        [self tabGruopPelple:btnPeopleTab];
        [btnCreate removeTarget:self action:@selector(createEventHandler:) forControlEvents:UIControlEventTouchUpInside];
        [btnCreate addTarget:self action:@selector(createGroupHandler:) forControlEvents:UIControlEventTouchUpInside];
        [btnCreate setTitle:@"Send Invitation" forState:UIControlStateNormal];
    }
    else if(inviteType == InviteTypeGroupAdmin){
        
        self.title = @"Manage Admins";
        tabsView.hidden = YES;
        self.searchTopSpace.constant = 0;
        [self.view setNeedsLayout];
        [self tabGruopPelple:btnPeopleTab];
        [btnCreate removeTarget:self action:@selector(createEventHandler:) forControlEvents:UIControlEventTouchUpInside];
        [btnCreate addTarget:self action:@selector(createGroupHandler:) forControlEvents:UIControlEventTouchUpInside];
        [btnCreate setTitle:@"Select as Admin" forState:UIControlStateNormal];
    }
}

#pragma mark - Initialize Objects
- (void)initializeObjects
{
    navigationManager = [NavigationManager sharedInstance];
    discussionManager = [DiscussionManager sharedInstance];
    eventManager = [EventManager sharedInstance];
    userManager = [UserManager sharedInstance];
    processIndicator = [ProcessIndicator sharedInstance];
}

- (IBAction)tabGruopPelple:(UIButton*)sender
{
    if(sender == btnGroupTab) {
    
        btnGroupTab.selected = YES;
        btnPeopleTab.selected = NO;
        txtSearch.placeholder = @"Search for groups";
    }
    else {
    
        btnGroupTab.selected = NO;
        btnPeopleTab.selected = YES;
        txtSearch.placeholder = @"Search for people";
    }
    
    TableView.tag = sender.tag;
    
    if (isSearching)
        [self applySearchWithSearchString];
    else
        [TableView reloadData];
}

- (IBAction)searchTextChanged:(UITextField *)sender
{
    if (sender.text.length > 0)
    {
        isSearching = YES;
        
        [searchTimer invalidate];
        searchTimer = nil;
        searchTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(applySearchWithSearchString) userInfo:nil repeats:NO];
    }
    else if (isSearching)
    {
        isSearching = NO;
        searchArray = nil;
        [TableView reloadData];
    }
}

- (void)applySearchWithSearchString
{
    searchArray = nil;
    NSArray * arrayToSearch;
    
    if (btnGroupTab.selected)
        arrayToSearch = discussionManager.myGroupsArray;
    else
        arrayToSearch = self.alumniArray;
    
    NSPredicate * searchPredicate = [NSPredicate predicateWithFormat:@"SELF.tag contains[cd] %@",txtSearch.text];
    searchArray = [arrayToSearch filteredArrayUsingPredicate:searchPredicate];
    [TableView reloadData];
}

#pragma mark - TableView Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isSearching) {
        return searchArray.count;
    }
    
    if(tableView.tag == 1)
        return discussionManager.myGroupsArray.count;
    else
        return self.alumniArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TableCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    UIImageView *imgPhoto = (UIImageView *)[cell.contentView viewWithTag:101];
    UILabel *lblName = (UILabel *)[cell.contentView viewWithTag:102];
    UIImageView *imgCheckMark = (UIImageView *)[cell.contentView viewWithTag:103];
    
    if (tableView.tag == 1) {
        
        GroupEntity *groupEntity = isSearching ? searchArray.count > indexPath.row ? [searchArray objectAtIndex:indexPath.row] : nil : [discussionManager.myGroupsArray objectAtIndex:indexPath.row];
        lblName.text = groupEntity.tag;
        
        if(groupEntity.getSelected)
            imgCheckMark.hidden = NO;
        else
            imgCheckMark.hidden = YES;
        
        imgPhoto.hidden = YES;
    }
    else {
    
        UserEntity *userEntity = isSearching ? searchArray.count > indexPath.row ? [searchArray objectAtIndex:indexPath.row] : nil : [self.alumniArray objectAtIndex:indexPath.row];
        
        [imgPhoto setImageWithURL:[NSURL URLWithString:userEntity.profilePic] placeholderImage:[UIImage imageNamed:DEFAULT_USER_IMAGE]];
        lblName.text = [NSString stringWithFormat:@"%@ %@",userEntity.firstName, userEntity.lastName];
        userEntity.tag = [NSString stringWithFormat:@"%@ %@",userEntity.firstName, userEntity.lastName];
        
        if(userEntity.getSelected)
            imgCheckMark.hidden = NO;
        else
            imgCheckMark.hidden = YES;
        
        imgPhoto.hidden = NO;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 1) {
        
        GroupEntity *groupEntity = isSearching ? searchArray.count > indexPath.row ? [searchArray objectAtIndex:indexPath.row] : nil : [discussionManager.myGroupsArray objectAtIndex:indexPath.row];
        
        if(groupEntity.getSelected)
            [groupEntity setSelected:NO];
        else
        {
            [groupEntity setSelected:YES];

            for(MyGroupsEntity *myGroupsEntity in groupEntity.userTagList)
            {
                for (UserEntity * userEntity in self.alumniArray) {
                    if (myGroupsEntity.aluminiId == userEntity.userId) {
                        [userEntity setSelected:YES];
                    }
                }
            }
        }
        
        [tableView reloadData];
    }
    else {
        
        UserEntity *userEntity = isSearching ? searchArray.count > indexPath.row ? [searchArray objectAtIndex:indexPath.row] : nil : [self.alumniArray objectAtIndex:indexPath.row];
        
        if(userEntity.getSelected)
            [userEntity setSelected:NO];
        else
            [userEntity setSelected:YES];
        
        [tableView reloadData];
    }
}

- (IBAction)createEventHandler:(id)sender
{
    NSString *alumniInvite = @"";
    for(UserEntity *user in  self.alumniArray)
    {
        if([user getSelected])
        {
            if (alumniInvite.length == 0)
                alumniInvite = [NSString stringWithFormat:@"%d", user.userId];
            else
                alumniInvite = [NSString stringWithFormat:@"%@,%d",alumniInvite, user.userId];
        }
    }
    
    NSMutableArray *groupListArray = [NSMutableArray new];
    for (GroupEntity * groupEntity in discussionManager.myGroupsArray)
    {
        if ([groupEntity getSelected])
        {
            [groupListArray addObject:[NSString stringWithFormat:@"%d",groupEntity.tagId]];
        }
    }
    NSString * groupTagsString = [groupListArray componentsJoinedByString:@","];
    
    if (alumniInvite.length == 0 && groupTagsString.length == 0) {
        
        [[[UIAlertView alloc] initWithTitle:@"Jambo" message:@"Please select people or groups to invite for event." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
    else if(self.isInEditMode) {
    
        [processIndicator showProcessIndicatorInView:self.view Message:@"Processing..."];
        eventManager.delegate = self;
        [eventManager updateEvent:[NSString stringWithFormat:@"%d",_eventEntity.eventId] EventName:_eventEntity.eventName StartDate:_eventEntity.startDate EndDate:_eventEntity.endDate Duration:_eventEntity.fromTime Description:_eventEntity.eventDesc Address:_eventEntity.address CityState:_eventEntity.city IsPaid:_eventEntity.isPaid Amount:_eventEntity.charges GuestAllowed:_eventEntity.isGuestAllowed GuestCharger:_eventEntity.guestCharges GroupList:groupTagsString PeopleList:alumniInvite];
    }
    else {
        
        [processIndicator showProcessIndicatorInView:self.view Message:@"Processing..."];
        eventManager.delegate = self;
        [eventManager createEvent:_eventEntity GroupList:groupTagsString PeopleList:alumniInvite];
    }
}

- (IBAction)createGroupHandler:(id)sender
{
    NSString *alumniInvite = @"";
    for(UserEntity *user in  self.alumniArray)
    {
        if([user getSelected])
        {
            if (alumniInvite.length == 0)
                alumniInvite = [NSString stringWithFormat:@"%d",user.userId];
            else
                alumniInvite = [NSString stringWithFormat:@"%@,%d",alumniInvite, user.userId];
        }
    }
    
    if (alumniInvite.length == 0) {
        
        [[[UIAlertView alloc] initWithTitle:@"Jambo" message:@"Please select people to invite for event." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
    else if(self.isInEditMode) {
        
        [processIndicator showProcessIndicatorInView:self.view Message:@"Processing..."];
        discussionManager.delegate = self;
        if(inviteType == InviteTypeGroupMember)
            [discussionManager inviteTagMembers:self.groupId InviteList:alumniInvite];
        else
            [discussionManager updateTagAdminBulk:self.groupId InviteList:alumniInvite];
    }
    else {
        
        [processIndicator showProcessIndicatorInView:self.view Message:@"Processing..."];
        discussionManager.delegate = self;
        [discussionManager createGroup:self.groupName Description:self.groupDescription InviteList:alumniInvite];
    }
}

#pragma mark - Request Complete
-(void)requestCompleted:(id)response{
    
    [processIndicator hideProcessIndicator];
    
    if ([[response objectForKey:REQUEST_KEY] integerValue] == requestGetAluminiList)
    {
        [TableView reloadData];
    }
    else if ([[response objectForKey:REQUEST_KEY] integerValue] == requestCreateEvent && ![[response objectForKey:RESULT_KEY] boolValue])
    {
        [self clearAllSelected];
        [eventManager storeEventToCalendar:self.eventEntity];
        [userManager getUserDetail:userManager.userEntity];
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CREATED_UPDATED_EVENT object:nil userInfo:nil];
        [[[UIAlertView alloc] initWithTitle:@"Alumni" message:@"Event Created Successfully" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        
        BOOL wentBack = NO;
        for (UIViewController *controller in [self.navigationController viewControllers])
        {
            if ([controller isKindOfClass:[EventsScreen class]])
            {
                wentBack = YES;
                [self.navigationController popToViewController:controller animated:YES];
                break;
            }
        }
        
        if (!wentBack)
        {
            for (UIViewController *controller in [self.navigationController viewControllers])
            {
                if ([controller isKindOfClass:[HomeScreen class]])
                {
                    wentBack = YES;
                    [self.navigationController popToViewController:controller animated:YES];
                    break;
                }
            }
        }
    }
    else if ([[response objectForKey:REQUEST_KEY] integerValue] == requestUpdateEvent && ![[response objectForKey:RESULT_KEY] boolValue])
    {
        [self clearAllSelected];
        [eventManager storeEventToCalendar:self.eventEntity];
        [userManager getUserDetail:userManager.userEntity];
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_CREATED_UPDATED_EVENT object:nil userInfo:nil];
        [[[UIAlertView alloc] initWithTitle:@"Alumni" message:@"Event Updated Successfully" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        
        for (UIViewController *controller in [self.navigationController viewControllers])
        {
            if ([controller isKindOfClass:[EventsScreen class]])
            {
                [self.navigationController popToViewController:controller animated:YES];
                break;
            }
        }
    }
    else if ([[response objectForKey:REQUEST_KEY] integerValue] == requestCreateTag && ![[response objectForKey:RESULT_KEY] boolValue])
    {
        [self clearAllSelected];
        [discussionManager getGroupsList];
        [[[UIAlertView alloc] initWithTitle:@"Alumni" message:@"Group Created Successfully" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        
        for (UIViewController *controller in [self.navigationController viewControllers])
        {
            if ([controller isKindOfClass:[HomeScreen class]])
            {
                [self.navigationController popToViewController:controller animated:YES];
                break;
            }
        }
    }
    else if (([[response objectForKey:REQUEST_KEY] integerValue] == requestInviteTagMembers || [[response objectForKey:REQUEST_KEY] integerValue] == requestUpdateTagAdminBulk) && ![[response objectForKey:RESULT_KEY] boolValue])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        if ([[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_INTERNET_NOT_AVAILABLE_CODE)
        {
            [[[UIAlertView alloc] initWithTitle:@"Alumni" message:@"No internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
        else if([[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_SERVER_CONNECTION_ERROR || [[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_INTERNAL_SERVICE_ERROR){
            [[[UIAlertView alloc] initWithTitle:@"Alumni" message:@"Something went wrong" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
        else
            [[[UIAlertView alloc] initWithTitle:@"Alumni" message:[response objectForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}

- (void)clearAllSelected
{
    for(UserEntity *user in  self.alumniArray)
    {
        [user setSelected:NO];
    }
    
    NSMutableArray *groupListArray = [NSMutableArray new];
    for (GroupEntity * groupEntity in discussionManager.myGroupsArray)
    {
        [groupEntity setSelected:NO];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - TextField Delegate Method
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


@end

//
//  AppInvitation.m
//  Jambo
//
//  Created by Rahul Chandera on 28/09/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "AppInvitation.h"
#import "ProcessIndicator.h"
#import "UserManager.h"
#import "AppConstants.h"

ProcessIndicator *processIndicator;
UserManager *userManager;

@interface AppInvitation () <UserManagerDelegate>

@end

@implementation AppInvitation
{
    InviteCell * currentInvitingCell;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initializeObjects];
}

#pragma mark - Initializer methods
- (void)initializeObjects
{
    userManager = [UserManager sharedInstance];
    processIndicator = [ProcessIndicator sharedInstance];
    
    lblInvitesRemaining.text = [NSString stringWithFormat:@"You have %d invites remaining",userManager.totalInvites - (int)userManager.myReferredUsers.count];
}

#pragma mark - TableView Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return userManager.totalInvites;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"InviteCell";
    InviteCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        cell = [[InviteCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    cell.delegate = self;
    
    AluminiEntity * alumini = indexPath.row < userManager.myReferredUsers.count ? userManager.myReferredUsers[indexPath.row] : nil;
    
    [cell setInvitedWithEmailId:alumini.email];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

#pragma mark - Invite cell delegate
- (void)btnInviteClickedForCell:(InviteCell *)cell
{
    [self.view endEditing:YES];

    if (cell.txtEmailId.text.length == 0) {
        
        [[[UIAlertView alloc] initWithTitle:@"Invite" message:@"Email ID should not be empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
    else {
        currentInvitingCell = cell;
        [processIndicator showProcessIndicatorInView:self.view Message:@"Sending invitation..."];
        userManager.delegate = self;
        [userManager inviteSomeoneToAppWithEmailId:cell.txtEmailId.text];
    }
}


#pragma mark - User manager delegate
- (void)requestCompleted:(id)response
{
    [processIndicator hideProcessIndicator];
    
    if ([[response objectForKey:REQUEST_KEY] integerValue] == requestInviteToApp) {
        
        if (![[response objectForKey:RESULT_KEY] boolValue])
        {
            AluminiEntity * alumni = [AluminiEntity new];
            alumni.email = currentInvitingCell.txtEmailId.text;
            
            if (!userManager.myReferredUsers)
                userManager.myReferredUsers = [NSMutableArray new];

            [userManager.myReferredUsers addObject:alumni];
            
            [currentInvitingCell setInvitedWithEmailId:alumni.email];
            lblInvitesRemaining.text = [NSString stringWithFormat:@"You have %d invites remaining",userManager.totalInvites - (int)userManager.myReferredUsers.count];
            
            [userManager getMyReferredUser];
        }
        else
        {
            if ([[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_INTERNET_NOT_AVAILABLE_CODE)
            {
                [[[UIAlertView alloc] initWithTitle:@"Invite" message:@"No internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
            }
            else
            {
                [[[UIAlertView alloc] initWithTitle:@"Invite" message:[response objectForKey:@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
            }
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end

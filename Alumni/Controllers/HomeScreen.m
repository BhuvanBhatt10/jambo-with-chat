//
//  HomeScreen.m
//  Alumni
//
//  Created by Rahul Chandera on 13/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "HomeScreen.h"
#import "AppConstants.h"
#import "NavigationManager.h"
#import "ApplicationManager.h"
#import "KLCPopup.h"
#import "GroupEntity.h"
#import "QuestionEntity.h"
#import "QuestionListScreen.h"
#import "QuestionDetailScreen.h"
#import "UserManager.h"
#import "MessageManager.h"
#import "UIImageView+WebCache.h"
#import "GroupCell.h"
#import "UserProfileScreen.h"
#import "Jambo-Swift.h"
#import "AddQuestionScreen.h"
#import "InviteScreen.h"
#import "MessageGroupEntity.h"
#import "ChatViewController.h"
#import "ChatManager.h"
#import <Quickblox/QBRequest+QBUsers.h>
#import "QuickBloxHelper.h"
#import "UserdefaultManager.h"
#import "ProcessIndicator.h"


@interface HomeScreen () <UserManagerDelegate,ContactInfoPopupDelegate>

@end

#define NEARBY_COLLECTION 1
#define TOPIC_COLLECTION  2
#define NEARBY_CELL_IDENTIFIRE @"NearbyCell"
#define GROUP_CELL_IDENTIFIRE  @"GroupCell"
#define TOPIC_CELL_IDENTIFIRE  @"TopicCell"

NavigationManager *navigationManager;
ApplicationManager *applicationManager;
DiscussionManager *discussionManager;
UserManager *userManager;
MessageManager *messageManager;
ChatManager *chatManager;
KLCPopup *popup;
ProcessIndicator *processIndicator;
QuickBloxHelper *quickbloxSharedInstance;

NSArray *questionArray;
UILabel *lblNotificationCount;
UIRefreshControl *refreshControl;
int messageSenderId;

@implementation HomeScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initializeUI];
    [self initializeObjects];
    quickbloxSharedInstance = [QuickBloxHelper sharedInstance];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTimerHit:) name:NOTIFICATION_UPDATE_TIMER_HIT object:nil];
}

#pragma mark - Initialize UI
- (void)initializeUI {
    nearbyCollection.userInteractionEnabled = YES;
  
    self.navigationController.navigationBarHidden = NO;
    
    //Menu Button
    UIButton *btnMenu = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [btnMenu addTarget:self action:@selector(openMenu) forControlEvents:UIControlEventTouchUpInside];
    [btnMenu setImage:[UIImage imageNamed:@"menu.png"] forState:UIControlStateNormal];
    [btnMenu setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    UIBarButtonItem *bbiMenu = [[UIBarButtonItem alloc]initWithCustomView:btnMenu];
    bbiMenu.style = UIBarButtonItemStylePlain;
    self.navigationItem.leftBarButtonItem = bbiMenu;
    
    //Search Button
    UIButton *btnSearch = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [btnSearch addTarget:self action:@selector(openSearch) forControlEvents:UIControlEventTouchUpInside];
    [btnSearch setImage:[UIImage imageNamed:@"search_white.png"] forState:UIControlStateNormal];
    [btnSearch setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    UIBarButtonItem *bbiSearch = [[UIBarButtonItem alloc]initWithCustomView:btnSearch];
    bbiSearch.style = UIBarButtonItemStylePlain;
    
    //Notifications Button
    UIView* container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30) ];
    UIButton *btnNotification = [[UIButton alloc] initWithFrame:CGRectMake(0, 6, 24, 24) ];
    [btnNotification addTarget:self action:@selector(openNotifications) forControlEvents:UIControlEventTouchUpInside];
    [btnNotification setImage:[UIImage imageNamed:@"notifications.png"] forState:UIControlStateNormal];
    [btnNotification setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [container addSubview:btnNotification];
    lblNotificationCount = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 20, 20)];
    [lblNotificationCount setFont:[UIFont boldSystemFontOfSize:12]];
    lblNotificationCount.textColor = [UIColor whiteColor];
    lblNotificationCount.textAlignment = NSTextAlignmentCenter;
    lblNotificationCount.layer.cornerRadius = 8;
    lblNotificationCount.layer.masksToBounds = YES;
    lblNotificationCount.backgroundColor = [UIColor redColor];
    [container addSubview:lblNotificationCount];
    UIBarButtonItem* bbiNotifications = [[UIBarButtonItem alloc] initWithCustomView:container];
    
    self.navigationItem.rightBarButtonItems = @[bbiNotifications, bbiSearch];
}


#pragma mark - Initialize Objects
- (void)initializeObjects
{
    navigationManager = [NavigationManager sharedInstance];
    applicationManager = [ApplicationManager sharedInstance];
    discussionManager = [DiscussionManager sharedInstance];
    
    userManager = [UserManager sharedInstance];
    [userManager getMyReferredUser];

    messageManager = [MessageManager sharedInstance];
    chatManager = [ChatManager sharedInstance];
    [chatManager loginOrSignUpWithCurrentUser];
    
    // Register the table cell
    [TableView registerClass:[GroupCell class] forCellReuseIdentifier:@"GroupCell"];
    
    refreshControl = [[UIRefreshControl alloc]init];
    [TableView addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
}

- (void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:YES];
    self.title = @"Alumni";
    self.navigationController.navigationBar.topItem.title = @"Alumni";
    
    //Handle Push Notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NotificationReceived:) name:@"NotificationReceived" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTableData) name:NOTIFICATION_REFRESH_GROUP_DATA_EVENT object:nil];
    
    if(applicationManager.notificationsArray.count > 0){
    
        lblNotificationCount.hidden = NO;
        lblNotificationCount.text = [NSString stringWithFormat:@"%d",(int)applicationManager.notificationsArray.count];
    }
    else
        lblNotificationCount.hidden = YES;
    
    viewBtnContainer.hidden = YES;
    btnPlusButton.selected = NO;

    discussionManager.delegate = self;
    userManager.delegate = self;
    
    if ([discussionManager.allGroupsArray count] > 0) {
        [userManager getNearByList:@"distance"];
    }
    else {
        [discussionManager getGroupsList];
        [refreshControl beginRefreshing];
    }
    
    [self reloadTableData];
}

#pragma mark - Listen For notifications
- (void)updateTimerHit:(NSNotification *)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([discussionManager.allGroupsArray count] > 0) {
            
            [self performSelector:@selector(reloadCollectionData) withObject:nil afterDelay:3.0f];
            [userManager getNearByList:@"distance"];
        }
    });
}

#pragma mark - Refresh Table
- (void)refreshTable {
    
    discussionManager.delegate = self;
    [discussionManager getGroupsList];
}

- (void)reloadTableData {
    [TableView reloadData];
}

- (void)reloadCollectionData {
    [nearbyCollection reloadData];
}
#pragma mark - Request Complete
-(void)requestCompleted:(id)response{
    
    [refreshControl endRefreshing];
    
    RequestType requestType = [[response objectForKey:REQUEST_KEY] intValue];
    
    if (requestType == requestDiscussionList)
    {
        [TableView reloadData];
        userManager.delegate = self;
        [userManager getNearByList:@"distance"];
    }
    else if (requestType == requestNearByList)
    {
        [nearbyCollection reloadData];
    }
    else
    {
        if ([[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_INTERNET_NOT_AVAILABLE_CODE)
        {
            [[[UIAlertView alloc] initWithTitle:@"Alumni" message:@"No internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
        else if([[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_SERVER_CONNECTION_ERROR || [[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_INTERNAL_SERVICE_ERROR){
            [[[UIAlertView alloc] initWithTitle:@"Alumni" message:@"Something went wrong" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
    }
}


#pragma mark - TableView Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [discussionManager.allGroupsArray count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 200;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GroupCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GroupCell"];
    
    if(indexPath.row % 2 == 0)
        cell.contentView.backgroundColor = LIGHT_GRAY_COLOR;
    else
        cell.contentView.backgroundColor = [UIColor whiteColor];
    
    GroupEntity *groupEntity = [discussionManager.allGroupsArray objectAtIndex:indexPath.row];
    [cell setQuestionsData:groupEntity Delegate:self Index:(int)indexPath.row];
    
    return cell;
}

#pragma mark - QuestionList Delegate
-(void)selectedQuestion:(QuestionEntity*)questionEntity
{
    QuestionDetailScreen *questionDetailScreen = (QuestionDetailScreen *)[navigationManager getScreen:nsQuestionDetailScreen];
    [questionDetailScreen setQuestionData:questionEntity];
    [self.navigationController pushViewController:questionDetailScreen animated:YES];
}

-(void)addQuestionToGroup:(int)index
{
    GroupEntity *groupEntity = [discussionManager.allGroupsArray objectAtIndex:index];
    AddQuestionScreen *addQuestionScreen = (AddQuestionScreen*)[navigationManager getScreen:nsAddQuestionScreen];
    addQuestionScreen.groupEntity = groupEntity;
    [self.navigationController pushViewController:addQuestionScreen animated:YES];
}

-(void)inviteAlumniToGroup:(int)index
{
    GroupEntity *groupEntity = [discussionManager.allGroupsArray objectAtIndex:index];
    InviteScreen *inviteScreen = (InviteScreen *)[navigationManager getScreen:nsInviteScreen];
    inviteScreen.isInEditMode = YES;
    inviteScreen.groupId = [NSString stringWithFormat:@"%d",groupEntity.tagId];
    inviteScreen.inviteType = InviteTypeGroupMember;
    inviteScreen.alumniArray = userManager.alumniArray;
    [self.navigationController pushViewController:inviteScreen animated:YES];
}

-(void)viewAllQuestionOfGroup:(int)index
{
    GroupEntity *groupEntity = [discussionManager.allGroupsArray objectAtIndex:index];
    QuestionListScreen *questionListScreen = (QuestionListScreen *)[navigationManager getScreen:nsQuestionListScreen];
    questionListScreen.groupEntity = groupEntity;
    [self.navigationController pushViewController:questionListScreen animated:YES];
}

#pragma mark - NSNotification Methonds
- (void)NotificationReceived:(NSNotification *)notification
{
    //NotificationEntity *notificationEntity = [notification object];
    if(applicationManager.notificationsArray.count > 0)
    {
        lblNotificationCount.hidden = NO;
        lblNotificationCount.text = [NSString stringWithFormat:@"%d",(int)applicationManager.notificationsArray.count];
    }
    else
        lblNotificationCount.hidden = YES;
}


#pragma mark - CollectionView
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [userManager.nearByArray count];
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(72, 72);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
   return 0.0;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NEARBY_CELL_IDENTIFIRE forIndexPath:indexPath];
    
    UIImageView *imgPhoto = (UIImageView*)[cell.contentView viewWithTag:101];
    UILabel *lblName = (UILabel *)[cell.contentView viewWithTag:102];
    
    UserEntity *userEntity;
    if (indexPath.row < userManager.nearByArray.count) {
        userEntity = [userManager.nearByArray objectAtIndex:indexPath.row];
    }
    
    [imgPhoto sd_setImageWithURL:[NSURL URLWithString:userEntity.profilePic] placeholderImage:[UIImage imageNamed:DEFAULT_USER_IMAGE]];
    //FIXME: Bhuvan Changes
    lblName.text = userEntity.firstName;
    
    if ([applicationManager.Latitude floatValue] == 0 && [applicationManager.Longitude floatValue] == 0) {
        cell.userInteractionEnabled = NO;
        cell.alpha = 0.5;
        btnNearBy.enabled = NO;
    } else {
        cell.userInteractionEnabled = YES;
        cell.alpha = 1.0;
        btnNearBy.enabled = YES;
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    self.contactInfoPopup = (ContactInfoPopup*)[navigationManager getScreen:nsContactInfoPopup];
    self.contactInfoPopup.view.frame = CGRectMake(0, 0, 222, 322);
    [self.contactInfoPopup setUserData:[userManager.nearByArray objectAtIndex:indexPath.row]];
    self.contactInfoPopup.delegate = self;
    
    popup = [KLCPopup popupWithContentView:self.contactInfoPopup.view
                                            showType:KLCPopupShowTypeBounceInFromTop
                                         dismissType:KLCPopupDismissTypeBounceOutToBottom
                                            maskType:KLCPopupMaskTypeDimmed
                            dismissOnBackgroundTouch:YES
                               dismissOnContentTouch:NO];
    [popup show];
}




#pragma mark - ContactInfo Delegate
- (void)sendMessageTo:(UserEntity*)_user
{
    [popup dismiss:YES];
    
    messageSenderId = _user.userId;
    
    MessageGroupEntity *messageGroupEntity = [[MessageGroupEntity alloc] init];
    messageGroupEntity.userId = _user.userId;
    messageGroupEntity.firstName = _user.firstName;
    messageGroupEntity.lastName = _user.lastName;
    

    
    //Start Chat except owns profile
    if (![[UserdefaultManager getUserEmail] isEqualToString:_user.email]) {
        [processIndicator showProcessIndicatorInView:self.view Message:@"Loading"];
        
        //Find user in storage
        [quickbloxSharedInstance isUserExistInStorage:_user.email Oncompletion:^(QBUUser *opponentUser) {
            if (opponentUser != nil) {
                if (opponentUser.fullName.length == 0 || opponentUser.fullName == nil) {
                    opponentUser.fullName = opponentUser.email;
                }
                
                [ServicesManager.instance.chatService createPrivateChatDialogWithOpponent:opponentUser completion:^(QBResponse *response, QBChatDialog *createdDialog) {
                    if( !response.success  && createdDialog == nil ) {
                        [processIndicator hideProcessIndicator];
                    } else {
                        [quickbloxSharedInstance connectQBOnCompletion:^(QBUUser *currentUser) {
                            if (currentUser != nil) {
                                // Connected
                                [processIndicator hideProcessIndicator];
                                ChatViewController *chatVC = (ChatViewController *)[navigationManager getScreen:nsChatScreen];
                                chatVC.dialog = createdDialog;
                                [self.navigationController pushViewController:chatVC animated:YES];
                            } else {
                                [processIndicator hideProcessIndicator];
                                // NSAssert(1<0, @"Quickblox not connected to internet");
                            }
                        }];
                    }
                }];
            } else {
                [processIndicator hideProcessIndicator];
            }
        }];
        
        /*[quickbloxSharedInstance isUserExistInStorage:_user.email Oncompletion:^(QBUUser *opponentUser) {
         if(opponentUser != nil) {
         // QBUUser found
         [quickbloxSharedInstance createChatDialogueWithOpponent:opponentUser Oncompletion:^(QBChatDialog *dialogue) {
         if (dialogue != nil) {
         // Dialogue created
         NSLog(@"Dialogue Created");
         
         // Quickblox Connection
         [quickbloxSharedInstance connectQBOnCompletion:^(QBUUser *currentUser) {
         if (currentUser != nil) {
         // Connected
         [processIndicator hideProcessIndicator];
         ChatViewController *chatVC = (ChatViewController *)[navigationManager getScreen:nsChatScreen];
         chatVC.dialog = dialogue;
         [self.navigationController pushViewController:chatVC animated:YES];
         } else {
         [processIndicator hideProcessIndicator];
         NSAssert(1<0, @"Quickblox not connected to internet");
         }
         }];
         } else {
         // Failed to create dialouge
         [processIndicator hideProcessIndicator];
         NSLog(@"Failed to create dialogue");
         }
         }];
         } else {
         // QBUUser not found
         [processIndicator hideProcessIndicator];
         }
         }];*/
        
    }

}

- (void)viewUserProfile:(UserEntity*)user
{
    [popup dismiss:YES];
    
    UserProfileScreen *userProfileScreen = (UserProfileScreen*)[navigationManager getScreen:nsUserProfileScreen];
    userProfileScreen.userEntity = user;
    [self.navigationController pushViewController:userProfileScreen animated:YES];
}

#pragma mark - Navigation Buttons
- (void)openMenu
{
    [navigationManager openSideMenuFrom:self.navigationController];
}

- (void)openSearch
{
    [self.navigationController pushViewController:[navigationManager getScreen:nsSearchScreen] animated:YES];
}

- (void)openNotifications
{
    [self.navigationController pushViewController:[navigationManager getScreen:nsNotificationsScreen] animated:YES];
}


- (IBAction)createGroup:(id)sender
{
    [self.navigationController pushViewController:[navigationManager getScreen:nsCreateGroupScreen] animated:YES];
}

- (IBAction)createEvent:(id)sender
{
    [self.navigationController pushViewController:[navigationManager getScreen:nsEventCreateScreen] animated:YES];
}

- (IBAction)plusButtonHandler:(UIButton*)sender
{
    sender.selected =! sender.selected;
    
    if (sender.selected) {

        viewBtnContainer.hidden = NO;
        viewBtnContainer.alpha = 0.0;
//        btnCreateEvent.hidden = NO;
//        btnCreateGroup.hidden = NO;
//        btnCreateEvent.alpha = 0.0;
//        btnCreateGroup.alpha = 0.0;
        
        [UIView animateWithDuration:0.5 animations:^{
            
            btnPlusButton.transform = CGAffineTransformMakeRotation(0.80);
            viewBtnContainer.alpha = 1.0;
            //            btnCreateEvent.alpha = 1.0;
//            btnCreateGroup.alpha = 1.0;
        } completion:nil];
    }
    else {
    
        [UIView animateWithDuration:0.5 animations:^{
            
            btnPlusButton.transform = CGAffineTransformMakeRotation(0.0);
            viewBtnContainer.alpha = 0.0;
            //            btnCreateEvent.alpha = 0.0;
//            btnCreateGroup.alpha = 0.0;
        }
        completion:^(BOOL finished){

            viewBtnContainer.hidden = YES;
//            btnCreateEvent.hidden = YES;
//            btnCreateGroup.hidden = YES;
        }];
    }
}



- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
    btnPlusButton.transform = CGAffineTransformMakeRotation(0.0);
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

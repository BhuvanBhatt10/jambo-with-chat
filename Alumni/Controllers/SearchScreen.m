//
//  SearchScreen.m
//  Jambo
//
//  Created by Rahul Chandera on 02/05/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "SearchScreen.h"
#import "UserManager.h"
#import "ProcessIndicator.h"
//#import "GiFHUD.h"
#import "AppConstants.h"
#import "UIImageView+WebCache.h"
#import "FilterScreen.h"
#import "UserProfileScreen.h"
#import "NavigationManager.h"

@interface SearchScreen () <UserManagerDelegate, FilterScreenDelegate, UITextFieldDelegate>

@end

UserManager *userManager;
ProcessIndicator *processIndicator;
NavigationManager *navigationManager;
UITextField *txtSearch;
NSMutableArray *alumniArray;

@implementation SearchScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeUI];
    [self initializeObjects];
}


#pragma mark - Initialize UI
- (void)initializeUI {
    
    //Menu Button
    UIButton *btnClear = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [btnClear addTarget:self action:@selector(clearSearch) forControlEvents:UIControlEventTouchUpInside];
    [btnClear setImage:[UIImage imageNamed:@"cancel_white.png"] forState:UIControlStateNormal];
    [btnClear setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    UIBarButtonItem *bbiClear = [[UIBarButtonItem alloc]initWithCustomView:btnClear];
    bbiClear.style = UIBarButtonItemStylePlain;
    self.navigationItem.rightBarButtonItem = bbiClear;
    
    UIView *titleView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 170, 30)];
    titleView.backgroundColor = [UIColor clearColor];
    txtSearch = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, 170, 25)];
    txtSearch.placeholder = @"Search for alumni";
    txtSearch.textColor = [UIColor whiteColor];
    txtSearch.delegate = self;
    txtSearch.returnKeyType  = UIReturnKeySearch;
    [titleView addSubview:txtSearch];
    UILabel *lblUnderLine = [[UILabel alloc]initWithFrame:CGRectMake(0, 28, 170, 1)];
    lblUnderLine.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.2];
    [titleView addSubview:lblUnderLine];
   
    self.navigationItem.titleView = titleView;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

#pragma mark - Initialize Objects
- (void)initializeObjects
{
    processIndicator = [ProcessIndicator sharedInstance];
    userManager = [UserManager sharedInstance];
    navigationManager = [NavigationManager sharedInstance];
    alumniArray = [[NSMutableArray alloc]init];
    
    [txtSearch becomeFirstResponder];
}

- (void)clearSearch
{
    txtSearch.text = @"";
    [txtSearch resignFirstResponder];
    
    [alumniArray removeAllObjects];
    [TableView reloadData];
}

#pragma mark - TextField Delegate Method
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if (textField.text.length > 1) {
        
        [processIndicator showProcessIndicatorInView:self.view Message:@"Searching..."];
        userManager.delegate = self;
        [userManager getAlumniList:textField.text Filters:@""];
    }
    
    return YES;
}

#pragma mark - Notifications listerners
- (void)keyboardDidShow:(NSNotification *)notification {
    
    // calculate the size of the keyboard and how much is and isn't covering the tableview
    NSDictionary *keyboardInfo = [notification userInfo];
    CGRect keyboardFrame = [keyboardInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    btnFilterBottom.constant = keyboardFrame.size.height + 10;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillHide:(NSNotification *)note
{
    btnFilterBottom.constant = 10;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}


#pragma mark - Request Complete
-(void)requestCompleted:(id)response{
    
    [processIndicator hideProcessIndicator];
//    [GiFHUD dismiss];
    
    if([response objectForKey:DATA_KEY])
    {
        alumniArray = [response objectForKey:DATA_KEY];
        [TableView reloadData];
    }
}

#pragma mark - TableView Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return alumniArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"AlumniCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    UIImageView *imgPhoto = (UIImageView *)[cell.contentView viewWithTag:101];
    UILabel *lblName = (UILabel *)[cell.contentView viewWithTag:102];
    
    UserEntity *userEntity = [userManager.alumniArray objectAtIndex:indexPath.row];
    
    [imgPhoto setImageWithURL:[NSURL URLWithString:userEntity.profilePic] placeholderImage:[UIImage imageNamed:DEFAULT_USER_IMAGE]];
    lblName.text = [NSString stringWithFormat:@"%@ %@",userEntity.firstName, userEntity.lastName];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserProfileScreen *userProfileScreen = (UserProfileScreen*)[navigationManager getScreen:nsUserProfileScreen];
    userProfileScreen.userEntity = [userManager.alumniArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:userProfileScreen animated:YES];
}



//Filter Screen Delegate
-(void)applyFilters:(NSString*)filters
{
    [processIndicator showProcessIndicatorInView:self.view Message:@"Searching..."];
//    [GiFHUD showWithOverlay];
    userManager.delegate = self;
    [userManager getAlumniList:txtSearch.text Filters:filters];
}


#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    FilterScreen *filterScreen = (FilterScreen *)[segue destinationViewController];
    filterScreen.delegate = self;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end

//
//  ForgotPasswordScreen.h
//  Jambo
//
//  Created by Rahul Chandera on 14/05/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordScreen : UIViewController
{
    IBOutlet UITextField *txtEmail;
}
- (IBAction)forgotPassword:(id)sender;
- (IBAction)backTapHandler:(id)sender;
@end

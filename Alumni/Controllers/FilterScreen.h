//
//  FilterScreen.h
//  Jambo
//
//  Created by Rahul Chandera on 02/05/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FilterScreenDelegate
-(void)applyFilters:(NSString*)filters;
@end

@interface FilterScreen : UIViewController
{
    IBOutlet UICollectionView *CollectionView;
    IBOutlet UITableView *TableView;
}

@property (strong,nonatomic) id <FilterScreenDelegate> delegate;

- (IBAction)applyFiltersHandler:(id)sender;

@end

//
//  EventsScreen.h
//  Alumni
//
//  Created by Rahul Chandera on 31/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventManager.h"
#import "EventEntity.h"
#import "UserManager.h"

@interface EventsScreen : UIViewController <EventManagerDelegate,UserManagerDelegate>
{
    IBOutlet UIButton *btnAll;
    IBOutlet UIButton *btnMyEvents;
    IBOutlet UIButton *btnInvited;
    IBOutlet UICollectionView *CollectionView;
    IBOutlet UIView *blankScreen;
}

- (IBAction)tabHandler:(UIButton*)sender;
- (IBAction)btnInviteEditClicked:(UIButton *)sender;
- (IBAction)btnAvailabilityClick:(UIButton *)sender;
- (IBAction)btnCopyEventClicked:(UIButton *)sender;

@end

//
//  AddQuestionScreen.m
//  Jambo
//
//  Created by Rahul Chandera on 27/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "AddQuestionScreen.h"
#import "AppConstants.h"
#import "DiscussionManager.h"
//#import "GiFHUD.h"
#import "ProcessIndicator.h"

@interface AddQuestionScreen () <DiscussionManagerDelegate>

@end

DiscussionManager *discussionManager;
ProcessIndicator *processIndicator;

@implementation AddQuestionScreen
@synthesize groupEntity;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeUI];
    [self initializeObjects];
}


#pragma mark - Initialize UI
- (void)initializeUI {
    
    self.navigationController.navigationBarHidden = NO;
    
    //Menu Button
    UIButton *btnPost = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 70, 44)];
    [btnPost addTarget:self action:@selector(postAnswer) forControlEvents:UIControlEventTouchUpInside];
    [btnPost setBackgroundColor:LIGHT_RED_COLOR];
    [btnPost setTitle:@"POST" forState:UIControlStateNormal];
    btnPost.titleLabel.textColor = [UIColor whiteColor];
    btnPost.titleLabel.font = [UIFont fontWithName:@"Roboto-Bold" size:14.0];
    [btnPost setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    UIBarButtonItem *bbiPost = [[UIBarButtonItem alloc]initWithCustomView:btnPost];
    bbiPost.style = UIBarButtonItemStylePlain;
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    spacer.width = -20;
    self.navigationItem.rightBarButtonItems = @[spacer, bbiPost];
    
    //Set up grouwing textview
    tvQuestion.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    tvQuestion.minNumberOfLines = 1;
    tvQuestion.maxNumberOfLines = 5;
    tvQuestion.returnKeyType = UIReturnKeyDone;
    tvQuestion.font = [UIFont fontWithName:@"Roboto-Regular" size:18.0f];
    tvQuestion.delegate = self;
    tvQuestion.placeholder = @"What're you thinking about...";
    tvQuestion.placeholderColor = [UIColor colorWithWhite:0.600 alpha:0.500];
}

#pragma mark - Growing textview delegates
-(void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    [self.view layoutIfNeeded];
    
    tvQuestionHeight.constant = height;
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (BOOL)growingTextViewShouldReturn:(HPGrowingTextView *)growingTextView;
{
    [growingTextView resignFirstResponder];
    return YES;
}

#pragma mark - Initialize Objects
- (void)initializeObjects
{
    discussionManager = [DiscussionManager sharedInstance];
    processIndicator = [ProcessIndicator sharedInstance];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [tvQuestion becomeFirstResponder];
}

- (void)postAnswer
{
    if (tvQuestion.text.length>0) {
        
        [tvQuestion resignFirstResponder];
        [processIndicator showProcessIndicatorInView:self.view Message:@"Processing..."];
        discussionManager.delegate = self;
        [discussionManager postQuestionToGroup:self.groupEntity.tagId Question:tvQuestion.text];
    }
}


#pragma mark - Request Complete
-(void)requestCompleted:(id)response{

    [processIndicator hideProcessIndicator];
    
    if (![[response objectForKey:RESULT_KEY] boolValue])
    {
        discussionManager.delegate = nil;
        [discussionManager getGroupsList];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        if ([[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_INTERNET_NOT_AVAILABLE_CODE)
        {
            [[[UIAlertView alloc] initWithTitle:@"Alumni" message:@"No internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
        else if([[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_SERVER_CONNECTION_ERROR || [[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_INTERNAL_SERVICE_ERROR){
            [[[UIAlertView alloc] initWithTitle:@"Alumni" message:@"Something went wrong" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
        else
            [[[UIAlertView alloc] initWithTitle:@"Alumni" message:[response objectForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end

//
//  NotificationsScreen.m
//  Jambo
//
//  Created by Rahul Chandera on 25/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "NotificationsScreen.h"
#import "NavigationManager.h"
#import "ApplicationManager.h"
#import "AppConstants.h"
#import "NotificationEntity.h"
#import "UIImageView+WebCache.h"

@interface NotificationsScreen ()

@end

NavigationManager *navigationManager;
ApplicationManager *applicationManager;


@implementation NotificationsScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeUI];
    [self initializeObjects];
    
    //Handle Push Notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NotificationReceived:) name:@"NotificationReceived" object:nil];
}


#pragma mark - Initialize UI
- (void)initializeUI {
    
    self.title = @"Notifications";
}

#pragma mark - Initialize Objects
- (void)initializeObjects
{
    navigationManager = [NavigationManager sharedInstance];
    applicationManager = [ApplicationManager sharedInstance];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [TableView reloadData];
}


#pragma mark - TableView Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [applicationManager.notificationsArray count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"NotificationCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    NotificationEntity *notificationEntity = [applicationManager.notificationsArray objectAtIndex:indexPath.row];
    
    UIImageView *imgPhoto = (UIImageView *)[cell.contentView viewWithTag:101];
    UILabel *lblDetail = (UILabel *)[cell.contentView viewWithTag:102];
    //UILabel *lblTime = (UILabel *)[cell.contentView viewWithTag:103];
    
    if (notificationEntity.message) {
        
        lblDetail.text = [NSString stringWithFormat:@"%@ %@ message you \"%@\"",notificationEntity.fromAlumini_firstName,notificationEntity.fromAlumini_lastName,notificationEntity.message];
        
    }
    else if (notificationEntity.question) {
        
        lblDetail.text = [NSString stringWithFormat:@"%@ %@ posted in %@ \"%@\"",notificationEntity.firstName,notificationEntity.lastName,notificationEntity.tag,notificationEntity.question];
        [imgPhoto setImageWithURL:[NSURL URLWithString:notificationEntity.profilePic] placeholderImage:[UIImage imageNamed:DEFAULT_USER_IMAGE]];
    }
    else {
        
        
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
}




- (void)NotificationReceived:(NSNotification *)notification
{
    [TableView reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

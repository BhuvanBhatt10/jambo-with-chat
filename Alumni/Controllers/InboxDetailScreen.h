//
//  InboxDetailScreen.h
//  Alumni
//
//  Created by Rahul Chandera on 24/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HPGrowingTextView.h"
#import "MNMPullToRefreshManager.h"
#import "MessageGroupEntity.h"

@class QBPopupMenu;

@interface InboxDetailScreen : UIViewController<HPGrowingTextViewDelegate,UIGestureRecognizerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,MNMPullToRefreshManagerClient>
{
    NSMutableArray *sphBubbledata;
    UIView *containerView;
    HPGrowingTextView *textView;
    int selectedRow;
    BOOL newMedia;
}
@property (nonatomic, readwrite, assign) NSUInteger reloads;
@property (nonatomic, readwrite, strong) MNMPullToRefreshManager *pullToRefreshManager;

@property (weak, nonatomic) IBOutlet UIImageView *Uploadedimage;
@property (nonatomic, strong) QBPopupMenu *popupMenu;
@property (weak, nonatomic) IBOutlet UITableView *sphChatTable;
@property (nonatomic, retain) UIImagePickerController *imgPicker;
@property (nonatomic, strong) MessageGroupEntity *messageGroupEntity;

- (IBAction)endViewedit:(id)sender;

- (void) handleURL:(NSURL *)url;

@end

//
//  UserProfileScreen.m
//  Jambo
//
//  Created by Rahul Chandera on 08/05/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "UserProfileScreen.h"
#import "AppConstants.h"
#import "UIImageView+WebCache.h"
#import "Jambo-Swift.h"
#import "MessageManager.h"
#import "GroupEntity.h"
#import "DiscussionManager.h"
#import "NavigationManager.h"
#import "MessageGroupEntity.h"
#import "InboxDetailScreen.h"
#import "ChatManager.h"
#import <Quickblox/QBRequest+QBUsers.h>
#import "ChatViewController.h"
#import "UserManager.h"

@interface UserProfileScreen ()

@end

MessageManager *messageManager;
DiscussionManager *discussionManager;
NavigationManager *navigationManager;
ChatManager *chatManager;
UserManager *userManager;

int messageSenderId;

@implementation UserProfileScreen
@synthesize userEntity;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeObjects];
}

#pragma mark - Initialize Objects
- (void)initializeObjects
{
    messageManager = [MessageManager sharedInstance];
    discussionManager = [DiscussionManager sharedInstance];
    navigationManager = [NavigationManager sharedInstance];
    chatManager = [ChatManager sharedInstance];
    userManager = [UserManager sharedInstance];
    
    [self.imgUserPhoto sd_setImageWithURL:[NSURL URLWithString:userEntity.profilePic] placeholderImage:[UIImage imageNamed:DEFAULT_USER_IMAGE]];
    self.lblUserName.text = [NSString stringWithFormat:@"%@ %@",userEntity.firstName, userEntity.lastName];
    self.lblBusiness.text = [NSString stringWithFormat:@"%@, %@",userEntity.designation, userEntity.city];
    self.lblUserCode.text = userEntity.occupation;
    self.lblMobileNo.text = userEntity.contact1;
    
    self.imgUserPhoto.layer.cornerRadius = 50;
    self.imgUserPhoto.layer.masksToBounds = YES;
    
    NSMutableArray *tags = [[NSMutableArray alloc]init];
    for(MyGroupsEntity *myGroupsEntity in userEntity.tags)
    {
        GroupEntity *groupEntity = [discussionManager getTagEntity:myGroupsEntity.tagId];
        
        if (groupEntity.tag)
            [tags addObject:groupEntity.tag];
    }
    [self.tagView setTags:tags];
    self.tagView.txtSearch.hidden = YES;
    
    if (userEntity.userId == userManager.userEntity.userId) {
        self.btnEditProfile.hidden = NO;
    }
    else
        self.btnEditProfile.hidden = YES;
}


- (IBAction)messageTapHandler:(id)sender {
    
    messageSenderId = userEntity.userId;
    
    MessageGroupEntity *messageGroupEntity = [[MessageGroupEntity alloc] init];
    messageGroupEntity.userId = userEntity.userId;
    messageGroupEntity.firstName = userEntity.firstName;
    messageGroupEntity.lastName = userEntity.lastName;
    
    //    InboxDetailScreen *inboxDetailScreen = (InboxDetailScreen *)[navigationManager getScreen:nsInboxDetailScreen];
    //    inboxDetailScreen.messageGroupEntity = messageGroupEntity;
    //    [self.navigationController pushViewController:inboxDetailScreen animated:YES];
    
    [QBRequest userWithLogin:userEntity.email successBlock:^(QBResponse * _Nonnull response, QBUUser * _Nullable user) {
        
        [chatManager.serviceManager.chatService createPrivateChatDialogWithOpponent:user completion:^(QBResponse *response, QBChatDialog *createdDialog) {
            if( !response.success  && createdDialog == nil ) {
                NSLog(@"Failed to create dialog!!");
            }
            else {
                //Change dialog name
                NSString * userName = [NSString stringWithFormat:@"%@ %@",userEntity.firstName,userEntity.lastName];
                [chatManager.serviceManager.chatService changeDialogName:userName forChatDialog:createdDialog completion:^(QBResponse *response, QBChatDialog *updatedDialog) {
                    NSLog(@"Dialog name changed!!");
                    
                    if (userEntity.profilePic) {
                        [chatManager.serviceManager.chatService changeDialogAvatar:userEntity.profilePic forChatDialog:createdDialog completion:^(QBResponse *response, QBChatDialog *updatedDialog) {
                            NSLog(@"Dialog avatar changed!!");
                        }];
                    }
                }];
                
                ChatViewController *chatVC = (ChatViewController *)[navigationManager getScreen:nsChatScreen];
                chatVC.dialog = createdDialog;
                [self.navigationController pushViewController:chatVC animated:YES];
            }
        }];
        
    } errorBlock:^(QBResponse * _Nonnull response) {
        NSLog(@"Failed to get user!!");
    }];
}

- (IBAction)callTapHandler:(id)sender {
    
    if ([userEntity.contact1 length]>0) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",userEntity.contact1]]];
    }
    else
        [[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Phone number not available." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (IBAction)btnEditProfileClicked:(UIButton *)sender {
  
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

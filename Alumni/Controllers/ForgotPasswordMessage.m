//
//  ForgotPasswordMessage.m
//  Jambo
//
//  Created by Rahul Chandera on 16/05/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "ForgotPasswordMessage.h"

NSString *emailId;

@interface ForgotPasswordMessage ()

@end

@implementation ForgotPasswordMessage

- (void)viewDidLoad {
    [super viewDidLoad];
    
    lblEmail.text = emailId;
}


- (void)setEmailId:(NSString*)email
{
    emailId = email;
}

- (IBAction)backTapHandler:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

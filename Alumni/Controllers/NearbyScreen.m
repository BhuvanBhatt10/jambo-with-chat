//
//  NearbyScreen.m
//  Alumni
//
//  Created by Rahul Chandera on 16/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "NearbyScreen.h"
#import "NavigationManager.h"
#import "ProcessIndicator.h"
#import "UserProfileScreen.h"
#import "AppConstants.h"
#import "UserEntity.h"
#import "UIImageView+WebCache.h"
#import "Utility.h"
#import "ApplicationManager.h"
#import "KLCPopup.h"
#import "ChatViewController.h"
#import "ChatManager.h"
#import <Quickblox/QBRequest+QBUsers.h>
#import "QuickBloxHelper.h"
#import "UserdefaultManager.h"

@interface NearbyScreen ()

@end

NavigationManager *navigationManager;
ProcessIndicator *processIndicator;
UserManager *userManager;
ApplicationManager *applicationManager;
ChatManager *chatManager;
QuickBloxHelper *quickbloxSharedInstance;

@implementation NearbyScreen
{
    KLCPopup *popup;
    
}
@synthesize contactInfoPopup;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeObjects];
    [self initializeUI];
    quickbloxSharedInstance = [QuickBloxHelper sharedInstance];
}

#pragma mark - Initialize Objects
- (void)initializeObjects
{
    navigationManager = [NavigationManager sharedInstance];
    userManager = [UserManager sharedInstance];
    applicationManager = [ApplicationManager sharedInstance];
    processIndicator = [ProcessIndicator sharedInstance];
    chatManager = [ChatManager sharedInstance];
    
    btnByDistance.selected = YES;
    btnByLastUpdate.selected = NO;
}

#pragma mark - Initialize UI
- (void)initializeUI {
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.topItem.title = @"";
}


- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    self.title = @"Nearby";
}

#pragma mark - Request Complete
-(void)requestCompleted:(id)response{
    
    [processIndicator hideProcessIndicator];
    //[GiFHUD dismiss];
    
    if ([userManager.nearByArray count]>0)
    {
        [TableView reloadData];
    }
    else
    {
        if ([[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_INTERNET_NOT_AVAILABLE_CODE)
        {
            [[[UIAlertView alloc] initWithTitle:@"Alumni" message:@"No internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
        else if([[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_SERVER_CONNECTION_ERROR || [[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_INTERNAL_SERVICE_ERROR){
            [[[UIAlertView alloc] initWithTitle:@"Alumni" message:@"Something went wrong" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
        else
            [[[UIAlertView alloc] initWithTitle:@"Alumni" message:[response objectForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}



#pragma mark - TableView Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [userManager.nearByArray count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"NearbyCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    UIImageView *imgPhoto = (UIImageView *)[cell.contentView viewWithTag:101];
    UILabel *lblName = (UILabel *)[cell.contentView viewWithTag:102];
    UILabel *lblDistance = (UILabel *)[cell.contentView viewWithTag:103];
    UILabel *lblTime = (UILabel *)[cell.contentView viewWithTag:104];
    
    UserEntity *userEntity = [userManager.nearByArray objectAtIndex:indexPath.row];
    
    [imgPhoto sd_setImageWithURL:[NSURL URLWithString:userEntity.profilePic] placeholderImage:[UIImage imageNamed:DEFAULT_USER_IMAGE]];
    lblName.text = [NSString stringWithFormat:@"%@ %@",userEntity.firstName, userEntity.lastName];
    lblDistance.text = [Utility distanceFromLat:applicationManager.Latitude FromLong:applicationManager.Longitude ToLat:userEntity.latitude ToLong:userEntity.longitude];
    lblTime.text = [Utility daysDiffrentFromDateString:userEntity.lastUpdated];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.contactInfoPopup = (ContactInfoPopup*)[navigationManager getScreen:nsContactInfoPopup];
    self.contactInfoPopup.view.frame = CGRectMake(0, 0, 222, 322);
    [self.contactInfoPopup setUserData:[userManager.nearByArray objectAtIndex:indexPath.row]];
    self.contactInfoPopup.delegate = self;
    
    popup = [KLCPopup popupWithContentView:self.contactInfoPopup.view
                                            showType:KLCPopupShowTypeBounceInFromTop
                                         dismissType:KLCPopupDismissTypeBounceOutToBottom
                                            maskType:KLCPopupMaskTypeDimmed
                            dismissOnBackgroundTouch:YES
                               dismissOnContentTouch:NO];
    [popup show];
}

#pragma mark - ContactInfo Delegate
- (void)sendMessageTo:(UserEntity*)_user
{
    [popup dismiss:YES];
    
    //Start Chat

    //Start Chat except owns profile
    if (![[UserdefaultManager getUserEmail] isEqualToString:_user.email]) {
        [processIndicator showProcessIndicatorInView:self.view Message:@"Loading"];
        
        //Find user in storage
        [quickbloxSharedInstance isUserExistInStorage:_user.email Oncompletion:^(QBUUser *opponentUser) {
            if (opponentUser != nil) {
                if (opponentUser.fullName.length == 0 || opponentUser.fullName == nil) {
                    opponentUser.fullName = opponentUser.email;
                }
                
                [ServicesManager.instance.chatService createPrivateChatDialogWithOpponent:opponentUser completion:^(QBResponse *response, QBChatDialog *createdDialog) {
                    if( !response.success  && createdDialog == nil ) {
                        [processIndicator hideProcessIndicator];
                    } else {
                        [processIndicator hideProcessIndicator];
                        [quickbloxSharedInstance connectQBOnCompletion:^(QBUUser *currentUser) {
                            if (currentUser != nil) {
                                // Connected
                                [processIndicator hideProcessIndicator];
                                ChatViewController *chatVC = (ChatViewController *)[navigationManager getScreen:nsChatScreen];
                                chatVC.dialog = createdDialog;
                                [self.navigationController pushViewController:chatVC animated:YES];
                            } else {
                                [processIndicator hideProcessIndicator];
                                // NSAssert(1<0, @"Quickblox not connected to internet");
                            }
                        }];
                    }
                }];
            } else {
                [processIndicator hideProcessIndicator];
            }
        }];
        
        /*[quickbloxSharedInstance isUserExistInStorage:_user.email Oncompletion:^(QBUUser *opponentUser) {
         if(opponentUser != nil) {
         // QBUUser found
         [quickbloxSharedInstance createChatDialogueWithOpponent:opponentUser Oncompletion:^(QBChatDialog *dialogue) {
         if (dialogue != nil) {
         // Dialogue created
         NSLog(@"Dialogue Created");
         
         // Quickblox Connection
         [quickbloxSharedInstance connectQBOnCompletion:^(QBUUser *currentUser) {
         if (currentUser != nil) {
         // Connected
         [processIndicator hideProcessIndicator];
         ChatViewController *chatVC = (ChatViewController *)[navigationManager getScreen:nsChatScreen];
         chatVC.dialog = dialogue;
         [self.navigationController pushViewController:chatVC animated:YES];
         } else {
         [processIndicator hideProcessIndicator];
         NSAssert(1<0, @"Quickblox not connected to internet");
         }
         }];
         } else {
         // Failed to create dialouge
         [processIndicator hideProcessIndicator];
         NSLog(@"Failed to create dialogue");
         }
         }];
         } else {
         // QBUUser not found
         [processIndicator hideProcessIndicator];
         }
         }];*/
        
    }
}

- (void)viewUserProfile:(UserEntity*)user
{
    [popup dismiss:YES];
    
    UserProfileScreen *userProfileScreen = (UserProfileScreen*)[navigationManager getScreen:nsUserProfileScreen];
    userProfileScreen.userEntity = user;
    [self.navigationController pushViewController:userProfileScreen animated:YES];
}


- (IBAction)shortingHandler:(UIButton*)sender {

    NSString *type = @"distance";
    
    if (sender.tag == 1) {
        
        btnByDistance.selected = YES;
        btnByLastUpdate.selected = NO;
    }
    else {
    
        btnByDistance.selected = NO;
        btnByLastUpdate.selected = YES;
        type = @"date";
    }
    
    [processIndicator showProcessIndicatorInView:self.view Message:@"Processing..."];
    //[GiFHUD showWithOverlay];
    userManager.delegate = self;
    [userManager getNearByList:type];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

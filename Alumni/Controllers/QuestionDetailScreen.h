//
//  QuestionDetailScreen.h
//  Alumni
//
//  Created by Rahul Chandera on 22/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuestionEntity.h"
#import "ContactInfoPopup.h"

@interface QuestionDetailScreen : UIViewController <ContactInfoPopupDelegate>
{
    IBOutlet UITableView *TableView;
    IBOutlet UIButton *btnExpandCollapse;
    IBOutlet NSLayoutConstraint *containerHeight;
}
- (void)setQuestionData:(QuestionEntity *)quesEntity;

@property(nonatomic,strong) ContactInfoPopup *contactInfoPopup;

@property (weak, nonatomic) IBOutlet UILabel *lblTopicTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgUserPhoto;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblPostTime;
@property (weak, nonatomic) IBOutlet UIImageView *imgTopicType;
@property (weak, nonatomic) IBOutlet UIButton *btnTopicLikes;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalComments;


- (IBAction)btnUpvoteQuestionClicked:(UIButton *)sender;
- (IBAction)btnUpvoteAnswerClicked:(UIButton *)sender;
- (IBAction)btnExpandCollapseClick:(UIButton *)sender;

@end

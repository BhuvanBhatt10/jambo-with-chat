//
//  GroupsListScreen.h
//  Jambo
//
//  Created by Rahul Chandera on 28/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DiscussionManager.h"

@interface GroupsListScreen : UIViewController <DiscussionManagerDelegate>
{
    IBOutlet UITableView *TableView;
    IBOutlet UIButton *tabAll;
    IBOutlet UIButton *tabScubscribed;
    IBOutlet UIButton *tabInvited;
    IBOutlet UITextField *txtSearch;
}
- (IBAction)tabHandler:(UIButton*)sender;
- (IBAction)createGroup:(id)sender;

@end

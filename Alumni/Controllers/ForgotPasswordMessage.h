//
//  ForgotPasswordMessage.h
//  Jambo
//
//  Created by Rahul Chandera on 16/05/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordMessage : UIViewController
{
    IBOutlet UILabel *lblEmail;
}
- (void)setEmailId:(NSString*)email;
- (IBAction)backTapHandler:(id)sender;

@end

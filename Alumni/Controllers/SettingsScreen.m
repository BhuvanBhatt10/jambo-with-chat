//
//  SettingsScreen.m
//  Jambo
//
//  Created by Rahul Chandera on 25/05/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "SettingsScreen.h"
#import "UserManager.h"

@interface SettingsScreen ()

@end

UserManager *userManager;

BOOL AllowCall;
BOOL AllowLocation;
BOOL AllowNotifications;

@implementation SettingsScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Settings";
    
    userManager = [UserManager sharedInstance];
    NSArray *settings = [userManager.userEntity.privacyLevel componentsSeparatedByString:@","];
    
    if(settings.count>0)
        AllowCall = [settings[0] boolValue];
    if(settings.count>1)
        AllowLocation = [settings[1] boolValue];
    if(settings.count>2)
        AllowNotifications = [settings[2] boolValue];
}


#pragma mark - TableView Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 25.0;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString *CellIdentifier = @"SectionHeader";
    UITableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    UILabel *lblTitle = (UILabel *)[headerView.contentView viewWithTag:101];
    switch (section) {
        case 0:
            lblTitle.text= @"PRIVACY";
            break;
        case 1:
            lblTitle.text= @"LOCATION";
            break;
        case 2:
            lblTitle.text= @"NOTIFICATIONS";
            break;
        default:
            break;
    }
    
    return headerView;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SettingsCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    UILabel *lblTitle = (UILabel *)[cell.contentView viewWithTag:101];
    UILabel *lblDetails = (UILabel *)[cell.contentView viewWithTag:102];
    UIButton *btnToggle = (UIButton *)[cell.contentView viewWithTag:103];
    
    switch (indexPath.section) {
        case 0:
        {
            lblTitle.text = @"Allow to call";
            lblDetails.text = @"Helps people to get in touch with you";
            btnToggle.selected = AllowCall;
        }
            break;
        case 1:
        {
            lblTitle.text = @"Use location at all times";
            lblDetails.text = @"Helps connect with nearby alumni";
            btnToggle.selected = AllowLocation;
        }
            break;
        case 2:
        {
            lblTitle.text = @"Receive Notifications";
            lblDetails.text = @"Messages, New Discution, Answers";
            btnToggle.selected = AllowNotifications;
        }
            break;
            
        default:
            break;
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
            AllowCall =! AllowCall;
            break;
        case 1:
            AllowLocation =! AllowLocation;
            break;
        case 2:
            AllowNotifications =! AllowNotifications;
            break;
        default:
            break;
    }
    [tableView reloadData];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    userManager.userEntity.privacyLevel = [NSString stringWithFormat:@"%d,%d,%d",AllowCall,AllowLocation,AllowNotifications];
    [userManager updateUser];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

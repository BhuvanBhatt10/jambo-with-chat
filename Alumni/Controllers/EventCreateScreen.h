//
//  EventCreateScreen.h
//  Alumni
//
//  Created by Rahul Chandera on 05/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventEntity.h"

@interface EventCreateScreen : UIViewController <UITextViewDelegate>
{
    IBOutlet UITableView * TableView;
}
@property(nonatomic,strong) EventEntity *eventEntity;

@property (weak, nonatomic) IBOutlet UIButton *btnCreateEvent;
@property(assign, nonatomic) BOOL isInEditMode;
@property (nonatomic, assign) BOOL isBeingCopied;

/*
@property (weak, nonatomic) IBOutlet UITextField *txtEventName;
@property (weak, nonatomic) IBOutlet UITextField *txtStartDate;
@property (weak, nonatomic) IBOutlet UITextField *txtEndDate;
@property (weak, nonatomic) IBOutlet UITextField *txtFromTime;
@property (weak, nonatomic) IBOutlet UITextField *txtToTime;
@property (weak, nonatomic) IBOutlet UITextField *txtAbout;
@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtCityState;
@property (weak, nonatomic) IBOutlet UIButton *btnIsPaid;
@property (weak, nonatomic) IBOutlet UITextField *txtAmount;
@property (weak, nonatomic) IBOutlet UIButton *btnGuestsAllowed;
@property (weak, nonatomic) IBOutlet UITextField *txtGuestCharges;

- (IBAction)isPaidSwitch:(UIButton*)sender;
- (IBAction)guestAllowedSwitch:(UIButton*)sender;
- (IBAction)invitePeoples:(id)sender;*/

@end

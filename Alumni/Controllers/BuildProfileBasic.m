//
//  BuildProfileScreen.m
//  Jambo
//
//  Created by Rahul Chandera on 09/05/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "BuildProfileBasic.h"
#import "BuildProfileDetails.h"
#import "NavigationManager.h"
#import "UserManager.h"
#import "ApplicationManager.h"
#import "MVPlaceSearchTextField.h"

@interface BuildProfileBasic () <UITextFieldDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate, PlaceSearchTextFieldDelegate>
{
    CGPoint contentOffsetForSearchTextField;
}
@end

NavigationManager *navigationManager;
UserManager *userManager;
ApplicationManager *applicationManager;

NSMutableArray *filedsArray;
UIImage *userImage;
UIView *inputAccessoryView;
UITextField *currentTextField;
UIPickerView *pickerView;
NSArray *pickerArray;
NSArray *locationArray;
NSArray *instituteArray;
NSArray *programArray;
NSArray *streamArray;
NSArray *yearsArray;
int selectedInstituteId;

@implementation BuildProfileBasic

- (void)viewDidLoad {
    [super viewDidLoad];
    
    navigationManager = [NavigationManager sharedInstance];
    userManager = [UserManager sharedInstance];
    applicationManager = [ApplicationManager sharedInstance];
    userImage = [UIImage imageNamed:@"upload_photo.png"];
    
    //TBD
    userManager.userEntity.firstName = @"";
    userManager.userEntity.lastName = @"";
    userManager.userEntity.contact1 = @"+91";
    
    pickerArray = [NSArray new];
    
    locationArray = [[NSArray alloc]init];
    
    instituteArray = [userManager.instituteList valueForKey:@"instituteName"];
    programArray = [[NSArray alloc]initWithObjects:@"AFP", @"FDP",@"FPM",@"PGP",@"PGP-ABM",@"PGPX",@"MBA", nil];
    
    streamArray = [[NSArray alloc]initWithObjects:@"Aerospace",@"Agricultural",@"Civil",@"Computer",@"Biomedical", @"Electrical",@"Electronics",@"Management",@"Mechanical",@"Robotics",@"Software", nil];
    
    yearsArray = [[NSArray alloc]initWithObjects:@"1981", @"1982",@"1983",@"1984",@"1985",@"1986",@"1987",@"1988",@"1989",@"1990",@"1991",@"1992",@"1993",@"1994",@"1995",@"1996",@"1997",@"1998",@"1999",@"2000",@"2001",@"2002",@"2003",@"2004",@"2005",@"2006",@"2007",@"2008",@"2009",@"2010",@"2011",@"2012",@"2013",@"2014",@"2015", nil];
    
    pickerView = [[UIPickerView alloc] init];
    [pickerView setDataSource: self];
    [pickerView setDelegate: self];
    pickerView.showsSelectionIndicator = YES;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}


#pragma mark - TableView Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0)
        return 4;
    else
        return 5;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0 && indexPath.row == 0)
        return 120;
    else
        return 50;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    if (indexPath.section == 0) {
        
        switch (indexPath.row) {
            case 0:
            {
                cell = [tableView dequeueReusableCellWithIdentifier:@"PhotoCell"];
                if (cell == nil)
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PhotoCell"];
                
                UIImageView *imgPhoto = (UIImageView*)[cell.contentView viewWithTag:101];
                imgPhoto.image = userImage;
            }
                break;
            case 1:
            {
                cell = [tableView dequeueReusableCellWithIdentifier:@"NameCell"];
                if (cell == nil)
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"NameCell"];
                
                UITextField *txtFirstName = (UITextField *)[cell.contentView viewWithTag:100];
                txtFirstName.delegate = self;
                
                UITextField *txtLastName = (UITextField *)[cell.contentView viewWithTag:101];
                txtLastName.delegate = self;
            }
                break;
                
            case 2:
            {
                cell = [tableView dequeueReusableCellWithIdentifier:@"AddressCell"];
                if (cell == nil)
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AddressCell"];
                
                MVPlaceSearchTextField *txtCityState = (MVPlaceSearchTextField *)[cell.contentView viewWithTag:102];
                txtCityState.delegate = self;
                NSMutableArray * locationStringArray = [NSMutableArray new];
                if (applicationManager.currentCity.length > 0)
                    [locationStringArray addObject:applicationManager.currentCity];
                
                if (applicationManager.currentCountry.length > 0)
                    [locationStringArray addObject:applicationManager.currentCountry];
                
                txtCityState.text = [locationStringArray componentsJoinedByString:@", "];
                CGRect cellRect = [tableView rectForRowAtIndexPath:indexPath];
                [self configureSearchTextField:txtCityState CellRect:cellRect];
                
                UIButton *btnChange = (UIButton *)[cell.contentView viewWithTag:111];
                [btnChange addTarget:self action:@selector(changeAddress:) forControlEvents:UIControlEventTouchUpInside];
            }
                break;
                
            case 3:
            {
                cell = [tableView dequeueReusableCellWithIdentifier:@"PhoneCell"];
                if (cell == nil)
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PhoneCell"];
                
                UITextField *txtCountryCode = (UITextField *)[cell.contentView viewWithTag:103];
                txtCountryCode.delegate = self;
                UITextField *txtFiled = (UITextField *)[cell.contentView viewWithTag:104];
                txtFiled.delegate = self;
            }
                break;
                
            default:
                break;
        }
    }
    else {
    
        switch (indexPath.row) {
            case 0:
            {
                cell = [tableView dequeueReusableCellWithIdentifier:@"SpaceCell"];
                if (cell == nil)
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SpaceCell"];
            }
                break;
              
            case 1:
            {
                cell = [tableView dequeueReusableCellWithIdentifier:@"InstituteCell"];
                if (cell == nil)
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"InstituteCell"];
                
                UITextField *txtFiled = (UITextField *)[cell.contentView viewWithTag:105];
                txtFiled.delegate = self;
                txtFiled.inputView = pickerView;
            }
                break;
                
            case 2:
            {
                cell = [tableView dequeueReusableCellWithIdentifier:@"ProgramCell"];
                if (cell == nil)
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ProgramCell"];
                
                UITextField *txtFiled = (UITextField *)[cell.contentView viewWithTag:106];
                txtFiled.delegate = self;
                txtFiled.inputView = pickerView;
            }
                break;
                
            case 3:
            {
                cell = [tableView dequeueReusableCellWithIdentifier:@"YearCell"];
                if (cell == nil)
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"YearCell"];
                
                UITextField *txtFiled = (UITextField *)[cell.contentView viewWithTag:107];
                txtFiled.delegate = self;
                txtFiled.inputView = pickerView;
            }
                break;
                
            case 4:
            {
                cell = [tableView dequeueReusableCellWithIdentifier:@"StreamCell"];
                if (cell == nil)
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"StreamCell"];
                
                UITextField *txtFiled = (UITextField *)[cell.contentView viewWithTag:108];
                txtFiled.delegate = self;
                txtFiled.inputView = pickerView;
            }
                break;
   
            default:
                break;
        }
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        
        switch (indexPath.row) {
            case 0:
                
                [[[UIActionSheet alloc]initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera",@"Gallery", nil] showInView:self.view];
                break;
            case 1:
                break;
                
            case 2:
                break;
                
            case 3:
                break;
                
            case 4:
                break;
                
            case 5:
                break;
                
            case 6:
                break;
                
            case 7:
                break;
                
            default:
                break;
        }
    }
}

#pragma mark - Photo Selection
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != 2)
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        if (buttonIndex == 0){
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            picker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        }
        else
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [picker setModalPresentationStyle:UIModalPresentationOverFullScreen];
        
        [[UIApplication sharedApplication]setStatusBarHidden:YES];
        [self presentViewController:picker animated:YES completion:NULL];
        [[UIApplication sharedApplication]setStatusBarHidden:NO];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    userImage = chosenImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    [TableView reloadData];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}





#pragma mark - Change Address
- (void)changeAddress:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:TableView];
    NSIndexPath *indexPath = [TableView indexPathForRowAtPoint:buttonPosition];
    UITableViewCell *cell = [TableView cellForRowAtIndexPath:indexPath];
    
    UITextField *txtAddress = (UITextField*)[cell.contentView viewWithTag:102];
    [txtAddress becomeFirstResponder];
}


#pragma mark - PickerView
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return pickerArray.count;
}
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return pickerArray[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    currentTextField.text = pickerArray[row];
    
    if (currentTextField.tag == 105) {
        InstituteEntity * institute = userManager.instituteList[row];
        selectedInstituteId = institute.instituteId;
        programArray = [userManager getProgramsArrayForInstituteId:selectedInstituteId];
        streamArray = [userManager getStreamsArrayForInstituteId:selectedInstituteId];
    }
}

#pragma mark - Save & Continue
- (IBAction)saveContinueHandler:(id)sender
{
    NSString *strMessage = @"";
    if (userManager.userEntity.firstName.length == 0)
        strMessage = @"Please provide your name.";
    else if (userManager.userEntity.city.length == 0)
        strMessage = @"Please provide your address.";
    else if (userManager.userEntity.contact1.length == 0)
        strMessage = @"Please provide country code.";
    else if (userManager.userEntity.contact2.length == 0)
        strMessage = @"Please provide your mobile number.";
    else if (userManager.userEntity.institude.length == 0)
        strMessage = @"Please provide your institude name.";
    else if (userManager.userEntity.program.length == 0)
        strMessage = @"Please provide program.";
    else if (userManager.userEntity.graduateYear.length == 0)
        strMessage = @"Please provide your graduation year.";
    else if(![self validatePhone:userManager.userEntity.contact2])
        strMessage = @"Please provide valid mobile number.";
    
    if (strMessage.length > 0) {
        
        [[[UIAlertView alloc]initWithTitle:@"Jambo" message:strMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
    else {
    
        BuildProfileDetails *buildProfileDetails = (BuildProfileDetails*)[navigationManager getScreen:nsBuildProfileDetails];
        [buildProfileDetails updateUserPhoto:userImage];
        [self.navigationController pushViewController:buildProfileDetails animated:YES];
        
//        UIPageViewController *pageViewScreen = (UIPageViewController *)[navigationManager getScreen:nsPageViewScreen];
//        [self.navigationController pushViewController:pageViewScreen animated:YES];
    }
}

#pragma mark - TextField Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    currentTextField = textField;
    if(textField.tag == 102) {
    
        pickerArray = locationArray;
        [TableView setContentOffset:contentOffsetForSearchTextField animated:YES];
    }
    else if(textField.tag == 103) {
        
        textField.inputAccessoryView = [self addAccessoryView];
    }
    else if(textField.tag == 104) {
        
        textField.inputAccessoryView = [self addAccessoryView];
    }
    else if(textField.tag == 105) {
        
        pickerArray = instituteArray;
        [pickerView reloadAllComponents];
        textField.inputAccessoryView = [self addAccessoryView];
    }
    else if(textField.tag == 106) {
        
        pickerArray = programArray;
        [pickerView reloadAllComponents];
        textField.inputAccessoryView = [self addAccessoryView];
    }
    else if(textField.tag == 107) {
        
        pickerArray = yearsArray;
        [pickerView reloadAllComponents];
        textField.inputAccessoryView = [self addAccessoryView];
    }
    else if(textField.tag == 108) {
        
        pickerArray = streamArray;
        [pickerView reloadAllComponents];
        textField.inputAccessoryView = [self addAccessoryView];
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.text.length > 0) {
        
        switch (textField.tag) {
            case 100:
                userManager.userEntity.firstName = textField.text;
                break;
                
            case 101:
                userManager.userEntity.lastName = textField.text;
                break;
               
            case 102:
                userManager.userEntity.city = textField.text;
                break;
                
            case 103:
                userManager.userEntity.contact1 = textField.text;
                break;
                
            case 104:
                userManager.userEntity.contact2 = textField.text;
                break;
                
            case 105:
                userManager.userEntity.institude = textField.text;
                break;
                
            case 106:
                userManager.userEntity.program = textField.text;
                break;
                
            case 107:
                userManager.userEntity.graduateYear = textField.text;
                break;
                
            case 108:
                userManager.userEntity.stream = textField.text;
                break;
                
            default:
                break;
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [TableView viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO;
}

- (UIView *)addAccessoryView {
    
    if (!inputAccessoryView) {
        
        CGRect accessFrame = CGRectMake(0.0, 0.0, self.view.frame.size.width, 40.0);
        inputAccessoryView = [[UIView alloc] initWithFrame:accessFrame];
        inputAccessoryView.backgroundColor = [UIColor colorWithRed:182.0/255.0 green:190.0/255.0 blue:203.0/255.0 alpha:1.0];
        UIButton *compButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        compButton.frame = CGRectMake(accessFrame.size.width - 70, 0.0, 60.0, 40.0);
        [compButton setTitle: @"Done" forState:UIControlStateNormal];
        [compButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        compButton.titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
        [compButton addTarget:self action:@selector(hideKeyboard)
             forControlEvents:UIControlEventTouchUpInside];
        [compButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [inputAccessoryView addSubview:compButton];
        
        UIButton *compButton2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        compButton2.frame = CGRectMake(10.0, 0.0, 60.0, 40.0);
        [compButton2 setTitle: @"Select" forState:UIControlStateNormal];
        [compButton2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        compButton2.titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
        [compButton2 addTarget:self action:@selector(selectedItemFromInputView)
             forControlEvents:UIControlEventTouchUpInside];
        [compButton2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [inputAccessoryView addSubview:compButton2];
    }
    return inputAccessoryView;
}

- (void)hideKeyboard
{
    [currentTextField resignFirstResponder];
}

- (void)selectedItemFromInputView
{
    currentTextField.text = pickerArray[[pickerView selectedRowInComponent:0]];
    
    if (currentTextField.tag == 105) {
        InstituteEntity * institute = userManager.instituteList[[pickerView selectedRowInComponent:0]];
        selectedInstituteId = institute.instituteId;
        programArray = [userManager getProgramsArrayForInstituteId:selectedInstituteId];
        streamArray = [userManager getStreamsArrayForInstituteId:selectedInstituteId];
    }
    
    [currentTextField resignFirstResponder];
}

#pragma mark - Config SearchText
- (void)configureSearchTextField:(MVPlaceSearchTextField *)txtPlaceSearch CellRect:(CGRect)cellRect
{
    txtPlaceSearch.placeSearchDelegate                 = self;
    txtPlaceSearch.strApiKey                           = @"AIzaSyCDi2dklT-95tEHqYoE7Tklwzn3eJP-MtM";
    txtPlaceSearch.superViewOfList                     = self.view;
    txtPlaceSearch.autoCompleteShouldHideOnSelection   = YES;
    txtPlaceSearch.maximumNumberOfAutoCompleteRows     = 5;
    
    //Optional Properties
    txtPlaceSearch.autoCompleteRegularFontName =  @"HelveticaNeue-Bold";
    txtPlaceSearch.autoCompleteBoldFontName = @"HelveticaNeue";
    txtPlaceSearch.autoCompleteTableCornerRadius=0.0;
    txtPlaceSearch.autoCompleteRowHeight=35;
    txtPlaceSearch.autoCompleteTableCellTextColor=[UIColor colorWithWhite:0.131 alpha:1.000];
    txtPlaceSearch.autoCompleteFontSize=14;
    txtPlaceSearch.autoCompleteTableBorderWidth=1.0;
    txtPlaceSearch.showTextFieldDropShadowWhenAutoCompleteTableIsOpen=NO;
    txtPlaceSearch.autoCompleteShouldHideOnSelection=YES;
    txtPlaceSearch.autoCompleteShouldHideClosingKeyboard=YES;
    txtPlaceSearch.autoCompleteShouldSelectOnExactMatchAutomatically = YES;
    txtPlaceSearch.autoCompleteTableFrame = CGRectMake(10, 140, self.view.frame.size.width - 20, 200.0);
    
    if (contentOffsetForSearchTextField.y <= 0)
    {
        contentOffsetForSearchTextField = TableView.contentOffset;
        contentOffsetForSearchTextField.y = cellRect.origin.y;
    }
}

#pragma mark - Place search Textfield Delegates
-(void)placeSearchResponseForSelectedPlace:(NSMutableDictionary*)responseDict
{
    [self.view endEditing:YES];
    NSLog(@"%@",responseDict);
    
    NSArray *aDictLocation=[[responseDict objectForKey:@"result"] objectForKey:@"address_components"];

    NSLog(@"SELECTED ADDRESS :%@",aDictLocation);
}
-(void)placeSearchWillShowResult{
    
}
-(void)placeSearchWillHideResult{
    
}
-(void)placeSearchResultCell:(UITableViewCell *)cell withPlaceObject:(PlaceObject *)placeObject atIndex:(NSInteger)index{
    if(index%2==0){
        cell.contentView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    }else{
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
}





#pragma mark - Keyboard
- (void)keyboardDidShow:(NSNotification *)notification {
    
    // calculate the size of the keyboard and how much is and isn't covering the tableview
    NSDictionary *keyboardInfo = [notification userInfo];
    CGRect keyboardFrame = [keyboardInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardFrame = [TableView.window convertRect:keyboardFrame toView:TableView.superview];
    CGFloat heightOfTableViewThatIsCoveredByKeyboard = TableView.frame.origin.y + TableView.frame.size.height - keyboardFrame.origin.y;
    CGFloat heightOfTableViewThatIsNotCoveredByKeyboard = TableView.frame.size.height - heightOfTableViewThatIsCoveredByKeyboard;
    
    UIEdgeInsets tableContentInset = TableView.contentInset;
    self.originalTableContentInset = tableContentInset;
    tableContentInset.bottom = heightOfTableViewThatIsCoveredByKeyboard;
    
    UIEdgeInsets tableScrollIndicatorInsets = TableView.scrollIndicatorInsets;
    tableScrollIndicatorInsets.bottom += heightOfTableViewThatIsCoveredByKeyboard;
    
    [UIView beginAnimations:nil context:nil];
    
    // adjust the tableview insets by however much the keyboard is overlapping the tableview
    TableView.contentInset = tableContentInset;
    TableView.scrollIndicatorInsets = tableScrollIndicatorInsets;
    
    UIView *firstResponder = FXFormsFirstResponder(TableView);
    if ([firstResponder isKindOfClass:[UITextView class]]) {
        
        UITextView *textView = (UITextView *)firstResponder;
        
        // calculate the position of the cursor in the textView
        NSRange range = textView.selectedRange;
        UITextPosition *beginning = textView.beginningOfDocument;
        UITextPosition *start = [textView positionFromPosition:beginning offset:range.location];
        UITextPosition *end = [textView positionFromPosition:start offset:range.length];
        CGRect caretFrame = [textView caretRectForPosition:end];
        
        // convert the cursor to the same coordinate system as the tableview
        CGRect caretViewFrame = [textView convertRect:caretFrame toView:TableView.superview];
        
        // padding makes sure that the cursor isn't sitting just above the keyboard and will adjust to 3 lines of text worth above keyboard
        CGFloat padding = textView.font.lineHeight * 3;
        CGFloat keyboardToCursorDifference = (caretViewFrame.origin.y + caretViewFrame.size.height) - heightOfTableViewThatIsNotCoveredByKeyboard + padding;
        
        // if there is a difference then we want to adjust the keyboard, otherwise the cursor is fine to stay where it is and the keyboard doesn't need to move
        if (keyboardToCursorDifference > 0.0f) {
            // adjust offset by this difference
            CGPoint contentOffset = TableView.contentOffset;
            contentOffset.y += keyboardToCursorDifference;
            [TableView setContentOffset:contentOffset animated:YES];
        }
    }
    
    [UIView commitAnimations];
}

- (void)keyboardWillHide:(NSNotification *)note
{
    NSDictionary *keyboardInfo = [note userInfo];
    UIEdgeInsets tableScrollIndicatorInsets = TableView.scrollIndicatorInsets;
    tableScrollIndicatorInsets.bottom = 0;
    
    //restore insets
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:(UIViewAnimationCurve)keyboardInfo[UIKeyboardAnimationCurveUserInfoKey]];
    [UIView setAnimationDuration:[keyboardInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
    TableView.contentInset = self.originalTableContentInset;
    TableView.scrollIndicatorInsets = tableScrollIndicatorInsets;
    self.originalTableContentInset = UIEdgeInsetsZero;
    [UIView commitAnimations];
}
static UIView *FXFormsFirstResponder(UIView *view)
{
    if ([view isFirstResponder])
    {
        return view;
    }
    for (UIView *subview in view.subviews)
    {
        UIView *responder = FXFormsFirstResponder(subview);
        if (responder)
        {
            return responder;
        }
    }
    return nil;
}




#pragma mark - Validate Phone
- (BOOL)validatePhone:(NSString *)phoneNumber
{
    NSString *numberRegEx = @"[0-9]{10}";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegEx];
    if ([numberTest evaluateWithObject:phoneNumber] == YES)
        return TRUE;
    else
        return FALSE;
}



- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
    [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardDidShowNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillHideNotification];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

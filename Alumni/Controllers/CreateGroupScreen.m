//
//  CreateGroupScreen.m
//  Jambo
//
//  Created by Rahul Chandera on 26/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "CreateGroupScreen.h"
#import "NavigationManager.h"
#import "UserManager.h"
//#import "ProcessIndicator.h"
#import "InviteScreen.h"

#define DESCRIPTION_Y 30

@interface CreateGroupScreen ()

@end

NavigationManager *navigationManager;
UserManager *userManager;

@implementation CreateGroupScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeUI];
    [self initializeObjects];
}


#pragma mark - Initialize UI
- (void)initializeUI {
    
    self.title = @"Create a Group";
}

#pragma mark - Initialize Objects
- (void)initializeObjects
{
    navigationManager = [NavigationManager sharedInstance];
    userManager = [UserManager sharedInstance];
    
    //Set growing textview
    _txtGroupDescription.contentInset = UIEdgeInsetsMake(0, -5, 0, 0);
    
    _txtGroupDescription.minNumberOfLines = 1;
    _txtGroupDescription.maxNumberOfLines = 6;

    _txtGroupDescription.returnKeyType = UIReturnKeyDone;
    _txtGroupDescription.font = [UIFont fontWithName:@"Roboto-Regular" size:15.0f];
    _txtGroupDescription.delegate = self;
    _txtGroupDescription.backgroundColor = [UIColor clearColor];
    _txtGroupDescription.placeholder = @"DESCRIPTION";
    _txtGroupDescription.placeholderColor = [UIColor colorWithWhite:0.600 alpha:0.500];
}



- (IBAction)invitePeoples:(id)sender {
    
    if(_txtGroupName.text.length == 0 || _txtGroupDescription.text.length == 0 ) {
        
        [[[UIAlertView alloc] initWithTitle:@"Jambo" message:@"Please provide required information." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
    else {
        
        UIBarButtonItem *newBackButton =
        [[UIBarButtonItem alloc] initWithTitle:@""
                                         style:UIBarButtonItemStyleBordered
                                        target:nil
                                        action:nil];
        [[self navigationItem] setBackBarButtonItem:newBackButton];
        
        InviteScreen *inviteScreen = (InviteScreen *)[navigationManager getScreen:nsInviteScreen];
        inviteScreen.inviteType = InviteTypeGroupMember;
        inviteScreen.groupName = _txtGroupName.text;
        inviteScreen.groupDescription = _txtGroupDescription.text;
        inviteScreen.alumniArray = userManager.alumniArray;
        [self.navigationController pushViewController:inviteScreen animated:YES];
    }
}


#pragma mark - TextField Delegate Method
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Growing textview delegates
-(void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    [self.view layoutIfNeeded];
    
    _dividerTopSpace.constant = DESCRIPTION_Y + height + 1;
    _descriptionHeight.constant = height;
    [UIView animateWithDuration:0.5
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}

- (BOOL)growingTextViewShouldReturn:(HPGrowingTextView *)growingTextView;
{
    [growingTextView resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  QuestionListScreen.h
//  Jambo
//
//  Created by Rahul Chandera on 22/06/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupEntity.h"
#import "DiscussionManager.h"

@interface QuestionListScreen : UIViewController <DiscussionManagerDelegate>
{
    IBOutlet UITableView *TableView;
    IBOutlet UITextField *txtSearch;
}
@property (strong, nonatomic) GroupEntity *groupEntity;

- (IBAction)btnUpvoteClicked:(UIButton *)sender;
- (IBAction)btnCommentClicked:(UIButton *)sender;

@end

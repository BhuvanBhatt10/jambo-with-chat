//
//  InviteCell.h
//  Jambo
//
//  Created by Rakesh Pethani on 27/10/15.
//  Copyright © 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>

@class InviteCell;

@protocol InviteCellProtocol <NSObject>

- (void)btnInviteClickedForCell:(InviteCell *)cell;

@end

@interface InviteCell : UITableViewCell <UITextFieldDelegate>

@property (strong,nonatomic) id <InviteCellProtocol> delegate;

@property (strong, nonatomic) IBOutlet UITextField *txtEmailId;
@property (strong, nonatomic) IBOutlet UIButton *btnInvite;

- (void)setInvitedWithEmailId:(NSString *)email;
- (IBAction)btnInviteClicked:(UIButton *)sender;

@end

//
//  QuestionDetailScreen.m
//  Alumni
//
//  Created by Rahul Chandera on 22/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "QuestionDetailScreen.h"
#import "AppConstants.h"
#import "UIImageView+WebCache.h"
#import "Utility.h"
#import "QuestionEntity.h"
#import "AddAnswerScreen.h"
#import "KLCPopup.h"
#import "NavigationManager.h"
#import "MessageGroupEntity.h"
#import "InboxDetailScreen.h"
#import "UserProfileScreen.h"
#import "DiscussionManager.h"
#import "ChatViewController.h"
#import "ChatManager.h"
#import <Quickblox/QBRequest+QBUsers.h>
#import "NavigationManager.h"
#import "ProcessIndicator.h"
#import "QuickBloxHelper.h"
#import "UserdefaultManager.h"
KLCPopup * popup;
DiscussionManager * discussionManager;
ChatManager * chatManager;
NavigationManager * navigationManager;
ProcessIndicator *processIndicator;
int messageSenderId;

@interface QuestionDetailScreen () <UIScrollViewDelegate,DiscussionManagerDelegate>
{
    UIImageView * currentPinchedImage;
    AnswerEntity * answerBeingUpvoted;
    QuickBloxHelper *quickbloxSharedInstance;
}
@end

QuestionEntity *questionEntity;

@implementation QuestionDetailScreen
//@synthesize questionEntity;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeObjects];
    
    //Process Indicator
    processIndicator = [ProcessIndicator sharedInstance];
    quickbloxSharedInstance = [QuickBloxHelper sharedInstance];
    
    //Remove all observer
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    // Add observer that will allow the nested collection cell to trigger the view controller select row at index path
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addNewAnswer:) name:@"addNewAnswer" object:nil];
}


- (void)setQuestionData:(QuestionEntity *)quesEntity
{
    questionEntity = [[QuestionEntity alloc]init];
    questionEntity = quesEntity;
}

#pragma mark - Initialize Objects
- (void)initializeObjects
{
    discussionManager = [DiscussionManager sharedInstance];
    chatManager = [ChatManager sharedInstance];
    
    _lblTopicTitle.text = questionEntity.question;
    [_imgUserPhoto sd_setImageWithURL:[NSURL URLWithString:questionEntity.createdBy.profilePic] placeholderImage:[UIImage imageNamed:DEFAULT_USER_IMAGE]];
    _lblUserName.text = [NSString stringWithFormat:@"%@ %@",questionEntity.createdBy.firstName, questionEntity.createdBy.lastName];
    _lblPostTime.text = [Utility daysDiffrentFromDateString:questionEntity.lastUpdated];
    [_btnTopicLikes setTitle:[NSString stringWithFormat:@"+%d",questionEntity.score] forState:UIControlStateNormal];
    _lblTotalComments.text = [NSString stringWithFormat:@"%d comments",(int)[questionEntity.answers count]];
    
    UITapGestureRecognizer *userImageTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userPhotoTapped)];
    _imgUserPhoto.contentMode = UIViewContentModeScaleAspectFit;
    _imgUserPhoto.userInteractionEnabled = YES;
    [_imgUserPhoto addGestureRecognizer:userImageTapGesture];
    
    if ([self lineCountForLabel:_lblTopicTitle] > 2)
        btnExpandCollapse.hidden = NO;
    else
        btnExpandCollapse.hidden = YES;
}


- (int)lineCountForLabel:(UILabel *)label
{
    CGSize maxSize = CGSizeMake(label.frame.size.width - 50, MAXFLOAT);
    
    CGRect labelRect = [label.text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:label.font} context:nil];
    
    int lines = ceil(labelRect.size.height / label.font.lineHeight);
    return lines;
}

#pragma mark - TableView Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [questionEntity.answers count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 135;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CommentCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];

    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    UIImageView *imgPhoto = (UIImageView *)[cell.contentView viewWithTag:101];
    UILabel *lblName = (UILabel *)[cell.contentView viewWithTag:102];
    UILabel *lblTime = (UILabel *)[cell.contentView viewWithTag:103];
    UILabel *lblAnswer = (UILabel *)[cell.contentView viewWithTag:104];
    UIButton *btnLike = (UIButton *)[cell.contentView viewWithTag:105];
    UIImageView *imgAnswer = (UIImageView *)[cell.contentView viewWithTag:107];
    
    AnswerEntity *answerEntity = [questionEntity.answers objectAtIndex:indexPath.row];
    [imgPhoto setImageWithURL:[NSURL URLWithString:answerEntity.createdBy.profilePic] placeholderImage:[UIImage imageNamed:DEFAULT_USER_IMAGE]];
    
    UITapGestureRecognizer *userImgTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userPhotoTappedInCell:)];
    imgPhoto.contentMode = UIViewContentModeScaleAspectFit;
    imgPhoto.userInteractionEnabled = YES;
    [imgPhoto addGestureRecognizer:userImgTapGesture];
    
    lblName.text = [NSString stringWithFormat:@"%@ %@",answerEntity.createdBy.firstName, answerEntity.createdBy.lastName];
    lblTime.text = [Utility daysDiffrentFromDateString:answerEntity.lastUpdated];
    lblAnswer.text = answerEntity.answer;
    [btnLike setTitle:[NSString stringWithFormat:@"+%d",answerEntity.score] forState:UIControlStateNormal];
    
    imgAnswer.hidden = YES;
    imgAnswer.image = nil;
    imgAnswer.userInteractionEnabled = NO;
    
    if (answerEntity.picture.length > 0)
    {
        imgAnswer.hidden = NO;
        [imgAnswer setImageWithURL:[NSURL URLWithString:answerEntity.picture] placeholderImage:[UIImage imageNamed:@"img_answer_placeholder"]];
        UITapGestureRecognizer *answerImageTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(answerImageTapped:)];
        imgAnswer.contentMode = UIViewContentModeScaleAspectFit;
        imgAnswer.userInteractionEnabled = YES;
        [imgAnswer addGestureRecognizer:answerImageTapGesture];
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Cell clicked");
}

#pragma mark - Answer image tap
- (void)answerImageTapped:(UITapGestureRecognizer *)sender
{
    UIImageView * answerImage = (UIImageView *)sender.view;
    
    UIScrollView * imageScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.width - 40)];
    [imageScrollView setMinimumZoomScale:1];
    [imageScrollView setMaximumZoomScale:5];
    [imageScrollView setClipsToBounds:NO];
    [imageScrollView setShowsHorizontalScrollIndicator:NO];
    [imageScrollView setShowsVerticalScrollIndicator:NO];
    [imageScrollView setDelegate:self];
    
    UIImageView * imageViewForPopup = [[UIImageView alloc] initWithImage:answerImage.image];
    imageViewForPopup.frame = CGRectMake(20, 0, self.view.bounds.size.width - 40, self.view.bounds.size.width - 40);
    imageViewForPopup.contentMode = UIViewContentModeScaleAspectFit;
    [imageScrollView addSubview:imageViewForPopup];
    currentPinchedImage = imageViewForPopup;
//    UIPinchGestureRecognizer * pinchImageRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchedImage:)];
//    imageViewForPopup.userInteractionEnabled = YES;
//    [imageViewForPopup addGestureRecognizer:pinchImageRecognizer];
    
    KLCPopup * popup1 = [KLCPopup popupWithContentView:imageScrollView
                                  showType:KLCPopupShowTypeFadeIn
                               dismissType:KLCPopupDismissTypeFadeOut
                                  maskType:KLCPopupMaskTypeDimmed
                  dismissOnBackgroundTouch:YES
                     dismissOnContentTouch:NO];
    [popup1 show];
}

- (void)pinchedImage:(UIPinchGestureRecognizer *)gesture
{
    NSLog(@"Pinched image");
    UIImageView * imageView = (UIImageView *)gesture.view;
    
    if (gesture.state == UIGestureRecognizerStateEnded
        || gesture.state == UIGestureRecognizerStateChanged) {
        if ([gesture scale]<1.0f) {
            [gesture setScale:1.0f];
        }
        
        if ([gesture scale] > 2.0f) {
            [gesture setScale:2.0f];
        }
        CGAffineTransform transform = CGAffineTransformMakeScale([gesture scale],  [gesture scale]);
        imageView.transform = transform;
    }
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return currentPinchedImage;
}

- (void)userPhotoTapped
{
    self.contactInfoPopup = (ContactInfoPopup*)[[NavigationManager sharedInstance] getScreen:nsContactInfoPopup];
    self.contactInfoPopup.view.frame = CGRectMake(0, 0, 222, 322);
    [self.contactInfoPopup setUserData:questionEntity.createdBy];
    self.contactInfoPopup.delegate = self;
    
    popup = [KLCPopup popupWithContentView:self.contactInfoPopup.view
                                  showType:KLCPopupShowTypeBounceInFromTop
                               dismissType:KLCPopupDismissTypeBounceOutToBottom
                                  maskType:KLCPopupMaskTypeDimmed
                  dismissOnBackgroundTouch:YES
                     dismissOnContentTouch:NO];
    [popup show];
}

- (void)userPhotoTappedInCell:(UITapGestureRecognizer *)gesture
{
    UIImageView * imageView = (UIImageView *)gesture.view;
    CGPoint imagePosition = [imageView convertPoint:CGPointZero toView:TableView];
    NSIndexPath *indexPath = [TableView indexPathForRowAtPoint:imagePosition];
    
    AnswerEntity *answerEntity = [questionEntity.answers objectAtIndex:indexPath.row];
    self.contactInfoPopup = (ContactInfoPopup*)[[NavigationManager sharedInstance] getScreen:nsContactInfoPopup];
    self.contactInfoPopup.view.frame = CGRectMake(0, 0, 222, 322);
    [self.contactInfoPopup setUserData:answerEntity.createdBy];
    self.contactInfoPopup.delegate = self;
    
    popup = [KLCPopup popupWithContentView:self.contactInfoPopup.view
                                  showType:KLCPopupShowTypeBounceInFromTop
                               dismissType:KLCPopupDismissTypeBounceOutToBottom
                                  maskType:KLCPopupMaskTypeDimmed
                  dismissOnBackgroundTouch:YES
                     dismissOnContentTouch:NO];
    [popup show];
}

#pragma mark - ContactInfo Delegate
- (void)sendMessageTo:(UserEntity*)_user
{
    [popup dismiss:YES];
    messageSenderId = _user.userId;
    
    MessageGroupEntity *messageGroupEntity = [[MessageGroupEntity alloc] init];
    messageGroupEntity.userId = _user.userId;
    messageGroupEntity.firstName = _user.firstName;
    messageGroupEntity.lastName = _user.lastName;
    
    

    //Start Chat except owns profile
    if (![[UserdefaultManager getUserEmail] isEqualToString:_user.email]) {
        [processIndicator showProcessIndicatorInView:self.view Message:@"Loading"];
        
        //Find user in storage
        [quickbloxSharedInstance isUserExistInStorage:_user.email Oncompletion:^(QBUUser *opponentUser) {
            if (opponentUser != nil) {
                if (opponentUser.fullName.length == 0 || opponentUser.fullName == nil) {
                    opponentUser.fullName = opponentUser.email;
                }
                
                [ServicesManager.instance.chatService createPrivateChatDialogWithOpponent:opponentUser completion:^(QBResponse *response, QBChatDialog *createdDialog) {
                    if( !response.success  && createdDialog == nil ) {
                        [processIndicator hideProcessIndicator];
                    } else {
                        [processIndicator hideProcessIndicator];
                        [quickbloxSharedInstance connectQBOnCompletion:^(QBUUser *currentUser) {
                            if (currentUser != nil) {
                                // Connected
                                [processIndicator hideProcessIndicator];
                                ChatViewController *chatVC = (ChatViewController *)[navigationManager getScreen:nsChatScreen];
                                chatVC.dialog = createdDialog;
                                [self.navigationController pushViewController:chatVC animated:YES];
                            } else {
                                [processIndicator hideProcessIndicator];
                                // NSAssert(1<0, @"Quickblox not connected to internet");
                            }
                        }];
                    }
                }];
            } else {
                [processIndicator hideProcessIndicator];
            }
        }];
        
        /*[quickbloxSharedInstance isUserExistInStorage:_user.email Oncompletion:^(QBUUser *opponentUser) {
         if(opponentUser != nil) {
         // QBUUser found
         [quickbloxSharedInstance createChatDialogueWithOpponent:opponentUser Oncompletion:^(QBChatDialog *dialogue) {
         if (dialogue != nil) {
         // Dialogue created
         NSLog(@"Dialogue Created");
         
         // Quickblox Connection
         [quickbloxSharedInstance connectQBOnCompletion:^(QBUUser *currentUser) {
         if (currentUser != nil) {
         // Connected
         [processIndicator hideProcessIndicator];
         ChatViewController *chatVC = (ChatViewController *)[navigationManager getScreen:nsChatScreen];
         chatVC.dialog = dialogue;
         [self.navigationController pushViewController:chatVC animated:YES];
         } else {
         [processIndicator hideProcessIndicator];
         NSAssert(1<0, @"Quickblox not connected to internet");
         }
         }];
         } else {
         // Failed to create dialouge
         [processIndicator hideProcessIndicator];
         NSLog(@"Failed to create dialogue");
         }
         }];
         } else {
         // QBUUser not found
         [processIndicator hideProcessIndicator];
         }
         }];*/
        
    }
}

- (void)viewUserProfile:(UserEntity*)user
{
    [popup dismiss:YES];
    
    UserProfileScreen *userProfileScreen = (UserProfileScreen*)[[NavigationManager sharedInstance] getScreen:nsUserProfileScreen];
    userProfileScreen.userEntity = user;
    [self.navigationController pushViewController:userProfileScreen animated:YES];
}

#pragma mark - NSNotification Methonds
- (void)addNewAnswer:(NSNotification *)notification
{
    NSDictionary *cellData = [notification object];
    if (cellData)
    {
        AnswerEntity *answerEntity = [cellData objectForKey:@"Answer"];
        [questionEntity.answers insertObject:answerEntity atIndex:0];
        [TableView reloadData];
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REFRESH_GROUP_DATA_EVENT object:nil];
    }
}


 #pragma mark - Navigation
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 
     if([[segue identifier] isEqualToString:@"PostAnswer"]){
         
         AddAnswerScreen *addAnswerScreen = (AddAnswerScreen *)[segue destinationViewController];
         addAnswerScreen.questionId = [NSString stringWithFormat:@"%d",questionEntity.queId];
     }
}
 

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnUpvoteQuestionClicked:(UIButton *)sender
{
    discussionManager.delegate = self;
    [discussionManager upvoteQuestionWithId:questionEntity.queId Score:questionEntity.score + 1];
}

- (IBAction)btnUpvoteAnswerClicked:(UIButton *)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:TableView];
    NSIndexPath *indexPath = [TableView indexPathForRowAtPoint:buttonPosition];
    
    if (indexPath) {
        AnswerEntity * answerEntity = [questionEntity.answers objectAtIndex:indexPath.row];
        answerBeingUpvoted = answerEntity;
        discussionManager.delegate = self;
        [discussionManager upvoteAnswerWithId:answerEntity.ansId Score:answerEntity.score + 1];
    }
}

- (IBAction)btnExpandCollapseClick:(UIButton *)sender
{
    if (sender.selected)
    {
        sender.selected = NO;
        
        containerHeight.constant = 155;
        
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
    else
    {
        sender.selected = YES;
        
        int lines = [self lineCountForLabel:_lblTopicTitle];
        
        containerHeight.constant = 100 + (lines * 27.5);
        
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
}

#pragma mark - Discussion manager delegate
- (void)requestCompleted:(id)response
{
    if ([[response objectForKey:REQUEST_KEY] intValue] == requestUpvoteQuestion)
    {
        questionEntity.score ++;
        [_btnTopicLikes setTitle:[NSString stringWithFormat:@"+%d",questionEntity.score] forState:UIControlStateNormal];
    }
    else if ([[response objectForKey:REQUEST_KEY] intValue] == requestUpvoteAnswer)
    {
        answerBeingUpvoted.score ++;
        [TableView reloadData];
    }
}

@end

//
//  InboxScreen.h
//  Alumni
//
//  Created by Rahul Chandera on 22/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageManager.h"

@interface InboxScreen : UIViewController <MessageManagerDelegate>
{
    IBOutlet UITableView *TableView;
    NSArray *allDialogs;
}
@end

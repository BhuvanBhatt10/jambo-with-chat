//
//  FilterScreen.m
//  Jambo
//
//  Created by Rahul Chandera on 02/05/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "FilterScreen.h"
#import "FilterGroupEntity.h"

@interface FilterScreen ()

@end

NSMutableArray *filterGroupArray;
NSArray *filterArray;

@implementation FilterScreen
@synthesize delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Filter";
    
    filterGroupArray = [[NSMutableArray alloc]init];
    filterArray = [[NSArray alloc]init];
    
    FilterGroupEntity *fg1 = [FilterGroupEntity new];
    fg1.groupName = @"LOCATION";
    [filterGroupArray addObject:fg1];
    
    FilterGroupEntity *fg2 = [FilterGroupEntity new];
    fg2.groupName = @"INDUSTRY";
    FilterEntity *f1 = [FilterEntity new];
    f1.filterName = @"Marketing";
    f1.isSelected = NO;
    FilterEntity *f2 = [FilterEntity new];
    f2.filterName = @"Finance";
    f2.isSelected = NO;
    FilterEntity *f3 = [FilterEntity new];
    f3.filterName = @"Jobs";
    f3.isSelected = NO;
    fg2.filters = @[f1,f2,f3];
    [filterGroupArray addObject:fg2];
    
    FilterGroupEntity *fg3 = [FilterGroupEntity new];
    fg3.groupName = @"DESIGNATION";
    [filterGroupArray addObject:fg3];
    
    FilterGroupEntity *fg4 = [FilterGroupEntity new];
    fg4.groupName = @"SKILLS";
    [filterGroupArray addObject:fg4];
    
    [CollectionView selectItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] animated:NO scrollPosition:UICollectionViewScrollPositionTop];
}




#pragma mark - CollectionView
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [filterGroupArray count];
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(100, 50);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.0;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FilterCell" forIndexPath:indexPath];
    
    UILabel *lblType = (UILabel *)[cell.contentView viewWithTag:101];
    FilterGroupEntity *filterGroupEntity = [filterGroupArray objectAtIndex:indexPath.row];
    lblType.text = filterGroupEntity.groupName;
    
    lblType.highlightedTextColor = [UIColor whiteColor];
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    FilterGroupEntity *filterGroupEntity = [filterGroupArray objectAtIndex:indexPath.row];
    filterArray = filterGroupEntity.filters;
    
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
//    [collectionView setContentOffset:CGPointMake(cell.center.x - collectionView.frame.size.width * 0.5, cell.frame.origin.y) animated:YES];
    [collectionView setContentOffset:CGPointMake(collectionView.frame.size.width - ((filterGroupArray.count - indexPath.row) * 90), cell.frame.origin.y) animated:YES];
    
    [TableView reloadData];
}




#pragma mark - TableView Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return filterArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"AlumniCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    UILabel *lblType = (UILabel *)[cell.contentView viewWithTag:101];
    UIImageView *imgCheckMark = (UIImageView *)[cell.contentView viewWithTag:102];
    
    FilterEntity *filterEntity = [filterArray objectAtIndex:indexPath.row];
    lblType.text = filterEntity.filterName;
    
    if(filterEntity.isSelected)
        imgCheckMark.hidden = NO;
    else
        imgCheckMark.hidden = YES;
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    FilterEntity *filterEntity = [filterArray objectAtIndex:indexPath.row];
    filterEntity.isSelected =! filterEntity.isSelected;
    [tableView reloadData];
}




- (IBAction)applyFiltersHandler:(id)sender
{
    NSString *groups = @"";
    for(FilterGroupEntity *filterGroupEntity in filterGroupArray)
    {
        NSString *filters = @"";
        for(FilterEntity *filterEntity in filterGroupEntity.filters)
        {
            if (filterEntity.isSelected) {
                if(filters.length == 0)
                    filters = filterEntity.filterName;
                else
                    filters = [NSString stringWithFormat:@"%@,%@",filters,filterEntity.filterName];
            }
        }
        
        if(groups.length == 0)
            groups = [NSString stringWithFormat:@"%@=%@",filterGroupEntity.groupName,filters];
        else
            groups = [NSString stringWithFormat:@"%@&%@=%@",groups,filterGroupEntity.groupName,filters];
    }
    
    [self.delegate applyFilters:groups];
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

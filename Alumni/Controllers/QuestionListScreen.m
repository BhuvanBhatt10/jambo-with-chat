//
//  QuestionListScreen.m
//  Jambo
//
//  Created by Rahul Chandera on 22/06/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "QuestionListScreen.h"
#import "QuestionDetailScreen.h"
#import "Utility.h"
#import "AddQuestionScreen.h"
#import "AppConstants.h"
#import "DiscussionManager.h"
#import "NavigationManager.h"

DiscussionManager * discussionManager;

@interface QuestionListScreen ()

@end

NSArray *questionsArray;

@implementation QuestionListScreen
{
    QuestionEntity * questionBeingUpvoted;
}

@synthesize groupEntity;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    discussionManager = [DiscussionManager sharedInstance];
    
    questionsArray = [NSArray new];
    questionsArray = groupEntity.questions;
    
    UIView * titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 50)];
    titleView.backgroundColor = [UIColor clearColor];
    
    UILabel * titleLabel = [UILabel new];
    titleLabel.frame = CGRectMake(0, 5, 200, 25);
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = self.groupEntity.tag;
    titleLabel.font = [UIFont systemFontOfSize:16.0f weight:0.5f];
    titleLabel.textColor = [UIColor whiteColor];
    [titleView addSubview:titleLabel];
    
    UILabel * noOfMember = [UILabel new];
    noOfMember.frame = CGRectMake(0, 25, 200, 20);
    noOfMember.textAlignment = NSTextAlignmentCenter;
    noOfMember.text = [NSString stringWithFormat:@"%d members",self.groupEntity.totalMembers];
    noOfMember.font = [UIFont systemFontOfSize:12.0f];
    noOfMember.textColor = [UIColor colorWithWhite:1.000 alpha:0.750];
    [titleView addSubview:noOfMember];
    
    self.navigationItem.titleView = titleView;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTableData) name:NOTIFICATION_REFRESH_GROUP_DATA_EVENT object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [TableView reloadData];
}

- (void)reloadTableData {
    self.groupEntity = [discussionManager getTagEntity:groupEntity.tagId];
    questionsArray = groupEntity.questions;
    [TableView reloadData];
}

#pragma mark - TableView Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [questionsArray count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 85;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"QuestionCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    UILabel *lblQuestion = (UILabel *)[cell.contentView viewWithTag:101];
    UIButton *btnLike = (UIButton *)[cell.contentView viewWithTag:102];
    UIButton *btnComment = (UIButton *)[cell.contentView viewWithTag:103];
    UILabel *lblTime = (UILabel *)[cell.contentView viewWithTag:104];
    
    QuestionEntity *questionEntity = [questionsArray objectAtIndex:indexPath.row];
    lblQuestion.text = questionEntity.question;
    lblTime.text = [Utility daysDiffrentFromDateString:questionEntity.lastUpdated];
    [btnLike setTitle:[NSString stringWithFormat:@"+%d",questionEntity.score] forState:UIControlStateNormal];
    [btnComment setTitle:[NSString stringWithFormat:@"%d",(int)[questionEntity.answers count]] forState:UIControlStateNormal];
    
    //Set corner radius and border for like button
    btnLike.layer.cornerRadius = 2.0f;
    btnLike.layer.borderColor = [UIColor colorWithWhite:0.600 alpha:1.000].CGColor;
    btnLike.layer.borderWidth = 1.0f;
    btnLike.layer.masksToBounds = YES;
    
    return cell;
}

#pragma mark - Search Question
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField.text.length >= 2)
        [self searchQuestion:textField.text];
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (void)searchQuestion:(NSString*)searchText
{
    NSPredicate *filterPredicateForFilter =  [NSPredicate predicateWithFormat:@"SELF.question contains[c] %@",searchText];
    questionsArray = [self.groupEntity.questions filteredArrayUsingPredicate:filterPredicateForFilter];
    [TableView reloadData];
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    questionsArray = self.groupEntity.questions;
    [TableView reloadData];
    [textField performSelector:@selector(resignFirstResponder) withObject:nil afterDelay:0.1];
    return YES;
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([[segue identifier] isEqualToString:@"QuestionDetail"]) {
        
        NSIndexPath *indexPath = [self->TableView indexPathForCell:sender];
        QuestionDetailScreen *questionDetailScreen = (QuestionDetailScreen *)[segue destinationViewController];
        [questionDetailScreen setQuestionData:[groupEntity.questions objectAtIndex:indexPath.row]];
    }
    else if ([[segue identifier] isEqualToString:@"AddQuestion"]) {
    
        AddQuestionScreen *addQuestionScreen = (AddQuestionScreen*)[segue destinationViewController];
        addQuestionScreen.groupEntity = self.groupEntity;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)btnUpvoteClicked:(UIButton *)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:TableView];
    NSIndexPath *indexPath = [TableView indexPathForRowAtPoint:buttonPosition];
    
    if (indexPath) {
        QuestionEntity * questionEntity = [questionsArray objectAtIndex:indexPath.row];
        questionBeingUpvoted = questionEntity;
        discussionManager.delegate = self;
        [discussionManager upvoteQuestionWithId:questionEntity.queId Score:questionEntity.score + 1];
    }
}

- (IBAction)btnCommentClicked:(UIButton *)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:TableView];
    NSIndexPath *indexPath = [TableView indexPathForRowAtPoint:buttonPosition];
    
    QuestionDetailScreen *questionDetailScreen = (QuestionDetailScreen *)[[NavigationManager sharedInstance] getScreen:nsQuestionDetailScreen];
    [questionDetailScreen setQuestionData:[groupEntity.questions objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:questionDetailScreen animated:YES];
}

#pragma mark - discurrsion manager delegate
-(void)requestCompleted:(id)response
{
    if (![[response objectForKey:RESULT_KEY] boolValue])
    {
        questionBeingUpvoted.score ++;
        [TableView reloadData];
    }
}

@end

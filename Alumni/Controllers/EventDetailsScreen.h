//
//  EventDetailsScreen.h
//  Jambo
//
//  Created by Rahul Chandera on 11/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventEntity.h"
#import "EventManager.h"
#import "ContactInfoPopup.h"

@interface EventDetailsScreen : UIViewController <EventManagerDelegate,ContactInfoPopupDelegate>

@property(nonatomic,strong) EventEntity *eventEntity;
@property (weak, nonatomic) IBOutlet UILabel *lblEventTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgUser;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UITableView *TableView;

@property (nonatomic, assign) BOOL isForMyEvent;
@property (nonatomic, assign) BOOL isInvited;

@property(nonatomic,strong) ContactInfoPopup *contactInfoPopup;;

- (IBAction)btnAvailabilityClick:(UIButton *)sender;
- (IBAction)btnCopyEventClick:(UIButton *)sender;

@end

//
//  ContactInfoPopup.h
//  Alumni
//
//  Created by Rahul Chandera on 16/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserEntity.h"

@protocol ContactInfoPopupDelegate
- (void)sendMessageTo:(UserEntity*)user;
- (void)viewUserProfile:(UserEntity*)user;
@end

@interface ContactInfoPopup : UIViewController

- (void)setUserData:(UserEntity *)userDetails;
- (void)setUserDataByAlumini:(AluminiEntity *)userDetails;

@property (strong,nonatomic) id <ContactInfoPopupDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIImageView *imgPhoto;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblDesignation;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblOccupation;

- (IBAction)callHandler:(id)sender;
- (IBAction)messageHandler:(id)sender;
- (IBAction)viewProfileHandler:(id)sender;

@end

//
//  UserProfileScreen.h
//  Jambo
//
//  Created by Rahul Chandera on 08/05/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DWTagList.h"
#import "UserEntity.h"

@interface UserProfileScreen : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imgUserPhoto;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblUserCode;
@property (weak, nonatomic) IBOutlet UILabel *lblBusiness;
@property (weak, nonatomic) IBOutlet UILabel *lblMobileNo;
@property (strong, nonatomic) IBOutlet UIButton *btnEditProfile;
@property (weak, nonatomic) IBOutlet DWTagList *tagView;

@property (nonatomic,strong) UserEntity *userEntity;

- (IBAction)messageTapHandler:(id)sender;
- (IBAction)callTapHandler:(id)sender;
- (IBAction)btnEditProfileClicked:(UIButton *)sender;

@end

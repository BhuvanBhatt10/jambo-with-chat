//
//  NotificationsScreen.h
//  Jambo
//
//  Created by Rahul Chandera on 25/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationsScreen : UIViewController
{
    IBOutlet UITableView *TableView;
}
@end

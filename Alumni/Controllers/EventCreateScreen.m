//
//  EventCreateScreen.m
//  Alumni
//
//  Created by Rahul Chandera on 05/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "EventCreateScreen.h"
#import "InviteScreen.h"
#import "NavigationManager.h"
#import "ApplicationManager.h"
#import "MVPlaceSearchTextField.h"
#import "DateTools.h"

@interface EventCreateScreen () <UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate, PlaceSearchTextFieldDelegate>

@end

NavigationManager *navigationManager;
ApplicationManager *applicationManager;
UserManager *userManager;
EventManager *eventManager;

UITextField *currentTextField;
UITextView *currentTextView;
UIPickerView *pickerView;
NSArray *locationArray;
UIEdgeInsets originalTableContentInset;
UIDatePicker *datePicker;
NSDateFormatter *DateFormatter;
UIView *inputAccessoryView;

@implementation EventCreateScreen
{
    CGPoint contentOffsetForSearchTextField;
    NSDate * fromDate;
    NSDate * fromTime;
    NSDate * tillDate;
    BOOL fromAndTillAreSame;
}

@synthesize isInEditMode;
@synthesize eventEntity;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initializeUI];
    [self initializeObjects];
}


#pragma mark - Initialize UI
- (void)initializeUI
{
    navigationManager = [NavigationManager sharedInstance];
    applicationManager = [ApplicationManager sharedInstance];
    userManager = [UserManager sharedInstance];
    eventManager = [EventManager sharedInstance];
}

#pragma mark - Initialize Objects
- (void)initializeObjects
{
    datePicker = [[UIDatePicker alloc]init];
    [datePicker setDate:[NSDate date]];
    [datePicker addTarget:self action:@selector(pickerDateChange:) forControlEvents:UIControlEventValueChanged];
    DateFormatter = [[NSDateFormatter alloc]init];
    
    pickerView = [[UIPickerView alloc] init];
    [pickerView setDataSource: self];
    [pickerView setDelegate: self];
    pickerView.showsSelectionIndicator = YES;
    
    locationArray = [[NSArray alloc]initWithObjects:@"Mumbai, India", @"Pune, India",@"Kolkata, India",@"Banglore, India",@"Surat, India",@"Baroda, India",@"Delhi, India", nil];
    
    if (self.isInEditMode) {
        
        [_btnCreateEvent setTitle:@"Save & Invite People" forState:UIControlStateNormal];
    }
    else {
    
        if (!_isBeingCopied)
        {
            if (!eventManager.eventBeingCreated)
            {
                eventManager.eventBeingCreated = [EventEntity new];
                eventManager.eventBeingCreated.city = applicationManager.currentCity;
                eventManager.eventBeingCreated.country = applicationManager.currentCountry;
            }
            
            self.eventEntity = eventManager.eventBeingCreated;
        }
        
        [_btnCreateEvent setTitle:@"Create & Invite People" forState:UIControlStateNormal];
    }
}

- (void)configureSearchTextField:(MVPlaceSearchTextField *)txtPlaceSearch CellRect:(CGRect)cellRect
{
    txtPlaceSearch.placeSearchDelegate                 = self;
    txtPlaceSearch.strApiKey                           = @"AIzaSyCDi2dklT-95tEHqYoE7Tklwzn3eJP-MtM";
    txtPlaceSearch.superViewOfList                     = self.view;
    txtPlaceSearch.autoCompleteShouldHideOnSelection   = YES;
    txtPlaceSearch.maximumNumberOfAutoCompleteRows     = 5;
    
    //Optional Properties
    txtPlaceSearch.autoCompleteRegularFontName =  @"HelveticaNeue-Bold";
    txtPlaceSearch.autoCompleteBoldFontName = @"HelveticaNeue";
    txtPlaceSearch.autoCompleteTableCornerRadius=0.0;
    txtPlaceSearch.autoCompleteRowHeight=35;
    txtPlaceSearch.autoCompleteTableCellTextColor=[UIColor colorWithWhite:0.131 alpha:1.000];
    txtPlaceSearch.autoCompleteFontSize=14;
    txtPlaceSearch.autoCompleteTableBorderWidth=1.0;
    txtPlaceSearch.showTextFieldDropShadowWhenAutoCompleteTableIsOpen=NO;
    txtPlaceSearch.autoCompleteShouldHideOnSelection=YES;
    txtPlaceSearch.autoCompleteShouldHideClosingKeyboard=YES;
    txtPlaceSearch.autoCompleteShouldSelectOnExactMatchAutomatically = YES;
    txtPlaceSearch.autoCompleteTableFrame = CGRectMake(10, 50, self.view.frame.size.width - 20, 200.0);
    
    if (contentOffsetForSearchTextField.y <= 0)
    {
        contentOffsetForSearchTextField = TableView.contentOffset;
        contentOffsetForSearchTextField.y = cellRect.origin.y;
    }
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    if (self.isInEditMode)
        self.title = @"Edit Event";
    else
        self.title = @"Create an Event";
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}


#pragma mark - TableView Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 9;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
        return 60;
    
    if (indexPath.row == 3)
        return 80;
        
    if(indexPath.row == 4 || indexPath.row == 7)
        return 10.0;
    else if(indexPath.row == 8)
        return 80.0;
    return 50.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    switch (indexPath.row) {
        case 0:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"NameCell"];
            if (cell == nil)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"NameCell"];
            
            UITextView *txtEventName = (UITextView *)[cell.contentView viewWithTag:101];

            txtEventName.delegate = self;
            txtEventName.text = eventEntity.eventName.length > 0 ? eventEntity.eventName : @"NAME OF THE EVENT";
            txtEventName.textColor = eventEntity.eventName.length > 0 ? [UIColor blackColor] : [UIColor colorWithWhite:0.667 alpha:0.750];
            txtEventName.font = [UIFont fontWithName:@"Roboto-Regular" size:14.0f];
            txtEventName.contentInset = UIEdgeInsetsMake(25, -2, 0, 0);
            txtEventName.inputAccessoryView = [self addAccessoryView];
        }
            break;
        case 1:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"DateCell"];
            if (cell == nil)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DateCell"];
            
            UITextField *txtStartDate = (UITextField *)[cell.contentView viewWithTag:102];
            txtStartDate.delegate = self;
            txtStartDate.text = [eventEntity.startDate stringByReplacingOccurrencesOfString:@" 00:00:00" withString:@""];
            txtStartDate.inputView = datePicker;
            
            UITextField *txtEndDate = (UITextField *)[cell.contentView viewWithTag:103];
            txtEndDate.delegate = self;
            txtEndDate.text = [eventEntity.endDate stringByReplacingOccurrencesOfString:@" 00:00:00" withString:@""];
            txtEndDate.inputView = datePicker;
        }
            break;
            
        case 2:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"TimeCell"];
            if (cell == nil)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TimeCell"];
            
            UITextField *txtFromTime = (UITextField *)[cell.contentView viewWithTag:104];
            txtFromTime.delegate = self;
            txtFromTime.text = eventEntity.fromTime;
            txtFromTime.inputView = datePicker;
            
            UITextField *txtToTime = (UITextField *)[cell.contentView viewWithTag:105];
            txtToTime.delegate = self;
            txtToTime.text = eventEntity.toTime;
            txtToTime.inputView = datePicker;
        }
            break;
            
        case 3:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"AboutCell"];
            if (cell == nil)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"AboutCell"];
            
            UITextView *txtAbout = (UITextView *)[cell.contentView viewWithTag:106];
            txtAbout.delegate = self;
            txtAbout.text = eventEntity.eventDesc.length > 0 ? eventEntity.eventDesc : @"ABOUT THE EVENT";
            txtAbout.textColor = eventEntity.eventDesc.length > 0 ? [UIColor blackColor] : [UIColor colorWithWhite:0.667 alpha:0.750];
            txtAbout.font = [UIFont fontWithName:@"Roboto-Regular" size:14.0f];
            txtAbout.contentInset = UIEdgeInsetsMake(45, -2, 0, 0);
            txtAbout.inputAccessoryView = [self addAccessoryView];
          }
            break;
            
        case 4:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"SpaceCell"];
            if (cell == nil)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SpaceCell"];
        }
            break;
            
        case 5:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"Address1Cell"];
            if (cell == nil)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Address1Cell"];
            
            UITextView *txtEventAddress = (UITextView *)[cell.contentView viewWithTag:107];
            txtEventAddress.delegate = self;
            txtEventAddress.text = eventEntity.address.length > 0 ? eventEntity.address : @"ADDRESS";
            txtEventAddress.textColor = eventEntity.address.length > 0 ? [UIColor blackColor] : [UIColor colorWithWhite:0.667 alpha:0.750];
            txtEventAddress.font = [UIFont fontWithName:@"Roboto-Regular" size:14.0f];
            txtEventAddress.contentInset = UIEdgeInsetsMake(25, -2, 0, 0);
            txtEventAddress.inputAccessoryView = [self addAccessoryView];
        }
            break;
            
        case 6:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"Address2Cell"];
            if (cell == nil)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Address2Cell"];
            
            MVPlaceSearchTextField *txtCityState = (MVPlaceSearchTextField *)[cell.contentView viewWithTag:108];
            txtCityState.delegate = self;
            NSMutableArray * locationStringArray = [NSMutableArray new];
            if (eventEntity.city.length > 0)
                [locationStringArray addObject:eventEntity.city];
            
            if (eventEntity.country.length > 0)
                [locationStringArray addObject:eventEntity.country];
            
            txtCityState.text = [locationStringArray componentsJoinedByString:@", "];
            CGRect cellRect = [tableView rectForRowAtIndexPath:indexPath];
            [self configureSearchTextField:txtCityState CellRect:cellRect];
            
            UIButton *btnChange = (UIButton *)[cell.contentView viewWithTag:111];
            [btnChange addTarget:self action:@selector(changeAddress:) forControlEvents:UIControlEventTouchUpInside];
        }
            break;
            
        case 7:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"SpaceCell"];
            if (cell == nil)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SpaceCell"];
        }
            break;
            
        case 8:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"ChargesCell"];
            if (cell == nil)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ChargesCell"];
            
            UITextField *txtAmount = (UITextField *)[cell.contentView viewWithTag:109];
            txtAmount.delegate = self;
            txtAmount.text = eventEntity.charges;
            
            UITextField *txtGuestCharges = (UITextField *)[cell.contentView viewWithTag:110];
            txtGuestCharges.delegate = self;
            txtGuestCharges.text = eventEntity.guestCharges;
            
            UIButton *btnIsPaid = (UIButton *)[cell.contentView viewWithTag:112];
            [btnIsPaid addTarget:self action:@selector(isPaidSwitch:) forControlEvents:UIControlEventTouchUpInside];
            btnIsPaid.selected = [eventEntity.isPaid boolValue];
            
            UIButton *btnGuestsAllowed = (UIButton *)[cell.contentView viewWithTag:113];
            [btnGuestsAllowed addTarget:self action:@selector(guestAllowedSwitch:) forControlEvents:UIControlEventTouchUpInside];
            btnGuestsAllowed.selected = [eventEntity.isGuestAllowed boolValue];
            
            txtAmount.hidden =! btnIsPaid.selected;
            txtGuestCharges.hidden =! btnGuestsAllowed.selected;
        }
            break;
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - Button Actions
- (void)isPaidSwitch:(UIButton*)sender
{
    if([eventEntity.isPaid boolValue])
        eventEntity.isPaid = @"0";
    else
        eventEntity.isPaid = @"1";
    [TableView reloadData];
}

- (void)guestAllowedSwitch:(UIButton*)sender
{
    if([eventEntity.isGuestAllowed boolValue])
        eventEntity.isGuestAllowed = @"0";
    else
        eventEntity.isGuestAllowed = @"1";
    [TableView reloadData];
}

- (void)changeAddress:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:TableView];
    NSIndexPath *indexPath = [TableView indexPathForRowAtPoint:buttonPosition];
    UITableViewCell *cell = [TableView cellForRowAtIndexPath:indexPath];
    
    UITextField *txtAddress = (UITextField*)[cell.contentView viewWithTag:108];
    eventEntity.city = @"";
    eventEntity.country = @"";
    txtAddress.text = @"";
    [txtAddress becomeFirstResponder];
}

#pragma mark - PickerView Delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return locationArray.count;
}
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return locationArray[row];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    currentTextField.text = locationArray[row];
}

#pragma mark - Date Picker Delegate
-(void)pickerDateChange:(id)sender
{
    currentTextField.text = [DateFormatter stringFromDate:datePicker.date];
    
    switch (currentTextField.tag) {
        case 102:
            fromDate = datePicker.date;
            [self validateFromAndTillDates];
            break;
            
        case 103:
            tillDate = datePicker.date;
            [self validateFromAndTillDates];
            break;
            
        case 104:
            fromTime = datePicker.date;
            break;
            
        case 105:

            break;
            
        default:
            break;
    }
}

- (void)validateFromAndTillDates
{
    fromAndTillAreSame = NO;
    
    if (fromDate && tillDate)
    {
        if ([fromDate isEqualToDate:tillDate]) {
            fromAndTillAreSame = YES;
        }
    }
}

#pragma mark - Invite People
- (IBAction)invitePeoples:(id)sender
{
    NSDate * frmDt;
    NSDate * tilDt;
    DateFormatter.dateFormat = @"dd-MM-yyyy";
    if (eventEntity.startDate.length > 0) {
        frmDt = [DateFormatter dateFromString:[eventEntity.startDate stringByReplacingOccurrencesOfString:@" 00:00:00" withString:@""]];
    }
    
    if (eventEntity.endDate.length > 0) {
        tilDt = [DateFormatter dateFromString:[eventEntity.endDate stringByReplacingOccurrencesOfString:@" 00:00:00" withString:@""]];
    }
    
    if (frmDt && tilDt && [frmDt isLaterThan:tilDt]) {
        [[[UIAlertView alloc] initWithTitle:@"Jambo" message:@"From date must be smaller or equal to Till date" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
    else if(eventEntity.eventName.length == 0 || eventEntity.startDate.length == 0 || eventEntity.fromTime.length == 0 || eventEntity.address.length == 0 || eventEntity.city.length == 0)
    {
        [[[UIAlertView alloc] initWithTitle:@"Jambo" message:@"Please provide required information." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
    else
    {
        InviteScreen *inviteScreen = (InviteScreen *)[navigationManager getScreen:nsInviteScreen];
        inviteScreen.isInEditMode = self.isInEditMode;
        inviteScreen.inviteType = InviteTypeEvent;
        inviteScreen.eventEntity = eventEntity;
        inviteScreen.alumniArray = userManager.alumniArray;
        [self.navigationController pushViewController:inviteScreen animated:YES];
    }
}

#pragma mark - TextField Delegate Method
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    currentTextField = textField;
    
    switch (textField.tag)
    {
        case 102:
            datePicker.datePickerMode = UIDatePickerModeDate;
            DateFormatter.dateFormat = @"dd-MM-yyyy";
            [datePicker setMinimumDate:nil];
            textField.inputAccessoryView = [self addAccessoryView];
            
            if (textField.text.length == 0) {
                textField.text = [DateFormatter stringFromDate:datePicker.date];
            }
            break;
            
        case 103:
            datePicker.datePickerMode = UIDatePickerModeDate;
            DateFormatter.dateFormat = @"dd-MM-yyyy";
            
            if (fromDate)
                [datePicker setMinimumDate:fromDate];
            
            textField.inputAccessoryView = [self addAccessoryView];
            
            if (textField.text.length == 0)
                textField.text = [DateFormatter stringFromDate:datePicker.date];
            break;
            
        case 104:
            datePicker.datePickerMode = UIDatePickerModeTime;
            DateFormatter.dateFormat = @"HH:mm aa";
            [datePicker setMinimumDate:nil];
            textField.inputAccessoryView = [self addAccessoryView];
            
            if (textField.text.length == 0)
                textField.text = [DateFormatter stringFromDate:datePicker.date];
            break;
            
        case 105:
            datePicker.datePickerMode = UIDatePickerModeTime;
            DateFormatter.dateFormat = @"HH:mm aa";
            if (fromAndTillAreSame)
                [datePicker setMinimumDate:fromTime];
            textField.inputAccessoryView = [self addAccessoryView];

            if (textField.text.length == 0)
                textField.text = [DateFormatter stringFromDate:datePicker.date];
            break;
            
        case 108:
            [TableView setContentOffset:contentOffsetForSearchTextField animated:YES];
            break;
            
        case 109:
        case 110:
            textField.inputAccessoryView = [self addAccessoryView];
        default:
            break;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.text.length > 0) {
        
        switch (textField.tag) {
               
            case 102:
                eventEntity.startDate = [NSString stringWithFormat:@"%@",textField.text];
                break;
                
            case 103:
                eventEntity.endDate = [NSString stringWithFormat:@"%@",textField.text];
                break;
                
            case 104:
                eventEntity.fromTime = textField.text;
                break;
                
            case 105:
                eventEntity.toTime = textField.text;
                break;
                
            case 107:
                eventEntity.address = textField.text;
                break;
                
            case 108:
                eventEntity.city = textField.text;
                eventEntity.country = @"";
                break;
                
            default:
                break;
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Textview delegate
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [textView setInputAccessoryView:[self addAccessoryView]];

    NSString * placeHolderText;
    
    if (textView.tag == 101)
        placeHolderText = @"NAME OF THE EVENT";
    else if (textView.tag == 106)
        placeHolderText = @"ABOUT THE EVENT";
    else if (textView.tag == 107)
        placeHolderText = @"ADDRESS";
    
    if ([textView.text isEqualToString:placeHolderText]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
    
    currentTextView = textView;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    NSString * placeHolderText;
    
    switch (textView.tag)
    {
        case 101:
            eventEntity.eventName = textView.text;
            placeHolderText = @"NAME OF THE EVENT";
            break;
            
            
        case 106:
            eventEntity.eventDesc = textView.text;
            placeHolderText = @"ABOUT THE EVENT";
            break;
            
        case 107:
            eventEntity.address = textView.text;
            placeHolderText = @"ADDRESS";
            break;
            
        default:
            break;
    }
    
    if ([textView.text isEqualToString:@""]) {
        textView.text = placeHolderText;
        textView.textColor = [UIColor colorWithWhite:0.667 alpha:0.750];
    }
    
    [textView resignFirstResponder];
}

#pragma mark - Accessory
- (UIView *)addAccessoryView
{
    if (!inputAccessoryView)
    {
        CGRect accessFrame = CGRectMake(0.0, 0.0, self.view.frame.size.width, 40.0);
        inputAccessoryView = [[UIView alloc] initWithFrame:accessFrame];
        inputAccessoryView.backgroundColor = [UIColor colorWithRed:182.0/255.0 green:190.0/255.0 blue:203.0/255.0 alpha:1.0];
        UIButton *compButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        compButton.frame = CGRectMake(accessFrame.size.width - 70, 0.0, 60.0, 40.0);
        [compButton setTitle: @"Done" forState:UIControlStateNormal];
        [compButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        compButton.titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
        [compButton addTarget:self action:@selector(hideKeyboard)
             forControlEvents:UIControlEventTouchUpInside];
        [compButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [inputAccessoryView addSubview:compButton];
    }
    return inputAccessoryView;
}

- (void)hideKeyboard
{
    [currentTextField resignFirstResponder];
    
    if(currentTextView)
        [currentTextView resignFirstResponder];
}

#pragma mark - Keyboard
- (void)keyboardDidShow:(NSNotification *)notification {
    
    // calculate the size of the keyboard and how much is and isn't covering the tableview
    NSDictionary *keyboardInfo = [notification userInfo];
    CGRect keyboardFrame = [keyboardInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardFrame = [TableView.window convertRect:keyboardFrame toView:TableView.superview];
    CGFloat heightOfTableViewThatIsCoveredByKeyboard = TableView.frame.origin.y + TableView.frame.size.height - keyboardFrame.origin.y;
    CGFloat heightOfTableViewThatIsNotCoveredByKeyboard = TableView.frame.size.height - heightOfTableViewThatIsCoveredByKeyboard;
    
    UIEdgeInsets tableContentInset = TableView.contentInset;
    originalTableContentInset = tableContentInset;
    tableContentInset.bottom = heightOfTableViewThatIsCoveredByKeyboard;
    
    UIEdgeInsets tableScrollIndicatorInsets = TableView.scrollIndicatorInsets;
    tableScrollIndicatorInsets.bottom += heightOfTableViewThatIsCoveredByKeyboard;

    UIView *firstResponder = FXFormsFirstResponder(TableView);
    
    if (![firstResponder isKindOfClass:[MVPlaceSearchTextField class]]) {

        // adjust the tableview insets by however much the keyboard is overlapping the tableview
        TableView.contentInset = tableContentInset;
        TableView.scrollIndicatorInsets = tableScrollIndicatorInsets;
    }
    
    if ([firstResponder isKindOfClass:[UITextView class]]) {
        
        UITextView *textView = (UITextView *)firstResponder;
        
        // calculate the position of the cursor in the textView
        NSRange range = textView.selectedRange;
        UITextPosition *beginning = textView.beginningOfDocument;
        UITextPosition *start = [textView positionFromPosition:beginning offset:range.location];
        UITextPosition *end = [textView positionFromPosition:start offset:range.length];
        CGRect caretFrame = [textView caretRectForPosition:end];
        
        // convert the cursor to the same coordinate system as the tableview
        CGRect caretViewFrame = [textView convertRect:caretFrame toView:TableView.superview];
        
        // padding makes sure that the cursor isn't sitting just above the keyboard and will adjust to 3 lines of text worth above keyboard
        CGFloat padding = textView.font.lineHeight * 3;
        CGFloat keyboardToCursorDifference = (caretViewFrame.origin.y + caretViewFrame.size.height) - heightOfTableViewThatIsNotCoveredByKeyboard + padding;

        if (keyboardToCursorDifference > 0.0f && textView.tag != 108) {
            // adjust offset by this difference
            CGPoint contentOffset = TableView.contentOffset;
            contentOffset.y += keyboardToCursorDifference;
            [TableView setContentOffset:contentOffset animated:YES];
        }
    }
}

- (void)keyboardWillHide:(NSNotification *)note
{
    NSDictionary *keyboardInfo = [note userInfo];
    UIEdgeInsets tableScrollIndicatorInsets = TableView.scrollIndicatorInsets;
    tableScrollIndicatorInsets.bottom = 0;
    
    //restore insets
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:(UIViewAnimationCurve)keyboardInfo[UIKeyboardAnimationCurveUserInfoKey]];
    [UIView setAnimationDuration:[keyboardInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
    TableView.contentInset = originalTableContentInset;
    TableView.scrollIndicatorInsets = tableScrollIndicatorInsets;
    originalTableContentInset = UIEdgeInsetsZero;
    [UIView commitAnimations];
}

static UIView *FXFormsFirstResponder(UIView *view)
{
    if ([view isFirstResponder])
    {
        return view;
    }
    for (UIView *subview in view.subviews)
    {
        UIView *responder = FXFormsFirstResponder(subview);
        if (responder)
        {
            return responder;
        }
    }
    return nil;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [currentTextField resignFirstResponder];
}

#pragma mark - Place search Textfield Delegates
-(void)placeSearchResponseForSelectedPlace:(NSMutableDictionary*)responseDict
{
    [self.view endEditing:YES];
    NSLog(@"%@",responseDict);
    
    NSArray *aDictLocation=[[responseDict objectForKey:@"result"] objectForKey:@"address_components"];
    
    
    NSLog(@"SELECTED ADDRESS :%@",aDictLocation);
}

-(void)placeSearchWillShowResult{
    
}

-(void)placeSearchWillHideResult{
    
}

-(void)placeSearchResultCell:(UITableViewCell *)cell withPlaceObject:(PlaceObject *)placeObject atIndex:(NSInteger)index{
    if(index%2==0){
        cell.contentView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    }else{
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
}

#pragma mark - viewWillDisappear
- (void)viewWillDisappear:(BOOL)animated {

    [super viewWillDisappear:YES];
    self.title = @"";
    
    [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardDidShowNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillHideNotification];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

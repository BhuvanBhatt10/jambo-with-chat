//
//  GroupCell.m
//  Jambo
//
//  Created by Rahul Chandera on 27/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "GroupCell.h"
#import "QuestionsCollectionView.h"
#import "NavigationManager.h"

@interface GroupCell ()
@property (strong, nonatomic) QuestionsCollectionView *QuestionsCollectionView;
@end

@implementation GroupCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.QuestionsCollectionView = (QuestionsCollectionView*)[[NavigationManager sharedInstance] getScreen:nsQuestionsCollectionView];
        self.QuestionsCollectionView.view.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
        [self.contentView addSubview:self.QuestionsCollectionView.view];
    }
    return self;
}

- (void)setQuestionsData:(GroupEntity*)groupEntity Delegate:(id)parent Index:(int)index
{
    self.QuestionsCollectionView.delegate = parent;
    self.QuestionsCollectionView.CollectionView.tag = index;
    [self.QuestionsCollectionView setQuestionsData:groupEntity];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end

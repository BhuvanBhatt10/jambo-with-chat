//
//  GroupCell.h
//  Jambo
//
//  Created by Rahul Chandera on 27/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupEntity.h"

@interface GroupCell : UITableViewCell

- (void)setQuestionsData:(GroupEntity*)groupEntity Delegate:(id)parent Index:(int)index;

@end

//
//  BuildProfileScreen.h
//  Jambo
//
//  Created by Rahul Chandera on 09/05/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuildProfileBasic : UIViewController
{
    IBOutlet UITableView * TableView;
}
@property (nonatomic, assign) UIEdgeInsets originalTableContentInset;

- (IBAction)saveContinueHandler:(id)sender;

@end

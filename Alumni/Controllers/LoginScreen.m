//
//  LoginSignupScreen.m
//  Alumni
//
//  Created by Rahul Chandera on 22/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "LoginScreen.h"
#import "AppConstants.h"
#import "NavigationManager.h"
#import "ProcessIndicator.h"
#import "UserManager.h"
#import "ApplicationManager.h"
#import "DiscussionManager.h"
#import "CoreDataManager.h"
#import "QuickBloxHelper.h"

@interface LoginScreen () <UserManagerDelegate>

@end

NavigationManager *navigationManager;
ProcessIndicator *processIndicator;
UserManager *userManager;
ApplicationManager *applicationManager;
DiscussionManager *discussionManager;
CoreDataManager *coreDataManager;

@implementation LoginScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeObjects];
}

#pragma mark - Initialize Objects
- (void)initializeObjects
{
    navigationManager = [NavigationManager sharedInstance];
    processIndicator = [ProcessIndicator sharedInstance];
    userManager = [UserManager sharedInstance];
    applicationManager =[ApplicationManager sharedInstance];
    discussionManager = [DiscussionManager sharedInstance];
    coreDataManager = [CoreDataManager sharedInstance];
    [navigationManager NavigationBarTheme:self.navigationController];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
    
    if ([[[NSUserDefaults standardUserDefaults]valueForKey:@"IsUserLogin"] boolValue])
    {
        loginView.hidden = YES;
        
        UserEntity * currentUserEntity = [coreDataManager getUserObject];
        
        if (currentUserEntity)
        {
            [indicator startAnimating];
            userManager.delegate = self;
            [userManager getUserDetail:currentUserEntity];
        }
        else
        {
            [self.navigationController pushViewController:[navigationManager getScreen:nsHomeScreen] animated:NO];
        }
    }
    else {
    
        [indicator stopAnimating];
        loginView.hidden = NO;
    }
}


- (IBAction)loginHandler:(id)sender {
    
//    txtEmail.text = @"mahesh21688@gmail.com";
//    txtPassword.text = @"abc";
    //txtEmail.text = @"rahul.crc@gmail.com";
    //txtPassword.text = @"qazqaz";
//    txtEmail.text = @"arun@bonoboz.in";
//    txtPassword.text = @"arun";
//    txtEmail.text = @"arun.avasthi@gmail.com";
//    txtPassword.text = @"8012";
    
    NSString *strMsg = nil;
    if (txtEmail.text.length == 0)
    {
        [txtEmail resignFirstResponder];
        [txtPassword resignFirstResponder];
        strMsg = @"Email ID should not be empty";
    }
    else if(txtPassword.text.length == 0)
    {
        [txtPassword resignFirstResponder];
        strMsg = @"Password should not be empty";
    }
    
    if(strMsg.length > 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login" message:strMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        strMsg = nil;
        return;
    }
    
    [processIndicator showProcessIndicatorInView:self.view Message:@"Processing..."];
    userManager.delegate = self;
    [userManager userLogin:txtEmail.text Password:txtPassword.text];
}

#pragma mark - Request Complete
-(void)requestCompleted:(id)response{
    
    [processIndicator hideProcessIndicator];
    [indicator stopAnimating];
    
    if ([[response objectForKey:REQUEST_KEY] integerValue] == requestGetInstituteList)
    {
        if (![[response objectForKey:RESULT_KEY] boolValue])
        {
            [processIndicator showProcessIndicatorInView:self.view Message:@"Processing..."];
            [userManager getCourseList];
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:@"Login" message:@"Something went wrong" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
    }
    
    if ([[response objectForKey:REQUEST_KEY] integerValue] == requestGetCourseList)
    {
        if (![[response objectForKey:RESULT_KEY] boolValue])
        {
            [self.navigationController pushViewController:[navigationManager getScreen:nsBuildProfileBasic] animated:YES];
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:@"Login" message:@"Something went wrong" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
    }
    
    if ([[response objectForKey:REQUEST_KEY] integerValue] == requestUserLogin) {

        if (![[response objectForKey:RESULT_KEY] boolValue])
        {
            if (userManager.userEntity.isVerified == 1) {
                
                userManager.userEntity.password = txtPassword.text;
                [coreDataManager saveUserObject:userManager.userEntity];
                [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"IsUserLogin"];
                [applicationManager registerAppForNotification];
                [self.navigationController pushViewController:[navigationManager getScreen:nsHomeScreen] animated:YES];
            }
            else {
                
                userManager.userEntity.password = txtPassword.text;
                [coreDataManager saveUserObject:userManager.userEntity];
                [applicationManager getSkillList];
                [discussionManager getGroupsList];
                
                [processIndicator showProcessIndicatorInView:self.view Message:@"Processing..."];
                userManager.delegate = self;
                [userManager getInstituteList];
            }
        }
        else
        {
            if ([[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_INTERNET_NOT_AVAILABLE_CODE)
            {
                [[[UIAlertView alloc] initWithTitle:@"Login" message:@"No internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
            }
            else
            {
                [[[UIAlertView alloc] initWithTitle:@"Login" message:@"Invalid credentials" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
            }
        }
    }
    
    if ([[response objectForKey:REQUEST_KEY] intValue] == requestGetUserDetail)
    {
        [userManager getMyInstitute];
        [self.navigationController pushViewController:[navigationManager getScreen:nsHomeScreen] animated:YES];
    }
}

#pragma mark - TextField Delegate Method
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == txtEmail)
    {
        [txtPassword becomeFirstResponder];
        return NO;
    }
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

//
//  EventsScreen.m
//  Alumni
//
//  Created by Rahul Chandera on 31/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "EventsScreen.h"
#import "NavigationManager.h"
#import "AppConstants.h"
#import "EventEntity.h"
#import "UIImageView+WebCache.h"
#import "EventDetailsScreen.h"
#import <EventKit/EventKit.h>
#import "ADLivelyCollectionView.h"
#import "EventCreateScreen.h"
#import "InviteScreen.h"
#import "ProcessIndicator.h"

#define BUTTON_ATTENDING_TAG 131
#define BUTTON_MAYBE_TAG     132
#define BUTTON_NOPE_TAG      133

@interface EventsScreen ()

@end

NavigationManager *navigationManager;
EventManager *eventManager;
UserManager *userManager;
ProcessIndicator *processIndicator;
UIRefreshControl *refreshControl;
NSDateFormatter *dateFormatter;

@implementation EventsScreen
{
    NSIndexPath * indexPathForResponcingEvent;
    int responceBeingSet;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initializeUI];
    [self initializeObjects];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(createdOrUpdatedEvent:) name:NOTIFICATION_CREATED_UPDATED_EVENT object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    eventManager.delegate = self;
    userManager.delegate = self;
}

#pragma mark - Notification listeners

- (void)createdOrUpdatedEvent:(NSNotification *)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        ADLivelyCollectionView * livelyCollectionView = (ADLivelyCollectionView *)CollectionView;
        livelyCollectionView.initialCellTransformBlock = nil;
        [CollectionView reloadData];
        [self performSelector:@selector(setCollectionAnimation) withObject:nil afterDelay:2.0f];
    });
}

- (void)setCollectionAnimation
{
    ADLivelyCollectionView * livelyCollectionView = (ADLivelyCollectionView *)CollectionView;
    livelyCollectionView.initialCellTransformBlock = ADLivelyTransformWave;
}

#pragma mark - Initialize UI
- (void)initializeUI {
    
    self.title = @"Events";
    
    self.navigationController.navigationBarHidden = NO;
    
    //Menu Button
    UIButton *btnMenu = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [btnMenu addTarget:self action:@selector(openMenu) forControlEvents:UIControlEventTouchUpInside];
    [btnMenu setImage:[UIImage imageNamed:@"menu.png"] forState:UIControlStateNormal];
    [btnMenu setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    UIBarButtonItem *bbiMenu = [[UIBarButtonItem alloc]initWithCustomView:btnMenu];
    bbiMenu.style = UIBarButtonItemStylePlain;
    self.navigationItem.leftBarButtonItem = bbiMenu;
    
    ADLivelyCollectionView * livelyCollectionView = (ADLivelyCollectionView *)CollectionView;
    livelyCollectionView.initialCellTransformBlock = ADLivelyTransformWave;
}

#pragma mark - Initialize Objects
- (void)initializeObjects
{
    navigationManager = [NavigationManager sharedInstance];
    eventManager = [EventManager sharedInstance];
    userManager = [UserManager sharedInstance];
    processIndicator = [ProcessIndicator sharedInstance];
    
    dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateFormat = @"dd/MM/yyyy HH:mm:ss";
    
    refreshControl = [[UIRefreshControl alloc]init];
    [CollectionView addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];

    [refreshControl beginRefreshing];
    [self refreshTable];
}



#pragma mark - Refresh Table
- (void)refreshTable {
    
    eventManager.delegate = self;
    [eventManager getEventsList];
}

- (IBAction)tabHandler:(UIButton*)sender {

    if(sender == btnAll) {
        
        btnAll.selected = YES;
        btnMyEvents.selected = NO;
        btnInvited.selected = NO;
    }
    else if(sender == btnMyEvents) {
        
        btnAll.selected = NO;
        btnMyEvents.selected = YES;
        btnInvited.selected = NO;
    }
    else {
        
        btnAll.selected = NO;
        btnMyEvents.selected = NO;
        btnInvited.selected = YES;
    }
    
    CollectionView.tag = sender.tag;
    [CollectionView reloadData];
}

- (IBAction)btnInviteEditClicked:(UIButton *)sender
{
    NSIndexPath *indexPath;
    indexPath = [CollectionView indexPathForItemAtPoint:[CollectionView convertPoint:sender.center fromView:sender.superview]];
    
    EventEntity * eventEntity;
    
    if(CollectionView.tag == 2)
        eventEntity = [eventManager.myEventsArray objectAtIndex:indexPath.row];

    if (sender.tag == 121)
    {
        InviteScreen *inviteScreen = (InviteScreen *)[navigationManager getScreen:nsInviteScreen];
        inviteScreen.eventEntity = eventEntity;
        inviteScreen.inviteType = InviteTypeEvent;
        inviteScreen.alumniArray = userManager.alumniArray;
        [self.navigationController pushViewController:inviteScreen animated:YES];
    }
    
    if (sender.tag == 122)
    {
        EventCreateScreen *eventCreateScreen = (EventCreateScreen *)[navigationManager getScreen:nsEventCreateScreen];
        eventCreateScreen.isInEditMode = YES;
        eventCreateScreen.eventEntity = eventEntity;
        [self.navigationController pushViewController:eventCreateScreen animated:YES];
    }
}

- (IBAction)btnAvailabilityClick:(UIButton *)sender
{
    NSIndexPath *indexPath;
    indexPath = [CollectionView indexPathForItemAtPoint:[CollectionView convertPoint:sender.center fromView:sender.superview]];
    
    if (indexPath)
    {
        EventEntity * eventEntity = [eventManager.invitedEventsArray objectAtIndex:indexPath.row];
        
        int responceStatus = 1;
        
        if (sender.tag == BUTTON_ATTENDING_TAG)
            responceStatus = 2;
        
        if (sender.tag == BUTTON_MAYBE_TAG)
            responceStatus = 3;
        
        if (sender.tag == BUTTON_NOPE_TAG)
            responceStatus = 4;
        
        indexPathForResponcingEvent = indexPath;
        responceBeingSet = responceStatus;
        
        [processIndicator showProcessIndicatorInView:self.view Message:@"Processing..."];
        eventManager.delegate = self;
        [eventManager responseEventInvite:eventEntity.eventId ResponseStatus:responceStatus];
    }
}

- (IBAction)btnCopyEventClicked:(UIButton *)sender
{
    NSIndexPath *indexPath;
    indexPath = [CollectionView indexPathForItemAtPoint:[CollectionView convertPoint:sender.center fromView:sender.superview]];
    
    if (indexPath)
    {
        EventEntity * eventEntity = [eventManager.myEventsArray objectAtIndex:indexPath.row];
        eventManager.eventBeingCopied = [eventManager getEventCopyFromEvent:(MyEventEntity *)eventEntity];
        EventCreateScreen *eventCreateScreen = (EventCreateScreen *)[navigationManager getScreen:nsEventCreateScreen];
        eventCreateScreen.isInEditMode = NO;
        eventCreateScreen.isBeingCopied = YES;
        eventCreateScreen.eventEntity = (EventEntity *)eventManager.eventBeingCopied;
        [self.navigationController pushViewController:eventCreateScreen animated:YES];
    }
}

#pragma mark - Request Complete
-(void)requestCompleted:(id)response{
    
    [refreshControl endRefreshing];

    [processIndicator hideProcessIndicator];
    
    if([[response objectForKey:REQUEST_KEY] intValue] == requestResponseEvent)
    {
        EventEntity * eventEntityToModify = [eventManager.invitedEventsArray objectAtIndex:indexPathForResponcingEvent.row];
        eventEntityToModify.status = [NSString stringWithFormat:@"%d",responceBeingSet];

        [CollectionView reloadItemsAtIndexPaths:@[indexPathForResponcingEvent]];
    }
    else if (eventManager.allEventsArray.count>0 || eventManager.myEventsArray.count>0 || eventManager.invitedEventsArray.count>0)
    {
        blankScreen.hidden = YES;
        
        [btnAll setTitle:[NSString stringWithFormat:@"ALL (%d)",[eventManager getEventsCountForEventType:1]] forState:UIControlStateNormal];
        [btnMyEvents setTitle:[NSString stringWithFormat:@"MY EVENTS (%d)",[eventManager getEventsCountForEventType:2]] forState:UIControlStateNormal];
        [btnInvited setTitle:[NSString stringWithFormat:@"INVITED (%d)",[eventManager getEventsCountForEventType:3]] forState:UIControlStateNormal];
        [CollectionView reloadData];
    }
    else
    {
        if (eventManager.allEventsArray.count == 0 && eventManager.myEventsArray.count == 0 && eventManager.invitedEventsArray.count == 0)
            blankScreen.hidden = NO;
        
        if ([[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_INTERNET_NOT_AVAILABLE_CODE)
        {
            [[[UIAlertView alloc] initWithTitle:@"Alumni" message:@"No internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
        else if([[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_SERVER_CONNECTION_ERROR || [[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_INTERNAL_SERVICE_ERROR){
            [[[UIAlertView alloc] initWithTitle:@"Alumni" message:@"Something went wrong" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
    }
}

#pragma mark - UICollectionView
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if(collectionView.tag == 1)
        return [eventManager.allEventsArray count];
    else if(collectionView.tag == 2)
        return [eventManager.myEventsArray count];
    else
        return [eventManager.invitedEventsArray count];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(collectionView.tag == 1)
    {
        EventEntity *eventEntity = [eventManager.allEventsArray objectAtIndex:indexPath.row];
        
        if(eventEntity.inviteSet.count > 0)
            return CGSizeMake(self.view.frame.size.width-10, 210);
        else
            return CGSizeMake(self.view.frame.size.width-10, 130);
    }
    else if(collectionView.tag == 2)
    {
        return CGSizeMake(self.view.frame.size.width-10, 170);
    }
    else
    {
        EventEntity *eventEntity = [eventManager.invitedEventsArray objectAtIndex:indexPath.row];
        if(eventEntity.inviteSet.count > 0)
            return CGSizeMake(self.view.frame.size.width-10, 260);
        else
            return CGSizeMake(self.view.frame.size.width-10, 180);
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"EventCell" forIndexPath:indexPath];
    
    UIImageView *imgPhoto = (UIImageView *)[cell.contentView viewWithTag:101];
    UILabel *lblTitle = (UILabel *)[cell.contentView viewWithTag:102];
    UILabel *lblTime = (UILabel *)[cell.contentView viewWithTag:103];
    UILabel *lblAddress = (UILabel *)[cell.contentView viewWithTag:104];
    UIImageView *imgInvite1 = (UIImageView *)[cell.contentView viewWithTag:111];
    UIImageView *imgInvite2 = (UIImageView *)[cell.contentView viewWithTag:112];
    UIImageView *imgInvite3 = (UIImageView *)[cell.contentView viewWithTag:113];
    UIImageView *imgInvite4 = (UIImageView *)[cell.contentView viewWithTag:114];
    UILabel *lblInviteTotal = (UILabel *)[cell.contentView viewWithTag:115];
    UIView *viewAttending = (UIView *)[cell.contentView viewWithTag:1000];
    UIView *viewInvite = (UIView *)[cell.contentView viewWithTag:1001];
    UIView *viewAttendance = (UIView *)[cell.contentView viewWithTag:1002];
    UIButton *btnAttending = (UIButton *)[cell.contentView viewWithTag:131];
    UIButton *btnMayBe = (UIButton *)[cell.contentView viewWithTag:132];
    UIButton *btnNope = (UIButton *)[cell.contentView viewWithTag:133];
    
    imgInvite1.hidden = YES;
    imgInvite2.hidden = YES;
    imgInvite3.hidden = YES;
    imgInvite4.hidden = YES;
    lblInviteTotal.hidden = YES;
    
    EventEntity *eventEntity;
    
    if(CollectionView.tag == 1)
    {
        eventEntity = [eventManager.allEventsArray objectAtIndex:indexPath.row];
        AluminiEntity *aluminiEntity = [userManager getAlumniById:eventEntity.createdBy];
        [imgPhoto setImageWithURL:[NSURL URLWithString:aluminiEntity.profilePic] placeholderImage:[UIImage imageNamed:DEFAULT_USER_IMAGE]];
        
        int tag = 111;
        for(InviteEntity *invite in eventEntity.inviteSet)
        {
            if(invite.responseStatus == 1){
                
                AluminiEntity *aluminiEntity = [userManager getAlumniById:invite.inviteId];
                UIImageView *imgInvite = (UIImageView*)[cell.contentView viewWithTag:tag];
                imgInvite.hidden = NO;
                [imgInvite setImageWithURL:[NSURL URLWithString:aluminiEntity.profilePic] placeholderImage:[UIImage imageNamed:DEFAULT_USER_IMAGE]];
                if(tag == 114)
                    break;
                tag++;
            }
        }
        
        int totalInvite = (int)eventEntity.inviteSet.count;
        if(totalInvite>4)
        {
            lblInviteTotal.hidden = NO;
            lblInviteTotal.text = [NSString stringWithFormat:@"+%d More",totalInvite-4];
        }
        else
            lblInviteTotal.hidden = YES;
        
        //Set height of attending view based on content
        NSLayoutConstraint * attendingHeightConstraint = [self getViewConstraintById:viewAttending.constraints Identifier:@"attendingHeight"];
        
        if (tag == 111)
        {
            if (attendingHeightConstraint)
                attendingHeightConstraint.constant = 0;
        }
        else
        {
            if (attendingHeightConstraint)
                attendingHeightConstraint.constant = 80;
        }
        
        [viewAttending updateConstraints];
        
        //Set height of invite view
        NSLayoutConstraint * inviteHeightConstraint = [self getViewConstraintById:viewInvite.constraints Identifier:@"inviteHeight"];
        if (inviteHeightConstraint)
            inviteHeightConstraint.constant = 0;
        
        [viewInvite updateConstraints];
        
        //Set height of attendance view
        NSLayoutConstraint * attendanceHeightConstraint = [self getViewConstraintById:viewAttendance.constraints Identifier:@"attendanceHeight"];
        if (attendanceHeightConstraint)
            attendanceHeightConstraint.constant = 0;
        
        [viewAttendance updateConstraints];
    }
    else if(CollectionView.tag == 2) {
        
        eventEntity = [eventManager.myEventsArray objectAtIndex:indexPath.row];
        [imgPhoto setImageWithURL:[NSURL URLWithString:userManager.userEntity.profilePic] placeholderImage:[UIImage imageNamed:DEFAULT_USER_IMAGE]];
        
        int tag = 111;
//        for(InviteEntity *invite in eventEntity.inviteSet)
//        {
//            if(invite.responseStatus == 1){
//                
//                AluminiEntity *aluminiEntity = [userManager getAlumniById:invite.inviteId];
//                UIImageView *imgInvite = (UIImageView*)[cell.contentView viewWithTag:tag];
//                imgInvite.hidden = NO;
//                [imgInvite setImageWithURL:[NSURL URLWithString:aluminiEntity.profilePic] placeholderImage:[UIImage imageNamed:DEFAULT_USER_IMAGE]];
//                if(tag == 114)
//                    break;
//                tag++;
//            }
//        }
//        
//        int totalInvite = (int)eventEntity.inviteSet.count;
//        if(totalInvite>4)
//        {
//            lblInviteTotal.hidden = NO;
//            lblInviteTotal.text = [NSString stringWithFormat:@"+%d More",totalInvite-4];
//        }
//        else
//            lblInviteTotal.hidden = YES;
        
        //Set height of attending view based on content
        NSLayoutConstraint * attendingHeightConstraint = [self getViewConstraintById:viewAttending.constraints Identifier:@"attendingHeight"];
        
        if (tag == 111)
        {
            if (attendingHeightConstraint)
                attendingHeightConstraint.constant = 0;
        }
        else
        {
            if (attendingHeightConstraint)
                attendingHeightConstraint.constant = 80;
        }
        
        [viewAttending updateConstraints];
        
        
        //Set height of invite view
        NSLayoutConstraint * inviteHeightConstraint = [self getViewConstraintById:viewInvite.constraints Identifier:@"inviteHeight"];
        if (inviteHeightConstraint)
            inviteHeightConstraint.constant = 40;
        
        [viewInvite updateConstraints];
        
        //Set height of attendance view
        NSLayoutConstraint * attendanceHeightConstraint = [self getViewConstraintById:viewAttendance.constraints Identifier:@"attendanceHeight"];
        if (attendanceHeightConstraint)
            attendanceHeightConstraint.constant = 0;
        
        [viewAttendance updateConstraints];
    }
    else {
        
        eventEntity = [eventManager.invitedEventsArray objectAtIndex:indexPath.row];
        AluminiEntity *aluminiEntity = [userManager getAlumniById:eventEntity.createdBy];
        [imgPhoto setImageWithURL:[NSURL URLWithString:aluminiEntity.profilePic] placeholderImage:[UIImage imageNamed:DEFAULT_USER_IMAGE]];
        
        int tag = 111;
        for(InviteEntity *invite in eventEntity.inviteSet)
        {
            if(invite.responseStatus == 1){
                
                AluminiEntity *aluminiEntity = [userManager getAlumniById:invite.inviteId];
                UIImageView *imgInvite = (UIImageView*)[cell.contentView viewWithTag:tag];
                imgInvite.hidden = NO;
                [imgInvite setImageWithURL:[NSURL URLWithString:aluminiEntity.profilePic] placeholderImage:[UIImage imageNamed:DEFAULT_USER_IMAGE]];
                if(tag == 114)
                    break;
                tag++;
            }
        }
        
        int totalInvite = (int)eventEntity.inviteSet.count;
        if(totalInvite>4)
        {
            lblInviteTotal.hidden = NO;
            lblInviteTotal.text = [NSString stringWithFormat:@"+%d More",totalInvite-4];
        }
        else
            lblInviteTotal.hidden = YES;
        
        //Set height of attending view based on content
        NSLayoutConstraint * attendingHeightConstraint = [self getViewConstraintById:viewAttending.constraints Identifier:@"attendingHeight"];
        
        if (tag == 111)
        {
            if (attendingHeightConstraint)
                attendingHeightConstraint.constant = 0;
        }
        else
        {
            if (attendingHeightConstraint)
                attendingHeightConstraint.constant = 80;
        }
        
        [viewAttending updateConstraints];
        
        
        //Set height of invite view
        NSLayoutConstraint * inviteHeightConstraint = [self getViewConstraintById:viewInvite.constraints Identifier:@"inviteHeight"];
        if (inviteHeightConstraint)
            inviteHeightConstraint.constant = 0;
        
        [viewInvite updateConstraints];
        
        //Set height of attendance view
        NSLayoutConstraint * attendanceHeightConstraint = [self getViewConstraintById:viewAttendance.constraints Identifier:@"attendanceHeight"];
        if (attendanceHeightConstraint)
            attendanceHeightConstraint.constant = 50;
        
        [viewAttendance updateConstraints];
        
        //Set status on respective button
        btnAttending.selected = NO;
        btnMayBe.selected = NO;
        btnNope.selected = NO;
        
        switch ([eventEntity.status intValue])
        {
            case 2:
                btnAttending.selected = YES;
                break;
                
            case 3:
                btnMayBe.selected = YES;
                break;
                
            case 4:
                btnNope.selected = YES;
                break;
                
            default:
                break;
        }
    }
    
    //Event Name
    lblTitle.text = eventEntity.eventName;
    
    //Address
    if(eventEntity.address.length > 0)
        lblAddress.text = eventEntity.address;
    if(eventEntity.city.length > 0) {
        
        if(lblAddress.text.length > 0)
            lblAddress.text = [NSString stringWithFormat:@"%@, %@",lblAddress.text,eventEntity.city];
        else
            lblAddress.text = eventEntity.city;
    }
    if(eventEntity.state.length > 0) {
        
        if(lblAddress.text.length > 0)
            lblAddress.text = [NSString stringWithFormat:@"%@, %@",lblAddress.text,eventEntity.state];
        else
            lblAddress.text = eventEntity.state;
    }
    if(eventEntity.country.length > 0) {
        
        if(lblAddress.text.length > 0)
            lblAddress.text = [NSString stringWithFormat:@"%@, %@",lblAddress.text,eventEntity.country];
        else
            lblAddress.text = eventEntity.country;
    }
    
    //Event Time
    if (eventEntity.startDate && eventEntity.startDate.length>0) {
        
        dateFormatter.dateFormat = @"dd/MM/yyyy HH:mm:ss";
        NSDate *date = [dateFormatter dateFromString:eventEntity.startDate];
        dateFormatter.dateFormat = @"dd MMM";
        NSString *finalDate = [NSString stringWithFormat:@"%@ / %@ %@",[dateFormatter stringFromDate:date],eventEntity.fromTime,eventEntity.toTime];
        lblTime.attributedText = [self getAttributeToText:finalDate];
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)addEventToCal:(UIButton*)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:CollectionView];
    NSIndexPath *indexPath = [CollectionView indexPathForItemAtPoint:buttonPosition];
    EventEntity *eventEntity = [eventManager.allEventsArray objectAtIndex:indexPath.row];
    
    EKEventStore *store = [EKEventStore new];
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if (!granted) { return; }
        EKEvent *event = [EKEvent eventWithEventStore:store];
        event.title = eventEntity.eventName;
        event.startDate = [dateFormatter dateFromString:eventEntity.startDate];
        event.endDate = [dateFormatter dateFromString:eventEntity.endDate];
        event.calendar = [store defaultCalendarForNewEvents];
        NSError *err = nil;
        [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
        
        [[[UIAlertView alloc]initWithTitle:@"Calendar" message:@"Event added to your calendar." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }];
}


- (NSMutableAttributedString*)getAttributeToText:(NSString*)text
{
    NSMutableAttributedString *degreeAttribute = [[NSMutableAttributedString alloc] initWithString:text];
    [degreeAttribute addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Roboto-Bold" size:14.0] range:NSMakeRange(0, 6)];
    [degreeAttribute addAttribute:NSForegroundColorAttributeName value:NAVIGATIONBAR_COLOR range:NSMakeRange(0, 6)];
    return degreeAttribute;
}

- (NSLayoutConstraint *)getViewConstraintById:(NSArray *)allConstraints Identifier:(NSString *)identifier
{
    for (NSLayoutConstraint * constraint in allConstraints) {
        if ([constraint.identifier isEqualToString:identifier]) {
            return constraint;
        }
    }
    
    return nil;
}

#pragma mark - Navigation Buttons
- (void)openMenu
{
    [navigationManager openSideMenuFrom:self.navigationController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"ShowDetails"]){
        EventDetailsScreen *detailsScreen = (EventDetailsScreen *)[segue destinationViewController];
        NSIndexPath *indexPath = [self->CollectionView indexPathForCell:sender];
        
        detailsScreen.isForMyEvent = NO;
        detailsScreen.isInvited = NO;
        
        if(CollectionView.tag == 1)
            detailsScreen.eventEntity = [eventManager.allEventsArray objectAtIndex:indexPath.row];
        else if(CollectionView.tag == 2)
        {
            detailsScreen.isForMyEvent = YES;
            detailsScreen.eventEntity = [eventManager.myEventsArray objectAtIndex:indexPath.row];
        }
        else
        {
            detailsScreen.isInvited = YES;
            detailsScreen.eventEntity = [eventManager.invitedEventsArray objectAtIndex:indexPath.row];
        }
    }
}

@end

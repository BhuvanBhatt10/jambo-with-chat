//
//  SearchScreen.h
//  Jambo
//
//  Created by Rahul Chandera on 02/05/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchScreen : UIViewController
{
    IBOutlet UITableView *TableView;
    IBOutlet NSLayoutConstraint *btnFilterBottom;
}
@end

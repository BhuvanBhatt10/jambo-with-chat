//
//  AddQuestionScreen.h
//  Jambo
//
//  Created by Rahul Chandera on 27/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupEntity.h"
#import "HPGrowingTextView.h"

@interface AddQuestionScreen : UIViewController <HPGrowingTextViewDelegate>
{
    IBOutlet HPGrowingTextView *tvQuestion;
    IBOutlet NSLayoutConstraint *tvQuestionHeight;
    
}

@property (strong, nonatomic) GroupEntity *groupEntity;

@end

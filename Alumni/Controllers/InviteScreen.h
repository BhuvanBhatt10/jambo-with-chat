//
//  EventInviteScreen.h
//  Alumni
//
//  Created by Rahul Chandera on 05/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventEntity.h"
#import "EventManager.h"
#import "UserManager.h"

//Screen Type
typedef enum {
    InviteTypeEvent,
    InviteTypeGroupMember,
    InviteTypeGroupAdmin
}InviteType;

@interface InviteScreen : UIViewController <EventManagerDelegate, UserManagerDelegate>
{
    IBOutlet UITableView *TableView;
    IBOutlet UIView *tabsView;
    IBOutlet UIButton *btnGroupTab;
    IBOutlet UIButton *btnPeopleTab;
    IBOutlet UIButton *btnCreate;
    IBOutlet UITextField *txtSearch;
}

@property(nonatomic,strong) EventEntity *eventEntity;
@property(assign, nonatomic) BOOL isInEditMode;
@property(assign, nonatomic) InviteType inviteType;
@property(nonatomic,strong) NSString *groupId;
@property(nonatomic,strong) NSString *groupName;
@property(nonatomic,strong) NSString *groupDescription;
@property(nonatomic,strong) NSArray *alumniArray;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchTopSpace;

- (IBAction)createEventHandler:(id)sender;
- (IBAction)tabGruopPelple:(UIButton*)sender;
- (IBAction)searchTextChanged:(UITextField *)sender;

@end

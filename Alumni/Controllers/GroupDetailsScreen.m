//
//  GroupDetailsScreen.m
//  Jambo
//
//  Created by Rahul Chandera on 28/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "GroupDetailsScreen.h"
#import "UIImageView+WebCache.h"
#import "DiscussionManager.h"
#import "ProcessIndicator.h"
//#import "GiFHUD.h"
#import "AppConstants.h"
#import "Utility.h"
#import "InviteScreen.h"
#import "NavigationManager.h"
#import "UserManager.h"
#import "CoreDataManager.h"

#define BUTTON_INVITE_MEMBER     1
#define BUTTON_MANAGE_ADMIN      2
#define BUTTON_ACCEPT_INVITATION 3
#define BUTTON_JOIN_GROUP        4
#define BUTTON_LEAVE_GROUP       5

@interface GroupDetailsScreen () <DiscussionManagerDelegate>
{
    BOOL isAdmin;
}
@end

DiscussionManager *discussionManager;
ProcessIndicator *processIndicator;
NavigationManager *navigationManager;
UserManager *userManager;
CoreDataManager *coreDataManager;

NSMutableArray *memberArray;
NSMutableArray *adminArray;

@implementation GroupDetailsScreen
@synthesize groupEntity;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeObjects];
}


#pragma mark - Initialize Objects
- (void)initializeObjects
{
    discussionManager = [DiscussionManager sharedInstance];
    processIndicator = [ProcessIndicator sharedInstance];
    navigationManager = [NavigationManager sharedInstance];
    userManager = [UserManager sharedInstance];
    coreDataManager = [CoreDataManager sharedInstance];
    
    _lblGroupName.text = self.groupEntity.tag;
    _lblGroupDescription.text = self.groupEntity.groupDesc;
    
    memberArray = [[NSMutableArray alloc]init];
    adminArray = [[NSMutableArray alloc]init];
    
    isAdmin = NO;
    if ([groupEntity.role isEqualToString:@"admin"] && groupEntity.getStatus == 1)
        isAdmin = YES;
}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    if (isAdmin) {
        
        self.btnAction.tag= BUTTON_INVITE_MEMBER;
        [self.btnAction setTitle:@"Invite People" forState:UIControlStateNormal];
    }
    else if(groupEntity.role.length > 0 && groupEntity.getStatus == 0) {
    
        self.btnAction.tag= BUTTON_ACCEPT_INVITATION;
        [self.btnAction setTitle:@"Accept Invitation" forState:UIControlStateNormal];
    }
    else if(groupEntity.role.length > 0 && groupEntity.getStatus == 1) {
        
        self.btnAction.tag= BUTTON_LEAVE_GROUP;
        [self.btnAction setTitle:@"Leave Group" forState:UIControlStateNormal];
    }
    else {
        
        self.btnAction.tag= BUTTON_JOIN_GROUP;
        [self.btnAction setTitle:@"Join Group" forState:UIControlStateNormal];
    }
    
    [processIndicator showProcessIndicatorInView:self.view Message:@"Processing..."];
    
    //[GiFHUD showWithOverlay];
    discussionManager.delegate = self;
    [discussionManager getTagById:self.groupEntity.tagId];
}


#pragma mark - Members & Admin Tab
- (IBAction)tabHandler:(UIButton*)sender {
    
    if(sender == self.btnMembers) {
        
        self.btnMembers.selected = YES;
        self.btnAdmin.selected = NO;
        if (isAdmin) {
            
            self.btnAction.tag= BUTTON_INVITE_MEMBER;
            [self.btnAction setTitle:@"Invite People" forState:UIControlStateNormal];
        }
    }
    else {
        
        self.btnMembers.selected = NO;
        self.btnAdmin.selected = YES;
        if (isAdmin) {
            
            self.btnAction.tag= BUTTON_MANAGE_ADMIN;
            [self.btnAction setTitle:@"Manage Admins" forState:UIControlStateNormal];
        }
    }
    
    TableView.tag = sender.tag;
    [TableView reloadData];
}


#pragma mark - Button Action
- (IBAction)userActionHandler:(UIButton*)sender {
    
    if(sender.tag == BUTTON_INVITE_MEMBER) {
        
        InviteScreen *inviteScreen = (InviteScreen *)[navigationManager getScreen:nsInviteScreen];
        inviteScreen.isInEditMode = YES;
        inviteScreen.groupId = [NSString stringWithFormat:@"%d",self.groupEntity.tagId];
        inviteScreen.inviteType = InviteTypeGroupMember;
        inviteScreen.alumniArray = userManager.alumniArray;
        [self.navigationController pushViewController:inviteScreen animated:YES];
    }
    else if(sender.tag == BUTTON_MANAGE_ADMIN) {
        
        InviteScreen *inviteScreen = (InviteScreen *)[navigationManager getScreen:nsInviteScreen];
        inviteScreen.isInEditMode = YES;
        inviteScreen.groupId = [NSString stringWithFormat:@"%d",self.groupEntity.tagId];
        inviteScreen.inviteType = InviteTypeGroupAdmin;
        NSMutableArray *members = [NSMutableArray new];
        for(MyGroupsEntity *myGroupsEntity in memberArray)
            [members addObject:[userManager getAlumniById:myGroupsEntity.aluminiId]];
        inviteScreen.alumniArray = members;
        [self.navigationController pushViewController:inviteScreen animated:YES];
    }
    else if(sender.tag == BUTTON_ACCEPT_INVITATION) {
    
        [processIndicator showProcessIndicatorInView:self.view Message:@"Processing..."];
        discussionManager.delegate = self;
        [discussionManager responseTagInvite:self.groupEntity.tagId ResponseStatus:1];
    }
    else if(sender.tag == BUTTON_JOIN_GROUP) {
        
        [processIndicator showProcessIndicatorInView:self.view Message:@"Processing..."];
        discussionManager.delegate = self;
        [discussionManager joinTag:self.groupEntity.tagId];
    }
    else if(sender.tag == BUTTON_LEAVE_GROUP) {
        
        [processIndicator showProcessIndicatorInView:self.view Message:@"Processing..."];
        discussionManager.delegate = self;
        [discussionManager leaveTag:self.groupEntity.tagId];
    }
}



#pragma mark - Request Complete
-(void)requestCompleted:(id)response{
    
    [processIndicator hideProcessIndicator];
    //[GiFHUD dismiss];
    
    if([[response objectForKey:REQUEST_KEY] intValue] == requestGetTagById)
    {
        self.groupEntity = [response objectForKey:DATA_KEY];
        
        [_imgUserPhoto sd_setImageWithURL:[NSURL URLWithString:self.groupEntity.createdBy.profilePic] placeholderImage:[UIImage imageNamed:DEFAULT_USER_IMAGE]];
        _lblUserName.text = [NSString stringWithFormat:@"%@ %@",self.groupEntity.createdBy.firstName, self.groupEntity.createdBy.lastName];
        _lblCreatedTime.text = [Utility daysDiffrentFromDateString:self.groupEntity.lastUpdated];
        
        [memberArray removeAllObjects];
        [adminArray removeAllObjects];
        
        for(MyGroupsEntity *myGroupsEntity in self.groupEntity.userTagList)
        {
            if([myGroupsEntity.role isEqualToString:@"member"])
                [memberArray addObject:myGroupsEntity];
            else
                [adminArray addObject:myGroupsEntity];
        }
        
        [TableView reloadData];
    }
    else if([[response objectForKey:REQUEST_KEY] intValue] == requestResponseTagInvite)
    {
        groupEntity.role = @"member";
        [groupEntity setStatus:1];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if([[response objectForKey:REQUEST_KEY] intValue] == requestJoinTag)
    {
        groupEntity.role = @"member";
        [groupEntity setStatus:1];

        MyGroupsEntity * joinedGroup = [MyGroupsEntity new];
        joinedGroup.tagId = groupEntity.tagId;
        joinedGroup.lastUpdated = groupEntity.lastUpdated;
        joinedGroup.role= groupEntity.role;
        joinedGroup.aluminiId = userManager.userEntity.userId;
        joinedGroup.status = [groupEntity getStatus];
        
        NSMutableArray * newTags = [NSMutableArray arrayWithArray:userManager.userEntity.tags];
        [newTags addObject:joinedGroup];

        userManager.userEntity.tags = (NSArray<MyGroupsEntity>*)[NSArray arrayWithArray:newTags];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if([[response objectForKey:REQUEST_KEY] intValue] == requestLeaveTag)
    {
        groupEntity.role = @"";
        [groupEntity setStatus:2];
        
        NSMutableArray * newTags = [NSMutableArray new];
        
        for (MyGroupsEntity * myGroupEntity in userManager.userEntity.tags) {
            
            if (groupEntity.tagId != myGroupEntity.tagId) {
                [newTags addObject:myGroupEntity];
            }
        }
        
        GroupEntity * groupToEdit = [discussionManager getTagEntity:groupEntity.tagId];
        groupToEdit.role = @"";
        [groupToEdit setStatus:2];
        
        userManager.userEntity.tags = (NSArray<MyGroupsEntity>*)[NSArray arrayWithArray:newTags];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    /*
    if ([discussionManager.allGroupsArray count]>0)
    {
        //[TableView reloadData];
    }
    else
    {
        if ([[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_INTERNET_NOT_AVAILABLE_CODE)
        {
            [[[UIAlertView alloc] initWithTitle:@"Alumni" message:@"No internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
        else if([[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_SERVER_CONNECTION_ERROR || [[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_INTERNAL_SERVICE_ERROR){
            [[[UIAlertView alloc] initWithTitle:@"Alumni" message:@"Something went wrong" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
        else
            [[[UIAlertView alloc] initWithTitle:@"Alumni" message:[response objectForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }*/
}




#pragma mark - TableView Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.tag == 1)
        return memberArray.count;
    else
        return adminArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"UserCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    UIImageView *imgPhoto = (UIImageView *)[cell.contentView viewWithTag:101];
    UILabel *lblName = (UILabel *)[cell.contentView viewWithTag:102];
    UILabel *lblRole = (UILabel *)[cell.contentView viewWithTag:103];
    UIButton *btnClose = (UIButton *)[cell.contentView viewWithTag:104];
    
    [btnClose addTarget:self action:@selector(deleteAdmin:) forControlEvents:UIControlEventTouchUpInside];
    btnClose.hidden = YES;
    lblRole.hidden = NO;
    
    AluminiEntity *aluminiEntity;
    if(tableView.tag == 1) {
        
        MyGroupsEntity *myGroupsEntity = [memberArray objectAtIndex:indexPath.row];
        aluminiEntity = [userManager getAlumniById:myGroupsEntity.aluminiId];
        //aluminiEntity = [[[memberArray objectAtIndex:indexPath.row] userTagId] aluminiId];
        lblRole.text = [[memberArray objectAtIndex:indexPath.row] role];
    }
    else {
        
        MyGroupsEntity *myGroupsEntity = [adminArray objectAtIndex:indexPath.row];
        aluminiEntity = [userManager getAlumniById:myGroupsEntity.aluminiId];
        //aluminiEntity = [[[adminArray objectAtIndex:indexPath.row] userTagId] aluminiId];
        lblRole.text = [[adminArray objectAtIndex:indexPath.row] role];
        if([lblRole.text isEqualToString:@"admin"]) {
            
            lblRole.hidden = YES;
            if ([groupEntity.role isEqualToString:@"admin"] && groupEntity.getStatus == 1)
                btnClose.hidden = NO;
            else
                btnClose.hidden = YES;
        }
    }
    [imgPhoto setImageWithURL:[NSURL URLWithString:aluminiEntity.profilePic] placeholderImage:[UIImage imageNamed:DEFAULT_USER_IMAGE]];
    lblName.text = [NSString stringWithFormat:@"%@ %@",aluminiEntity.firstName, aluminiEntity.lastName];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}



- (void)deleteAdmin:(UIButton*)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:TableView];
    NSIndexPath *indexPath = [TableView indexPathForRowAtPoint:buttonPosition];
    //AluminiEntity *aluminiEntity = [[[adminArray objectAtIndex:indexPath.row] userTagId] aluminiId];
    
    [TableView beginUpdates];
    [adminArray removeObjectAtIndex:indexPath.row];
    [TableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationRight];
    [TableView endUpdates];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

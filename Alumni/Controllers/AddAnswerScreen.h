//
//  AddAnswerScreen.h
//  Jambo
//
//  Created by Rahul Chandera on 27/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HPGrowingTextView.h"

@interface AddAnswerScreen : UIViewController <HPGrowingTextViewDelegate>
{
    IBOutlet HPGrowingTextView *tvAnswer;
    IBOutlet NSLayoutConstraint *tvHeight;
    IBOutlet UIImageView *imgAnswer;
}

@property(nonatomic, strong) NSString *questionId;

- (IBAction)addPhotoHandler:(UIButton*)sender;

@end

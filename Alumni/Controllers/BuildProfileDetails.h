//
//  BuildProfileDetails.h
//  Jambo
//
//  Created by Rahul Chandera on 10/05/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLPAutoCompleteTextField.h"
#import "MLPAutoCompleteTextFieldDelegate.h"
#import "MLPAutoCompleteTextFieldDataSource.h"

@class MLPAutoCompleteTextField;

@interface BuildProfileDetails : UIViewController <UITextFieldDelegate ,MLPAutoCompleteTextFieldDelegate, MLPAutoCompleteTextFieldDataSource>

@property (weak, nonatomic) IBOutlet UIPageControl *PageControl;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleMessage;
@property (weak, nonatomic) IBOutlet UIImageView *imgUserPhoto;
@property (weak, nonatomic) IBOutlet UIScrollView *ScrollView;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UITableView *TableView;
@property (nonatomic, assign) UIEdgeInsets originalTableContentInset;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeightConstraint;

- (IBAction)saveContinueHandler:(id)sender;
- (void)updateUserPhoto:(UIImage*)photo;
- (IBAction)backTapHandler:(id)sender;

@end

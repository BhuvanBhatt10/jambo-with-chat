//
//  AppInvitation.h
//  Jambo
//
//  Created by Rahul Chandera on 28/09/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InviteCell.h"

@interface AppInvitation : UIViewController <InviteCellProtocol>
{
    IBOutlet UITableView *TableView;
    IBOutlet UILabel *lblInvitesRemaining;
}
@end

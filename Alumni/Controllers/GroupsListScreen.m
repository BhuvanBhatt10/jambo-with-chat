//
//  GroupsListScreen.m
//  Jambo
//
//  Created by Rahul Chandera on 28/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "GroupsListScreen.h"
#import "AppConstants.h"
#import "NavigationManager.h"
#import "DiscussionManager.h"
#import "UserManager.h"
//#import "ProcessIndicator.h"
//#import "GiFHUD.h"
#import "InviteScreen.h"
#import "GroupEntity.h"
#import "GroupDetailsScreen.h"

@interface GroupsListScreen () <UserManagerDelegate>

@end

NavigationManager *navigationManager;
DiscussionManager *discussionManager;
UserManager *userManager;
//ProcessIndicator *processIndicator;

NSArray *groupArray;
UIRefreshControl *refreshControl;

@implementation GroupsListScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeUI];
    [self initializeObjects];
}

#pragma mark - Initialize UI
- (void)initializeUI {
    
    self.title = @"Groups";
    
    self.navigationController.navigationBarHidden = NO;
    
    //Menu Button
    UIButton *btnMenu = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    [btnMenu addTarget:self action:@selector(openMenu) forControlEvents:UIControlEventTouchUpInside];
    [btnMenu setImage:[UIImage imageNamed:@"menu.png"] forState:UIControlStateNormal];
    [btnMenu setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    UIBarButtonItem *bbiMenu = [[UIBarButtonItem alloc]initWithCustomView:btnMenu];
    bbiMenu.style = UIBarButtonItemStylePlain;
    self.navigationItem.leftBarButtonItem = bbiMenu;
}

#pragma mark - Initialize Objects
- (void)initializeObjects
{
    navigationManager = [NavigationManager sharedInstance];
    discussionManager = [DiscussionManager sharedInstance];
    userManager = [UserManager sharedInstance];
    //processIndicator = [ProcessIndicator sharedInstance];

    refreshControl = [[UIRefreshControl alloc]init];
    [TableView addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    groupArray = [[NSArray alloc]init];
    [self tabHandler:tabAll];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [discussionManager filterGroupOfUser:userManager.userEntity];
    
    [tabAll setTitle:[NSString stringWithFormat:@"ALL (%lu)",(unsigned long)discussionManager.allGroupsArray.count] forState:UIControlStateNormal];
    [tabScubscribed setTitle:[NSString stringWithFormat:@"SUBSCRIBED (%lu)",(unsigned long)discussionManager.subscribedGroupsArray.count] forState:UIControlStateNormal];
    [tabInvited setTitle:[NSString stringWithFormat:@"INVITED (%lu)",(unsigned long)discussionManager.invitedGroupsArray.count] forState:UIControlStateNormal];
    
    [TableView reloadData];
}

#pragma mark - Refresh Table
- (void)refreshTable {
    
    discussionManager.delegate = self;
    [discussionManager getGroupsList];
}

#pragma mark - Request Complete
-(void)requestCompleted:(id)response{
    
    [refreshControl endRefreshing];
    //[processIndicator hideProcessIndicator];
    //[GiFHUD dismiss];
    
    RequestType requestType = [[response objectForKey:REQUEST_KEY] intValue];
    
    if (requestType == requestDiscussionList)
    {
        [TableView reloadData];
        userManager.delegate = self;
    }
    else
    {
        if ([[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_INTERNET_NOT_AVAILABLE_CODE)
        {
            [[[UIAlertView alloc] initWithTitle:@"Alumni" message:@"No internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
        else if([[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_SERVER_CONNECTION_ERROR || [[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_INTERNAL_SERVICE_ERROR){
            [[[UIAlertView alloc] initWithTitle:@"Alumni" message:@"Something went wrong" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
        else
            [[[UIAlertView alloc] initWithTitle:@"Alumni" message:[response objectForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}

#pragma mark - Tab Handler
- (IBAction)tabHandler:(UIButton*)sender
{
    if(sender == tabAll) {
        
        tabAll.selected = YES;
        tabScubscribed.selected = NO;
        tabInvited.selected = NO;
        groupArray = discussionManager.allGroupsArray;
    }
    else if(sender == tabScubscribed) {
        
        tabAll.selected = NO;
        tabScubscribed.selected = YES;
        tabInvited.selected = NO;
        groupArray = discussionManager.subscribedGroupsArray;
    }
    else {
        
        tabAll.selected = NO;
        tabScubscribed.selected = NO;
        tabInvited.selected = YES;
        groupArray = discussionManager.invitedGroupsArray;
    }
    
    TableView.tag = sender.tag;
    [TableView reloadData];
}



#pragma mark - TableView Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [groupArray count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"GroupCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    UILabel *lblGroupName = (UILabel *)[cell.contentView viewWithTag:101];
    UILabel *lblTotalMember = (UILabel *)[cell.contentView viewWithTag:102];
    UILabel *lblRole = (UILabel *)[cell.contentView viewWithTag:103];
    
    GroupEntity *groupEntity = [groupArray objectAtIndex:indexPath.row];
    lblGroupName.text = groupEntity.tag;
    lblTotalMember.text = [NSString stringWithFormat:@"%d Members",groupEntity.totalMembers];
    lblRole.text = groupEntity.role;
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}




#pragma mark - Navigation Buttons
- (void)openMenu
{
    [navigationManager openSideMenuFrom:self.navigationController];
}




#pragma mark - Search Group
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField.text.length >= 2)
        [self searchItem:textField.text];
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}
- (void)searchItem:(NSString*)searchText
{
    NSPredicate *filterPredicateForFilter =  [NSPredicate predicateWithFormat:@"SELF.tag contains[c] %@",searchText];
    if(TableView.tag == 1)
        groupArray = [discussionManager.allGroupsArray filteredArrayUsingPredicate:filterPredicateForFilter];
    else if(TableView.tag == 2)
        groupArray = [discussionManager.subscribedGroupsArray filteredArrayUsingPredicate:filterPredicateForFilter];
    else
        groupArray = [discussionManager.invitedGroupsArray filteredArrayUsingPredicate:filterPredicateForFilter];
    [TableView reloadData];
}
- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    if(TableView.tag == 1)
        groupArray = discussionManager.allGroupsArray;
    else if(TableView.tag == 2)
        groupArray = discussionManager.subscribedGroupsArray;
    else
        groupArray = discussionManager.invitedGroupsArray;
    [TableView reloadData];
    [textField performSelector:@selector(resignFirstResponder) withObject:nil afterDelay:0.1];
    return YES;
}



#pragma mark - Create Group
- (IBAction)createGroup:(id)sender
{
    [self.navigationController pushViewController:[navigationManager getScreen:nsCreateGroupScreen] animated:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([[segue identifier] isEqualToString:@"ShowDetails"]){
        
        GroupDetailsScreen *groupDetailsScreen = (GroupDetailsScreen *)[segue destinationViewController];
        NSIndexPath *indexPath = [self->TableView indexPathForCell:sender];
        groupDetailsScreen.groupEntity = [GroupEntity new];
        groupDetailsScreen.groupEntity = [groupArray objectAtIndex:indexPath.row];
        
    }
}


@end

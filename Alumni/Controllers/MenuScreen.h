//
//  MenuScreen.h
//  Alumni
//
//  Created by Rahul Chandera on 14/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuScreen : UIViewController
{
    IBOutlet UIImageView *imgHeaderBackground;
    IBOutlet UIImageView *imgUserPhoto;
    IBOutlet UILabel *lblUserName;
    IBOutlet UILabel *lblUserCode;
}

- (IBAction)openUserProfile:(id)sender;

@end

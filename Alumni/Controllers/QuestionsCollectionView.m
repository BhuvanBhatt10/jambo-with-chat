//
//  QuestionsCollectionView.m
//  Jambo
//
//  Created by Rahul Chandera on 27/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "QuestionsCollectionView.h"
#import "QuestionDetailScreen.h"
#import "NavigationManager.h"

@interface QuestionsCollectionView ()

@end

#define TOPIC_CELL_IDENTIFIRE  @"TopicCell"
#define EMPTY_CELL_IDENTIFIRE  @"EmptyCell"

NavigationManager *navigationManager;
GroupEntity *grpEntity;

@implementation QuestionsCollectionView
@synthesize CollectionView;
@synthesize delegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    navigationManager = [NavigationManager sharedInstance];
}

- (void)setQuestionsData:(GroupEntity*)groupEntity
{
    grpEntity = groupEntity;
    
    self.questionsArray = groupEntity.questions;
    lblGroupName.text = groupEntity.tag;
    if([self.questionsArray count] == 0)
        btnViewAll.hidden = YES;
    else
        btnViewAll.hidden = NO;
    [CollectionView reloadData];
}

#pragma mark - CollectionView
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if([self.questionsArray count] == 0)
        return 1;
    else
        return [self.questionsArray count];
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.questionsArray count] == 0)
        return CGSizeMake(collectionView.frame.size.width-20, 155);
    else
        return CGSizeMake(200, 155);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
   return 0.0;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.questionsArray count] == 0)
    {
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:EMPTY_CELL_IDENTIFIRE forIndexPath:indexPath];
        
        UIButton *btnAddQuestion = (UIButton *)[cell.contentView viewWithTag:101];
        UIButton *btnInvite = (UIButton *)[cell.contentView viewWithTag:102];
        
        [btnAddQuestion addTarget:self action:@selector(addQuestionTapHandler) forControlEvents:UIControlEventTouchUpInside];
        [btnInvite addTarget:self action:@selector(inviteAlumniTapHandler) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
    else
    {
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:TOPIC_CELL_IDENTIFIRE forIndexPath:indexPath];
        
        UILabel *lblQuestion = (UILabel *)[cell.contentView viewWithTag:101];
        UILabel *lblComment = (UILabel *)[cell.contentView viewWithTag:102];
        UIButton *btnLike = (UIButton *)[cell.contentView viewWithTag:104];
        UIButton *btnComment = (UIButton *)[cell.contentView viewWithTag:105];
        
        QuestionEntity *questionEntity = [self.questionsArray objectAtIndex:indexPath.row];
        lblQuestion.text = questionEntity.question;

        btnLike.hidden = NO;
        btnComment.hidden = NO;
        
        [btnLike setTitle:[NSString stringWithFormat:@"+%d",questionEntity.score] forState:UIControlStateNormal];
        if (questionEntity.score == 0)
            btnLike.hidden = YES;
        
        [btnComment setTitle:[NSString stringWithFormat:@"%d",(int)[questionEntity.answers count]] forState:UIControlStateNormal];
        if (questionEntity.answers.count == 0)
            btnComment.hidden = YES;
            
        if([questionEntity.answers count]>0)
            lblComment.text = [NSString stringWithFormat:@"\"%@\"",[[questionEntity.answers objectAtIndex:0] answer]];
        else
            lblComment.text = @"";
        
        //Set corner radius and border for like button
        btnLike.layer.cornerRadius = 2.0f;
        btnLike.layer.borderColor = [UIColor colorWithWhite:0.600 alpha:1.000].CGColor;
        btnLike.layer.borderWidth = 1.0f;
        btnLike.layer.masksToBounds = YES;
        
        return cell;
    }
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.delegate selectedQuestion:[self.questionsArray objectAtIndex:indexPath.row]];
}

- (IBAction)viewAllTapHandler:(id)sender
{
    [self.delegate viewAllQuestionOfGroup:(int)CollectionView.tag];
}

- (void)addQuestionTapHandler
{
    [self.delegate addQuestionToGroup:(int)CollectionView.tag];
}

- (void)inviteAlumniTapHandler
{
    [self.delegate inviteAlumniToGroup:(int)CollectionView.tag];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end

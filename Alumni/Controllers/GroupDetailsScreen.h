//
//  GroupDetailsScreen.h
//  Jambo
//
//  Created by Rahul Chandera on 28/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupEntity.h"

@interface GroupDetailsScreen : UIViewController
{
    IBOutlet UITableView *TableView;
}
@property (strong, nonatomic) GroupEntity *groupEntity;
@property (weak, nonatomic) IBOutlet UILabel *lblGroupName;
@property (weak, nonatomic) IBOutlet UILabel *lblGroupDescription;
@property (weak, nonatomic) IBOutlet UIImageView *imgUserPhoto;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblCreatedTime;
@property (weak, nonatomic) IBOutlet UIButton *btnMembers;
@property (weak, nonatomic) IBOutlet UIButton *btnAdmin;
@property (weak, nonatomic) IBOutlet UIButton *btnAction;

@end

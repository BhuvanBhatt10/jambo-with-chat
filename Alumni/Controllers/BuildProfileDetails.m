//
//  BuildProfileDetails.m
//  Jambo
//
//  Created by Rahul Chandera on 10/05/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "BuildProfileDetails.h"
#import "DWTagList.h"
#import "ApplicationManager.h"
#import "DiscussionManager.h"
#import "GroupEntity.h"
#import "UserManager.h"
#import "ProcessIndicator.h"
//#import "GiFHUD.h"
#import "AppConstants.h"
#import "NavigationManager.h"

@interface BuildProfileDetails () <UIScrollViewDelegate, UITextFieldDelegate, UserManagerDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@end

ApplicationManager *applicationManager;
DiscussionManager *discussionManager;
UserManager *userManager;
DWTagList *skillsTagView;
DWTagList *groupsTagView;
ProcessIndicator *processIndicator;
NavigationManager *navigationManager;

UIImage *userPhoto;
UIView *inputAccessoryView;
UITextField *currentTextField;
UIPickerView *pickerView;
NSArray *pickerArray;
NSArray *industryArray;
NSArray *functionArray;

@implementation BuildProfileDetails

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initializeObjects];
    [self initializeUI];
}


#pragma mark - Initialize Objects
- (void)initializeObjects
{
    applicationManager = [ApplicationManager sharedInstance];
    discussionManager = [DiscussionManager sharedInstance];
    userManager = [UserManager sharedInstance];
    processIndicator = [ProcessIndicator sharedInstance];
    navigationManager = [NavigationManager sharedInstance];
    
    pickerArray = [NSArray new];
    
    industryArray = [[NSArray alloc]initWithObjects:@"Agriculture",@"Accounting",@"Advertising",@"Aviation",@"Apparel & Accessories",@"Automotive",@"Banking",@"Broadcasting",@"Biotechnology",@"Chemical",@"Computer",@"Consulting",@"Consumer Products",@"Defense",@"Education",@"Electronics",@"Energy",@"Entertainment & Leisure",@"Financial Services",@"Food, Beverage & Tobacco",@"Health Care",@"Hospitality",@"Internet Publishing",@"Investment Banking",@"Legal",@"Manufacturing",@"Music",@"Newspaper Publishers",@"Pharmaceuticals",@"Private Equity",@"Publishing",@"Real Estate",@"Retail & Wholesale",@"Securities & Commodity Exchanges",@"Software",@"Sports",@"Technology",@"Telecommunications",@"Television",@"Transportation",@"Venture Capital", nil];
    
    functionArray = [[NSArray alloc]initWithObjects:@"Marketing", @"HR",@"Accounts",@"Finance",@"Strategy",@"Board Member",@"Operations",@"Product",@"Sales", nil];
    
    pickerView = [[UIPickerView alloc] init];
    [pickerView setDataSource: self];
    [pickerView setDelegate: self];
    pickerView.showsSelectionIndicator = YES;
}


#pragma mark - Initialize UI
- (void)initializeUI {
    
    self.navigationController.navigationBarHidden = YES;
    
    self.ScrollView.contentSize = CGSizeMake(self.view.frame.size.width*3, self.view.frame.size.height - 175);
    self.ScrollView.delegate = self;
    
    self.tableWidthConstraint.constant = self.view.frame.size.width;
    self.tableHeightConstraint.constant = self.view.frame.size.height - 175;
    
    CGRect tagFrame = self.TableView.frame;
    tagFrame.origin.x = self.view.frame.size.width;
    tagFrame.size.width = self.view.frame.size.width;
    tagFrame.size.height = self.view.frame.size.height - 175;
    
    skillsTagView = [[DWTagList alloc]initWithFrame:tagFrame];
    skillsTagView.cornerRadius = 20.0;
    [skillsTagView setTags:applicationManager.skillsArray];
    skillsTagView.originalArray = applicationManager.skillsArray;
    [self.ScrollView addSubview:skillsTagView];
    
    NSMutableArray *groupArray = [[NSMutableArray alloc]init];
    for(GroupEntity *groupEntity in discussionManager.allGroupsArray)
        [groupArray addObject:groupEntity.tag];
    
    CGRect tagFrame2 = self.TableView.frame;
    tagFrame2.origin.x = self.view.frame.size.width*2;
    tagFrame2.size.width = self.view.frame.size.width;
    tagFrame2.size.height = self.view.frame.size.height - 175;
    groupsTagView = [[DWTagList alloc]initWithFrame:tagFrame2];
    groupsTagView.cornerRadius = 5.0;
    [groupsTagView setTags:groupArray];
    groupsTagView.originalArray = groupArray;
    [self.ScrollView addSubview:groupsTagView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.imgUserPhoto.image = userPhoto;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)updateUserPhoto:(UIImage*)photo
{
    userPhoto = photo;
}

#pragma mark - TableView Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 6;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
        return 30;
    else
        return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    switch (indexPath.row) {
        case 0:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"SpaceCell"];
            if (cell == nil)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SpaceCell"];
        }
            break;
            
        case 1:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"OrganizationCell"];
            if (cell == nil)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"OrganizationCell"];
            
            MLPAutoCompleteTextField *txtFiled = (MLPAutoCompleteTextField *)[cell.contentView viewWithTag:101];
            txtFiled.autoCompleteDelegate = self;
            txtFiled.autoCompleteDataSource = self;
            [txtFiled setAutoCompleteTableAppearsAsKeyboardAccessory:YES];
        }
            break;
            
        case 2:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"DesignationCell"];
            if (cell == nil)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DesignationCell"];
            
            MLPAutoCompleteTextField *txtFiled = (MLPAutoCompleteTextField *)[cell.contentView viewWithTag:102];
            txtFiled.autoCompleteDelegate = self;
            txtFiled.autoCompleteDataSource = self;
            [txtFiled setAutoCompleteTableAppearsAsKeyboardAccessory:YES];
        }
            break;
            
        case 3:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"IndustryCell"];
            if (cell == nil)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"IndustryCell"];
            
            UITextField *txtFiled = (UITextField *)[cell.contentView viewWithTag:103];
            txtFiled.delegate = self;
            txtFiled.inputView = pickerView;
        }
            break;
            
        case 4:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"FunctionCell"];
            if (cell == nil)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FunctionCell"];
            
            UITextField *txtFiled = (UITextField *)[cell.contentView viewWithTag:104];
            txtFiled.delegate = self;
            txtFiled.inputView = pickerView;
        }
            break;
            
        case 5:
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"ExperienceCell"];
            if (cell == nil)
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ExperienceCell"];
            
            UITextField *txtFiled = (UITextField *)[cell.contentView viewWithTag:105];
            txtFiled.delegate = self;
        }
            break;
            
        default:
            break;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (IBAction)pageControlHandler{
    
    CGRect frame;
    frame.origin.x = self.ScrollView.frame.size.width * self.PageControl.currentPage;
    frame.origin.y = 0;
    frame.size = self.ScrollView.frame.size;
    [self.ScrollView scrollRectToVisible:frame animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat pageWidth = self.ScrollView.frame.size.width;
    int page = floor((self.ScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.PageControl.currentPage = page;
    
    if (self.PageControl.currentPage == 0) {
        
        self.lblTitleMessage.text = @"Please provide us with your work details";
        [self.btnSave setTitle:@"Save & Continue" forState:UIControlStateNormal];
    }
    else if (self.PageControl.currentPage == 1) {
        
        self.lblTitleMessage.text = @"Select skills that closely match your profession";
        [self.btnSave setTitle:@"Almost There..." forState:UIControlStateNormal];
    }
    else {
    
        self.lblTitleMessage.text = @"Subscribe to groups matching your interests";
        [self.btnSave setTitle:@"Done" forState:UIControlStateNormal];
    }
}

#pragma mark - Tag Delegate
- (void)selectedTag:(NSString *)tagName tagIndex:(NSInteger)tagIndex
{
    
}

- (void)deSelectedTag:(NSString *)tagName tagIndex:(NSInteger)tagIndex
{
    
}

- (IBAction)backTapHandler:(id)sender
{
    if (self.PageControl.currentPage == 2)
    {
        self.PageControl.currentPage = 1;
        CGRect frame;
        frame.origin.x = self.ScrollView.frame.size.width * self.PageControl.currentPage;
        frame.origin.y = 0;
        frame.size = self.ScrollView.frame.size;
        [self.ScrollView scrollRectToVisible:frame animated:YES];
    }
    else if (self.PageControl.currentPage == 1)
    {
        self.PageControl.currentPage = 0;
        CGRect frame;
        frame.origin.x = self.ScrollView.frame.size.width * self.PageControl.currentPage;
        frame.origin.y = 0;
        frame.size = self.ScrollView.frame.size;
        [self.ScrollView scrollRectToVisible:frame animated:YES];
    }
    else
        [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)saveContinueHandler:(id)sender {
    
    if (self.PageControl.currentPage == 0) {
        
        NSString *strMessage = @"";
        if (userManager.userEntity.companyName.length == 0)
            strMessage = @"Please provide your organization name.";
        else if (userManager.userEntity.designation.length == 0)
            strMessage = @"Please provide your designation.";
        else if (userManager.userEntity.industry.length == 0)
            strMessage = @"Please provide your industry name.";
        else if (userManager.userEntity.occupation.length == 0)
            strMessage = @"Please provide function.";
        else if (userManager.userEntity.totalExperience.length == 0)
            strMessage = @"Please provide your total experience.";
        
        if (strMessage.length > 0) {
            
            [[[UIAlertView alloc]initWithTitle:@"Jambo" message:strMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
        else {
            
            self.PageControl.currentPage = 1;
            CGRect frame;
            frame.origin.x = self.ScrollView.frame.size.width * self.PageControl.currentPage;
            frame.origin.y = 0;
            frame.size = self.ScrollView.frame.size;
            [self.ScrollView scrollRectToVisible:frame animated:YES];
        }
    }
    else if (self.PageControl.currentPage == 1) {
        
        self.PageControl.currentPage = 2;
        CGRect frame;
        frame.origin.x = self.ScrollView.frame.size.width * self.PageControl.currentPage;
        frame.origin.y = 0;
        frame.size = self.ScrollView.frame.size;
        [self.ScrollView scrollRectToVisible:frame animated:YES];
    }
    else {
        
        [processIndicator showProcessIndicatorInView:self.view Message:@"Processing..."];
        //[GiFHUD showWithOverlay];
        userManager.delegate = self;
        [userManager updateUser];
    }
}

#pragma mark - Request Complete
-(void)requestCompleted:(id)response{
    
    [processIndicator hideProcessIndicator];
    //[GiFHUD dismiss];
    
    if ([[response objectForKey:REQUEST_KEY] integerValue] == requestUpdateUser) {
        
        if (![[response objectForKey:RESULT_KEY] boolValue])
        {
            [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"IsUserLogin"];
            [[NSUserDefaults standardUserDefaults] setValue:userManager.userEntity.firstName forKey:@"firstName"];
            [[NSUserDefaults standardUserDefaults] setValue:userManager.userEntity.lastName forKey:@"lastName"];
            [self.navigationController pushViewController:[navigationManager getScreen:nsHomeScreen] animated:YES];
        }
        else
        {
            if ([[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_INTERNET_NOT_AVAILABLE_CODE)
            {
                [[[UIAlertView alloc] initWithTitle:@"Profile" message:@"No internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
            }
        }
    }
}

#pragma mark - MLPAutoCompleteTextField Delegate
- (BOOL)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
          shouldConfigureCell:(UITableViewCell *)cell
       withAutoCompleteString:(NSString *)autocompleteString
         withAttributedString:(NSAttributedString *)boldedString
        forAutoCompleteObject:(id<MLPAutoCompletionObject>)autocompleteObject
            forRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return YES;
}

- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
  didSelectAutoCompleteString:(NSString *)selectedString
       withAutoCompleteObject:(id<MLPAutoCompletionObject>)selectedObject
            forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(selectedObject){
        NSLog(@"selected object from autocomplete menu %@ with string %@", selectedObject, [selectedObject autocompleteString]);
    } else {
        NSLog(@"selected string '%@' from autocomplete menu", selectedString);
    }
}

#pragma mark - MLPAutoCompleteTextField DataSource
//example of asynchronous fetch:
- (void)autoCompleteTextField:(MLPAutoCompleteTextField *)textField
 possibleCompletionsForString:(NSString *)string
            completionHandler:(void (^)(NSArray *))handler
{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_async(queue, ^{

        NSArray *completions;
        
        if (textField.text.length > 2) {
            
            if (textField.tag == 101)
                completions = [userManager getOrganizationListForString:textField.text];
            else
                completions = [userManager getDesignationListForString:textField.text];
        }
        
        handler(completions);
    });
}

#pragma mark - TextField Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    currentTextField = textField;
    if(textField.tag == 103) {
        
        pickerArray = industryArray;
        [pickerView reloadAllComponents];
        textField.inputAccessoryView = [self addAccessoryView];
    }
    else if(textField.tag == 104) {
        
        pickerArray = functionArray;
        [pickerView reloadAllComponents];
        textField.inputAccessoryView = [self addAccessoryView];
    }
    else if(textField.tag == 105) {
        
        textField.inputAccessoryView = [self addAccessoryView];
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.text.length > 0) {
        
        switch (textField.tag) {
            case 101:
                userManager.userEntity.companyName = textField.text;
                break;
                
            case 102:
                userManager.userEntity.designation = textField.text;
                break;
                
            case 103:
                userManager.userEntity.industry = textField.text;
                break;
                
            case 104:
                userManager.userEntity.occupation = textField.text;
                break;
                
            case 105:
                userManager.userEntity.totalExperience = textField.text;
                break;
                
            default:
                break;
        }
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [self.TableView viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO;
}

- (UIView *)addAccessoryView {
    
    if (!inputAccessoryView) {
        
        CGRect accessFrame = CGRectMake(0.0, 0.0, self.view.frame.size.width, 50.0);
        inputAccessoryView = [[UIView alloc] initWithFrame:accessFrame];
        inputAccessoryView.backgroundColor = [UIColor colorWithRed:182.0/255.0 green:190.0/255.0 blue:203.0/255.0 alpha:1.0];
        UIButton *compButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        compButton.frame = CGRectMake(accessFrame.size.width - 70, 0.0, 60.0, 50.0);
        [compButton setTitle: @"Done" forState:UIControlStateNormal];
        [compButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        compButton.titleLabel.font = [UIFont boldSystemFontOfSize:16.0];
        [compButton addTarget:self action:@selector(hideKeyboard)
             forControlEvents:UIControlEventTouchUpInside];
        [compButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [inputAccessoryView addSubview:compButton];
    }
    return inputAccessoryView;
}

- (void)hideKeyboard
{
    [currentTextField resignFirstResponder];
}

#pragma mark - PickerView
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return pickerArray.count;
}
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return pickerArray[row];
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    currentTextField.text = pickerArray[row];
}

#pragma mark - Keyboard
- (void)keyboardDidShow:(NSNotification *)notification {
    
    // calculate the size of the keyboard and how much is and isn't covering the tableview
    NSDictionary *keyboardInfo = [notification userInfo];
    CGRect keyboardFrame = [keyboardInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    keyboardFrame = [self.TableView.window convertRect:keyboardFrame toView:self.TableView.superview];
    CGFloat heightOfTableViewThatIsCoveredByKeyboard = self.TableView.frame.origin.y + self.TableView.frame.size.height - keyboardFrame.origin.y;
    CGFloat heightOfTableViewThatIsNotCoveredByKeyboard = self.TableView.frame.size.height - heightOfTableViewThatIsCoveredByKeyboard;
    
    UIEdgeInsets tableContentInset = self.TableView.contentInset;
    self.originalTableContentInset = tableContentInset;
    tableContentInset.bottom = heightOfTableViewThatIsCoveredByKeyboard;
    
    UIEdgeInsets tableScrollIndicatorInsets = self.TableView.scrollIndicatorInsets;
    tableScrollIndicatorInsets.bottom += heightOfTableViewThatIsCoveredByKeyboard;
    
    [UIView beginAnimations:nil context:nil];
    
    // adjust the tableview insets by however much the keyboard is overlapping the tableview
    self.TableView.contentInset = tableContentInset;
    self.TableView.scrollIndicatorInsets = tableScrollIndicatorInsets;
    
    UIView *firstResponder = FXFormsFirstResponder(self.TableView);
    if ([firstResponder isKindOfClass:[UITextView class]]) {
        
        UITextView *textView = (UITextView *)firstResponder;
        
        // calculate the position of the cursor in the textView
        NSRange range = textView.selectedRange;
        UITextPosition *beginning = textView.beginningOfDocument;
        UITextPosition *start = [textView positionFromPosition:beginning offset:range.location];
        UITextPosition *end = [textView positionFromPosition:start offset:range.length];
        CGRect caretFrame = [textView caretRectForPosition:end];
        
        // convert the cursor to the same coordinate system as the tableview
        CGRect caretViewFrame = [textView convertRect:caretFrame toView:self.TableView.superview];
        
        // padding makes sure that the cursor isn't sitting just above the keyboard and will adjust to 3 lines of text worth above keyboard
        CGFloat padding = textView.font.lineHeight * 3;
        CGFloat keyboardToCursorDifference = (caretViewFrame.origin.y + caretViewFrame.size.height) - heightOfTableViewThatIsNotCoveredByKeyboard + padding;
        
        // if there is a difference then we want to adjust the keyboard, otherwise the cursor is fine to stay where it is and the keyboard doesn't need to move
        if (keyboardToCursorDifference > 0.0f) {
            // adjust offset by this difference
            CGPoint contentOffset = self.TableView.contentOffset;
            contentOffset.y += keyboardToCursorDifference;
            [self.TableView setContentOffset:contentOffset animated:YES];
        }
    }
    
    [UIView commitAnimations];
}

- (void)keyboardWillHide:(NSNotification *)note
{
    NSDictionary *keyboardInfo = [note userInfo];
    UIEdgeInsets tableScrollIndicatorInsets = self.TableView.scrollIndicatorInsets;
    tableScrollIndicatorInsets.bottom = 0;
    
    //restore insets
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:(UIViewAnimationCurve)keyboardInfo[UIKeyboardAnimationCurveUserInfoKey]];
    [UIView setAnimationDuration:[keyboardInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue]];
    self.TableView.contentInset = self.originalTableContentInset;
    self.TableView.scrollIndicatorInsets = tableScrollIndicatorInsets;
    self.originalTableContentInset = UIEdgeInsetsZero;
    [UIView commitAnimations];
}

static UIView *FXFormsFirstResponder(UIView *view)
{
    if ([view isFirstResponder])
    {
        return view;
    }
    for (UIView *subview in view.subviews)
    {
        UIView *responder = FXFormsFirstResponder(subview);
        if (responder)
        {
            return responder;
        }
    }
    return nil;
}



- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
    [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardDidShowNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillHideNotification];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

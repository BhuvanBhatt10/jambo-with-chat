//
//  EventDetailsScreen.m
//  Jambo
//
//  Created by Rahul Chandera on 11/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "EventDetailsScreen.h"
#import "AppConstants.h"
#import "UIImageView+WebCache.h"
#import "InviteEntity.h"
#import "EventCreateScreen.h"
#import "InviteScreen.h"
#import "UserManager.h"
#import "NavigationManager.h"
#import "ProcessIndicator.h"
#import "KLCPopup.h"
#import "InboxDetailScreen.h"
#import "UserProfileScreen.h"
#import "DiscussionManager.h"
#import "ChatViewController.h"
#import "ChatManager.h"
#import <Quickblox/QBRequest+QBUsers.h>
#import "QuickBloxHelper.h"
#import "UserdefaultManager.h"

#define CELL_TIME    @"TimeCell"
#define CELL_ABOUT   @"AboutCell"
#define CELL_COST    @"CostCell"
#define CELL_ATTENDY @"AttendyCell"

#define BUTTON_ATTENDING_TAG 131
#define BUTTON_MAYBE_TAG     132
#define BUTTON_NOPE_TAG      133

UserManager *userManager;
NavigationManager *navigationManager;
ProcessIndicator *processIndicator;
EventManager *eventManager;
ChatManager *chatManager;
KLCPopup * popup;

NSMutableArray *detailsArray;
NSDateFormatter *dateFormatter;

@implementation EventDetailsScreen
{
    NSIndexPath * indexPathForResponcingEvent;
    int responceBeingSet;
    QuickBloxHelper *quickbloxSharedInstance;
}

@synthesize eventEntity;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeUI];
    [self initializeObjects];
    quickbloxSharedInstance = [QuickBloxHelper sharedInstance];
}


#pragma mark - Initialize UI
- (void)initializeUI {
    
    self.title = @"";
    self.navigationController.navigationBarHidden = NO;
}

#pragma mark - Initialize Objects
- (void)initializeObjects
{
    userManager = [UserManager sharedInstance];
    navigationManager = [NavigationManager sharedInstance];
    processIndicator = [ProcessIndicator sharedInstance];
    eventManager = [EventManager sharedInstance];    
    chatManager = [ChatManager sharedInstance];
    
    self.lblEventTitle.text = self.eventEntity.eventName;
    AluminiEntity *aluminiEntity = [userManager getAlumniById:self.isForMyEvent ? userManager.userEntity.userId : self.eventEntity.createdBy];
    [self.imgUser sd_setImageWithURL:[NSURL URLWithString:aluminiEntity.profilePic] placeholderImage:[UIImage imageNamed:DEFAULT_USER_IMAGE]];
    self.lblUserName.text = [NSString stringWithFormat:@"%@ %@",aluminiEntity.firstName, aluminiEntity.lastName];
    
    detailsArray = [[NSMutableArray alloc]init];
    [self configureTableData];
    
    dateFormatter = [[NSDateFormatter alloc]init];
}

- (void)configureTableData
{
    [detailsArray removeAllObjects];

    [detailsArray addObject:CELL_TIME];
    
    if(self.eventEntity.description.length > 0)
        [detailsArray addObject:CELL_ABOUT];
    
    [detailsArray addObject:CELL_COST];
    
    if(!self.isForMyEvent && self.eventEntity.inviteSet.count > 0)
        [detailsArray addObject:CELL_ATTENDY];
    
    //This will remove extra separators from tableview
    self.TableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 20)];
    
    [self.TableView reloadData];
}

#pragma mark - TableView Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return detailsArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([[detailsArray objectAtIndex:indexPath.row] isEqualToString:CELL_TIME])
    {
        if (!self.isForMyEvent && !self.isInvited) {
            return 65;
        }
        return 115;
    }
    else if([[detailsArray objectAtIndex:indexPath.row] isEqualToString:CELL_ABOUT]) {
        
        NSAttributedString *attrStr = [[NSAttributedString alloc]initWithString:eventEntity.eventDesc];
        CGFloat width = self.view.frame.size.width - 80; // whatever your desired width is
        CGRect rect = [attrStr boundingRectWithSize:CGSizeMake(width, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
        return rect.size.height + 30;
    }
    else if([[detailsArray objectAtIndex:indexPath.row] isEqualToString:CELL_COST])
        return 56;
    else
        return 93;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
    
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 65, 0, 15)];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [detailsArray objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    if([[detailsArray objectAtIndex:indexPath.row] isEqualToString:CELL_TIME]) {
        
        UILabel *lblTime = (UILabel *)[cell.contentView viewWithTag:101];
        UILabel *lblAddress = (UILabel *)[cell.contentView viewWithTag:102];
        
        UIView *viewInvite = (UIView *)[cell.contentView viewWithTag:1000];
        UIView *viewAttendance = (UIView *)[cell.contentView viewWithTag:1001];
        
        UIButton *btnAttending = (UIButton *)[cell.contentView viewWithTag:131];
        UIButton *btnMayBe = (UIButton *)[cell.contentView viewWithTag:132];
        UIButton *btnNope = (UIButton *)[cell.contentView viewWithTag:133];
        
        //Event Time
        if (eventEntity.startDate && eventEntity.startDate.length>0) {
            
            dateFormatter.dateFormat = @"dd/MM/yyyy HH:mm:ss";
            NSDate *date = [dateFormatter dateFromString:eventEntity.startDate];
            dateFormatter.dateFormat = @"dd MMM";
            lblTime.text = [NSString stringWithFormat:@"%@ / %@ %@",[dateFormatter stringFromDate:date],eventEntity.fromTime,eventEntity.toTime];
        }
        
        //Address
        if(eventEntity.address.length > 0)
            lblAddress.text = eventEntity.address;
        if(eventEntity.city.length > 0) {
            
            if(lblAddress.text.length > 0)
                lblAddress.text = [NSString stringWithFormat:@"%@, %@",lblAddress.text,eventEntity.city];
            else
                lblAddress.text = eventEntity.city;
        }
        if(eventEntity.state.length > 0) {
            
            if(lblAddress.text.length > 0)
                lblAddress.text = [NSString stringWithFormat:@"%@, %@",lblAddress.text,eventEntity.state];
            else
                lblAddress.text = eventEntity.state;
        }
        if(eventEntity.country.length > 0) {
            
            if(lblAddress.text.length > 0)
                lblAddress.text = [NSString stringWithFormat:@"%@, %@",lblAddress.text,eventEntity.country];
            else
                lblAddress.text = eventEntity.country;
        }
        
        //Set height of invite view
        NSLayoutConstraint * inviteHeightConstraint = [self getViewConstraintById:viewInvite.constraints Identifier:@"inviteHeight"];
        if (inviteHeightConstraint)
        {
            if (self.isForMyEvent)
                inviteHeightConstraint.constant = 50;
            else
                inviteHeightConstraint.constant = 0;
        }
        
        [viewInvite updateConstraints];
        
        //Set height of attendance view
        NSLayoutConstraint * attendanceHeightConstraint = [self getViewConstraintById:viewAttendance.constraints Identifier:@"attendanceHeight"];
        if (attendanceHeightConstraint)
        {
            if (self.isInvited)
                attendanceHeightConstraint.constant = 50;
            else
                attendanceHeightConstraint.constant = 0;
        }

        //Set status on respective button
        if (self.isInvited)
        {
            btnAttending.selected = NO;
            btnMayBe.selected = NO;
            btnNope.selected = NO;
            
            switch ([eventEntity.status intValue])
            {
                case 2:
                    btnAttending.selected = YES;
                    break;
                    
                case 3:
                    btnMayBe.selected = YES;
                    break;
                    
                case 4:
                    btnNope.selected = YES;
                    break;
                    
                default:
                    break;
            }
        }
    }
    else if([[detailsArray objectAtIndex:indexPath.row] isEqualToString:CELL_ABOUT]) {
        
        UILabel *lblAbout = (UILabel *)[cell.contentView viewWithTag:101];
        lblAbout.text = eventEntity.eventDesc;
    }
    else if([[detailsArray objectAtIndex:indexPath.row] isEqualToString:CELL_COST]) {
        
        UILabel *lblPaidText = (UILabel *)[cell.contentView viewWithTag:101];
        UILabel *lblCost = (UILabel *)[cell.contentView viewWithTag:102];
        UILabel *lblGustText = (UILabel *)[cell.contentView viewWithTag:103];
        UILabel *lblAmount = (UILabel *)[cell.contentView viewWithTag:104];
        
        if([eventEntity.isPaid boolValue]) {
            lblPaidText.text = @"This is a paid event";
            lblCost.text = eventEntity.charges;
        }
        else {
            lblPaidText.text = @"This is a free event";
            lblCost.text = @"";
        }
        
        if([eventEntity.isGuestAllowed boolValue]) {
            lblGustText.text = @"Guests are allowed";
            lblAmount.text = eventEntity.guestCharges;
        }
        else {
            lblGustText.text = @"Guests are NOT allowed";
            lblAmount.text = @"";
        }
    }
    else {
    
        UIImageView *imgInvite1 = (UIImageView *)[cell.contentView viewWithTag:111];
        UIImageView *imgInvite2 = (UIImageView *)[cell.contentView viewWithTag:112];
        UIImageView *imgInvite3 = (UIImageView *)[cell.contentView viewWithTag:113];
        UIImageView *imgInvite4 = (UIImageView *)[cell.contentView viewWithTag:114];
        UILabel *lblInviteTotal = (UILabel *)[cell.contentView viewWithTag:115];
        
        imgInvite1.hidden = YES;
        imgInvite2.hidden = YES;
        imgInvite3.hidden = YES;
        imgInvite4.hidden = YES;
        lblInviteTotal.hidden = YES;
        
        int tag = 111;
        for(InviteEntity *invite in eventEntity.inviteSet)
        {
            if(invite.responseStatus == 1){
                
                AluminiEntity *aluminiEntity = [userManager getAlumniById:invite.inviteId];
                UIImageView *imgInvite = (UIImageView*)[cell.contentView viewWithTag:tag];
                imgInvite.hidden = NO;
                [imgInvite sd_setImageWithURL:[NSURL URLWithString:aluminiEntity.profilePic] placeholderImage:[UIImage imageNamed:DEFAULT_USER_IMAGE]];
                
//                UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userPhotoTappedInCell:)];
//                [imgInvite addGestureRecognizer:tapGesture];
//                imgInvite.userInteractionEnabled = YES;
                
                if(tag == 114)
                    break;
                tag++;
            }
        }
        int totalInvite = (int)eventEntity.inviteSet.count;
        if(totalInvite>4)
        {
            lblInviteTotal.hidden = NO;
            lblInviteTotal.text = [NSString stringWithFormat:@"+%d More",totalInvite-4];
        }
        else
            lblInviteTotal.hidden = YES;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)userPhotoTappedInCell:(UITapGestureRecognizer *)gesture
{
    UIImageView * imageView = (UIImageView *)gesture.view;
    
    int tag = 111;
    for(InviteEntity *invite in eventEntity.inviteSet)
    {
        if(invite.responseStatus == 1){
            
            if (tag == imageView.tag) {
                AluminiEntity *aluminiEntity = [userManager getAlumniById:invite.inviteId];
                self.contactInfoPopup = (ContactInfoPopup*)[[NavigationManager sharedInstance] getScreen:nsContactInfoPopup];
                self.contactInfoPopup.view.frame = CGRectMake(0, 0, 222, 322);
                [self.contactInfoPopup setUserDataByAlumini:aluminiEntity];
                self.contactInfoPopup.delegate = self;
                
                popup = [KLCPopup popupWithContentView:self.contactInfoPopup.view
                                              showType:KLCPopupShowTypeBounceInFromTop
                                           dismissType:KLCPopupDismissTypeBounceOutToBottom
                                              maskType:KLCPopupMaskTypeDimmed
                              dismissOnBackgroundTouch:YES
                                 dismissOnContentTouch:NO];
                [popup show];
                break;
            }
            tag++;
        }
    }
}

#pragma mark - ContactInfo Delegate
- (void)sendMessageTo:(UserEntity*)_user {
    [popup dismiss:YES];
    
    //Start Chat except owns profile
    if (![[UserdefaultManager getUserEmail] isEqualToString:_user.email]) {
        [processIndicator showProcessIndicatorInView:self.view Message:@"Loading"];

        //Find user in storage
        [quickbloxSharedInstance isUserExistInStorage:_user.email Oncompletion:^(QBUUser *opponentUser) {
            if (opponentUser != nil) {
                if (opponentUser.fullName.length == 0 || opponentUser.fullName == nil) {
                    opponentUser.fullName = opponentUser.email;
                }
                
                [ServicesManager.instance.chatService createPrivateChatDialogWithOpponent:opponentUser completion:^(QBResponse *response, QBChatDialog *createdDialog) {
                    if( !response.success  && createdDialog == nil ) {
                        [processIndicator hideProcessIndicator];
                    } else {
                        [processIndicator hideProcessIndicator];
                        [quickbloxSharedInstance connectQBOnCompletion:^(QBUUser *currentUser) {
                            if (currentUser != nil) {
                                // Connected
                                [processIndicator hideProcessIndicator];
                                ChatViewController *chatVC = (ChatViewController *)[navigationManager getScreen:nsChatScreen];
                                chatVC.dialog = createdDialog;
                                [self.navigationController pushViewController:chatVC animated:YES];
                            } else {
                                [processIndicator hideProcessIndicator];
                               // NSAssert(1<0, @"Quickblox not connected to internet");
                            }
                        }];
                    }
                }];
            } else {
                [processIndicator hideProcessIndicator];
            }
        }];
        
        /*[quickbloxSharedInstance isUserExistInStorage:_user.email Oncompletion:^(QBUUser *opponentUser) {
            if(opponentUser != nil) {
                // QBUUser found
                [quickbloxSharedInstance createChatDialogueWithOpponent:opponentUser Oncompletion:^(QBChatDialog *dialogue) {
                    if (dialogue != nil) {
                        // Dialogue created
                        NSLog(@"Dialogue Created");
                        
                        // Quickblox Connection
                        [quickbloxSharedInstance connectQBOnCompletion:^(QBUUser *currentUser) {
                            if (currentUser != nil) {
                                // Connected
                                [processIndicator hideProcessIndicator];
                                ChatViewController *chatVC = (ChatViewController *)[navigationManager getScreen:nsChatScreen];
                                chatVC.dialog = dialogue;
                                [self.navigationController pushViewController:chatVC animated:YES];
                            } else {
                                [processIndicator hideProcessIndicator];
                                NSAssert(1<0, @"Quickblox not connected to internet");
                            }
                        }];
                    } else {
                        // Failed to create dialouge
                        [processIndicator hideProcessIndicator];
                        NSLog(@"Failed to create dialogue");
                    }
                }];
            } else {
                // QBUUser not found
                [processIndicator hideProcessIndicator];
            }
        }];*/

    }
}

- (void)viewUserProfile:(UserEntity*)user
{
    [popup dismiss:YES];
    
    UserProfileScreen *userProfileScreen = (UserProfileScreen*)[[NavigationManager sharedInstance] getScreen:nsUserProfileScreen];
    userProfileScreen.userEntity = user;
    [self.navigationController pushViewController:userProfileScreen animated:YES];
}

#pragma mark - Actions
- (IBAction)btnAvailabilityClick:(UIButton *)sender
{
    NSIndexPath *indexPath;
    indexPath = [self.TableView indexPathForRowAtPoint:[self.TableView convertPoint:sender.center fromView:sender.superview]];

    if (indexPath)
    {
        int responceStatus = 1;
        
        if (sender.tag == BUTTON_ATTENDING_TAG)
            responceStatus = 2;
        
        if (sender.tag == BUTTON_MAYBE_TAG)
            responceStatus = 3;
        
        if (sender.tag == BUTTON_NOPE_TAG)
            responceStatus = 4;
        
        indexPathForResponcingEvent = indexPath;
        responceBeingSet = responceStatus;
        
        [processIndicator showProcessIndicatorInView:self.view Message:@"Processing..."];
        eventManager.delegate = self;
        [eventManager responseEventInvite:eventEntity.eventId ResponseStatus:responceStatus];
    }
}

- (IBAction)btnCopyEventClick:(UIButton *)sender
{
    eventManager.eventBeingCopied = [eventManager getEventCopyFromEvent:(MyEventEntity *)self.eventEntity];
    EventCreateScreen *eventCreateScreen = (EventCreateScreen *)[navigationManager getScreen:nsEventCreateScreen];
    eventCreateScreen.isInEditMode = NO;
    eventCreateScreen.isBeingCopied = YES;
    eventCreateScreen.eventEntity = (EventEntity *)eventManager.eventBeingCopied;
    [self.navigationController pushViewController:eventCreateScreen animated:YES];
}

#pragma mark - Get needed constraint by identifier from array of constraints
- (NSLayoutConstraint *)getViewConstraintById:(NSArray *)allConstraints Identifier:(NSString *)identifier
{
    for (NSLayoutConstraint * constraint in allConstraints) {
        if ([constraint.identifier isEqualToString:identifier]) {
            return constraint;
        }
    }
    
    return nil;
}

#pragma mark - Request Complete
-(void)requestCompleted:(id)response{
    
    [processIndicator hideProcessIndicator];
    
    if([[response objectForKey:REQUEST_KEY] intValue] == requestResponseEvent)
    {
        eventEntity.status = [NSString stringWithFormat:@"%d",responceBeingSet];
        [self.TableView reloadRowsAtIndexPaths:@[indexPathForResponcingEvent] withRowAnimation:UITableViewRowAnimationNone];
    }
    else
    {
        if ([[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_INTERNET_NOT_AVAILABLE_CODE)
        {
            [[[UIAlertView alloc] initWithTitle:@"Alumni" message:@"No internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
        else if([[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_SERVER_CONNECTION_ERROR || [[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_INTERNAL_SERVICE_ERROR){
            [[[UIAlertView alloc] initWithTitle:@"Alumni" message:@"Something went wrong" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
        else
            [[[UIAlertView alloc] initWithTitle:@"Alumni" message:[response objectForKey:@"msg"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([[segue identifier] isEqualToString:@"EditEvent"]){
        
        EventCreateScreen *eventCreateScreen = (EventCreateScreen *)[segue destinationViewController];
        eventCreateScreen.isInEditMode = YES;
        eventCreateScreen.eventEntity = self.eventEntity;
    }
    else if([[segue identifier] isEqualToString:@"InviteToEvent"]){
        
        InviteScreen *inviteScreen = (InviteScreen *)[segue destinationViewController];
        inviteScreen.eventEntity = self.eventEntity;
        inviteScreen.inviteType = InviteTypeEvent;
        inviteScreen.alumniArray = userManager.alumniArray;
    }
}

@end

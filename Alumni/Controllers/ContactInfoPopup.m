//
//  ContactInfoPopup.m
//  Alumni
//
//  Created by Rahul Chandera on 16/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "ContactInfoPopup.h"
#import "AppConstants.h"
#import "UIImageView+WebCache.h"
#import "NavigationManager.h"

@interface ContactInfoPopup ()

@end

NavigationManager *navigationManager;
UserEntity *userEntity;

@implementation ContactInfoPopup
@synthesize delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    navigationManager = [NavigationManager sharedInstance];
}

- (void)setUserData:(UserEntity *)userDetails
{
    userEntity = userDetails;
    [_imgPhoto sd_setImageWithURL:[NSURL URLWithString:userEntity.profilePic] placeholderImage:[UIImage imageNamed:DEFAULT_USER_IMAGE]];
    _lblName.text = [NSString stringWithFormat:@"%@ %@",userEntity.firstName, userEntity.lastName];
    _lblDesignation.text = userEntity.designation;
    _lblAddress.text = [NSString stringWithFormat:@"%@, %@",userEntity.companyName, userEntity.city];
    _lblOccupation.text = userEntity.occupation;
    
    _imgPhoto.layer.borderWidth = 5;
    _imgPhoto.layer.borderColor = [UIColor whiteColor].CGColor;
}

- (void)setUserDataByAlumini:(AluminiEntity *)userDetails
{
    userEntity = userDetails;
    [_imgPhoto sd_setImageWithURL:[NSURL URLWithString:userEntity.profilePic] placeholderImage:[UIImage imageNamed:DEFAULT_USER_IMAGE]];
    _lblName.text = [NSString stringWithFormat:@"%@ %@",userEntity.firstName, userEntity.lastName];
    _lblDesignation.text = userEntity.designation;
    _lblAddress.text = [NSString stringWithFormat:@"%@, %@",userEntity.companyName, userEntity.city];
    _lblOccupation.text = userEntity.occupation;
    
    _imgPhoto.layer.borderWidth = 5;
    _imgPhoto.layer.borderColor = [UIColor whiteColor].CGColor;
}

- (IBAction)callHandler:(id)sender
{
    if ([userEntity.contact1 length]>0) {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",userEntity.contact1]]];
    }
    else
        [[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Phone number not available." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (IBAction)messageHandler:(id)sender
{
    [self.delegate sendMessageTo:userEntity];
}

- (IBAction)viewProfileHandler:(id)sender
{
    [self.delegate viewUserProfile:userEntity];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

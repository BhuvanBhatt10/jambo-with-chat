//
//  ForgotPasswordScreen.m
//  Jambo
//
//  Created by Rahul Chandera on 14/05/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "ForgotPasswordScreen.h"
#import "UserManager.h"
#import "ProcessIndicator.h"
//#import "GiFHUD.h"
#import "AppConstants.h"
#import "KLCPopup.h"
#import "NavigationManager.h"
#import "ForgotPasswordMessage.h"

@interface ForgotPasswordScreen () <UserManagerDelegate>

@end

UserManager *userManager;
ProcessIndicator *processIndicator;
NavigationManager *navigationManager;

@implementation ForgotPasswordScreen

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initializeObjects];
}


#pragma mark - Initialize Objects
- (void)initializeObjects
{
    processIndicator = [ProcessIndicator sharedInstance];
    userManager = [UserManager sharedInstance];
    navigationManager = [NavigationManager sharedInstance];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
}

- (IBAction)forgotPassword:(id)sender {
    
    NSString *strMsg = nil;
    if (txtEmail.text.length == 0) {
        
        [txtEmail resignFirstResponder];
        strMsg = @"Email ID should not be empty";
        [[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Email ID should not be empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
    else {
        
        [processIndicator showProcessIndicatorInView:self.view Message:@"Processing..."];
//        [GiFHUD showWithOverlay];
        userManager.delegate = self;
        [userManager forgotPassword:txtEmail.text];
    }
}



#pragma mark - Request Complete
-(void)requestCompleted:(id)response{
    
    [processIndicator hideProcessIndicator];
//    [GiFHUD dismiss];
    
    if (![[response objectForKey:RESULT_KEY] boolValue])
    {
        ForgotPasswordMessage *forgotPasswordMessage = (ForgotPasswordMessage*)[navigationManager getScreen:nsForgotPasswordMessage];
        [forgotPasswordMessage setEmailId:txtEmail.text];
        [self.navigationController pushViewController:forgotPasswordMessage animated:YES];
    }
    else
    {
        if ([[response objectForKey:ERROR_CODE_KEY] integerValue] == ERROR_INTERNET_NOT_AVAILABLE_CODE)
        {
            [[[UIAlertView alloc] initWithTitle:@"Login" message:@"No internet connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        }
        else
            [[[UIAlertView alloc] initWithTitle:@"Alumni" message:@"Something went wrong" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
}



- (IBAction)backTapHandler:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

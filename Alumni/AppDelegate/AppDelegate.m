//
//  AppDelegate.m
//  Alumni
//
//  Created by Rahul Chandera on 10/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "AppDelegate.h"
#import "ApplicationManager.h"
#import "NavigationManager.h"
#import "NotificationEntity.h"
#import "GiFHUD.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "UserdefaultManager.h"
#import "QuickBloxHelper.h"

@interface AppDelegate ()

@end

ApplicationManager *applicationManager;

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    [Fabric with:@[[Crashlytics class]]];

    [QBSettings setApplicationID:29127];
    [QBSettings setAuthKey:@"GDpeP5xXEUQdehg"];
    [QBSettings setAuthSecret:@"ZUH-3yT3zdqAmhW"];
    [QBSettings setAccountKey:@"fsFXnwFeayLpiKv23Nxu"];
    
    // Enables Quickblox REST API calls debug console output
    [QBSettings setLogLevel:QBLogLevelDebug];
    
    [QBSettings setReconnectTimerInterval:5.0];
    // Enables detailed XMPP logging in console output
    [QBSettings enableXMPPLogging];
    
    applicationManager = [ApplicationManager sharedInstance];
    
    if([UserdefaultManager getQBUserID] && [UserdefaultManager getQBPassword]) {
        if (![[QBChat instance] isConnected])  {
         [[QuickBloxHelper sharedInstance] connectQBOnCompletion:^(QBUUser *currentUser) {
         
         }];
         }
    }

    
    [self gifHudSetUp];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeNewsstandContentAvailability| UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
    
    
    NSDictionary *remoteNotif = [launchOptions objectForKey: UIApplicationLaunchOptionsRemoteNotificationKey];
    if (remoteNotif)
    {
        [self redirectToScreen:remoteNotif];
    }
    
    return YES;
}


- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSString *deviceIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    QBMSubscription *subscription = [QBMSubscription subscription];
    subscription.notificationChannel = QBMNotificationChannelAPNS;
    subscription.deviceUDID = deviceIdentifier;
    subscription.deviceToken = deviceToken;
    if ([UserdefaultManager getUserEmail]) {
        [QBRequest logInWithUserLogin:[UserdefaultManager getUserEmail] password:[UserdefaultManager getUserEmail] successBlock:^(QBResponse * _Nonnull response, QBUUser * _Nullable user) {
            [QBRequest createSubscription:subscription successBlock:^(QBResponse *response, NSArray *objects) {
                
            } errorBlock:^(QBResponse *response) {
                
            }];
        } errorBlock:^(QBResponse * _Nonnull response) {
            [QBRequest createSubscription:subscription successBlock:^(QBResponse *response, NSArray *objects) {
                
            } errorBlock:^(QBResponse *response) {
                
            }];
        }];
    }
    

    
    NSString *token = [NSString stringWithFormat:@"%@",deviceToken];
    
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    applicationManager.deviceToken = token;
    applicationManager.deviceTokenData = deviceToken;
    NSLog(@"My token is: %@", token);
    
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    [self redirectToScreen:userInfo];
    application.applicationIconBadgeNumber = applicationManager.notificationsArray.count;
}

- (void)redirectToScreen:(NSDictionary *)userInfo
{
    NSLog(@"Push Message = %@",userInfo);
    
    NSError* err = nil;
    NotificationEntity *notificationEntity = [[NotificationEntity alloc]initWithDictionary:userInfo error:&err];
    [applicationManager.notificationsArray insertObject:notificationEntity atIndex:0];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificationReceived" object:notificationEntity];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "azilen.TestCoreData" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Model.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

-(void)deleteAndRecreateStore{
    [_managedObjectContext lock];
    NSArray *stores = [_persistentStoreCoordinator persistentStores];
    for(NSPersistentStore *store in stores) {
        [_persistentStoreCoordinator removePersistentStore:store error:nil];
        [[NSFileManager defaultManager] removeItemAtPath:store.URL.path error:nil];
    }
    [_managedObjectContext unlock];
    _managedObjectModel    = nil;
    _managedObjectContext  = nil;
    _persistentStoreCoordinator = nil;
    [self managedObjectContext];
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - GIF Process Indicator
-(void)gifHudSetUp
{
    NSMutableArray *loadingImgs = [[NSMutableArray alloc]init];
    for(int i=0; i<20; i++)
        [loadingImgs addObject:[UIImage imageNamed:[NSString stringWithFormat:@"loader_%d.png",i]]];
    [GiFHUD setGifWithImages:loadingImgs];
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [ServicesManager.instance.chatService disconnectWithCompletionBlock:nil];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    if([UserdefaultManager getQBUserID] && [UserdefaultManager getQBPassword]) {
        if (![[QBChat instance] isConnected])  {
            [[QuickBloxHelper sharedInstance] connectQBOnCompletion:^(QBUUser *currentUser) {
                
            }];
        }
    }
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end

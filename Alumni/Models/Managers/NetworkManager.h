//
//  NetworkManager.h
//  Alumni
//
//  Created by Rahul Chandera on 22/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppConstants.h"

@interface NetworkManager : NSObject

+ (NetworkManager*)sharedInstance;
- (NSMutableDictionary*)requestToServer:(RequestType)requestType requestData:(id)requestData;
- (NSMutableDictionary*)postRequestToServer:(RequestType)requestType requestData:(id)requestData;
- (NSMutableDictionary*)multipartPostRequestToServer:(RequestType)requestType requestData:(NSMutableData*)httpBody Boundary:(NSString*)boundary;

@end

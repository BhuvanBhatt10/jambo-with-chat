//
//  CoreDataManager.m
//  Jambo
//
//  Created by Rahul Chandera on 19/07/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "CoreDataManager.h"
#import "AppDelegate.h"
#import "User.h"
#import "Event.h"
#import "MessageGroup.h"
#import "Message.h"
#import "UserManager.h"

AppDelegate *appDelegate;
UserManager *userManager;

@implementation CoreDataManager

//Singleton object
static CoreDataManager *coreDataManager = nil;
+ (CoreDataManager*)sharedInstance {
    if (!coreDataManager) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            coreDataManager = [[self alloc] init];
            [coreDataManager initialize];
        });
    }
    return coreDataManager;
}

-(void)initialize
{
    appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    userManager = [UserManager sharedInstance];
}

#pragma mark - User
- (void)saveUserObject:(UserEntity*)userEntity
{
    User *user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:appDelegate.managedObjectContext];
    user.email = userEntity.email;
    user.password = userEntity.password;
    user.firstName = userEntity.firstName;
    user.lastName = userEntity.lastName;
    user.profilePic = userEntity.profilePic;
    user.userId = [NSString stringWithFormat:@"%d",userEntity.userId];
    [appDelegate.managedObjectContext save:nil];
}

- (UserEntity*)getUserObject
{
    UserEntity *userEntity = [[UserEntity alloc]init];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"User" inManagedObjectContext:appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSError *error = nil;
    NSArray *result = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
    else if(result.count > 0){
        
        NSLog(@"%@", result);
        User *user = [result objectAtIndex:0];
        userEntity.email = user.email;
        userEntity.password = user.password;
        userEntity.firstName = user.firstName;
        userEntity.lastName = user.lastName;
        userEntity.profilePic = user.profilePic;
        userEntity.userId = [user.userId intValue];
    }
    
    return userEntity;
}

#pragma mark - Event
- (void)saveEventObject:(MyEventEntity*)myEventEntity
{
    Event *event = [NSEntityDescription insertNewObjectForEntityForName:@"Event" inManagedObjectContext:appDelegate.managedObjectContext];
    event.eventId = [NSString stringWithFormat:@"%d",myEventEntity.eventId];
    event.eventName = myEventEntity.eventName;
    event.eventDesc = myEventEntity.eventDesc;
    event.lastUpdated = myEventEntity.lastUpdated;
    event.startDate = myEventEntity.startDate;
    event.endDate = myEventEntity.endDate;
    event.status = myEventEntity.status;
    event.fromTime = myEventEntity.fromTime;
    event.toTime = myEventEntity.toTime;
    event.address = myEventEntity.address;
    event.city = myEventEntity.city;
    event.state = myEventEntity.state;
    event.country = myEventEntity.country;
    event.isPaid = myEventEntity.isPaid;
    event.charges = myEventEntity.charges;
    event.isGuestAllowed = myEventEntity.isGuestAllowed;
    event.guestCharges = myEventEntity.guestCharges;
    [appDelegate.managedObjectContext save:nil];
}

- (NSMutableArray*)getEventObject
{
    MyEventEntity *myEventEntity = [[MyEventEntity alloc]init];
    NSMutableArray *eventsArray = [[NSMutableArray alloc]init];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Event" inManagedObjectContext:appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSError *error = nil;
    NSArray *result = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
    else if(result.count > 0){
        
        for(Event *event in result) {
        
            myEventEntity.eventId = [event.eventId intValue];
            myEventEntity.eventName = event.eventName;
            myEventEntity.eventDesc = event.eventDesc;
            myEventEntity.lastUpdated = event.lastUpdated;
            myEventEntity.startDate = event.startDate;
            myEventEntity.endDate = event.endDate;
            myEventEntity.status = event.status;
            myEventEntity.fromTime = event.fromTime;
            myEventEntity.toTime = event.toTime;
            myEventEntity.address = event.address;
            myEventEntity.city = event.city;
            myEventEntity.state = event.state;
            myEventEntity.country = event.country;
            myEventEntity.isPaid = event.isPaid;
            myEventEntity.charges = event.charges;
            myEventEntity.isGuestAllowed = event.isGuestAllowed;
            myEventEntity.guestCharges = event.guestCharges;
            [myEventEntity setDates];
            [eventsArray addObject:myEventEntity];
        }
    }
    
    return eventsArray;
}

#pragma mark - Messages
- (void)saveMessageGroupObject:(MessageGroupEntity*)messageGroupEntity
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"MessageGroup" inManagedObjectContext:appDelegate.managedObjectContext];
    request.predicate = [NSPredicate predicateWithFormat:@"userId  contains[cd] %d",messageGroupEntity.userId];
    
    NSArray *arr = [appDelegate.managedObjectContext executeFetchRequest:request error:nil];
    if(arr.count > 0)
    {
        MessageGroup *messageGroup = [arr objectAtIndex:0];
        if (![messageGroup.lastUpdated isEqualToString:messageGroupEntity.lastUpdated]) {
            
            NSMutableSet *messageList = [[NSMutableSet alloc]init];
            for(MessageEntity *messageEntity in messageGroupEntity.messageList)
            {
                Message *message = [NSEntityDescription insertNewObjectForEntityForName:@"Message" inManagedObjectContext:appDelegate.managedObjectContext];
                message.messageId = [NSNumber numberWithInt:messageEntity.messageId];
                message.messageText = messageEntity.messageText;
                message.lastUpdated = messageEntity.lastUpdated;
                message.status = [NSNumber numberWithInt:messageEntity.status];
                message.picture = messageEntity.picture;
                message.fromAluminiId = [NSString stringWithFormat:@"%d",messageGroupEntity.userId];
                [messageList addObject:message];
            }
            [messageGroup addMessageList:messageList];
            [appDelegate.managedObjectContext save:nil];
        }
    }
    else
    {
        MessageGroup *messageGroup = [NSEntityDescription insertNewObjectForEntityForName:@"MessageGroup" inManagedObjectContext:appDelegate.managedObjectContext];
        messageGroup.userId = [NSNumber numberWithInt:messageGroupEntity.userId];
        messageGroup.firstName = messageGroupEntity.firstName;
        messageGroup.lastName = messageGroupEntity.lastName;
        messageGroup.profilePic = messageGroupEntity.profilePic;
        messageGroup.lastUpdated = messageGroupEntity.lastUpdated;
        
        NSMutableSet *messageList = [[NSMutableSet alloc]init];
        for(MessageEntity *messageEntity in messageGroupEntity.messageList)
        {
            Message *message = [NSEntityDescription insertNewObjectForEntityForName:@"Message" inManagedObjectContext:appDelegate.managedObjectContext];
            message.messageId = [NSNumber numberWithInt:messageEntity.messageId];
            message.messageText = messageEntity.messageText;
            message.lastUpdated = messageEntity.lastUpdated;
            message.status = [NSNumber numberWithInt:messageEntity.status];
            message.picture = messageEntity.picture;
            message.fromAluminiId = [NSString stringWithFormat:@"%d",messageGroupEntity.userId];
            [messageList addObject:message];
        }
        [messageGroup addMessageList:messageList];
        [appDelegate.managedObjectContext save:nil];
    }
}

- (NSMutableArray*)getMessageGroupObject
{
    NSMutableArray *messageArray = [[NSMutableArray alloc]init];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"MessageGroup" inManagedObjectContext:appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSError *error = nil;
    NSArray *result = [appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        
        NSLog(@"Unable to execute fetch request.");
        NSLog(@"%@, %@", error, error.localizedDescription);
    }
    else if(result.count > 0){
        
        NSLog(@"%@", result);
        for(MessageGroup *messageGroup in result)
        {
            MessageGroupEntity *messageGroupEntity = [[MessageGroupEntity alloc]init];
            messageGroupEntity.firstName = messageGroup.firstName;
            messageGroupEntity.lastName = messageGroup.lastName;
            messageGroupEntity.profilePic = messageGroup.profilePic;
            messageGroupEntity.userId = [messageGroup.userId intValue];
            messageGroupEntity.lastUpdated = messageGroup.lastUpdated;
            
            NSMutableArray *messageList = [[NSMutableArray alloc]init];
            for(Message *message in messageGroup.messageList)
            {
                MessageEntity *messageEntity = [[MessageEntity alloc]init];
                messageEntity.messageId = [message.messageId intValue];
                messageEntity.messageText = message.messageText;
                messageEntity.lastUpdated = message.lastUpdated;
                messageEntity.status = [message.status intValue];
                messageEntity.picture = message.picture;
                
                messageEntity.fromAlumini = [UserEntity new];
                messageEntity.fromAlumini.userId = [message.fromAluminiId intValue];
                [messageList addObject:messageEntity];
            }
            messageGroupEntity.messageList = messageList;
            [messageArray addObject:messageGroupEntity];
        }
    }
    return messageArray;
}

- (void)addMessageToGroup:(MessageGroupEntity*)messageGroupEntity MessageEntity:(MessageEntity*)messageEntity
{
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"MessageGroup" inManagedObjectContext:appDelegate.managedObjectContext];
    request.predicate = [NSPredicate predicateWithFormat:@"userId == %d",messageGroupEntity.userId];
    
    NSArray *arr = [appDelegate.managedObjectContext executeFetchRequest:request error:nil];
    if(arr.count > 0)
    {
        MessageGroup *messageGroup = [arr objectAtIndex:0];
        
        Message *message = [NSEntityDescription insertNewObjectForEntityForName:@"Message" inManagedObjectContext:appDelegate.managedObjectContext];
        message.messageId = [NSNumber numberWithInt:messageEntity.messageId];
        message.messageText = messageEntity.messageText;
        message.lastUpdated = messageEntity.lastUpdated;
        message.status = [NSNumber numberWithInt:1];
        message.picture = messageEntity.picture;
        message.fromAluminiId = [NSString stringWithFormat:@"%d",userManager.userEntity.userId];
        
        [messageGroup addMessageList:[[NSSet alloc]initWithObjects:message, nil]];
        [appDelegate.managedObjectContext save:nil];
    }
    else
    {
        MessageGroup *messageGroup = [NSEntityDescription insertNewObjectForEntityForName:@"MessageGroup" inManagedObjectContext:appDelegate.managedObjectContext];
        messageGroup.userId = [NSNumber numberWithInt:messageGroupEntity.userId];
        messageGroup.firstName = messageGroupEntity.firstName;
        messageGroup.lastName = messageGroupEntity.lastName;
        messageGroup.profilePic = messageGroupEntity.profilePic;
        messageGroup.lastUpdated = messageGroupEntity.lastUpdated;
        
        NSMutableSet *messageList = [[NSMutableSet alloc]init];
        Message *message = [NSEntityDescription insertNewObjectForEntityForName:@"Message" inManagedObjectContext:appDelegate.managedObjectContext];
        message.messageId = [NSNumber numberWithInt:messageEntity.messageId];
        message.messageText = messageEntity.messageText;
        message.lastUpdated = messageEntity.lastUpdated;
        message.status = [NSNumber numberWithInt:1];
        message.picture = messageEntity.picture;
        message.fromAluminiId = [NSString stringWithFormat:@"%d",userManager.userEntity.userId];
        [messageList addObject:message];
        
        [messageGroup addMessageList:messageList];
        [appDelegate.managedObjectContext save:nil];
    }
}


@end

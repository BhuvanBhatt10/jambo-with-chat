//
//  NetworkManager.m
//  Alumni
//
//  Created by Rahul Chandera on 22/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "NetworkManager.h"


#define PARENT_SERVICE_URL   @"http://ec2-54-186-38-114.us-west-2.compute.amazonaws.com:8080/AluminiBackend/"

@implementation NetworkManager

//Singleton object
static NetworkManager *networkManager = nil;
+ (NetworkManager*)sharedInstance {
    if (!networkManager){
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            networkManager = [[self alloc] init];
        });
    }
    return networkManager;
}

#pragma mark - Request to server
- (NSMutableDictionary*)requestToServer:(RequestType)requestType requestData:(id)requestData
{
    NSString *requestURLString = [self getUrlForRequestType:requestType];
    NSLog(@"Request = %@",requestURLString);
    requestURLString = [requestURLString stringByAppendingString:requestData];
    NSURL *url = [NSURL URLWithString:requestURLString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    NSError *errorReturned = nil;
    NSURLResponse *theResponse =[[NSURLResponse alloc]init];
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&theResponse error:&errorReturned];
    
    if (errorReturned) {
        NSLog(@"Request Error = %@",errorReturned);
        return nil;
    }
    else
    {
        NSError *jsonParsingError = nil;
        NSMutableDictionary *responseDict = (NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers|NSJSONReadingAllowFragments error:&jsonParsingError];
        NSLog(@"Response = %@",responseDict);
        return responseDict;
    }
}

- (NSMutableDictionary*)postRequestToServer:(RequestType)requestType requestData:(id)requestData
{
    NSString *requestURLString = [self getUrlForRequestType:requestType];
    NSLog(@"Request = %@",requestURLString);
    //requestURLString = [requestURLString stringByAppendingString:requestData];
    NSURL *url = [NSURL URLWithString:requestURLString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[requestData dataUsingEncoding:NSUTF8StringEncoding]];
    //[request addValue:@"multipart/form-data" forHTTPHeaderField: @"Content-Type"];
    
    NSError *errorReturned = nil;
    NSURLResponse *theResponse =[[NSURLResponse alloc]init];
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&theResponse error:&errorReturned];
    
    if (errorReturned) {
        NSLog(@"Request Error = %@",errorReturned);
        return nil;
    }
    else
    {
        NSError *jsonParsingError = nil;
        NSMutableDictionary *responseDict = (NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers|NSJSONReadingAllowFragments error:&jsonParsingError];
        NSLog(@"Response = %@",responseDict);
        return responseDict;
    }
}




- (NSMutableDictionary*)multipartPostRequestToServer:(RequestType)requestType requestData:(NSMutableData*)httpBody Boundary:(NSString*)boundary
{
    NSURL *url = [NSURL URLWithString:[self getUrlForRequestType:requestType]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"POST"];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:httpBody];
    //request.timeoutInterval = 60.0;
    NSError *errorReturned = nil;
    NSURLResponse *theResponse =[[NSURLResponse alloc]init];
    NSData *responsedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&theResponse error:&errorReturned];
    
    if (errorReturned) {
        NSLog(@"Request Error = %@",errorReturned);
        return nil;
    }
    else
    {
        NSError *jsonParsingError = nil;
        NSMutableDictionary *responseDict = (NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:responsedData options:NSJSONReadingMutableContainers|NSJSONReadingAllowFragments error:&jsonParsingError];
        NSLog(@"Response = %@",responseDict);
        return responseDict;
    }
}

- (NSString*)getUrlForRequestType:(RequestType)requestType
{
    NSString *requestString;
    
    switch (requestType) {
        case requestRegisterApp:
            requestString = [NSString stringWithFormat:@"%@registerApp",PARENT_SERVICE_URL];
            break;
            
        case requestUserLogin:
            requestString = [NSString stringWithFormat:@"%@login?",PARENT_SERVICE_URL];
            break;

        case requestGetUserDetail:
            requestString = [NSString stringWithFormat:@"%@getUser",PARENT_SERVICE_URL];
            break;
            
        case requestForgotPassword:
            requestString = [NSString stringWithFormat:@"%@forgotPassword?",PARENT_SERVICE_URL];
            break;
            
        case requestUpdateUser:
            requestString = [NSString stringWithFormat:@"%@updateUser",PARENT_SERVICE_URL];
            break;
            
        case requestDiscussionList:
            requestString = [NSString stringWithFormat:@"%@getTagList?",PARENT_SERVICE_URL];
            break;
            
        case requestCreateTag:
            requestString = [NSString stringWithFormat:@"%@createTag?",PARENT_SERVICE_URL];
            break;
            
        case requestPostAnswer:
            requestString = [NSString stringWithFormat:@"%@postAnswer",PARENT_SERVICE_URL];
            break;
          
        case requestPostQuestion:
            requestString = [NSString stringWithFormat:@"%@postQuestion",PARENT_SERVICE_URL];
            break;
            
        case requestUpvoteQuestion:
            requestString = [NSString stringWithFormat:@"%@upvoteQuestion",PARENT_SERVICE_URL];
            break;
            
        case requestUpvoteAnswer:
            requestString = [NSString stringWithFormat:@"%@upvoteAnswer",PARENT_SERVICE_URL];
            break;
            
        case requestGetAluminiList:
            requestString = [NSString stringWithFormat:@"%@getAluminiList?",PARENT_SERVICE_URL];
            break;
            
        case requestNearByList:
            requestString = [NSString stringWithFormat:@"%@nearByList?",PARENT_SERVICE_URL];
            break;
            
        case requestGetMessageList:
            requestString = [NSString stringWithFormat:@"%@getUnreadMessageListUserWise?",PARENT_SERVICE_URL];
            break;
            
        case requestGetEventList:
            requestString = [NSString stringWithFormat:@"%@getEventList?",PARENT_SERVICE_URL];
            break;
            
        case requestCreateEvent:
            requestString = [NSString stringWithFormat:@"%@createEvent",PARENT_SERVICE_URL];
            break;
          
        case requestUpdateEvent:
            requestString = [NSString stringWithFormat:@"%@updateEvent",PARENT_SERVICE_URL];
            break;
            
        case requestSendMessage:
            requestString = [NSString stringWithFormat:@"%@sendMessage",PARENT_SERVICE_URL];
            break;
            
        case requestUpdateMessage:
            requestString = [NSString stringWithFormat:@"%@updateMessage",PARENT_SERVICE_URL];
            break;
            
        case requestGetTagById:
            requestString = [NSString stringWithFormat:@"%@getTagById",PARENT_SERVICE_URL];
            break;
            
        case requestInviteTagMembers:
            requestString = [NSString stringWithFormat:@"%@inviteTagMembers",PARENT_SERVICE_URL];
            break;
            
        case requestResponseTagInvite:
            requestString = [NSString stringWithFormat:@"%@responseTagInvite",PARENT_SERVICE_URL];
            break;
            
        case requestResponseEvent:
            requestString = [NSString stringWithFormat:@"%@responseEventInvite",PARENT_SERVICE_URL];
            break;
            
        case requestJoinTag:
            requestString = [NSString stringWithFormat:@"%@joinTag",PARENT_SERVICE_URL];
            break;
            
        case requestLeaveTag:
            requestString = [NSString stringWithFormat:@"%@leaveTag",PARENT_SERVICE_URL];
            break;
            
        case requestUpdateTagAdminBulk:
            requestString = [NSString stringWithFormat:@"%@updateTagAdminBulk",PARENT_SERVICE_URL];
            break;
            
        case requestGetSkillList:
            requestString = [NSString stringWithFormat:@"%@getSkillList",PARENT_SERVICE_URL];
            break;
            
        case requestMapUserWithDevice:
            requestString = [NSString stringWithFormat:@"%@mapUserWithDevice",PARENT_SERVICE_URL];
            break;
            
        case requestInviteToApp :
            requestString = [NSString stringWithFormat:@"%@createByReferral",PARENT_SERVICE_URL];
            break;
            
        case requestGetMyInstitute:
            requestString = [NSString stringWithFormat:@"%@getMyInstitute",PARENT_SERVICE_URL];
            break;
            
        case requestMyReferredUsers:
            requestString = [NSString stringWithFormat:@"%@myReferredUser",PARENT_SERVICE_URL];
            break;
            
        case requestGetInstituteList:
            requestString = [NSString stringWithFormat:@"%@getInstituteList",PARENT_SERVICE_URL];
            break;

        case requestGetCourseList:
            requestString = [NSString stringWithFormat:@"%@getCourseList",PARENT_SERVICE_URL];
            break;
            
        case requestGetOrganizationList:
            requestString = [NSString stringWithFormat:@"%@getCompanyList",PARENT_SERVICE_URL];
            break;
            
        case requestGetDesignationList:
            requestString = [NSString stringWithFormat:@"%@getDesignationsList",PARENT_SERVICE_URL];
            break;
            
        default:
            break;
    }
    return requestString;
}


@end

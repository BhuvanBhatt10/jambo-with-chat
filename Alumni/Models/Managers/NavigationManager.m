//
//  NavigationManager.m
//  Alumni
//
//  Created by Rahul Chandera on 13/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "NavigationManager.h"
#import "AppConstants.h"
#import "MCPanelViewController.h"
#import "LoginScreen.h"
#import "BuildProfileBasic.h"
#import "BuildProfileDetails.h"
#import "HomeScreen.h"
#import "QuestionsCollectionView.h"
#import "QuestionListScreen.h"
#import "MenuScreen.h"
#import "QuestionDetailScreen.h"
#import "NearbyScreen.h"
#import "ContactInfoPopup.h"
#import "InboxScreen.h"
#import "InboxDetailScreen.h"
#import "AppInvitation.h"
#import "EventsScreen.h"
#import "EventCreateScreen.h"
#import "InviteScreen.h"
#import "NotificationsScreen.h"
#import "CreateGroupScreen.h"
#import "GroupsListScreen.h"
#import "GroupDetailsScreen.h"
#import "SearchScreen.h"
#import "UserProfileScreen.h"
#import "ForgotPasswordMessage.h"
#import "SettingsScreen.h"
#import "AddQuestionScreen.h"
#import "ChatViewController.h"

@interface NavigationManager ()

@end


@implementation NavigationManager

static NavigationManager *navigationManager = nil;
+ (NavigationManager*)sharedInstance {
    if (!navigationManager) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            navigationManager = [[self alloc] init];
            [navigationManager initialize];
        });
    }
    return navigationManager;
}
-(void)initialize
{
    
}


//This method initialize and return controllers based on eNum
-(UIViewController*)getScreen:(NavigateToScreen)screenType
{
    switch (screenType) {
        case nsLoginScreen:
        {
            UIStoryboard *loginStoryboard = [UIStoryboard storyboardWithName:@"Login" bundle: nil];
            LoginScreen *loginScreen = (LoginScreen *)[loginStoryboard instantiateViewControllerWithIdentifier:@"LoginScreen"];
            return loginScreen;
        }
            break;
        case nsBuildProfileBasic:
        {
            UIStoryboard *loginStoryboard = [UIStoryboard storyboardWithName:@"Login" bundle: nil];
            BuildProfileBasic *buildProfileBasic = (BuildProfileBasic *)[loginStoryboard instantiateViewControllerWithIdentifier:@"BuildProfileBasic"];
            return buildProfileBasic;
        }
            break;
        case nsBuildProfileDetails:
        {
            UIStoryboard *loginStoryboard = [UIStoryboard storyboardWithName:@"Login" bundle: nil];
            BuildProfileDetails *buildProfileDetails = (BuildProfileDetails *)[loginStoryboard instantiateViewControllerWithIdentifier:@"BuildProfileDetails"];
            return buildProfileDetails;
        }
            break;
           
        case nsHomeScreen:
        {
            UIStoryboard *homeStoryboard = [UIStoryboard storyboardWithName:@"Home" bundle: nil];
            HomeScreen *homeScreen = (HomeScreen *)[homeStoryboard instantiateViewControllerWithIdentifier:@"HomeScreen"];
            return homeScreen;
        }
            break;
        case nsQuestionsCollectionView:
        {
            UIStoryboard *homeStoryboard = [UIStoryboard storyboardWithName:@"Home" bundle: nil];
            QuestionsCollectionView *questionsCollectionView = (QuestionsCollectionView *)[homeStoryboard instantiateViewControllerWithIdentifier:@"QuestionsCollectionView"];
            return questionsCollectionView;
        }
            break;
        case nsQuestionDetailScreen:
        {
            UIStoryboard *discussionStoryboard = [UIStoryboard storyboardWithName:@"Discussion" bundle: nil];
            QuestionDetailScreen *questionDetailScreen = (QuestionDetailScreen *)[discussionStoryboard instantiateViewControllerWithIdentifier:@"QuestionDetailScreen"];
            return questionDetailScreen;
        }
            break;
        case nsMenuScreen:
        {
            UIStoryboard *homeStoryboard = [UIStoryboard storyboardWithName:@"Home" bundle: nil];
            MenuScreen *menuScreen = (MenuScreen *)[homeStoryboard instantiateViewControllerWithIdentifier:@"MenuScreen"];
            menuScreen.preferredContentSize = CGSizeMake(270, menuScreen.view.frame.size.height);
            return menuScreen;
        }
            break;
        case nsQuestionListScreen:
        {
            UIStoryboard *discussionStoryboard = [UIStoryboard storyboardWithName:@"Discussion" bundle: nil];
            QuestionListScreen *questionListScreen = (QuestionListScreen *)[discussionStoryboard instantiateViewControllerWithIdentifier:@"QuestionListScreen"];
            return questionListScreen;
        }
            break;
        case nsCreateGroupScreen:
        {
            UIStoryboard *discussionStoryboard = [UIStoryboard storyboardWithName:@"Discussion" bundle: nil];
            CreateGroupScreen *createGroupScreen = (CreateGroupScreen *)[discussionStoryboard instantiateViewControllerWithIdentifier:@"CreateGroupScreen"];
            return createGroupScreen;
        }
            break;
        case nsGroupsListScreen:
        {
            UIStoryboard *discussionStoryboard = [UIStoryboard storyboardWithName:@"Discussion" bundle: nil];
            GroupsListScreen *groupsListScreen = (GroupsListScreen *)[discussionStoryboard instantiateViewControllerWithIdentifier:@"GroupsListScreen"];
            return groupsListScreen;
        }
            break;
        case nsGroupDetailsScreen:
        {
            UIStoryboard *discussionStoryboard = [UIStoryboard storyboardWithName:@"Discussion" bundle: nil];
            GroupDetailsScreen *groupDetailsScreen = (GroupDetailsScreen *)[discussionStoryboard instantiateViewControllerWithIdentifier:@"GroupDetailsScreen"];
            return groupDetailsScreen;
        }
            break;
        case nsNearbyScreen:
        {
            UIStoryboard *homeStoryboard = [UIStoryboard storyboardWithName:@"Home" bundle: nil];
            NearbyScreen *nearbyScreen = (NearbyScreen *)[homeStoryboard instantiateViewControllerWithIdentifier:@"NearbyScreen"];
            return nearbyScreen;
        }
            break;
        case nsContactInfoPopup:
        {
            UIStoryboard *homeStoryboard = [UIStoryboard storyboardWithName:@"Home" bundle: nil];
            ContactInfoPopup *contactInfoPopup = (ContactInfoPopup *)[homeStoryboard instantiateViewControllerWithIdentifier:@"ContactInfoPopup"];
            return contactInfoPopup;
        }
            break;
        case nsInboxScreen:
        {
            UIStoryboard *homeStoryboard = [UIStoryboard storyboardWithName:@"Home" bundle: nil];
            InboxScreen *inboxScreen = (InboxScreen *)[homeStoryboard instantiateViewControllerWithIdentifier:@"InboxScreen"];
            return inboxScreen;
        }
            break;
        case nsAppInvitation:
        {
            UIStoryboard *homeStoryboard = [UIStoryboard storyboardWithName:@"Home" bundle: nil];
            AppInvitation *appInvitation = (AppInvitation *)[homeStoryboard instantiateViewControllerWithIdentifier:@"AppInvitation"];
            return appInvitation;
        }
            break;
        case nsInboxDetailScreen:
        {
            UIStoryboard *homeStoryboard = [UIStoryboard storyboardWithName:@"Home" bundle: nil];
            InboxDetailScreen *inboxDetailScreen = (InboxDetailScreen *)[homeStoryboard instantiateViewControllerWithIdentifier:@"InboxDetailScreen"];
            return inboxDetailScreen;
        }
            break;
        case nsEventsScreen:
        {
            UIStoryboard *eventsStoryboard = [UIStoryboard storyboardWithName:@"Events" bundle: nil];
            EventsScreen *eventsScreen = (EventsScreen *)[eventsStoryboard instantiateViewControllerWithIdentifier:@"EventsScreen"];
            return eventsScreen;
        }
            break;
        case nsEventCreateScreen:
        {
            UIStoryboard *eventsStoryboard = [UIStoryboard storyboardWithName:@"Events" bundle: nil];
            EventCreateScreen *eventCreateScreen = (EventCreateScreen *)[eventsStoryboard instantiateViewControllerWithIdentifier:@"EventCreateScreen"];
            return eventCreateScreen;
        }
            break;
        case nsInviteScreen:
        {
            UIStoryboard *eventsStoryboard = [UIStoryboard storyboardWithName:@"Events" bundle: nil];
            InviteScreen *inviteScreen = (InviteScreen *)[eventsStoryboard instantiateViewControllerWithIdentifier:@"InviteScreen"];
            return inviteScreen;
        }
            break;
        case nsNotificationsScreen:
        {
            UIStoryboard *homeStoryboard = [UIStoryboard storyboardWithName:@"Home" bundle: nil];
            NotificationsScreen *notificationsScreen = (NotificationsScreen *)[homeStoryboard instantiateViewControllerWithIdentifier:@"NotificationsScreen"];
            return notificationsScreen;
        }
            break;
        case nsSearchScreen:
        {
            UIStoryboard *homeStoryboard = [UIStoryboard storyboardWithName:@"Home" bundle: nil];
            SearchScreen *searchScreen = (SearchScreen *)[homeStoryboard instantiateViewControllerWithIdentifier:@"SearchScreen"];
            return searchScreen;
        }
            break;
        case nsUserProfileScreen:
        {
            UIStoryboard *loginStoryboard = [UIStoryboard storyboardWithName:@"Login" bundle: nil];
            UserProfileScreen *userProfileScreen = (UserProfileScreen *)[loginStoryboard instantiateViewControllerWithIdentifier:@"UserProfileScreen"];
            return userProfileScreen;
        }
        case nsForgotPasswordMessage:
        {
            UIStoryboard *loginStoryboard = [UIStoryboard storyboardWithName:@"Login" bundle: nil];
            ForgotPasswordMessage *forgotPasswordMessage = (ForgotPasswordMessage *)[loginStoryboard instantiateViewControllerWithIdentifier:@"ForgotPasswordMessage"];
            return forgotPasswordMessage;
        }
            break;
        case nsSettingsScreen:
        {
            UIStoryboard *homeStoryboard = [UIStoryboard storyboardWithName:@"Home" bundle: nil];
            SettingsScreen *settingsScreen = (SettingsScreen *)[homeStoryboard instantiateViewControllerWithIdentifier:@"SettingsScreen"];
            return settingsScreen;
        }
            break;
        case nsAddQuestionScreen:
        {
            UIStoryboard *discussionStoryboard = [UIStoryboard storyboardWithName:@"Discussion" bundle: nil];
            AddQuestionScreen *addQuestionScreen = (AddQuestionScreen *)[discussionStoryboard instantiateViewControllerWithIdentifier:@"AddQuestionScreen"];
            return addQuestionScreen;
        }
            break;
            
        case nsChatScreen:
        {
            UIStoryboard *homeStoryboard = [UIStoryboard storyboardWithName:@"Home" bundle: nil];
            ChatViewController *chatVC = (ChatViewController *)[homeStoryboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
            return chatVC;
        }
            break;
    }
    
    return nil;
}


-(void)NavigationBarTheme:(UINavigationController*)navigationController
{
    navigationController.navigationBar.barTintColor = NAVIGATIONBAR_COLOR;
    navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    navigationController.navigationBar.translucent = NO;
    [navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
    navigationController.navigationBar.backIndicatorImage = [UIImage imageNamed:@"back.png"];
    navigationController.navigationBar.backIndicatorTransitionMaskImage = [UIImage imageNamed:@"back.png"];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(-100.f, 0) forBarMetrics:UIBarMetricsDefault];
}


- (void)openSideMenuFrom:(UIViewController*)centerScreen
{
    MCPanelViewController *PanelViewController = [[MCPanelViewController alloc] initWithRootViewController:[self getScreen:nsMenuScreen]];
    [PanelViewController presentInViewController:centerScreen withDirection:MCPanelAnimationDirectionLeft];
}

@end

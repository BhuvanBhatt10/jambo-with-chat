//
//  UserManager.m
//  Alumni
//
//  Created by Rahul Chandera on 22/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "UserManager.h"
#import "AppConstants.h"
#import "NetworkManager.h"
#import "NetworkAvailability.h"
#import "ApplicationManager.h"
#import "AppDelegate.h"
#import "CoreDataManager.h"
#import "EventManager.h"
#import "QuickBloxHelper.h"
#import "ServicesManager.h"
#import "UserdefaultManager.h"

NetworkManager *networkManager;
ApplicationManager *applicationManager;
CoreDataManager *coreDataManager;
EventManager *eventManager;

@implementation UserManager
@synthesize delegate;
 @synthesize managedObjectContext;

//Singleton object
static UserManager *userManager = nil;
+ (UserManager*)sharedInstance {
    if (!userManager) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            userManager = [[self alloc] init];
            [userManager initialize];
        });
    }
    return userManager;
}
-(void)initialize
{
    networkManager = [NetworkManager sharedInstance];
    applicationManager = [ApplicationManager sharedInstance];
    coreDataManager = [CoreDataManager sharedInstance];
    eventManager = [EventManager sharedInstance];
    
    self.nearByArray = [[NSMutableArray alloc]init];
    self.alumniArray = [[NSMutableArray alloc]init];
    
    [self getAlumniList:@"" Filters:@""];
    self.userEntity = [coreDataManager getUserObject];
    self.totalInvites = 5;
    /*
    self.userEntity.userId = [[[NSUserDefaults standardUserDefaults]valueForKey:@"userId"] intValue];
    self.userEntity.email = [[NSUserDefaults standardUserDefaults] valueForKey:@"email"];
    self.userEntity.firstName = [[NSUserDefaults standardUserDefaults] valueForKey:@"firstName"];
    self.userEntity.lastName = [[NSUserDefaults standardUserDefaults] valueForKey:@"lastName"];
    self.userEntity.profilePic = [[NSUserDefaults standardUserDefaults] valueForKey:@"profilePic"];
    */
    /*
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    managedObjectContext = [appDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"UserEntity" inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    NSArray *fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest error:nil];
    for (NSManagedObject *info in fetchedObjects)
    {
        self.userEntity.userId = (int)[info valueForKey:@"userId"];
        self.userEntity.email = [info valueForKey:@"email"];
        self.userEntity.firstName = [info valueForKey:@"firstName"];
        self.userEntity.lastName = [info valueForKey:@"lastName"];
        self.userEntity.profilePic = [info valueForKey:@"profilePic"];
    }*/
}



#pragma mark - User Login
-(void)userLogin:(NSString *)emailId Password:(NSString*)password {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *requestData = [NSString stringWithFormat:@"email=%@&password=%@&latitude=%@&longitude=%@",emailId,password,applicationManager.Latitude,applicationManager.Longitude];
        
        NSMutableDictionary *serviceResponse = [[NSMutableDictionary alloc]init];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            serviceResponse = [networkManager requestToServer:requestUserLogin requestData:requestData];
           
            if (serviceResponse != nil) {
                //Parse Data
                NSError* err = nil;
                NSMutableArray * eventsList = [NSMutableArray new];
                
                // Bhuvan- Parsing user data into model
                self.userEntity = [[UserEntity alloc]initWithDictionary:serviceResponse error:&err];
                
                
                if(self.userEntity.eventSet)
                {
                    for(MyEventEntity *event in self.userEntity.eventSet)
                    {
                        [event setDates];
                        [eventsList addObject:event];
                    }
                }
                
                NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"self.sDate" ascending:NO];
                NSArray *descriptors = [NSArray arrayWithObject:descriptor];
                NSArray *reverseOrder = [eventsList sortedArrayUsingDescriptors:descriptors];
                eventManager.myEventsArray = [NSMutableArray arrayWithArray:reverseOrder];
            }
            else{
                
                //If server cannot be connected
                serviceResponse = [[NSMutableDictionary alloc]init];
                [serviceResponse setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
                [serviceResponse setObject:[NSString stringWithFormat:@"%d",ERROR_SERVER_CONNECTION_ERROR] forKey:ERROR_CODE_KEY];
            }
        }
        else {
            
            //Connection not available
            [serviceResponse setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
            [serviceResponse setObject:[NSString stringWithFormat:@"%d",ERROR_INTERNET_NOT_AVAILABLE_CODE] forKey:ERROR_CODE_KEY];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //Call controller delegate method, and provide data
            [serviceResponse setObject:[NSString stringWithFormat:@"%d",requestUserLogin] forKey:REQUEST_KEY];

            //Setup Quickblox (Login, Connection etc.)
            QBUUser *loginUser = [QBUUser user];
            loginUser.email    = emailId;
            loginUser.login    = emailId;
            loginUser.password = emailId;
            
            [[ServicesManager instance] logInWithUser:loginUser completion:^(BOOL success, NSString *errorMessage) {
                if (success) {
                    
                    //Set Session
                    [[QuickBloxHelper sharedInstance] setSessionWithEmail:emailId];
                    
                    QBUpdateUserParameters *updateParameter = [QBUpdateUserParameters new];
                    updateParameter.fullName = [NSString stringWithFormat:@"%@ %@",self.userEntity.firstName,self.userEntity.lastName];
                    [QBRequest updateCurrentUser:updateParameter successBlock:^(QBResponse * _Nonnull response, QBUUser * _Nullable user) {
                        [self registerForRemoteNotifications];
                        NSString *deviceIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
                        // subscribing for push notifications
                        QBMSubscription *subscription = [QBMSubscription subscription];
                        subscription.notificationChannel = QBMNotificationChannelAPNS;
                        subscription.deviceUDID = deviceIdentifier;
                        subscription.deviceToken = applicationManager.deviceTokenData;
                        [QBRequest createSubscription:subscription successBlock:^(QBResponse *response, NSArray *objects) {
                            [delegate requestCompleted:serviceResponse];
                        } errorBlock:^(QBResponse *response) {
                            [delegate requestCompleted:serviceResponse];
                        }];
                    } errorBlock:^(QBResponse * _Nonnull response) {
                        [delegate requestCompleted:serviceResponse];
                    }];
                    
                } else {
                    [delegate requestCompleted:serviceResponse];
                }
            }];
            
            /*[[QuickBloxHelper sharedInstance] loginWithUsername:emailId andPassword:emailId success:^(id responseObject) {
                NSLog(@"User %@",responseObject);
                
                
                QBUpdateUserParameters *updateParameter = [QBUpdateUserParameters new];
                updateParameter.fullName = [NSString stringWithFormat:@"%@ %@",self.userEntity.firstName,self.userEntity.lastName];
                [QBRequest updateCurrentUser:updateParameter successBlock:^(QBResponse * _Nonnull response, QBUUser * _Nullable user) {
                    [self registerForRemoteNotifications];
                    NSString *deviceIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
                    
                    // subscribing for push notifications
                    QBMSubscription *subscription = [QBMSubscription subscription];
                    subscription.notificationChannel = QBMNotificationChannelAPNS;
                    subscription.deviceUDID = deviceIdentifier;
                    subscription.deviceToken = applicationManager.deviceTokenData;
                    [QBRequest createSubscription:subscription successBlock:^(QBResponse *response, NSArray *objects) {
                        [delegate requestCompleted:serviceResponse];
                    } errorBlock:^(QBResponse *response) {
                        [delegate requestCompleted:serviceResponse];
                    }];

                    
                } errorBlock:^(QBResponse * _Nonnull response) {
                   [delegate requestCompleted:serviceResponse];
                }];
            } failure:^(id error) {
                [delegate requestCompleted:serviceResponse];
                NSLog(@"Error Response %@",error);
            }];*/
        });
    });
}

- (void)registerForRemoteNotifications{
    
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else{
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound];
    }
}

#pragma mark - Get user detail
-(void)getUserDetail:(UserEntity *)currentUserEntity
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *boundary = [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
        
        NSMutableData *httpBody = [NSMutableData data];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"email\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", currentUserEntity.email] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"password\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", currentUserEntity.password] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSMutableDictionary *serviceResponse = [[NSMutableDictionary alloc]init];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            serviceResponse = [networkManager multipartPostRequestToServer:requestGetUserDetail requestData:httpBody Boundary:boundary];
            
            if (serviceResponse != nil) {
                
                //Parse Data
                NSError* err = nil;
                
                NSMutableArray * eventsList = [NSMutableArray new];
                
                NSString *password = [[NSString alloc]initWithString:self.userEntity.password];
                self.userEntity = [[UserEntity alloc]initWithDictionary:serviceResponse error:&err];
                self.userEntity.password = password;
                
                if(self.userEntity.eventSet)
                {
                    for(MyEventEntity *event in self.userEntity.eventSet)
                    {
                        [event setDates];
                        [eventsList addObject:event];
                    }
                }
                
                NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"self.sDate" ascending:NO];
                NSArray *descriptors = [NSArray arrayWithObject:descriptor];
                NSArray *reverseOrder = [eventsList sortedArrayUsingDescriptors:descriptors];
                eventManager.myEventsArray = [NSMutableArray arrayWithArray:reverseOrder];
            }
            else{
                
                //If server cannot be connected
                serviceResponse = [[NSMutableDictionary alloc]init];
                [serviceResponse setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
                [serviceResponse setObject:[NSString stringWithFormat:@"%d",ERROR_SERVER_CONNECTION_ERROR] forKey:ERROR_CODE_KEY];
            }
        }
        else{
            
            //Connection not available
            [serviceResponse setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
            [serviceResponse setObject:[NSString stringWithFormat:@"%d",ERROR_INTERNET_NOT_AVAILABLE_CODE] forKey:ERROR_CODE_KEY];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //Call controller delegate method, and provide data
            [serviceResponse setObject:[NSString stringWithFormat:@"%d",requestGetUserDetail] forKey:REQUEST_KEY];
            [delegate requestCompleted:serviceResponse];
        });
    });
}


#pragma mark - Update Profile
-(void)updateUser {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *boundary = [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
        
        NSMutableData *httpBody = [NSMutableData data];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"email\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", self.userEntity.email] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"password\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", self.userEntity.password] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"isVerified\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", @"1"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"firstName\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", self.userEntity.firstName] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"occupation\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", self.userEntity.occupation] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"companyName\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", self.userEntity.companyName] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"designation\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", self.userEntity.designation] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"latitude\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", self.userEntity.latitude] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"longitude\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", self.userEntity.longitude] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"experence\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", self.userEntity.totalExperience] dataUsingEncoding:NSUTF8StringEncoding]];
    
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"industry\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", self.userEntity.industry] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"jobFunction\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", self.userEntity.occupation] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"courseId\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", self.userEntity.program] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"privacyLevel\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", self.userEntity.privacyLevel] dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        NSMutableDictionary *responseData = [[NSMutableDictionary alloc]init];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            NSMutableDictionary *serviceResponse = [networkManager multipartPostRequestToServer:requestUpdateUser requestData:httpBody Boundary:boundary];
            
            if (serviceResponse != nil) {
                
            }
            else{
                
                //If server cannot be connected
                [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
                [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_SERVER_CONNECTION_ERROR] forKey:ERROR_CODE_KEY];
            }
        }
        else{
            
            //Connection not available
            [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
            [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_INTERNET_NOT_AVAILABLE_CODE] forKey:ERROR_CODE_KEY];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //Call controller delegate method, and provide data
            [responseData setObject:[NSString stringWithFormat:@"%d",requestUpdateUser] forKey:REQUEST_KEY];
            [delegate requestCompleted:responseData];
        });
    });
}

#pragma mark - Nearby List
-(void)getNearByList:(NSString*)orderBy {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *requestData = [NSString stringWithFormat:@"email=%@&password=%@&latitude=%@&longitude=%@&orderby=%@",userManager.userEntity.email,userManager.userEntity.password,applicationManager.Latitude,applicationManager.Longitude,orderBy];
        NSMutableDictionary *responseData = [[NSMutableDictionary alloc]init];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            NSMutableDictionary *serviceResponse = [networkManager requestToServer:requestNearByList requestData:requestData];
            
            if (serviceResponse != nil) {
                
                [self.nearByArray removeAllObjects];
                
                //Parse Data
                NSError* err = nil;
                for(NSDictionary *group in serviceResponse) {
                    
                    UserEntity *user = [[UserEntity alloc]initWithDictionary:group error:&err];
                    if(user)
                        [self.nearByArray addObject:user];
                }
            }
            else{
                
                //If server cannot be connected
                [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
                [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_SERVER_CONNECTION_ERROR] forKey:ERROR_CODE_KEY];
            }
        }
        else{
            
            //Connection not available
            [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
            [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_INTERNET_NOT_AVAILABLE_CODE] forKey:ERROR_CODE_KEY];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //Call controller delegate method, and provide data
            [responseData setObject:[NSString stringWithFormat:@"%d",requestNearByList] forKey:REQUEST_KEY];
            [delegate requestCompleted:responseData];
        });
    });
}



#pragma mark - Nearby List
-(void)getAlumniList:(NSString*)generalSearch Filters:(NSString*)filters{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *requestData = [NSString stringWithFormat:@"email=%@&password=%@&generalSearch=%@&%@",userManager.userEntity.email,userManager.userEntity.password,generalSearch,filters];
        NSMutableDictionary *responseData = [[NSMutableDictionary alloc]init];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            NSMutableDictionary *serviceResponse = [networkManager requestToServer:requestGetAluminiList requestData:requestData];
            
            if (serviceResponse != nil) {
                
                [self.alumniArray removeAllObjects];
                
                //Parse Data
                NSError* err = nil;
                for(NSDictionary *alumni in serviceResponse) {
                    
                    UserEntity *user= [[UserEntity alloc]initWithDictionary:alumni error:&err];
                    if(user)
                        [self.alumniArray addObject:user];
                }
                [responseData setObject:self.alumniArray forKey:DATA_KEY];
            }
            else{
                
                //If server cannot be connected
                [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
                [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_SERVER_CONNECTION_ERROR] forKey:ERROR_CODE_KEY];
            }
        }
        else{
            
            //Connection not available
            [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
            [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_INTERNET_NOT_AVAILABLE_CODE] forKey:ERROR_CODE_KEY];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //Call controller delegate method, and provide data
            [responseData setObject:[NSString stringWithFormat:@"%d",requestGetAluminiList] forKey:REQUEST_KEY];
            [delegate requestCompleted:responseData];
        });
    });
}



#pragma mark - Update User
-(void)mapUserWithDevice {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *requestData = [NSString stringWithFormat:@"email=%@&password=%@&registrationId=%@",userManager.userEntity.email,userManager.userEntity.password,applicationManager.deviceToken];
        NSMutableDictionary *serviceResponse = [[NSMutableDictionary alloc]init];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            serviceResponse = [networkManager postRequestToServer:requestMapUserWithDevice requestData:requestData];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
        });
    });
}



#pragma mark - Forgot Password
-(void)forgotPassword:(NSString *)emailId {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *requestData = [NSString stringWithFormat:@"email=%@",emailId];
        NSMutableDictionary *serviceResponse = [[NSMutableDictionary alloc]init];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            serviceResponse = [networkManager requestToServer:requestForgotPassword requestData:requestData];
            
            if (serviceResponse == nil) {
                
                //If server cannot be connected
                serviceResponse = [[NSMutableDictionary alloc]init];
                [serviceResponse setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
                [serviceResponse setObject:[NSString stringWithFormat:@"%d",ERROR_SERVER_CONNECTION_ERROR] forKey:ERROR_CODE_KEY];
            }
        }
        else{
            
            //Connection not available
            [serviceResponse setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
            [serviceResponse setObject:[NSString stringWithFormat:@"%d",ERROR_INTERNET_NOT_AVAILABLE_CODE] forKey:ERROR_CODE_KEY];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //Call controller delegate method, and provide data
            [serviceResponse setObject:[NSString stringWithFormat:@"%d",requestForgotPassword] forKey:REQUEST_KEY];
            [delegate requestCompleted:serviceResponse];
        });
    });
}

#pragma mark - Invite someone to app
-(void)inviteSomeoneToAppWithEmailId:(NSString *)emailId
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *requestData = [NSString stringWithFormat:@"email=%@&password=%@&newEmail=%@&instituteName=%@",userManager.userEntity.email,userManager.userEntity.password,emailId,self.userInstitute.instituteName];
        NSMutableDictionary *serviceResponse = [[NSMutableDictionary alloc]init];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            serviceResponse = [networkManager postRequestToServer:requestInviteToApp requestData:requestData];
            
            if (serviceResponse == nil) {
                
                //If server cannot be connected
                serviceResponse = [[NSMutableDictionary alloc]init];
                [serviceResponse setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
                [serviceResponse setObject:[NSString stringWithFormat:@"%d",ERROR_SERVER_CONNECTION_ERROR] forKey:ERROR_CODE_KEY];
            }
        }
        else{
            
            //Connection not available
            [serviceResponse setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
            [serviceResponse setObject:[NSString stringWithFormat:@"%d",ERROR_INTERNET_NOT_AVAILABLE_CODE] forKey:ERROR_CODE_KEY];
            
        }

        dispatch_async(dispatch_get_main_queue(), ^{
            //Call controller delegate method, and provide data
            [serviceResponse setObject:[NSString stringWithFormat:@"%d",requestInviteToApp] forKey:REQUEST_KEY];
            [delegate requestCompleted:serviceResponse];
        });
    });
}

#pragma mark - Get my institute
-(void)getMyReferredUser
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *requestData = [NSString stringWithFormat:@"email=%@&password=%@",userManager.userEntity.email,userManager.userEntity.password];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            NSMutableDictionary *serviceResponse = [networkManager postRequestToServer:requestMyReferredUsers requestData:requestData];
            
            if (serviceResponse != nil) {
                
                NSMutableArray * aluminis = [NSMutableArray new];
                NSError * error;
                
                for (NSDictionary * aluminiDic in serviceResponse) {
                    AluminiEntity * alumini = [[AluminiEntity alloc] initWithDictionary:aluminiDic error:&error];
                    //[aluminis addObject:alumini];
                }
                
                self.myReferredUsers = aluminis;
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
        });
    });
}

#pragma mark - Get my institute
-(void)getMyInstitute
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *requestData = [NSString stringWithFormat:@"email=%@&password=%@",userManager.userEntity.email,userManager.userEntity.password];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            NSMutableDictionary *serviceResponse = [networkManager postRequestToServer:requestGetMyInstitute requestData:requestData];
            
            if (serviceResponse != nil) {
                
                NSError * error;
                NSDictionary * instituteDict = [serviceResponse objectForKey:@"instituteId"];
                self.userInstitute = [[InstituteEntity alloc] initWithDictionary:instituteDict error:&error];
            }
        }

        dispatch_async(dispatch_get_main_queue(), ^{
        });
    });
}

#pragma mark - Get institute list
-(void)getInstituteList
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *requestData = [NSString stringWithFormat:@"email=%@&password=%@",userManager.userEntity.email,userManager.userEntity.password];
        
        NSMutableDictionary *serviceResponse = [NSMutableDictionary new];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            NSMutableDictionary *serviceResponse = [networkManager postRequestToServer:requestGetInstituteList requestData:requestData];
            
            if (serviceResponse != nil) {
                
                self.instituteList = [NSMutableArray new];
                NSError * error;
                
                for (NSDictionary * instituteDic in serviceResponse) {
                    InstituteEntity * institute = [[InstituteEntity alloc] initWithDictionary:instituteDic error:&error];
                    [self.instituteList addObject:institute];
                }
            }
            else
            {
                serviceResponse = [NSMutableDictionary new];
                [serviceResponse setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
                [serviceResponse setObject:[NSString stringWithFormat:@"%d",ERROR_SERVER_CONNECTION_ERROR] forKey:ERROR_CODE_KEY];
            }
        }
        else
        {
            [serviceResponse setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
            [serviceResponse setObject:[NSString stringWithFormat:@"%d",ERROR_INTERNET_NOT_AVAILABLE_CODE] forKey:ERROR_CODE_KEY];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [serviceResponse setObject:[NSString stringWithFormat:@"%d",requestGetInstituteList] forKey:REQUEST_KEY];
            [delegate requestCompleted:serviceResponse];
        });
    });
}

#pragma mark - Get organizations list
-(NSArray *)getOrganizationListForString:(NSString *)orgName
{
    NSString *requestData = [NSString stringWithFormat:@"companyName=%@",orgName];
    
    NSMutableDictionary *serviceResponse = [networkManager postRequestToServer:requestGetOrganizationList requestData:requestData];
    
    if (serviceResponse) {
        NSArray * companies = [serviceResponse valueForKey:@"companyName"];
        return companies;
    }
    
    return nil;
}

#pragma mark - Get organizations list
-(NSArray *)getDesignationListForString:(NSString *)desName
{
    NSString *requestData = [NSString stringWithFormat:@"designation=%@",desName];
    
    NSMutableDictionary *serviceResponse = [networkManager postRequestToServer:requestGetDesignationList requestData:requestData];
    
    if (serviceResponse) {
        NSArray * designations = [serviceResponse valueForKey:@"designation"];
        return designations;
    }
    
    return nil;
}

#pragma mark - Get course list
-(void)getCourseList
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *requestData = [NSString stringWithFormat:@"email=%@&password=%@",userManager.userEntity.email,userManager.userEntity.password];
        
        NSMutableDictionary *serviceResponse = [NSMutableDictionary new];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            NSMutableDictionary *serviceResponse = [networkManager postRequestToServer:requestGetCourseList requestData:requestData];
            
            if (serviceResponse != nil) {
                
                self.coursesList = [NSMutableArray new];
                NSError * error;
                
                for (NSDictionary * courseDic in serviceResponse) {
                    CourseEntity * course = [[CourseEntity alloc] initWithDictionary:courseDic error:&error];
                    [self.coursesList addObject:course];
                }                
            }
            else
            {
                serviceResponse = [NSMutableDictionary new];
                [serviceResponse setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
                [serviceResponse setObject:[NSString stringWithFormat:@"%d",ERROR_SERVER_CONNECTION_ERROR] forKey:ERROR_CODE_KEY];
            }
        }
        else
        {
            [serviceResponse setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
            [serviceResponse setObject:[NSString stringWithFormat:@"%d",ERROR_INTERNET_NOT_AVAILABLE_CODE] forKey:ERROR_CODE_KEY];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [serviceResponse setObject:[NSString stringWithFormat:@"%d",requestGetCourseList] forKey:REQUEST_KEY];
            [delegate requestCompleted:serviceResponse];
        });
    });
}

-(NSArray *)getProgramsArrayForInstituteId:(int)instituteId
{
    NSPredicate *filterPredicateForFilter =  [NSPredicate predicateWithFormat:@"SELF.instituteId == %d",instituteId];
    NSArray *courses = [self.coursesList filteredArrayUsingPredicate:filterPredicateForFilter];

    if(courses.count>0)
        return [courses valueForKey:@"courseName"];
    else
        return nil;
}

-(NSArray *)getStreamsArrayForInstituteId:(int)instituteId
{
    NSPredicate *filterPredicateForFilter =  [NSPredicate predicateWithFormat:@"SELF.instituteId == %d",instituteId];
    NSArray *courses = [self.coursesList filteredArrayUsingPredicate:filterPredicateForFilter];
    
    if(courses.count>0)
        return [courses valueForKey:@"stream"];
    else
        return nil;
}

-(AluminiEntity*)getAlumniById:(int)alumniId
{
    NSPredicate *filterPredicateForFilter =  [NSPredicate predicateWithFormat:@"SELF.userId == %d",alumniId];
    NSArray *alumni = [self.alumniArray filteredArrayUsingPredicate:filterPredicateForFilter];
    if(alumni.count>0)
        return alumni[0];
    else
        return nil;
}


@end

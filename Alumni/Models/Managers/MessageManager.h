//
//  MessageManager.h
//  Alumni
//
//  Created by Rahul Chandera on 26/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol MessageManagerDelegate
-(void)requestCompleted:(id)response;
@end

@interface MessageManager : NSObject

@property (strong,nonatomic) id <MessageManagerDelegate> delegate;
@property(nonatomic,strong) NSMutableArray *messageArray;

+ (MessageManager*)sharedInstance;
-(void)getMessageList;
-(void)sendMessage:(NSString*)messageText withImage:(id)image ToAluminiId:(int)toAluminiId;
-(void)updateMessageStatus:(NSString*)messageIdList Status:(int)status;

@end

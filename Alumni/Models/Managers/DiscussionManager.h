//
//  DiscussionManager.h
//  Alumni
//
//  Created by Rahul Chandera on 23/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "GroupEntity.h"

@protocol DiscussionManagerDelegate
-(void)requestCompleted:(id)response;
@end

@interface DiscussionManager : NSObject

@property (strong,nonatomic) id <DiscussionManagerDelegate> delegate;
@property(nonatomic,strong) NSMutableArray *allGroupsArray;
@property(nonatomic,strong) NSMutableArray *subscribedGroupsArray;
@property(nonatomic,strong) NSMutableArray *invitedGroupsArray;
@property(nonatomic,strong) NSMutableArray *myGroupsArray;

+ (DiscussionManager*)sharedInstance;
-(void)getGroupsList;
-(void)createGroup:(NSString*)name Description:(NSString*)description InviteList:(NSString*)inviteList;
-(void)postAnswer:(NSString*)queId Answer:(NSString*)answer withImage:(id)image;
-(void)postQuestionToGroup:(int)tagId Question:(NSString*)question;
-(void)upvoteQuestionWithId:(int)questionId Score:(int)score;
-(void)upvoteAnswerWithId:(int)answerId Score:(int)score;
-(void)getTagById:(int)tagId;
-(void)inviteTagMembers:(NSString*)tagId InviteList:(NSString*)inviteList;
-(void)updateTagAdminBulk:(NSString*)tagId InviteList:(NSString*)inviteList;
-(void)responseTagInvite:(int)tagId ResponseStatus:(int)responseStatus;
-(void)joinTag:(int)tagId;
-(void)leaveTag:(int)tagId;
- (GroupEntity*)getTagEntity:(int)tagId;
- (void)filterGroupOfUser:(UserEntity*)userEntity;

@end

//
//  NavigationManager.h
//  Alumni
//
//  Created by Rahul Chandera on 13/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <UIKit/UIKit.h>

//Screen Type
typedef enum {
    nsLoginScreen,
    nsBuildProfileBasic,
    nsBuildProfileDetails,
    nsHomeScreen,
    nsQuestionsCollectionView,
    nsQuestionListScreen,
    nsMenuScreen,
    nsQuestionDetailScreen,
    nsCreateGroupScreen,
    nsGroupsListScreen,
    nsGroupDetailsScreen,
    nsEventCreateScreen,
    nsNearbyScreen,
    nsContactInfoPopup,
    nsInboxScreen,
    nsAppInvitation,
    nsInboxDetailScreen,
    nsEventsScreen,
    nsInviteScreen,
    nsNotificationsScreen,
    nsSearchScreen,
    nsUserProfileScreen,
    nsForgotPasswordMessage,
    nsSettingsScreen,
    nsAddQuestionScreen,
    nsChatScreen
}NavigateToScreen;

@interface NavigationManager : UIViewController

+ (NavigationManager*)sharedInstance;
-(UIViewController*)getScreen:(NavigateToScreen)screenType;
-(void)NavigationBarTheme:(UINavigationController*)navigationController;
- (void)openSideMenuFrom:(UIViewController*)centerScreen;

@end

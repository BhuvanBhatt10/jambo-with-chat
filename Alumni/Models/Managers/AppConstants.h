//
//  AppConstants.h
//  Alumni
//
//  Created by Rahul Chandera on 14/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>

//API key to use for Google places API
#define GOOGLE_PLACES_API_KEY @"AIzaSyA3eQ4IE8D5GtmoezVhxfYNy8qxGKP1D6A"

//Colors
#define NAVIGATIONBAR_COLOR    [UIColor colorWithRed:33.0/255.0 green:150.0/255.0 blue:243.0/255.0 alpha:1.0]
#define LIGHT_GRAY_COLOR       [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:242.0/255.0 alpha:1.0]
#define LIGHT_RED_COLOR        [UIColor colorWithRed:255.0/255.0 green:102.0/255.0 blue:102.0/255.0 alpha:1.0]
#define GREEN_TRASPARENT_COLOR [UIColor colorWithRed:20.0/255.0 green:213.0/255.0 blue:80.0/255.0 alpha:0.8]
#define LIGHT_GREENT_COLOR     [UIColor colorWithRed:40.0/255.0 green:180.0/255.0 blue:80.0/255.0 alpha:1.0]
#define LIGHT_BLUE_COLOR       [UIColor colorWithRed:51.0/255.0 green:153.0/255.0 blue:200.0/255.0 alpha:1.0]
#define WHITE_TRASPARENT_COLOR [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.4]

//Types of service request
typedef enum {
    requestRegisterApp,
    requestUserLogin,
    requestGetUserDetail,
    requestUpdateUser,
    requestDiscussionList,
    requestNearByList,
    requestGetMessageList,
    requestGetAluminiList,
    requestGetEventList,
    requestCreateEvent,
    requestUpdateEvent,
    requestSendMessage,
    requestUpdateMessage,
    requestCreateTag,
    requestPostAnswer,
    requestPostQuestion,
    requestUpvoteQuestion,
    requestUpvoteAnswer,
    requestGetTagById,
    requestInviteTagMembers,
    requestResponseTagInvite,
    requestJoinTag,
    requestLeaveTag,
    requestUpdateTagAdminBulk,
    requestGetSkillList,
    requestMapUserWithDevice,
    requestForgotPassword,
    requestResponseEvent,
    requestInviteToApp,
    requestGetMyInstitute,
    requestGetInstituteList,
    requestGetCourseList,
    requestMyReferredUsers,
    requestGetOrganizationList,
    requestGetDesignationList
}RequestType;

//Notification keys
#define NOTIFICATION_CREATED_UPDATED_EVENT     @"CreatedOrUpdatedEvent"
#define NOTIFICATION_REFRESH_GROUP_DATA_EVENT  @"RefreshGroupData"
#define NOTIFICATION_UPDATE_TIMER_HIT          @"UpdateTimerHit"

//Keys used in requests
#define RESULT_KEY                  @"error"
#define FALSE_RESULT_KEY            @"1"
#define ERROR_CODE_KEY              @"errorCode"
#define DATA_KEY                    @"data"
#define REQUEST_KEY                 @"RequestType"


//error codes
#define ERROR_INTERNET_NOT_AVAILABLE_CODE             1000
#define ERROR_SERVER_CONNECTION_ERROR                 404
#define ERROR_AUTHENTICATION_FAILED                   101
#define ERROR_DATABASE_SERVICE_NOT_AVAILABLE          102
#define ERROR_VALIDATION_ERROR                        103
#define ERROR_USER_ALREADY_EXISTS                     104
#define ERROR_INTERNAL_SERVICE_ERROR                  105
#define ERROR_IO_OPERATION_FAILED                     106
#define ERROR_WRONG_PARAMETERS                        107
#define ERROR_ORDER_DOES_NOT_EXISTS                   108
#define ERROR_SERVICE_DOES_NOT_EXISTS                 109
#define ERROR_ITEMS_NOT_VALIDATED_IN_ORDER            110
#define ERROR_RESTAURANT_VERSION_MISMATCH             111
#define ERROR_USER_NOT_FOUND                          114
#define ERROR_APPLICATION_VERSION_MISMATCH            115
#define ERROR_UNKNOWN_ERROR                           116
#define ERROR_DUPLICATE_REST                          117
#define ERROR_DUPLICATE_EMAIL                         118
#define ERROR_INVALID_PASSWORD                        119
#define SAME_RESTAURANT_VERSION                       202
#define ERROR_LICENSE_LIMIT_EXCEEDED                  121
#define ERROR_SUBSCRIPTION_EXPIRED                    127
#define ERROR_INVALID_TABLE_NUMBER                    122
#define ERROR_CODE_VALID_RESPONSE                     200

#define DEFAULT_USER_IMAGE @"default_user.png"


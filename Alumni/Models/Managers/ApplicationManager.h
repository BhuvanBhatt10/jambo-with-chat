//
//  ApplicationManager.h
//  Alumni
//
//  Created by Rahul Chandera on 22/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface ApplicationManager : NSObject <CLLocationManagerDelegate>

@property (nonatomic,strong) NSString *Latitude;
@property (nonatomic,strong) NSString *Longitude;
@property (nonatomic,strong) NSString *currentCity;
@property (nonatomic,strong) NSString *currentCountry;
@property (nonatomic,strong) NSTimer *updateTimer;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) NSString *deviceToken;
@property (strong, nonatomic) NSData *deviceTokenData;
@property (strong, nonatomic) NSMutableArray *notificationsArray;
@property (strong, nonatomic) NSMutableArray *skillsArray;

+(ApplicationManager*)sharedInstance;
- (void)updateLocation;
-(void)registerAppForNotification;
-(void)getSkillList;

@end

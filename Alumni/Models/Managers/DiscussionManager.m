//
//  DiscussionManager.m
//  Alumni
//
//  Created by Rahul Chandera on 23/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "DiscussionManager.h"
#import "AppConstants.h"
#import "NetworkManager.h"
#import "NetworkAvailability.h"
#import "ApplicationManager.h"
#import "MyGroupsEntity.h"
#import "UserManager.h"

NetworkManager *networkManager;
ApplicationManager *applicationManager;
UserManager *userManager;

@implementation DiscussionManager
@synthesize delegate;
@synthesize allGroupsArray, subscribedGroupsArray, invitedGroupsArray, myGroupsArray;

//Singleton object
static DiscussionManager *discussionManager = nil;
+ (DiscussionManager*)sharedInstance {
    if (!discussionManager) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            discussionManager = [[self alloc] init];
            [discussionManager initialize];
        });
    }
    return discussionManager;
}
-(void)initialize
{
    networkManager = [NetworkManager sharedInstance];
    applicationManager = [ApplicationManager sharedInstance];
    userManager = [UserManager sharedInstance];
    self.allGroupsArray = [[NSMutableArray alloc]init];
    self.subscribedGroupsArray = [[NSMutableArray alloc]init];
    self.invitedGroupsArray = [[NSMutableArray alloc]init];
    self.myGroupsArray = [[NSMutableArray alloc] init];
}

#pragma mark - User Login
-(void)getGroupsList {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *requestData = [NSString stringWithFormat:@"email=%@&password=%@",userManager.userEntity.email,userManager.userEntity.password];
        NSMutableDictionary *responseData = [[NSMutableDictionary alloc]init];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            NSMutableDictionary *serviceResponse = [networkManager requestToServer:requestDiscussionList requestData:requestData];
            
            if (serviceResponse != nil) {
                
                [self.allGroupsArray removeAllObjects];
                [self.myGroupsArray removeAllObjects];
                
                //Parse Data
                NSError* err = nil;
                for(NSDictionary *group in serviceResponse) {
                
                    GroupEntity *groupEntity = [[GroupEntity alloc]initWithDictionary:group error:&err];
                    
                    if(groupEntity)
                    {
                        [groupEntity applySortingOnQuestions];
                        [self.allGroupsArray addObject:groupEntity];
                        
                        for(MyGroupsEntity *group in userManager.userEntity.tags)
                        {
                            if (groupEntity.tagId == group.tagId && [group.role isEqualToString:@"admin"]) {
                                [self.myGroupsArray addObject:groupEntity];
                            }
                        }
                    }
                }
            }
            else{
                
                //If server cannot be connected
                [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
                [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_SERVER_CONNECTION_ERROR] forKey:ERROR_CODE_KEY];
            }
        }
        else{
            
            //Connection not available
            [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
            [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_INTERNET_NOT_AVAILABLE_CODE] forKey:ERROR_CODE_KEY];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //Call controller delegate method, and provide data
            [responseData setObject:[NSString stringWithFormat:@"%d",requestDiscussionList] forKey:REQUEST_KEY];
            [delegate requestCompleted:responseData];
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REFRESH_GROUP_DATA_EVENT object:nil userInfo:nil];
        });
    });
}

#pragma mark - Create Group
-(void)createGroup:(NSString*)name Description:(NSString*)description InviteList:(NSString*)inviteList {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *requestData = [NSString stringWithFormat:@"email=%@&password=%@&tagName=%@&description=%@&inviteAluminiList=%@&instituteId=1",userManager.userEntity.email,userManager.userEntity.password,name,description,inviteList];
        NSMutableDictionary *responseData = [[NSMutableDictionary alloc]init];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            NSMutableDictionary *serviceResponse = [networkManager postRequestToServer:requestCreateTag requestData:requestData];
            
            if (serviceResponse == nil || [[serviceResponse objectForKey:RESULT_KEY] boolValue]) {
            
                //If server cannot be connected
                [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
                [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_SERVER_CONNECTION_ERROR] forKey:ERROR_CODE_KEY];
            }
        }
        else{
            
            //Connection not available
            [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
            [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_INTERNET_NOT_AVAILABLE_CODE] forKey:ERROR_CODE_KEY];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //Call controller delegate method, and provide data
            [responseData setObject:[NSString stringWithFormat:@"%d",requestCreateTag] forKey:REQUEST_KEY];
            [delegate requestCompleted:responseData];
        });
    });
}

#pragma mark - Post Answer
-(void)postAnswer:(NSString*)queId Answer:(NSString*)answer withImage:(id)image {
    
    //Remove whitespace
    answer = [answer stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    answer = [answer stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    answer = [answer stringByReplacingOccurrencesOfString:@"\U0000fffc" withString:@""];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *boundary = [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
         
        NSMutableData *httpBody = [NSMutableData data];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"email\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", userManager.userEntity.email] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"password\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", userManager.userEntity.password] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"queId\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", queId] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"answer\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", answer] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *imageData = UIImagePNGRepresentation(image);
        if (imageData)
        {
            [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"picture\"; filename=answerImage.png\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [httpBody appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [httpBody appendData:imageData];
            [httpBody appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        NSMutableDictionary *responseData = [[NSMutableDictionary alloc]init];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            NSMutableDictionary *serviceResponse = [networkManager multipartPostRequestToServer:requestPostAnswer requestData:httpBody Boundary:boundary];
            
            if (serviceResponse == nil || [[serviceResponse objectForKey:RESULT_KEY] boolValue]) {
                
                //If server cannot be connected
                [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
                [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_SERVER_CONNECTION_ERROR] forKey:ERROR_CODE_KEY];
            }
            else
            {
                //Response true
                responseData = serviceResponse;
            }
        }
        else{
            
            //Connection not available
            [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
            [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_INTERNET_NOT_AVAILABLE_CODE] forKey:ERROR_CODE_KEY];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //Call controller delegate method, and provide data
            [responseData setObject:[NSString stringWithFormat:@"%d",requestPostAnswer] forKey:REQUEST_KEY];
            [delegate requestCompleted:responseData];
        });
    });
}

#pragma mark - Post Question
-(void)postQuestionToGroup:(int)tagId Question:(NSString*)question {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSMutableDictionary *responseData = [[NSMutableDictionary alloc]init];
        
        NSString *boundary = [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
        NSMutableData *httpBody = [NSMutableData data];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"email\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", userManager.userEntity.email] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"password\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", userManager.userEntity.password] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"tagId\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%d\r\n", tagId] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"question\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", question] dataUsingEncoding:NSUTF8StringEncoding]];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            NSMutableDictionary *serviceResponse = [networkManager multipartPostRequestToServer:requestPostQuestion requestData:httpBody Boundary:boundary];
            
            if (serviceResponse == nil || [[serviceResponse objectForKey:RESULT_KEY] boolValue]) {
                
                //If server cannot be connected
                [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
                [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_SERVER_CONNECTION_ERROR] forKey:ERROR_CODE_KEY];
            }
        }
        else{
            
            //Connection not available
            [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
            [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_INTERNET_NOT_AVAILABLE_CODE] forKey:ERROR_CODE_KEY];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //Call controller delegate method, and provide data
            [responseData setObject:[NSString stringWithFormat:@"%d",requestPostQuestion] forKey:REQUEST_KEY];
            [delegate requestCompleted:responseData];
        });
    });
}

#pragma mark - Upvote question
-(void)upvoteQuestionWithId:(int)questionId Score:(int)score
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *requestData = [NSString stringWithFormat:@"email=%@&password=%@&queId=%d&score=%d",userManager.userEntity.email,userManager.userEntity.password,questionId,score];
        NSMutableDictionary *responseData = [[NSMutableDictionary alloc]init];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            NSMutableDictionary *serviceResponse = [networkManager postRequestToServer:requestUpvoteQuestion requestData:requestData];
            
            if (serviceResponse != nil) {

                
            }
            else{
                
                //If server cannot be connected
                [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
                [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_SERVER_CONNECTION_ERROR] forKey:ERROR_CODE_KEY];
            }
        }
        else{
            
            //Connection not available
            [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
            [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_INTERNET_NOT_AVAILABLE_CODE] forKey:ERROR_CODE_KEY];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //Call controller delegate method, and provide data
            [responseData setObject:[NSString stringWithFormat:@"%d",requestUpvoteQuestion] forKey:REQUEST_KEY];
            [delegate requestCompleted:responseData];
        });
    });
}

#pragma mark - Upvote question
-(void)upvoteAnswerWithId:(int)answerId Score:(int)score
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *requestData = [NSString stringWithFormat:@"email=%@&password=%@&ansId=%d&score=%d",userManager.userEntity.email,userManager.userEntity.password,answerId,score];
        NSMutableDictionary *responseData = [[NSMutableDictionary alloc]init];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            NSMutableDictionary *serviceResponse = [networkManager postRequestToServer:requestUpvoteAnswer requestData:requestData];
            
            if (serviceResponse != nil) {
                
                
            }
            else {
                
                //If server cannot be connected
                [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
                [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_SERVER_CONNECTION_ERROR] forKey:ERROR_CODE_KEY];
            }
        }
        else {
            
            //Connection not available
            [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
            [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_INTERNET_NOT_AVAILABLE_CODE] forKey:ERROR_CODE_KEY];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //Call controller delegate method, and provide data
            [responseData setObject:[NSString stringWithFormat:@"%d",requestUpvoteAnswer] forKey:REQUEST_KEY];
            [delegate requestCompleted:responseData];
        });
    });
}

#pragma mark - Get Tag By Id
-(void)getTagById:(int)tagId {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *requestData = [NSString stringWithFormat:@"email=%@&password=%@&tagId=%@",userManager.userEntity.email,userManager.userEntity.password,[NSString stringWithFormat:@"%d",tagId]];
        NSMutableDictionary *responseData = [[NSMutableDictionary alloc]init];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            NSMutableDictionary *serviceResponse = [networkManager postRequestToServer:requestGetTagById requestData:requestData];
            
            if (serviceResponse != nil) {
                
                //Parse Data
                NSError* err = nil;
                GroupEntity *groupEntity = [[GroupEntity alloc]initWithDictionary:serviceResponse error:&err];
                
                if(groupEntity)
                {
                    [groupEntity applySortingOnQuestions];
                    [responseData setObject:groupEntity forKey:DATA_KEY];
                }
                
            }
            else{
                
                //If server cannot be connected
                [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
                [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_SERVER_CONNECTION_ERROR] forKey:ERROR_CODE_KEY];
            }
        }
        else{
            
            //Connection not available
            [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
            [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_INTERNET_NOT_AVAILABLE_CODE] forKey:ERROR_CODE_KEY];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //Call controller delegate method, and provide data
            [responseData setObject:[NSString stringWithFormat:@"%d",requestGetTagById] forKey:REQUEST_KEY];
            [delegate requestCompleted:responseData];
        });
    });
}

#pragma mark - Invite TagMembers
-(void)inviteTagMembers:(NSString*)tagId InviteList:(NSString*)inviteList {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *requestData = [NSString stringWithFormat:@"email=%@&password=%@&tagId=%@&inviteAluminiList=%@",userManager.userEntity.email,userManager.userEntity.password,tagId,inviteList];
        NSMutableDictionary *responseData = [[NSMutableDictionary alloc]init];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            NSMutableDictionary *serviceResponse = [networkManager postRequestToServer:requestInviteTagMembers requestData:requestData];
            
            if (serviceResponse == nil || [[serviceResponse objectForKey:RESULT_KEY] boolValue]) {
                
                //If server cannot be connected
                [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
                [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_SERVER_CONNECTION_ERROR] forKey:ERROR_CODE_KEY];
            }
        }
        else{
            
            //Connection not available
            [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
            [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_INTERNET_NOT_AVAILABLE_CODE] forKey:ERROR_CODE_KEY];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //Call controller delegate method, and provide data
            [responseData setObject:[NSString stringWithFormat:@"%d",requestInviteTagMembers] forKey:REQUEST_KEY];
            [delegate requestCompleted:responseData];
        });
    });
}

#pragma mark - Update TagAdmin Bulk
-(void)updateTagAdminBulk:(NSString*)tagId InviteList:(NSString*)inviteList {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *requestData = [NSString stringWithFormat:@"email=%@&password=%@&tagId=%@&aluminiList=%@",userManager.userEntity.email,userManager.userEntity.password,tagId,inviteList];
        NSMutableDictionary *responseData = [[NSMutableDictionary alloc]init];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            NSMutableDictionary *serviceResponse = [networkManager postRequestToServer:requestUpdateTagAdminBulk requestData:requestData];
            
            if (serviceResponse == nil || [[serviceResponse objectForKey:RESULT_KEY] boolValue]) {
                
                //If server cannot be connected
                [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
                [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_SERVER_CONNECTION_ERROR] forKey:ERROR_CODE_KEY];
            }
        }
        else{
            
            //Connection not available
            [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
            [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_INTERNET_NOT_AVAILABLE_CODE] forKey:ERROR_CODE_KEY];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //Call controller delegate method, and provide data
            [responseData setObject:[NSString stringWithFormat:@"%d",requestUpdateTagAdminBulk] forKey:REQUEST_KEY];
            [delegate requestCompleted:responseData];
        });
    });
}

#pragma mark - Response TagInvite
-(void)responseTagInvite:(int)tagId ResponseStatus:(int)responseStatus {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *requestData = [NSString stringWithFormat:@"email=%@&password=%@&tagId=%d&responseStatus=%d",userManager.userEntity.email,userManager.userEntity.password,tagId,responseStatus];
        NSMutableDictionary *responseData = [[NSMutableDictionary alloc]init];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            NSMutableDictionary *serviceResponse = [networkManager postRequestToServer:requestResponseTagInvite requestData:requestData];
            
            if (serviceResponse == nil || [[serviceResponse objectForKey:RESULT_KEY] boolValue]) {
                
                //If server cannot be connected
                [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
                [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_SERVER_CONNECTION_ERROR] forKey:ERROR_CODE_KEY];
            }
        }
        else{
            
            //Connection not available
            [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
            [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_INTERNET_NOT_AVAILABLE_CODE] forKey:ERROR_CODE_KEY];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //Call controller delegate method, and provide data
            [responseData setObject:[NSString stringWithFormat:@"%d",requestResponseTagInvite] forKey:REQUEST_KEY];
            [delegate requestCompleted:responseData];
        });
    });
}

#pragma mark - Join Tag
-(void)joinTag:(int)tagId {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *requestData = [NSString stringWithFormat:@"email=%@&password=%@&tagIdList=%d",userManager.userEntity.email,userManager.userEntity.password,tagId];
        NSMutableDictionary *responseData = [[NSMutableDictionary alloc]init];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            NSMutableDictionary *serviceResponse = [networkManager postRequestToServer:requestJoinTag requestData:requestData];
            
            if (serviceResponse == nil || [[serviceResponse objectForKey:RESULT_KEY] boolValue]) {
                
                //If server cannot be connected
                [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
                [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_SERVER_CONNECTION_ERROR] forKey:ERROR_CODE_KEY];
            }
        }
        else{
            
            //Connection not available
            [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
            [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_INTERNET_NOT_AVAILABLE_CODE] forKey:ERROR_CODE_KEY];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //Call controller delegate method, and provide data
            [responseData setObject:[NSString stringWithFormat:@"%d",requestJoinTag] forKey:REQUEST_KEY];
            [delegate requestCompleted:responseData];
        });
    });
}

#pragma mark - Leave Tag
-(void)leaveTag:(int)tagId {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *requestData = [NSString stringWithFormat:@"email=%@&password=%@&tagId=%d",userManager.userEntity.email,userManager.userEntity.password,tagId];
        NSMutableDictionary *responseData = [[NSMutableDictionary alloc]init];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            NSMutableDictionary *serviceResponse = [networkManager postRequestToServer:requestLeaveTag requestData:requestData];
            
            if (serviceResponse == nil || [[serviceResponse objectForKey:RESULT_KEY] boolValue]) {
                
                //If server cannot be connected
                [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
                [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_SERVER_CONNECTION_ERROR] forKey:ERROR_CODE_KEY];
            }
        }
        else{
            
            //Connection not available
            [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
            [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_INTERNET_NOT_AVAILABLE_CODE] forKey:ERROR_CODE_KEY];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //Call controller delegate method, and provide data
            [responseData setObject:[NSString stringWithFormat:@"%d",requestLeaveTag] forKey:REQUEST_KEY];
            [delegate requestCompleted:responseData];
        });
    });
}

- (GroupEntity*)getTagEntity:(int)tagId
{
    NSPredicate *filterPredicateForFilter =  [NSPredicate predicateWithFormat:@"SELF.tagId == %d",tagId];
    NSArray *alumni = [self.allGroupsArray filteredArrayUsingPredicate:filterPredicateForFilter];
    if(alumni.count>0)
        return alumni[0];
    else
        return nil;
}

- (void)filterGroupOfUser:(UserEntity*)userEntity
{
    [self.subscribedGroupsArray removeAllObjects];
    [self.invitedGroupsArray removeAllObjects];
    
    for(MyGroupsEntity *grpEntity in userEntity.tags)
    {
        if(grpEntity.status == 0)
        {
            GroupEntity *groupEntity = [self getTagEntity:grpEntity.tagId];
            [groupEntity applySortingOnQuestions];
            groupEntity.role = grpEntity.role;
            [groupEntity setStatus:grpEntity.status];
            
            if (groupEntity)
                [self.invitedGroupsArray addObject:groupEntity];
        }
        else if(grpEntity.status == 1)
        {
            GroupEntity *groupEntity = [self getTagEntity:grpEntity.tagId];
            [groupEntity applySortingOnQuestions];
            groupEntity.role = grpEntity.role;
            [groupEntity setStatus:grpEntity.status];
            
            if (groupEntity)
                [self.subscribedGroupsArray addObject:groupEntity];
        }
    }
}

@end

//
//  CoreDataManager.h
//  Jambo
//
//  Created by Rahul Chandera on 19/07/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserEntity.h"
#import "MessageGroupEntity.h"
#import "MessageEntity.h"

@interface CoreDataManager : NSObject

+ (CoreDataManager*)sharedInstance;
- (void)saveUserObject:(UserEntity*)userEntity;
- (UserEntity*)getUserObject;

- (void)saveEventObject:(MyEventEntity*)myEventEntity;
- (NSMutableArray*)getEventObject;

- (void)saveMessageGroupObject:(MessageGroupEntity*)messageGroupEntity;
- (NSMutableArray*)getMessageGroupObject;
- (void)addMessageToGroup:(MessageGroupEntity*)messageGroupEntity MessageEntity:(MessageEntity*)messageEntity;

@end

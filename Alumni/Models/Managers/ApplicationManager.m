//
//  ApplicationManager.m
//  Alumni
//
//  Created by Rahul Chandera on 22/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "ApplicationManager.h"
#import "NetworkManager.h"
#import "NetworkAvailability.h"
#import "UserManager.h"
#import <MapKit/MapKit.h>
#import <Quickblox/Quickblox.h>

NetworkManager *networkManager;
UserManager *userManager;

@implementation ApplicationManager
@synthesize Latitude,Longitude;
@synthesize deviceToken;
@synthesize notificationsArray,skillsArray;
@synthesize deviceTokenData;

//Singleton object
static ApplicationManager *applicationManager = nil;
+ (ApplicationManager*)sharedInstance {
    if (!applicationManager) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            applicationManager = [[self alloc] init];
            [applicationManager initialize];
        });
    }
    return applicationManager;
}

-(void)initialize
{
    networkManager = [NetworkManager sharedInstance];
    userManager = [UserManager sharedInstance];
    self.deviceToken = [[NSString alloc]init];
    self.deviceTokenData = [[NSData alloc] init];
    self.notificationsArray = [[NSMutableArray alloc]init];
    self.skillsArray = [[NSMutableArray alloc]init];
    
    self.updateTimer = [NSTimer scheduledTimerWithTimeInterval:10000 target:self selector:@selector(updateTimerHit) userInfo:nil repeats:YES];
    
    self.Latitude = @"0.0";
    self.Longitude = @"0.0";
    self.currentCity = @"";
    self.currentCountry = @"";
    [self updateLocation];
}

- (void)updateTimerHit
{
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_UPDATE_TIMER_HIT object:nil userInfo:nil];
}

- (void)updateLocation {

    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    BOOL notify = NO;
    
    if ([self.Latitude floatValue] == 0 && [self.Longitude floatValue] == 0)
        notify = YES;
        
    self.Latitude = [NSString stringWithFormat:@"%f",[[locations lastObject] coordinate].latitude];
    self.Longitude = [NSString stringWithFormat:@"%f",[[locations lastObject] coordinate].longitude];
    NSLog(@"Detecting location : %@, %@", Latitude, Longitude);
    
    CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
    [geoCoder reverseGeocodeLocation:[locations lastObject] completionHandler:^(NSArray *placemarks, NSError *error) {
        for (CLPlacemark * placemark in placemarks) {
            
            self.currentCity = [placemark locality];
            self.currentCountry = [placemark country];
            NSLog(@"locality = %@, %@, %@",self.currentCity, self.currentCountry, [placemark ISOcountryCode]);
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_UPDATE_TIMER_HIT object:nil userInfo:nil];
        }
    }];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    self.Latitude = @"0.0";
    self.Longitude = @"0.0";
    self.currentCity = @"";
    self.currentCountry = @"";
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusDenied || status == kCLAuthorizationStatusNotDetermined) {
        self.Latitude = @"0.0";
        self.Longitude = @"0.0";
        self.currentCity = @"";
        self.currentCountry = @"";
    }
    
    if (status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_UPDATE_TIMER_HIT object:nil userInfo:nil];
    }
}

#pragma mark - Register App
-(void)registerAppForNotification {
    
    if (self.deviceToken.length > 0) {
        

        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            NSString *requestData = [NSString stringWithFormat:@"email=%@&registrationId=%@&os=ios",userManager.userEntity.email,self.deviceToken];
            NSMutableDictionary *serviceResponse = [[NSMutableDictionary alloc]init];
            
            //If Connection available then move ahead
            if ([NetworkAvailability CheckInternetConnection]) {
                
                serviceResponse = [networkManager postRequestToServer:requestRegisterApp requestData:requestData];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [userManager mapUserWithDevice];
            });
        });
    }
}

#pragma mark - Get Skills
-(void)getSkillList {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *requestData = [NSString stringWithFormat:@"email=%@&password=%@",userManager.userEntity.email,userManager.userEntity.password];
        NSMutableDictionary *serviceResponse = [[NSMutableDictionary alloc]init];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            serviceResponse = [networkManager postRequestToServer:requestGetSkillList requestData:requestData];
            
            if (serviceResponse != nil) {
                
                [self.skillsArray removeAllObjects];
                for(NSDictionary *skill in serviceResponse)
                {
                    NSString *str = [skill valueForKey:@"skill"];
                    [self.skillsArray addObject:str];
                }
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
        });
    });
}


@end

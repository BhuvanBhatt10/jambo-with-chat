//
//  ChatManager.m
//  Jambo
//
//  Created by Rakesh Pethani on 17/10/15.
//  Copyright © 2015 Enerjik. All rights reserved.
//

#import "ChatManager.h"
#import "UserManager.h"

UserManager *userManager;

@implementation ChatManager

@synthesize serviceManager;

//Singleton object
static ChatManager *chatManager = nil;
+ (ChatManager*)sharedInstance {
    if (!chatManager) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            chatManager = [[self alloc] init];
            [chatManager initialize];
        });
    }
    return chatManager;
}

- (void)initialize
{
    userManager = [UserManager sharedInstance];
    
    [self loginOrSignUpWithCurrentUser];
}

- (void)loginOrSignUpWithCurrentUser
{
    QBUUser * currentUser = [QBUUser user];
    currentUser.login = userManager.userEntity.email;
    currentUser.password = [NSString stringWithFormat:@"%@_alumni",userManager.userEntity.password];
    currentUser.fullName = userManager.userEntity.firstName;
    currentUser.email = userManager.userEntity.email;
    currentUser.externalUserID = userManager.userEntity.userId;
    
    serviceManager = [[QMServicesManager alloc] init];
    
    [serviceManager logInWithUser:currentUser completion:^(BOOL success, NSString *errorMessage) {
        if (success) {
            NSLog(@"Login successful.");
            [self loadDialogs];
        }
        else {
            if ([errorMessage rangeOfString:@"401"].location == NSNotFound) {
                NSLog(@"Unknown login error");
            } else {
                NSLog(@"User not registered");
                
                [serviceManager.authService signUpAndLoginWithUser:currentUser completion:^(QBResponse *response, QBUUser *userProfile) {
                    
                    if (response.error == nil) {
                        NSLog(@"Login successful.");
                        [self loadDialogs];
                    }
                }];
            }
        }
    }];
}

- (void)loadDialogs
{
    BOOL shouldShowSuccessStatus = NO;
    if ([self dialogs].count == 0) {
        shouldShowSuccessStatus = YES;
    }

    /*[serviceManager.chatService allDialogsWithPageLimit:10 extendedRequest:nil iterationBlock:^(QBResponse *response, NSArray *dialogObjects, NSSet *dialogsUsersIDs, BOOL *stop) {
        if (response.error != nil) {

        }
        
        for (QBChatDialog* dialog in dialogObjects) {
            if (dialog.type != QBChatDialogTypePrivate) {
                // Joining to group chat dialogs.
                [serviceManager.chatService joinToGroupDialog:dialog failed:^(NSError *error) {
                    NSLog(@"Failed to join room with error: %@", error.localizedDescription);
                }];
            }
        }
    } completion:^(QBResponse *response) {
        if (shouldShowSuccessStatus) {

        }
    }];*/
}

- (NSArray *)dialogs
{
    // Retrieving dialogs sorted by last message date from memory storage.
    return [serviceManager.chatService.dialogsMemoryStorage dialogsSortByLastMessageDateWithAscending:NO];
}

@end

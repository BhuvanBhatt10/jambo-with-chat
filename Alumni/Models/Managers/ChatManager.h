//
//  ChatManager.h
//  Jambo
//
//  Created by Rakesh Pethani on 17/10/15.
//  Copyright © 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Quickblox/Quickblox.h>
#import <QMServices.h>

@interface ChatManager : NSObject

@property (nonatomic, strong) QMServicesManager * serviceManager;

+ (ChatManager*)sharedInstance;

- (void)loginOrSignUpWithCurrentUser;
- (NSArray *)dialogs;

@end

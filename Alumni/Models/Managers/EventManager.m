//
//  EventManager.m
//  Alumni
//
//  Created by Rahul Chandera on 06/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "EventManager.h"
#import "NetworkManager.h"
#import "NetworkAvailability.h"
#import "EventEntity.h"
#import "UserManager.h"
#import "CoreDataManager.h"
#import <EventKit/EventKit.h>
#import "DateTools.h"

NetworkManager *networkManager;
UserManager *userManager;
CoreDataManager *coreDataManager;

@implementation EventManager
@synthesize delegate;
@synthesize allEventsArray,myEventsArray,invitedEventsArray;

//Singleton object
static EventManager *eventManager = nil;
+ (EventManager*)sharedInstance {
    if (!eventManager) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            eventManager = [[self alloc] init];
            [eventManager initialize];
        });
    }
    return eventManager;
}
-(void)initialize
{
    networkManager = [NetworkManager sharedInstance];
    userManager = [UserManager sharedInstance];
    coreDataManager = [CoreDataManager sharedInstance];
    allEventsArray = [[NSMutableArray alloc]init];
    myEventsArray = [[NSMutableArray alloc]init];
    invitedEventsArray = [[NSMutableArray alloc]init];
}

- (void)getMyEvents
{
    myEventsArray = [coreDataManager getEventObject];
}

#pragma mark - User Login
-(void)getEventsList {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *requestData = [NSString stringWithFormat:@"email=%@&password=%@",userManager.userEntity.email,userManager.userEntity.password];
        NSMutableDictionary *responseData = [[NSMutableDictionary alloc]init];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            NSMutableDictionary *serviceResponse = [networkManager requestToServer:requestGetEventList requestData:requestData];
            
            if (serviceResponse != nil) {
                
                //Parse Data
                [self.allEventsArray removeAllObjects];
                [self.invitedEventsArray removeAllObjects];
                
                NSError* err = nil;
                
                for(NSDictionary *events in serviceResponse)
                {
                    EventEntity *eventEntity = [[EventEntity alloc]initWithDictionary:events error:&err];
                    if(eventEntity)
                    {
                        [eventEntity setDates];
                        
                        //All Events
                        [self.allEventsArray addObject:eventEntity];
                        
                        //Invited Events
                        for(InviteEntity *invite in eventEntity.inviteSet)
                        {
                            if(invite.alumini.userId == userManager.userEntity.userId)
                                [self.invitedEventsArray addObject:eventEntity];
                        }
                    }
                }
                
                NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"self.sDate" ascending:NO];
                NSArray *descriptors = [NSArray arrayWithObject:descriptor];
                NSArray *reverseOrder = [self.allEventsArray sortedArrayUsingDescriptors:descriptors];
                self.allEventsArray = [NSMutableArray arrayWithArray:reverseOrder];
                
                NSArray *reverseOrder2 = [self.invitedEventsArray sortedArrayUsingDescriptors:descriptors];
                self.invitedEventsArray = [NSMutableArray arrayWithArray:reverseOrder2];
            }
            else{
                
                //If server cannot be connected
                [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
                [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_SERVER_CONNECTION_ERROR] forKey:ERROR_CODE_KEY];
            }
        }
        else{
            
            //Connection not available
            [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
            [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_INTERNET_NOT_AVAILABLE_CODE] forKey:ERROR_CODE_KEY];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //Call controller delegate method, and provide data
            [responseData setObject:[NSString stringWithFormat:@"%d",requestGetEventList] forKey:REQUEST_KEY];
            [delegate requestCompleted:responseData];
        });
    });
}


#pragma mark - Create Event
-(void)createEvent:(EventEntity*)eventEntity GroupList:(NSString*)groupList PeopleList:(NSString*)peopleList;  {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *requestData = [NSString stringWithFormat:@"email=%@&password=%@&eventName=%@&eventDesc=%@&startDate=%@ 00:00:00&endDate=%@ 00:00:00&fromTime=%@&toTime=%@&address=%@&city=%@&state=%@&country=%@&inviteTagIdList=%@&inviteAluminiIdList=%@&duration=%@",userManager.userEntity.email,userManager.userEntity.password,eventEntity.eventName,eventEntity.eventDesc,eventEntity.startDate,eventEntity.endDate,eventEntity.fromTime,eventEntity.toTime,eventEntity.address,@"",@"",@"",groupList,peopleList,@""];
        NSMutableDictionary *responseData = [[NSMutableDictionary alloc]init];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            NSMutableDictionary *serviceResponse = [networkManager postRequestToServer:requestCreateEvent requestData:requestData];
            
            if (serviceResponse != nil && ![[serviceResponse objectForKey:RESULT_KEY] boolValue]) {
                
                self.eventBeingCreated = nil;
            }
            else{
                
                //If server cannot be connected
                [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
                [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_SERVER_CONNECTION_ERROR] forKey:ERROR_CODE_KEY];
            }
        }
        else{
            
            //Connection not available
            [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
            [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_INTERNET_NOT_AVAILABLE_CODE] forKey:ERROR_CODE_KEY];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //Call controller delegate method, and provide data
            [responseData setObject:[NSString stringWithFormat:@"%d",requestCreateEvent] forKey:REQUEST_KEY];
            [delegate requestCompleted:responseData];
        });
    });
}


#pragma mark - Create Event
-(void)updateEvent:(NSString*)eventId EventName:(NSString*)name StartDate:(NSString*)startDate EndDate:(NSString*)endDate Duration:(NSString*)duration Description:(NSString*)description Address:(NSString*)address CityState:(NSString*)cityState IsPaid:(NSString*)isPaid Amount:(NSString*)amount GuestAllowed:(NSString*)guestAllowed GuestCharger:(NSString*)guestCharges GroupList:(NSString*)groupList PeopleList:(NSString*)peopleList  {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *requestData = [NSString stringWithFormat:@"eventId=%@&email=%@&password=%@&eventName=%@&eventDesc=%@&startDate=%@&endDate=%@&fromTime=%@&toTime=%@&address=%@&city=%@&state=%@&country=%@&inviteTagIdList=%@&inviteAluminiIdList=%@&duration=%@",eventId,userManager.userEntity.email,userManager.userEntity.password,name,description,startDate,endDate,duration,@"",address,cityState,@"",@"",groupList,peopleList,@""];
        NSMutableDictionary *responseData = [[NSMutableDictionary alloc]init];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            NSMutableDictionary *serviceResponse = [networkManager postRequestToServer:requestUpdateEvent requestData:requestData];
            
            if (serviceResponse != nil && ![[serviceResponse objectForKey:RESULT_KEY] boolValue]) {
                
                
            }
            else{
                
                //If server cannot be connected
                [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
                [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_SERVER_CONNECTION_ERROR] forKey:ERROR_CODE_KEY];
            }
        }
        else{
            
            //Connection not available
            [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
            [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_INTERNET_NOT_AVAILABLE_CODE] forKey:ERROR_CODE_KEY];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //Call controller delegate method, and provide data
            [responseData setObject:[NSString stringWithFormat:@"%d",requestUpdateEvent] forKey:REQUEST_KEY];
            [delegate requestCompleted:responseData];
        });
    });
}

#pragma mark - Response TagInvite
-(void)responseEventInvite:(int)eventId ResponseStatus:(int)responseStatus {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *requestData = [NSString stringWithFormat:@"email=%@&password=%@&eventId=%d&responseStatus=%d",userManager.userEntity.email,userManager.userEntity.password,eventId,responseStatus];
        NSMutableDictionary *responseData = [[NSMutableDictionary alloc]init];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            NSMutableDictionary *serviceResponse = [networkManager postRequestToServer:requestResponseEvent requestData:requestData];
            
            if (serviceResponse == nil || [[serviceResponse objectForKey:RESULT_KEY] boolValue]) {
                
                //If server cannot be connected
                [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
                [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_SERVER_CONNECTION_ERROR] forKey:ERROR_CODE_KEY];
            }
        }
        else{
            
            //Connection not available
            [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
            [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_INTERNET_NOT_AVAILABLE_CODE] forKey:ERROR_CODE_KEY];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //Call controller delegate method, and provide data
            [responseData setObject:[NSString stringWithFormat:@"%d",requestResponseEvent] forKey:REQUEST_KEY];
            [delegate requestCompleted:responseData];
        });
    });
}

#pragma mark - Store event to calendar
-(void)storeEventToCalendar:(EventEntity *)eventEntityToStore
{
    NSDateFormatter *dateFormatter;
    dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateFormat = @"dd/MM/yyyy HH:mm:ss";
    
    EKEventStore *store = [EKEventStore new];
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if (!granted) { return; }
        EKEvent *event = [EKEvent eventWithEventStore:store];
        event.title = eventEntityToStore.eventName;
        
        if (eventEntityToStore.startDate.length > 0)
            event.startDate = [dateFormatter dateFromString:eventEntityToStore.startDate];
        
        if (eventEntityToStore.endDate.length > 0)
            event.endDate = [dateFormatter dateFromString:eventEntityToStore.endDate];
        
        event.calendar = [store defaultCalendarForNewEvents];
        NSError *err = nil;
        [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];        
    }];
}

#pragma mark - Copy event
-(MyEventEntity *)getEventCopyFromEvent:(MyEventEntity *)eventToCopy
{
    MyEventEntity * newEvent = [MyEventEntity new];
    
    newEvent.eventName = [NSString stringWithString:eventToCopy.eventName ? eventToCopy.eventName : @""];
    newEvent.eventDesc = [NSString stringWithString:eventToCopy.eventDesc ? eventToCopy.eventDesc : @""];
    newEvent.lastUpdated = [NSString stringWithString:eventToCopy.lastUpdated ? eventToCopy.lastUpdated : @""];
    newEvent.status = [NSString stringWithString:eventToCopy.status ? eventToCopy.status : @""];
    newEvent.address = [NSString stringWithString:eventToCopy.address ? eventToCopy.address : @""];
    newEvent.city = [NSString stringWithString:eventToCopy.city ? eventToCopy.city : @""];
    newEvent.state = [NSString stringWithString:eventToCopy.state ? eventToCopy.state : @""];
    newEvent.country = [NSString stringWithString:eventToCopy.country ? eventToCopy.country : @""];
    newEvent.isPaid = [NSString stringWithString:eventToCopy.isPaid ? eventToCopy.isPaid : @""];
    newEvent.charges = [NSString stringWithString:eventToCopy.charges ? eventToCopy.charges : @""];
    newEvent.isGuestAllowed = [NSString stringWithString:eventToCopy.isGuestAllowed ? eventToCopy.isGuestAllowed : @""];
    newEvent.guestCharges = [NSString stringWithString:eventToCopy.guestCharges ? eventToCopy.guestCharges : @""];
    
    return newEvent;
}


#pragma mark - Get events count for non-expired events
- (int)getEventsCountForEventType:(int)eventType
{
    int count = 0;
    
    if (eventType == 1) {
        for (EventEntity * eventEntity in allEventsArray) {
            if ([eventEntity.eDate isLaterThan:[NSDate date]]) {
                count++;
            }
        }
    }
    
    if (eventType == 2) {
        for (MyEventEntity * eventEntity in myEventsArray) {
            if ([eventEntity.eDate isLaterThan:[NSDate date]]) {
                count++;
            }
        }
    }
    
    if (eventType == 3) {
        for (EventEntity * eventEntity in invitedEventsArray) {
            if ([eventEntity.eDate isLaterThan:[NSDate date]]) {
                count++;
            }
        }
    }
    
    return count;
}

@end

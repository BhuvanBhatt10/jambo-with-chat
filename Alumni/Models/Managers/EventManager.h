//
//  EventManager.h
//  Alumni
//
//  Created by Rahul Chandera on 06/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EventEntity.h"
#import "MyEventEntity.h"

@protocol EventManagerDelegate
-(void)requestCompleted:(id)response;
@end

@interface EventManager : NSObject

@property (strong,nonatomic) id <EventManagerDelegate> delegate;
@property(nonatomic,strong) NSMutableArray *allEventsArray;
@property(nonatomic,strong) NSMutableArray *myEventsArray;
@property(nonatomic,strong) NSMutableArray *invitedEventsArray;
@property(nonatomic,strong) EventEntity * eventBeingCreated;
@property(nonatomic,strong) MyEventEntity * eventBeingCopied;

+ (EventManager*)sharedInstance;
-(void)getMyEvents;
-(void)getEventsList;
-(void)createEvent:(EventEntity*)eventEntity GroupList:(NSString*)groupList PeopleList:(NSString*)peopleList;
-(void)updateEvent:(NSString*)eventId EventName:(NSString*)name StartDate:(NSString*)startDate EndDate:(NSString*)endDate Duration:(NSString*)duration Description:(NSString*)description Address:(NSString*)address CityState:(NSString*)cityState IsPaid:(NSString*)isPaid Amount:(NSString*)amount GuestAllowed:(NSString*)guestAllowed GuestCharger:(NSString*)guestCharges GroupList:(NSString*)groupList PeopleList:(NSString*)peopleList;
-(void)responseEventInvite:(int)eventId ResponseStatus:(int)responseStatus;
-(void)storeEventToCalendar:(EventEntity *)eventEntityToStore;
-(MyEventEntity *)getEventCopyFromEvent:(MyEventEntity *)eventToCopy;
-(int)getEventsCountForEventType:(int)eventType;

@end

//
//  UserManager.h
//  Alumni
//
//  Created by Rahul Chandera on 22/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserEntity.h"
#import "EventEntity.h"
#import "InstituteEntity.h"
#import "CourseEntity.h"
#import <CoreData/CoreData.h>

@protocol UserManagerDelegate
-(void)requestCompleted:(id)response;
@end

@interface UserManager : NSObject

@property (strong,nonatomic) id <UserManagerDelegate> delegate;
@property(nonatomic,strong) NSMutableArray *nearByArray;
@property(nonatomic,strong) NSMutableArray *alumniArray;
@property(nonatomic,strong) NSMutableArray *myReferredUsers;
@property(nonatomic,strong) NSMutableArray *instituteList;
@property(nonatomic,strong) NSMutableArray *coursesList;
@property(nonatomic,assign) int totalInvites;
@property(nonatomic,strong) UserEntity *userEntity;
@property(nonatomic,strong) InstituteEntity *userInstitute;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;

+ (UserManager*)sharedInstance;
-(void)userLogin:(NSString *)emailId Password:(NSString*)password;
-(void)getUserDetail:(UserEntity *)currentUserEntity;
-(void)updateUser;
-(void)getNearByList:(NSString*)orderBy;
-(void)getAlumniList:(NSString*)generalSearch Filters:(NSString*)filters;
-(AluminiEntity*)getAlumniById:(int)alumniId;
-(void)mapUserWithDevice;
-(void)forgotPassword:(NSString *)emailId;
-(void)inviteSomeoneToAppWithEmailId:(NSString *)emailId;
-(void)getInstituteList;
-(void)getCourseList;
-(NSArray *)getProgramsArrayForInstituteId:(int)instituteId;
-(NSArray *)getStreamsArrayForInstituteId:(int)instituteId;
-(void)getMyInstitute;
-(void)getMyReferredUser;
-(NSArray *)getOrganizationListForString:(NSString *)orgName;
-(NSArray *)getDesignationListForString:(NSString *)desName;

@end

//
//  MessageManager.m
//  Alumni
//
//  Created by Rahul Chandera on 26/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "MessageManager.h"
#import "AppConstants.h"
#import "NetworkManager.h"
#import "NetworkAvailability.h"
#import "ApplicationManager.h"
#import "MessageGroupEntity.h"
#import "UserManager.h"
#import "CoreDataManager.h"
#import <MobileCoreServices/MobileCoreServices.h>
//#import "AFNetworking.h"

NetworkManager *networkManager;
ApplicationManager *applicationManager;
UserManager *userManager;
CoreDataManager *coreDataManager;

@implementation MessageManager
@synthesize delegate;

//Singleton object
static MessageManager *messageManager = nil;
+ (MessageManager*)sharedInstance {
    if (!messageManager) {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            messageManager = [[self alloc] init];
            [messageManager initialize];
        });
    }
    return messageManager;
}
-(void)initialize
{
    networkManager = [NetworkManager sharedInstance];
    applicationManager = [ApplicationManager sharedInstance];
    userManager = [UserManager sharedInstance];
    coreDataManager = [CoreDataManager sharedInstance];
    [self getMessageList];
}



#pragma mark - Get Message List
-(void)getMessageList {
    
    self.messageArray = [coreDataManager getMessageGroupObject];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *requestData = [NSString stringWithFormat:@"email=%@&password=%@",userManager.userEntity.email,userManager.userEntity.password];
        NSMutableDictionary *responseData = [[NSMutableDictionary alloc]init];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            NSMutableDictionary *serviceResponse = [networkManager requestToServer:requestGetMessageList requestData:requestData];
            
            if (serviceResponse != nil) {
                
                //Parse Data
                [self.messageArray removeAllObjects];
                NSError* err = nil;
                for(NSDictionary *group in serviceResponse) {
                    
                    MessageGroupEntity *messageGroupEntity = [[MessageGroupEntity alloc]initWithDictionary:group error:&err];
                    if(messageGroupEntity) {
                        
                        [self.messageArray addObject:messageGroupEntity];
                        [coreDataManager saveMessageGroupObject:messageGroupEntity];
                    }
                }
                self.messageArray = [coreDataManager getMessageGroupObject];
            }
            else{
                
                //If server cannot be connected
                [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
                [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_SERVER_CONNECTION_ERROR] forKey:ERROR_CODE_KEY];
            }
        }
        else{
            
            //Connection not available
            [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
            [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_INTERNET_NOT_AVAILABLE_CODE] forKey:ERROR_CODE_KEY];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //Call controller delegate method, and provide data
            [responseData setObject:[NSString stringWithFormat:@"%d",requestGetMessageList] forKey:REQUEST_KEY];
            [delegate requestCompleted:responseData];
        });
    });
}


#pragma mark - Send Message
-(void)sendMessage:(NSString*)messageText  withImage:(id)image ToAluminiId:(int)toAluminiId {
    /*
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    [manager POST:@"http://ec2-54-186-38-114.us-west-2.compute.amazonaws.com:8080/AluminiBackend/sendMessage" parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        //[formData appendPartWithFileData:imageData
                                    name:@"files"
                                fileName:photoName mimeType:@"image/jpeg"];
        
        [formData appendPartWithFormData:[userManager.userEntity.email dataUsingEncoding:NSUTF8StringEncoding] name:@"email"];
        [formData appendPartWithFormData:[userManager.userEntity.password dataUsingEncoding:NSUTF8StringEncoding] name:@"password"];
        [formData appendPartWithFormData:[[NSString stringWithFormat:@"%d",toAluminiId] dataUsingEncoding:NSUTF8StringEncoding] name:@"toAluminiId"];
        [formData appendPartWithFormData:[messageText dataUsingEncoding:NSUTF8StringEncoding] name:@"messageText"];
        [formData appendPartWithFormData:[@"26" dataUsingEncoding:NSUTF8StringEncoding] name:@"fromAluminiId"];
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Response: %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    */
    
    /*
    self.operationManager = [AFHTTPRequestOperationManager manager];
    self.operationManager.responseSerializer = [AFHTTPResponseSerializer serializer]; // only needed if the server is not returning JSON; if web service returns JSON, remove this line
    AFHTTPRequestOperation *operation = [self.operationManager POST:urlString parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        NSError *error;
        if (![formData appendPartWithFileURL:[NSURL fileURLWithPath:path] name:@"avatar" fileName:[path lastPathComponent] mimeType:@"image/png" error:&error]) {
            NSLog(@"error appending part: %@", error);
        }
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"responseObject = %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error = %@", error);
    }];
    
    if (!operation) {
        NSLog(@"Creation of operation failed.");
    }
    */
    
    /*
    NSDictionary *params = @{@"email"     : userManager.userEntity.email,
                             @"password"    : userManager.userEntity.password,
                             @"toAluminiId"    : @"15",
                             @"messageText"    : messageText,
                             @"fromAluminiId" : @"26"};
    
    //NSString *path = [[NSBundle mainBundle] pathForResource:@"avatar" ofType:@"png"];
    
    NSString *boundary = [self generateBoundaryString];
    
    // configure the request
    NSURL *url = [NSURL URLWithString:@"http://ec2-54-186-38-114.us-west-2.compute.amazonaws.com:8080/AluminiBackend/sendMessage"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    // set content type
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // create body
    
    NSData *httpBody = [self createBodyWithBoundary:boundary parameters:params paths:nil fieldName:@""];
    
    
    request.HTTPBody = httpBody;
    
    
    NSError *errorReturned = nil;
    NSURLResponse *theResponse =[[NSURLResponse alloc]init];
    NSData *responsedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&theResponse error:&errorReturned];
    
    if (errorReturned) {
        NSLog(@"Request Error = %@",errorReturned);
    }
    else
    {
        NSError *jsonParsingError = nil;
        NSMutableDictionary *responseDict = (NSMutableDictionary *)[NSJSONSerialization JSONObjectWithData:responsedData options:NSJSONReadingMutableContainers|NSJSONReadingAllowFragments error:&jsonParsingError];
        NSLog(@"Response = %@",responseDict);
    }
    */
    
    
    
    /*
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (connectionError) {
            NSLog(@"error = %@", connectionError);
            return;
        }
        
        NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"result = %@", result);
    }];
    */
    
    
    //Remove whitespace
    messageText = [messageText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    messageText = [messageText stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    messageText = [messageText stringByReplacingOccurrencesOfString:@"\U0000fffc" withString:@""];
    
    NSString *fromAlumni = [NSString stringWithFormat:@"%d",userManager.userEntity.userId];
    NSString *toAlumni = [NSString stringWithFormat:@"%d",toAluminiId];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *boundary = [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
        
        NSMutableData *httpBody = [NSMutableData data];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"email\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", userManager.userEntity.email] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"password\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", userManager.userEntity.password] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"toAluminiId\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", toAlumni] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"fromAluminiId\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", fromAlumni] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"messageText\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", messageText] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSData *imageData = UIImagePNGRepresentation(image);
        if (imageData)
        {
            [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"picture\"; filename=\"messageImage.png\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [httpBody appendData:[@"Content-Type: image/png\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [httpBody appendData:imageData];
            [httpBody appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        NSMutableDictionary *responseData = [[NSMutableDictionary alloc]init];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            NSMutableDictionary *serviceResponse = [networkManager multipartPostRequestToServer:requestSendMessage requestData:httpBody Boundary:boundary];
      
            if (serviceResponse != nil) {
                
                NSError* err = nil;
                MessageEntity *messageEntity = [[MessageEntity alloc]initWithDictionary:serviceResponse error:&err];
                if(messageEntity) {
                 
                    [responseData setObject:messageEntity forKey:DATA_KEY];
                }
            }
            else{
                
                //If server cannot be connected
                [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
                [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_SERVER_CONNECTION_ERROR] forKey:ERROR_CODE_KEY];
            }
        }
        else{
            
            //Connection not available
            [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
            [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_INTERNET_NOT_AVAILABLE_CODE] forKey:ERROR_CODE_KEY];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //Call controller delegate method, and provide data
            [responseData setObject:[NSString stringWithFormat:@"%d",requestSendMessage] forKey:REQUEST_KEY];
            [delegate requestCompleted:responseData];
        });
    });
}
- (NSData *)createBodyWithBoundary:(NSString *)boundary
                        parameters:(NSDictionary *)parameters
                             paths:(NSArray *)paths
                         fieldName:(NSString *)fieldName
{
    NSMutableData *httpBody = [NSMutableData data];
    
    // add params (all params are strings)
    
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *parameterKey, NSString *parameterValue, BOOL *stop) {
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", parameterKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"%@\r\n", parameterValue] dataUsingEncoding:NSUTF8StringEncoding]];
    }];
    
    // add image data
    /*
    for (NSString *path in paths) {
        NSString *filename  = [path lastPathComponent];
        NSData   *data      = [NSData dataWithContentsOfFile:path];
        NSString *mimetype  = [self mimeTypeForPath:path];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", fieldName, filename] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", mimetype] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:data];
        [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    */
    [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    return httpBody;
}
- (NSString *)mimeTypeForPath:(NSString *)path
{
    // get a mime type for an extension using MobileCoreServices.framework
    
    CFStringRef extension = (__bridge CFStringRef)[path pathExtension];
    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, extension, NULL);
    assert(UTI != NULL);
    
    NSString *mimetype = CFBridgingRelease(UTTypeCopyPreferredTagWithClass(UTI, kUTTagClassMIMEType));
    assert(mimetype != NULL);
    
    CFRelease(UTI);
    
    return mimetype;
}

- (NSString *)generateBoundaryString
{
    return [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
    
    // if supporting iOS versions prior to 6.0, you do something like:
    //
    // // generate boundary string
    // //
    // adapted from http://developer.apple.com/library/ios/#samplecode/SimpleURLConnections
    //
    // CFUUIDRef  uuid;
    // NSString  *uuidStr;
    //
    // uuid = CFUUIDCreate(NULL);
    // assert(uuid != NULL);
    //
    // uuidStr = CFBridgingRelease(CFUUIDCreateString(NULL, uuid));
    // assert(uuidStr != NULL);
    //
    // CFRelease(uuid);
    //
    // return uuidStr;
}


#pragma mark - Get Message List
-(void)updateMessageStatus:(NSString*)messageIdList Status:(int)status {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *requestData = [NSString stringWithFormat:@"email=%@&password=%@&messageIdList=%@&status=%d",userManager.userEntity.email,userManager.userEntity.password,messageIdList,status];
        NSMutableDictionary *responseData = [[NSMutableDictionary alloc]init];
        
        //If Connection available then move ahead
        if ([NetworkAvailability CheckInternetConnection]) {
            
            NSMutableDictionary *serviceResponse = [networkManager postRequestToServer:requestUpdateMessage requestData:requestData];
            
            if (serviceResponse != nil) {
                
                
            }
            else{
                
                //If server cannot be connected
                [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
                [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_SERVER_CONNECTION_ERROR] forKey:ERROR_CODE_KEY];
            }
        }
        else{
            
            //Connection not available
            [responseData setObject:FALSE_RESULT_KEY forKey:RESULT_KEY];
            [responseData setObject:[NSString stringWithFormat:@"%d",ERROR_INTERNET_NOT_AVAILABLE_CODE] forKey:ERROR_CODE_KEY];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //Call controller delegate method, and provide data
            [responseData setObject:[NSString stringWithFormat:@"%d",requestUpdateMessage] forKey:REQUEST_KEY];
            [delegate requestCompleted:responseData];
        });
    });
}



@end

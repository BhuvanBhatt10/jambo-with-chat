//
//  Event.m
//  Jambo
//
//  Created by Rahul Chandera on 04/08/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "Event.h"


@implementation Event

@dynamic eventId;
@dynamic eventName;
@dynamic eventDesc;
@dynamic lastUpdated;
@dynamic startDate;
@dynamic endDate;
@dynamic status;
@dynamic fromTime;
@dynamic toTime;
@dynamic address;
@dynamic city;
@dynamic state;
@dynamic country;
@dynamic isPaid;
@dynamic charges;
@dynamic isGuestAllowed;
@dynamic guestCharges;

@end

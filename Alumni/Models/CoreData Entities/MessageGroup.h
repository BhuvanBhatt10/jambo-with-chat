//
//  MessageGroup.h
//  Jambo
//
//  Created by Rahul Chandera on 21/07/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NSManagedObject;

@interface MessageGroup : NSManagedObject

@property (nonatomic, retain) NSNumber * userId;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * profilePic;
@property (nonatomic, retain) NSString * lastUpdated;
@property (nonatomic, retain) NSSet *messageList;
@end

@interface MessageGroup (CoreDataGeneratedAccessors)

- (void)addMessageListObject:(NSManagedObject *)value;
- (void)removeMessageListObject:(NSManagedObject *)value;
- (void)addMessageList:(NSSet *)values;
- (void)removeMessageList:(NSSet *)values;

@end

//
//  Event.h
//  Jambo
//
//  Created by Rahul Chandera on 04/08/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Event : NSManagedObject

@property (nonatomic, retain) NSString * eventId;
@property (nonatomic, retain) NSString * eventName;
@property (nonatomic, retain) NSString * eventDesc;
@property (nonatomic, retain) NSString * lastUpdated;
@property (nonatomic, retain) NSString * startDate;
@property (nonatomic, retain) NSString * endDate;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * fromTime;
@property (nonatomic, retain) NSString * toTime;
@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * state;
@property (nonatomic, retain) NSString * country;
@property (nonatomic, retain) NSString * isPaid;
@property (nonatomic, retain) NSString * charges;
@property (nonatomic, retain) NSString * isGuestAllowed;
@property (nonatomic, retain) NSString * guestCharges;

@end

//
//  MessageGroup.m
//  Jambo
//
//  Created by Rahul Chandera on 21/07/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "MessageGroup.h"


@implementation MessageGroup

@dynamic userId;
@dynamic firstName;
@dynamic lastName;
@dynamic profilePic;
@dynamic lastUpdated;
@dynamic messageList;

@end

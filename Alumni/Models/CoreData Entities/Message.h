//
//  Message.h
//  Jambo
//
//  Created by Rahul Chandera on 03/08/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MessageGroup;

@interface Message : NSManagedObject

@property (nonatomic, retain) NSString * lastUpdated;
@property (nonatomic, retain) NSNumber * messageId;
@property (nonatomic, retain) NSString * messageText;
@property (nonatomic, retain) NSString * picture;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) MessageGroup *messageList;
@property (nonatomic, retain) NSString *fromAluminiId;
@property (nonatomic, retain) NSString *toAluminiId;

@end

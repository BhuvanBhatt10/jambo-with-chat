//
//  User.m
//  Jambo
//
//  Created by Rahul Chandera on 03/08/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "User.h"


@implementation User

@dynamic email;
@dynamic firstName;
@dynamic lastName;
@dynamic password;
@dynamic profilePic;
@dynamic userId;

@end

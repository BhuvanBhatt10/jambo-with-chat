//
//  Message.m
//  Jambo
//
//  Created by Rahul Chandera on 03/08/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "Message.h"
#import "MessageGroup.h"
#import "User.h"


@implementation Message

@dynamic lastUpdated;
@dynamic messageId;
@dynamic messageText;
@dynamic picture;
@dynamic status;
@dynamic messageList;
@dynamic fromAluminiId;
@dynamic toAluminiId;

@end

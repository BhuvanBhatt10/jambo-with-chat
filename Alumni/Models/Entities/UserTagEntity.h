//
//  UserTagEntity.h
//  Jambo
//
//  Created by Rahul Chandera on 30/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserGroupEntity.h"
#import "AluminiEntity.h"

@protocol UserTagEntity
@end

@interface UserTagEntity : JSONModel

@property(nonatomic,strong) AluminiEntity<Optional>* aluminiId;
@property(nonatomic,strong) UserGroupEntity<Optional>* tag;

@end

//
//  CourseEntity.h
//  Jambo
//
//  Created by Rakesh Pethani on 03/11/15.
//  Copyright © 2015 Enerjik. All rights reserved.
//

#import "JSONModel.h"

@protocol CourseEntity
@end

@interface CourseEntity : JSONModel

@property(assign,nonatomic) int courseId;
@property(assign,nonatomic) int instituteId;
@property(nonatomic,strong) NSString<Optional>* courseName;
@property(nonatomic,strong) NSString<Optional>* lastUpdated;
@property(nonatomic,strong) NSString<Optional>* stream;

@end

//
//  UserGroupEntity.m
//  Jambo
//
//  Created by Rahul Chandera on 30/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "UserGroupEntity.h"

@implementation UserGroupEntity

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"description": @"groupDesc"
                                                       }];
}

@end

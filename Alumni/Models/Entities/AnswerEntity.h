//
//  AnswerEntity.h
//  Alumni
//
//  Created by Rahul Chandera on 25/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "UserEntity.h"
#import "CommentEntity.h"

@protocol AnswerEntity
@end

@interface AnswerEntity : JSONModel

@property(assign,nonatomic) int ansId;
@property(nonatomic,strong) NSString<Optional>* answer;
@property(assign,nonatomic) int score;
@property(nonatomic,strong) UserEntity<Optional>* createdBy;
@property(nonatomic,strong) NSArray<CommentEntity,Optional>* comments;
@property(nonatomic,strong) NSString<Optional>* lastUpdated;
@property(nonatomic,strong) NSString<Optional>* picture;

@end

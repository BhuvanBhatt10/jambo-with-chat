//
//  MyEventEntity.h
//  Jambo
//
//  Created by Rahul Chandera on 09/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@protocol MyEventEntity
@end

@interface MyEventEntity : JSONModel

@property(assign,nonatomic) int eventId;
@property(nonatomic,strong) NSString<Optional>* eventName;
@property(nonatomic,strong) NSString<Optional>* eventDesc;
@property(nonatomic,strong) NSString<Optional>* lastUpdated;
@property(nonatomic,strong) NSString<Optional>* startDate;
@property(nonatomic,strong) NSString<Optional>* endDate;
@property(nonatomic,strong) NSString<Optional>* status;
@property(nonatomic,strong) NSString<Optional>* fromTime;
@property(nonatomic,strong) NSString<Optional>* toTime;
@property(nonatomic,strong) NSString<Optional>* address;
@property(nonatomic,strong) NSString<Optional>* city;
@property(nonatomic,strong) NSString<Optional>* state;
@property(nonatomic,strong) NSString<Optional>* country;
@property(nonatomic,strong) NSString<Optional>* isPaid;
@property(nonatomic,strong) NSString<Optional>* charges;
@property(nonatomic,strong) NSString<Optional>* isGuestAllowed;
@property(nonatomic,strong) NSString<Optional>* guestCharges;
@property(nonatomic,strong) NSArray<Ignore>* inviteSet;

@property(nonatomic,strong) NSDate<Ignore>* sDate;
@property(nonatomic,strong) NSDate<Ignore>* eDate;

- (void)setDates;

@end

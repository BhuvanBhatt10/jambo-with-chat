//
//  FilterGroupEntity.h
//  Jambo
//
//  Created by Rahul Chandera on 08/05/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "FilterEntity.h"

@interface FilterGroupEntity : JSONModel

@property(nonatomic,strong) NSString* groupName;
@property(nonatomic,strong) NSArray* filters;

@end

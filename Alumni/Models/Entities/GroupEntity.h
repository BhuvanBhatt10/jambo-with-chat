//
//  GroupEntity.h
//  Alumni
//
//  Created by Rahul Chandera on 25/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "QuestionEntity.h"
#import "UserEntity.h"
#import "MyGroupsEntity.h"

@protocol GroupEntity
@end

@interface GroupEntity : JSONModel
{
    int status;
    BOOL isSelected;
}
@property(assign,nonatomic) int tagId;
@property(nonatomic,strong) NSString<Optional>* tag;
@property(nonatomic,strong) NSArray<QuestionEntity,Optional>* questions;
@property(nonatomic,strong) NSString<Optional>* groupDesc;
@property(assign,nonatomic) int totalMembers;

@property(nonatomic,strong) NSString<Optional>* lastUpdated;
@property(nonatomic,strong) NSArray<MyGroupsEntity,Optional>* userTagList;
@property(nonatomic,strong) UserEntity<Optional>* createdBy;

@property(nonatomic,strong) NSString<Ignore>* role;

- (void)setStatus:(int)Status;
- (int)getStatus;
- (void)setSelected:(BOOL)selected;
- (BOOL)getSelected;
- (void)applySortingOnQuestions;

@end

//
//  MyGroupsEntity.h
//  Jambo
//
//  Created by Rahul Chandera on 30/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "UserTagEntity.h"

@protocol MyGroupsEntity
@end

@interface MyGroupsEntity : JSONModel

@property(assign,nonatomic) int tagId;
@property(assign,nonatomic) int aluminiId;
@property(nonatomic,strong) NSString<Optional>* lastUpdated;
@property(nonatomic,strong) NSString<Optional>* role;
@property(assign,nonatomic) int status;

@end

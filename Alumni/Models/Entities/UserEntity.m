//
//  UserEntity.m
//  Alumni
//
//  Created by Rahul Chandera on 25/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "UserEntity.h"

@implementation UserEntity

-(id)init {
    if (self = [super init])  {
        isSelected = NO;
    }
    return self;
}

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"id": @"userId"
                                                       }];
}


- (void)setSelected:(BOOL)selected
{
    isSelected = selected;
}

- (BOOL)getSelected
{
    return isSelected;
}

@end

//
//  NotificationEntity.h
//  Jambo
//
//  Created by Rahul Chandera on 24/05/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@interface NotificationEntity : JSONModel

//Message
@property(nonatomic,strong) NSString<Optional>* fromAluminiId;
@property(nonatomic,strong) NSString<Optional>* fromAlumini_email;
@property(nonatomic,strong) NSString<Optional>* fromAlumini_firstName;
@property(nonatomic,strong) NSString<Optional>* fromAlumini_lastName;
@property(nonatomic,strong) NSString<Optional>* message;
@property(nonatomic,strong) NSString<Optional>* messageId;
@property(nonatomic,strong) NSString<Optional>* picture;

//Question
@property(nonatomic,strong) NSString<Optional>* queId;
@property(nonatomic,strong) NSString<Optional>* question;
@property(nonatomic,strong) NSString<Optional>* score;
@property(nonatomic,strong) NSString<Optional>* createdBy;
@property(nonatomic,strong) NSString<Optional>* firstName;
@property(nonatomic,strong) NSString<Optional>* lastName;
@property(nonatomic,strong) NSString<Optional>* profilePic;
@property(nonatomic,strong) NSString<Optional>* tagId;
@property(nonatomic,strong) NSString<Optional>* tag;
@property(nonatomic,strong) NSString<Optional>* lastUpdated;

//Answer

@end

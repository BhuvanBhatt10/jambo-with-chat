//
//  MessageGroupEntity.h
//  Alumni
//
//  Created by Rahul Chandera on 01/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "MessageEntity.h"

@interface MessageGroupEntity : JSONModel

@property(assign, nonatomic) int userId;
@property(nonatomic,strong) NSString<Optional>* firstName;
@property(nonatomic,strong) NSString<Optional>* lastName;
@property(nonatomic,strong) NSString<Optional>* profilePic;
@property(nonatomic,strong) NSString<Optional>* lastUpdated;
@property(nonatomic,strong) NSArray<MessageEntity,Optional>* messageList;

@end

//
//  CommentEntity.h
//  Alumni
//
//  Created by Rahul Chandera on 25/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "UserEntity.h"

@protocol CommentEntity
@end

@interface CommentEntity : JSONModel

@property(assign,nonatomic) int commentsId;
@property(nonatomic,strong) NSString<Optional>* comment;
@property(nonatomic,strong) UserEntity<Optional>* createdBy;
@property(nonatomic,strong) NSString<Optional>* lastUpdated;
@property(assign,nonatomic) int score;

@end

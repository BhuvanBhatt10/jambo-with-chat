//
//  MessageGroupEntity.m
//  Alumni
//
//  Created by Rahul Chandera on 01/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "MessageGroupEntity.h"

@implementation MessageGroupEntity

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"id": @"userId"
                                                       }];
}


@end

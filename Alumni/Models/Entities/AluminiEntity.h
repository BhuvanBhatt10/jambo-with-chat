//
//  AluminiEntity.h
//  Jambo
//
//  Created by Rahul Chandera on 07/05/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@protocol AluminiEntity
@end

@interface AluminiEntity : JSONModel
{
    BOOL isSelected;
}
@property(assign, nonatomic) int userId;
@property(nonatomic,strong) NSString<Optional>* email;
@property(nonatomic,strong) NSString<Optional>* contact1;
@property(nonatomic,strong) NSString<Optional>* contact2;
@property(nonatomic,strong) NSString<Optional>* city;
@property(nonatomic,strong) NSString<Optional>* state;
@property(nonatomic,strong) NSString<Optional>* country;
@property(nonatomic,strong) NSString<Optional>* privacyLevel;
@property(assign,nonatomic) NSNumber<Optional>* userType;
@property(nonatomic,strong) NSString<Optional>* occupation;
@property(nonatomic,strong) NSString<Optional>* companyName;
@property(nonatomic,strong) NSString<Optional>* designation;
@property(nonatomic,strong) NSString<Optional>* profilePic;
@property(nonatomic,strong) NSString<Optional>* industry;
@property(nonatomic,strong) NSString<Optional>* firstName;
@property(nonatomic,strong) NSString<Optional>* lastName;
@property(nonatomic,strong) NSString<Optional>* latitude;
@property(nonatomic,strong) NSString<Optional>* longitude;
@property(nonatomic,strong) NSString<Optional>* lastUpdated;

- (void)setSelected:(BOOL)selected;
- (BOOL)getSelected;

@end

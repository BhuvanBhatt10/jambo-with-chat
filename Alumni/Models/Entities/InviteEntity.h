//
//  InviteEntity.h
//  Alumni
//
//  Created by Rahul Chandera on 07/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "UserEntity.h"

@protocol InviteEntity
@end

@interface InviteEntity : JSONModel

@property(assign,nonatomic) int inviteId;
@property(nonatomic,strong) NSString<Optional>* event;
@property(nonatomic,strong) UserEntity<Optional>* alumini;
@property(assign,nonatomic) int responseStatus;
@property(nonatomic,strong) NSString<Optional>* lastUpdated;

@end

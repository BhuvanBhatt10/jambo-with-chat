//
//  UserEntity.h
//  Alumni
//
//  Created by Rahul Chandera on 25/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "MyEventEntity.h"
#import "MyGroupsEntity.h"

@protocol UserEntity
@end

@interface UserEntity : JSONModel
{
    BOOL isSelected;
}
@property(assign,nonatomic) int userId;
@property(assign,nonatomic) int isVerified;
@property(nonatomic,strong) NSString<Optional>* email;
@property(nonatomic,strong) NSString<Optional>* contact1;
@property(nonatomic,strong) NSString<Optional>* contact2;
@property(nonatomic,strong) NSString<Optional>* city;
@property(nonatomic,strong) NSString<Optional>* state;
@property(nonatomic,strong) NSString<Optional>* country;
@property(nonatomic,strong) NSString<Optional>* privacyLevel;
@property(nonatomic,strong) NSString<Optional>* profilePic;
@property(nonatomic,strong) NSString<Optional>* firstName;
@property(nonatomic,strong) NSString<Optional>* lastName;
@property(nonatomic,strong) NSString<Optional>* latitude;
@property(nonatomic,strong) NSString<Optional>* longitude;
@property(nonatomic,strong) NSString<Optional>* lastUpdated;
@property(nonatomic,strong) NSArray<MyGroupsEntity,Optional>* tags;
@property(nonatomic,strong) NSArray<MyEventEntity,Optional>* eventSet;
@property(nonatomic,strong) NSString<Optional>* skillList;

@property(nonatomic,strong) NSString<Optional>* companyName;
@property(nonatomic,strong) NSString<Optional>* designation;
@property(nonatomic,strong) NSString<Optional>* industry;
@property(nonatomic,strong) NSString<Optional>* occupation;
@property(nonatomic,strong) NSString<Ignore>* totalExperience;

@property(nonatomic,strong) NSString<Ignore>* tag;
@property(nonatomic,strong) NSString<Ignore>* institude;
@property(nonatomic,strong) NSString<Ignore>* program;
@property(nonatomic,strong) NSString<Ignore>* graduateYear;
@property(nonatomic,strong) NSString<Ignore>* stream;
@property(nonatomic,strong) NSString<Ignore>* password;

- (void)setSelected:(BOOL)selected;
- (BOOL)getSelected;

@end

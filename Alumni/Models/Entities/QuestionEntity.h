//
//  QuestionEntity.h
//  Alumni
//
//  Created by Rahul Chandera on 25/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "UserEntity.h"
#import "AnswerEntity.h"
#import "CommentEntity.h"

@protocol QuestionEntity
@end

@interface QuestionEntity : JSONModel

@property(assign,nonatomic) int queId;
@property(nonatomic,strong) NSString<Optional>* question;
@property(assign,nonatomic) int score;
@property(nonatomic,strong) UserEntity<Optional>* createdBy;
@property(nonatomic,strong) NSMutableArray<AnswerEntity,Optional>* answers;
@property(nonatomic,strong) NSArray<CommentEntity,Optional>* comments;
@property(nonatomic,strong) NSString<Optional>* lastUpdated;
@property(nonatomic,strong) NSDate<Ignore>* sortDate;
@property(nonatomic,strong) NSNumber<Ignore>* sortCount;

- (void)setSortingDate;
- (void)setSortingCount;

@end

//
//  AluminiEntity.m
//  Jambo
//
//  Created by Rahul Chandera on 07/05/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "AluminiEntity.h"

@implementation AluminiEntity

-(id)init {
    if (self = [super init])  {
        isSelected = NO;
    }
    return self;
}

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"id": @"userId"
                                                       }];
}


- (void)setSelected:(BOOL)selected
{
    isSelected = selected;
}

- (BOOL)getSelected
{
    return isSelected;
}
@end

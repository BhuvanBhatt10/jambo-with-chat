//
//  MessageEntity.h
//  Alumni
//
//  Created by Rahul Chandera on 26/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "UserEntity.h"

@protocol MessageEntity
@end

@interface MessageEntity : JSONModel

@property(assign,nonatomic) int messageId;
@property(nonatomic,strong) UserEntity<Optional>* fromAlumini;
@property(nonatomic,strong) UserEntity<Optional>* toAlumini;
@property(nonatomic,strong) NSString<Optional>* messageText;
@property(nonatomic,strong) NSString<Optional>* picture;
@property(nonatomic,strong) NSString<Optional>* lastUpdated;
@property(assign,nonatomic) int status;

@end

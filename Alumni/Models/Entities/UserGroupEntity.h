//
//  UserGroupEntity.h
//  Jambo
//
//  Created by Rahul Chandera on 30/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@interface UserGroupEntity :  JSONModel

@property(assign,nonatomic) int tagId;
@property(nonatomic,strong) NSString<Optional>* tag;
@property(nonatomic,strong) NSString<Optional>* groupDesc;

@end

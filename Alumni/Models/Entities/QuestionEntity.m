//
//  QuestionEntity.m
//  Alumni
//
//  Created by Rahul Chandera on 25/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "QuestionEntity.h"

@implementation QuestionEntity

- (void)setSortingDate
{
    NSDateFormatter * formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    self.sortDate = [formatter dateFromString:self.lastUpdated];
}

- (void)setSortingCount
{
    self.sortCount = [NSNumber numberWithInt:self.score + (int)self.answers.count];
}

@end

//
//  EventEntity.m
//  Alumni
//
//  Created by Rahul Chandera on 07/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "EventEntity.h"

@implementation EventEntity

- (void)setDates
{
    NSDateFormatter * formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    
    self.sDate = self.startDate ? [formatter dateFromString:self.startDate] : nil;
    self.eDate = self.endDate ? [formatter dateFromString:self.endDate] : nil;
}

@end

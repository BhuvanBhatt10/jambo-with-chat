//
//  InstituteEntity.h
//  Jambo
//
//  Created by Rakesh Pethani on 28/10/15.
//  Copyright © 2015 Enerjik. All rights reserved.
//

#import "JSONModel.h"

@protocol InstituteEntity
@end

@interface InstituteEntity : JSONModel

@property(assign,nonatomic) int instituteId;
@property(nonatomic,strong) NSString<Optional>* coverPic;
@property(nonatomic,strong) NSString<Optional>* instituteKey;
@property(nonatomic,strong) NSString<Optional>* instituteName;
@property(nonatomic,strong) NSString<Optional>* institutePic;

@end

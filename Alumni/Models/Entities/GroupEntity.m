//
//  GroupEntity.m
//  Alumni
//
//  Created by Rahul Chandera on 25/03/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "GroupEntity.h"

@implementation GroupEntity

-(id)init {
    if (self = [super init])  {
        self.role = @"";
        isSelected = NO;
    }
    return self;
}

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"description": @"groupDesc"
                                                       }];
}

- (void)setStatus:(int)Status
{
    status = Status;
}

- (int)getStatus
{
    return status;
}

- (void)setSelected:(BOOL)selected
{
    isSelected = selected;
}

- (BOOL)getSelected
{
    return isSelected;
}

- (void)applySortingOnQuestions
{
    int maxSortingCount = 0;
    
    for (QuestionEntity * questionEntity in self.questions) {
        [questionEntity setSortingDate];
        [questionEntity setSortingCount];
        
        if (questionEntity.sortCount.intValue > maxSortingCount) {
            maxSortingCount = questionEntity.sortCount.intValue;
        }
    }
    
    NSSortDescriptor *descriptor=[[NSSortDescriptor alloc] initWithKey:@"self.sortCount" ascending:NO];
    NSArray *descriptors=[NSArray arrayWithObject: descriptor];
    NSArray *sortedArray=[self.questions sortedArrayUsingDescriptors:descriptors];
    self.questions = (NSArray<QuestionEntity,Optional>*)sortedArray;
}

@end

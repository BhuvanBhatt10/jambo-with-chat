//
//  FilterEntity.h
//  Jambo
//
//  Created by Rahul Chandera on 08/05/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@protocol FilterEntity
@end

@interface FilterEntity : JSONModel

@property(nonatomic,strong) NSString* filterName;
@property(assign,nonatomic) BOOL isSelected;

@end

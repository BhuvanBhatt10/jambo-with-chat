//
//  CourseEntity.m
//  Jambo
//
//  Created by Rakesh Pethani on 03/11/15.
//  Copyright © 2015 Enerjik. All rights reserved.
//

#import "CourseEntity.h"

@implementation CourseEntity

+(JSONKeyMapper*)keyMapper
{
    return [[JSONKeyMapper alloc] initWithDictionary:@{
                                                       @"id": @"courseId"
                                                       }];
}

@end

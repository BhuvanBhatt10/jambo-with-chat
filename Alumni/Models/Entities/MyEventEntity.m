//
//  MyEventEntity.m
//  Jambo
//
//  Created by Rahul Chandera on 09/04/15.
//  Copyright (c) 2015 Enerjik. All rights reserved.
//

#import "MyEventEntity.h"

@implementation MyEventEntity

- (void)setDates
{
    NSDateFormatter * formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    
    self.sDate = self.startDate ? [formatter dateFromString:self.startDate] : nil;
    self.eDate = self.endDate ? [formatter dateFromString:self.endDate] : nil;
}

@end
